-- ----------------------------------
function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
  --              analiz:log("zz:"..str)
        end
        return t
end

local dir_lic = analiz:get("dir").."\\Lic"
analiz:log("license dir:"..dir_lic)

local fs = CreateFileServ()

local lics = fs:GetFiles(dir_lic,"*.lic",false)
analiz:log("license count:"..#lics)

for i=1,#lics do
   local licf = lics[i]
   local type = fs:ChangeFileExt(licf,"")
  analiz:log("license:"..type.."________________________________________________start")
  analiz:set("tmp",dir_lic.."\\"..licf)
  analiz:set("tmp2",type)
  analiz:execute("LICENSING_INSTALL $(tmp2) $(tmp) 1")
  analiz:log("license:"..type.."________________________________________________end")
end
