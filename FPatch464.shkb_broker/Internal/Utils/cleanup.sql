use staging_namecheck
go

truncate table namecheck

truncate table nameresult

truncate table nameresult_pub

truncate table namecheck_src_data

truncate table processinginfo

truncate table processinginfoparams

truncate table processingstat

truncate table processinginfo_snc

truncate table recordstatus

truncate table TaskInfo

truncate table TaskInfoParams

truncate table TestsReports

truncate table [dbo].[CommentLex]

truncate table graph

truncate table NameCheckIdentity

truncate table [CommentBID_LISTID]
truncate table [dbo].[CommentLex]
truncate table [comment_attach_paths]
truncate table file_storage

truncate table session_id
truncate table [online_session_id]
truncate table alert_id
truncate table [avapi_processes]
truncate table [avapi_processes_alerts]
truncate table avapi_processes_comments
truncate table avapi_processes_tasks
truncate table batch_alert_username

truncate table [dbo].[avapi_inquiry]
truncate table [dbo].[avapi_inquiry_comments]
truncate table [dbo].[avapi_inquiry_questions]
truncate table [dbo].[avapi_inquiry_recipients]


use plib_namecheck
go


truncate table singlenamecheckresults

truncate table whitelist

truncate table OtherList

truncate table otherlist_comment
truncate table otherlist_username

truncate table modelusage
truncate table modelaudit
truncate table TranSeriesInfo

go

use ccr
go

truncate table modelusage
truncate table modelaudit
truncate table TranSeriesInfo
go

