local ss = analiz.libs.serv
local fileserv =CreateFileServ()

local exclusion_items={
"Namechecking\\p.Secco processing nodes\\std.oracle\\lexEngine\\xml_cache",
"Namechecking\\p.Secco processing nodes\\std.oracle\\lexEngine\\meta",
"Namechecking\\p.Secco processing nodes\\std.oracle\\lexEngine\\install_ora_scripts",
"Namechecking\\p.Secco processing nodes\\std.oracle\\lexEngine\\xml_cache",
"Namechecking\\p.Secco processing nodes\\std.oracle\\work",
"logs",
"TXMInterface\\Temp"
}

local root_dir = ss.b("$(bin)\\..\\..\\..")
local engine = GetLFiles({root_=root_dir,threshold_=ss.g("arg1"),exclusion_items_ = exclusion_items})

local files =engine:GetFiles()

table.sort(files,function(a,b) return a.modification>b.modification end) 

local function date_to_s(dd,is_short)
   local d = os.date("!*t",dd)
   local res =""

   if is_short then
     if ( d.year==1970 and d.month==1 and d.day~=1) then res =d.day.." day"
     elseif (d.year==1970 and d.month~=1 ) then res =d.month.." month "..d.day.." day"
     elseif (d.year~=1970  ) then res =d.year.."years "..d.month.." month "..d.day.." day" end
   else
     res =d.day.."."..d.month.."."..d.year
   end
   if is_short then
      if d.hour==0 and d.min==0 then res=res.." "..d.sec.." sec"
      elseif d.hour==0 and d.min~=0 then res=res.." "..d.min.." min "..d.sec.." sec"
      elseif d.hour~=0 then res=res.." "..d.hour.."hour "..d.min.." min "..d.sec.." sec" end
   else
    res = res.." "..d.hour..":"..d.min..":"..d.sec
   end

  return res
end
-- ------------------------------------------------------------------------------------
local function write_(files,fn)
 local f = io.open(fn,"w")
 f:write("NAME\tDT\tDELTA")
 for i=1,#files do
   local ff=files[i]
   f:write("\n"..ff.fn.."\t"..date_to_s(ff.modification,false).."\t"..date_to_s(ff.delta_,true))
 end
 f:close()
end

local function copyFiles_(files,to_root)
 local cache_={}
 function copy_(f)
  local to_fn = to_root.."\\"..f
  local from_fn = root_dir.."\\"..f
  local to_dir =fileserv:ExtractPath(to_fn)
  if cache_[to_dir]==nil then
    fileserv:ForceDir(to_dir)
     cache_[to_dir]="1"
  end
   fileserv:CopyFile(from_fn,to_fn)
 end
 for i=1,#files do
   local ff=files[i]
   copy_(ff.fn)
 end
end
-- ------------------------------------------------------------------------------------
local result_ = ss.b("$(bin)\\..\\..\\Utils\\files_$(arg1)_$(DATETIME_FOR_LOG_FILE).txt")

write_(files,result_..".txt")


if ss.g("arg2")~="" then
  copyFiles_(files,result_)
end