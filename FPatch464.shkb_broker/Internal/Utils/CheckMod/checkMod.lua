local ss = analiz.libs.serv
local lfs = require"lfs"

local sep = string.match (package.config, "[^\n]+")

local function getThreshold_(val)
  local t = val:sub(val:len(),val:len())
  local v = math.floor(analiz:tonumber(val:sub(1,val:len()-1)))
  if t=="s" then return v end   --sec
  if t=="m" then return v*60 end --min
  if t=="h" then return v*60*60 end --hour
  if t=="d" then return v*24*60*60 end --day
  return v
end
-- ---------------------------------------------------------------------
function GetLFiles(option)
  local engine = {op_=option} 
  engine.p0 = os.time()
  engine.threshold = getThreshold_(option.threshold_)
engine.exclusion={}
  if option.exclusion_items_~=nil then
    for i=1,#option.exclusion_items_ do
      local l=option.exclusion_items_[i]
       engine.exclusion[l]=1
    end
  end


  function engine:add(patch,out)
     if engine.exclusion[patch]~=nil then
       ss.l(ss.def(patch,"")..":exclusion")
       return 
     end
     ss.l(ss.def(patch,""))
      local fpatch = patch
      if fpatch==nil  then
         fpatch=""
     else
           fpatch="\\"..fpatch
      end
      local l=patch
      if l==nil then l="" else l=l.."\\" end
 
      for file in lfs.dir(self.op_.root_..fpatch) do
            local f = self.op_.root_..fpatch..sep..file
          if file ~= "." and file ~= ".." then
             local attr = lfs.attributes (f,{"modification","mode"})
             if attr.mode == "directory" then


               self:add(l..file,out)
             else
              local delta = self.p0-attr.modification
              if delta<self.threshold then 
                local r ={fn=l..file,
                          modification=attr.modification,
                          delta_=delta
                         }
                out[#out+1]=r
               end
             end
          end
      end
  end

  function engine:GetFiles()
    local out={}
    self.p0 = os.time()
    self:add(nil,out)
    return out
  end


  return engine
end