
services_subsystem =
{
  nc_std = {"namecheck_single_stds(.+)"},
  nc_ora = {"namecheck_single_o(.+)"},
  nc_kundestamm = {"namecheck_single_k(.+)"},
  nc_finalize ={"namecheck_FinalizingSession"},
  ora_finas = {"namecheck_ora_finas(.+)"},
  nc_post = {"namechecking_finnova_post(.+)"},
  nc_release = {"namechecking_finnova_release_(.+)"},
  ora_proxy = {"namechecking_finnova_proxy_(.+)"},
  txm = {"txm_(.+)"}
}


local function add_(a,v)
   for ii=1,#v do
     a[#a+1]=v[ii]
   end
 end


-- all
--  ---------------------------
 local all = {}
 for i,v in pairs(services_subsystem) do
   add_(all,v)
 end
 services_subsystem.all=all
--  ---------------------------