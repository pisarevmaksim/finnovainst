local ss = analiz.libs.serv

local args = {}
args.arg1=ss.g("arg1")
args.arg2=ss.g("arg2")
args.arg3=ss.g("arg3")
args.arg4=ss.g("arg4")
args.arg5=ss.g("arg5")



ss.l("arg1:"..args.arg1)
ss.l("arg2:"..args.arg2)
ss.l("arg3:"..args.arg3)
ss.l("arg4:"..args.arg4)
--ss.a("1")
if args.arg1=="help" or args.arg1=="short_help" then 
--swap
  args.arg1,args.arg2 =args.arg2,args.arg1
end

local arg2_ =ss.mysplit(args.arg2,":")
local cmd_name =arg2_[1]:lower()

local cmd =commands[cmd_name] 


if cmd==nil then
  ss.l(cmd_name.." not found")
  cmd =commands["short_help"] 
end


--ss.l("cmd:"..cmd:name())
cmd:execute(args)
