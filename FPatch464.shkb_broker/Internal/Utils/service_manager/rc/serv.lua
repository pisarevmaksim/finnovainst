run_s ={}

local ss = analiz.libs.serv

-- ------------------------------------------------------
local function checkMask_(s,masks)
--  ss.l("check masks")
  for i=1,#masks do
  --  ss.l("mask:"..masks[i])
    if s:find(masks[i]) then return true end
  end
  return false
end
-- ------------------------------------------------------
local function getStatus(name)
  ss.s("tmp",name)
  ss.e("QUERYSERVICESTATUS . $(tmp) tmp2")
  return ss.g("tmp2")
end
-- ------------------------------------------------------
function run_s.get_services(masks)

  ss.e("GET_SERVICE_LIST . tmp")
  local l = ss.g("tmp")
  local s=ss.mysplit(l,";")

  local res={}
  for i=1,#s do
    local service=s[i]
    if checkMask_(service,masks) then
      res[#res+1]=service
    end
  end
  return res
end

-- ------------------------------------------------------
function run_s.get_status(services)
  local rr = {}
  for i=1,#services do
    local s = services[i]
    rr[s]=getStatus(s)
  end
  return rr
end
-- ------------------------------------------------------
-- ------------------------------------------------------

function run_s.get_sortkey(arr)
  local k ={}
  for kk,v in pairs(arr) do k[#k+1]=kk end
  table.sort(k)
  return k
end
-- ------------------------------------------------------
-- ------------------------------------------------------
function run_s.save_status(status,fname)
  local f = io.open(fname,"w")
  local k =run_s.get_sortkey(status)

  f:write("{")
  local rr = {}
  for i=1,#k do
    local kk=k[i]
    local s = status[kk]
    if i~=1 then f:write(",") end
    f:write("\n"..kk.."='"..s.."'")
  end
  f:write("\n}")
  f:close()
  return rr
end
function run_s.go_(services,cmd)
  for i=1,#services do
   ss.l(i.."."..cmd..":"..services[i])
    ss.e(cmd.." . "..services[i].." 0")
  end
end
-- ------------------------------------------------------
function run_s.stop_services(services)
 run_s.go_(services,"STOP_WIN32_SERVICE")
end
-- ------------------------------------------------------
function run_s.start_services(services)
 run_s.go_(services,"START_WIN32_SERVICE")
end
-- ------------------------------------------------------

function run_s.uninstall_services(services)
 run_s.go_(services,"UNINSTALL_WIN32_SERVICE")
end
-- ------------------------------------------------------
function run_s.get_masks(arg1)
local masks = services_subsystem[arg1]

if masks==nil then
  ss.l("type not found:"..arg1)
  ss.l("current types:")
  for k,v in pairs(services_subsystem) do
   ss.l(k)
  end
  ss.a("1")
end
 return masks
end

-- ------------------------------------------------------