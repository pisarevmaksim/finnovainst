commands = {}

local ss = analiz.libs.serv

local cmd_get = {}
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
function cmd_get:name()
 return "get"
end

function cmd_get:short_help()
  ss.l("mask get:<file_name> -get status services. result save to file <filename>")
end
function cmd_get:help()
 self:short_help()
end

function cmd_get:execute(args)
  local masks = run_s.get_masks(args.arg1)
  local arg2_ =ss.mysplit(args.arg2,":")

  local services  = run_s.get_services(masks)
  local cur_state = run_s.get_status(services)
  run_s.save_status(cur_state,arg2_[2])

end

-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
function CreateSimpleCmd(name,go,help)
 local cmd ={name_=name,go_ =go,short_help_}
function cmd:name()
 return self.name_
end

function cmd:short_help()
  ss.l(self.short_help_)
end
function cmd:help()
 self:short_help()
end

function cmd:execute(args)
  local masks = run_s.get_masks(args.arg1)
  local services  = run_s.get_services(masks)
  run_s.go_(services,self.go_)
end
 return cmd
end

-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------

local login_cmd={}
function login_cmd:name()
 return "login"
end

function login_cmd:short_help()
  ss.l("mask login user pwd - ...")
end
function login_cmd:help()
 self:short_help()
end

function login_cmd:execute(args)
  local masks = run_s.get_masks(args.arg1)
  local services  = run_s.get_services(masks)

    analiz:set("tmp2",args.arg3)
    analiz:set("tmp3",args.arg4)

  for i=1,#services do
    analiz:set("tmp1",services[i])
    analiz:execute("echo SERVICE_CONFIG_SETTING_USER_PWD tmp . $(tmp1) $(tmp2) $(tmp3)")
    analiz:execute("SERVICE_CONFIG_SETTING_USER_PWD tmp . $(tmp1) $(tmp2) $(tmp3)")
     analiz:execute("echo cnahge login for $(tmp1)$(tab)ret_code:$(tmp)$(tab)login:$(tmp2)")
  end
end


-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------

function CreateHelp(cmd_type)
 local cmd ={name_ = cmd_type}
function cmd:name()
 return self.name_
end

function cmd:short_help()
  ss.l(self.name_ .." - print command help")
end
function cmd:help()
 self:short_help()
end

function cmd:execute(args)
  local skey = run_s.get_sortkey(commands)
  for i=1,#skey do
    local c =commands[skey[i]]
    if self.name_ =="short_help" then
      c:short_help()
    else
      c:help()
    end
  end
end

 return cmd
end
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
local function add(cmd)
  commands[cmd:name()]=cmd
end
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------

add(cmd_get)
add(CreateSimpleCmd("start","START_WIN32_SERVICE","mask start - start all services"))
add(CreateSimpleCmd("stop","STOP_WIN32_SERVICE","mask start - start all services"))
add(CreateSimpleCmd("uninstall","UNINSTALL_WIN32_SERVICE","mask start - start all services"))
add(login_cmd)
add(CreateHelp("help"))
add(CreateHelp("short_help"))

--[[
ss.l("point11111____________________________________")
  local skey = run_s.get_sortkey(commands)
  ss.l("count:"..#skey)
  for i=1,#skey do
    ss.l(skey[i])
  end
ss.a("1")
--]]