local fs = CreateFileServ()
local ss = analiz.libs.serv

local function backup_(src_dir,from,to_dir,stat)

local function stat_inc(stat,name)
    local ff = stat[name]
    if ff==nil then stat[name]=1 else stat[name]=ff+1 end

end

ss.l("1.from:"..from)
  fs:ForceDir(to_dir)
  local f = fs:GetFiles(from,"*.*",false)
  for i=1,#f do
ss.l("1.1")
   local src =  src_dir.."\\"..f[i]
ss.l("1.2")
   if fs:FileExists(src) then
    ss.l("copy.src:"..src.."\tto:"..to_dir)
    fs:CopyFile(src,to_dir.."\\"..f[i])
    stat.all = stat.all+1
    local extr = fs:ExtractExt(f[i])
    stat_inc(stat.files,extr)
    stat_inc(stat.dir,src_dir)
   end
  end
ss.l("2:"..from)
  local dirs = fs:GetSubDirs(from)
--  ss.tprint(dirs)
--  ss.a("a")

  for i=1,#dirs do
    ss.l("copy.dir:"..dirs[i])
    backup_(src_dir.."\\"..dirs[i],from.."\\"..dirs[i],to_dir.."\\"..dirs[i],stat)
  end

  return stat
end

function backup(root_dir,bak_dir,patch,stat)
ss.l("b1")
  local from = patch_dir.."\\"..patch
  backup_(root_dir,from,bak_dir,stat)
end

-- ---------------------------------------------------------------
local function ExtractSqlTag_(f,tag)
  local ff = io.open(f,"r")
  local res = {}
  local t = "--mirgate.meta."..tag..":"
  local s =t:len()+1
  for l in ff:lines() do
    if l:find(t)==1 then
       local val = l:sub(s,-1)
       res[#res+1]=val
    end
  end
  ff:close()
  return res
end
-- ---------------------------------------------------------------
local function GetBodyFor_(pwd,name)
 local sql = "SELECT    OBJECT_DEFINITION( OBJECT_ID( '"..name.."') ) as t;" 
 ss.s("sql",sql)
 ss.s("out","")
 ss.e("SET_PARAM_FROM_SQL out "..pwd.." t $(sql)")
 return ss.g("out")
end
-- ---------------------------------------------------------------
local function prepare_backup_sqlImpl(patch,f,sql_summary)
  local skip_backups = ExtractSqlTag_(f,"SKIP_BACKUP")
  if #skip_backups>0 then

  end

  local pwds = ExtractSqlTag_(f,"PWD")
  local backups = ExtractSqlTag_(f,"BACKUP")
  if #pwds~=1 then
    ss.l("warning.no reference to PWD was found in the file:"..f)
    ss.l("pwds:")
    ss.tprint(pwds)
    sql_summary.warning_sql = sql_summary.warning_sql.."\n"..f..":PWD problem"
    return 
  end
 local pwd = pwds[1]

 if #backups==0 then
    sql_summary.warning_sql = sql_summary.warning_sql.."\n"..f..": information procedures for backup"
 end
 
  local cur = sql_summary.pwds[pwd]
  if cur==nil then 
     sql_summary.pwds[pwd]={} 
     cur = sql_summary.pwds[pwd] 
     if sql_summary.pwd~="" then sql_summary.pwd=sql_summary.pwd..";" end
     sql_summary.pwd=sql_summary.pwd..pwd
  end

  for i=1,#backups do
    local n = backups[i]
    if cur[n]==nil then
      local info = {}
      info.name = n
      info.patch =patch
      info.file = f
      info.body = GetBodyFor_(pwd,n)
      cur[n]=info
      sql_summary.count=sql_summary.count+1 
      if info.body=="" then
        sql_summary.warning_sql = sql_summary.warning_sql.."\n"..f..":"..n.." is empty"
      end
    end
    
  end

end
-- ---------------------------------------------------------------
function backup_sql(bak_dir,sql_summary)
 bak_dir = bak_dir.."\\SQL_BACKUP"
 fs:ForceDir(bak_dir)
 for p,w in pairs(sql_summary.pwds) do
  for k,i in pairs(w) do
    local fn = bak_dir.."\\"..p.."_"..k..".sql"
    local ff = io.open(fn,"w")
    ff:write("--DT:"..ss.b("$(now)").."\n")
    ff:write("--backup initiator.name:"..i.name.."\n")
    ff:write("--backup initiator.patch:"..i.patch.."\n")
    ff:write("--backup initiator.file:"..i.file.."\n")
    ff:write("--rollback.meta.PWD:"..p.."\n")
    ff:write("--rollback.meta.BACKUP:"..i.name.."\n")
    ff:write("\n")
    ff:write(i.body)

    ff:close()
 end
end
end
-- ---------------------------------------------------------------
function prepare_backup_sql(patch,sql_summary)
  local from = patch_dir.."\\"..patch.."\\SQL"
  local sqls_f = fs:GetFiles(from,"*.sql",true)
  table.sort(sqls_f)
  for i=1,#sqls_f do
    sql_summary.count_files=sql_summary.count_files+1
    prepare_backup_sqlImpl(patch,from.."\\"..sqls_f[i],sql_summary)
  end
end
-- ---------------------------------------------------------------
function writeCrc32(out_stream,root_dir,dir,ignore_dir)
   local d = ignore_dir[dir:lower()]
   if d~=nil then return end
  ss.l("1")
  ss.l("1.1:"..dir.."'")
   ss.e("SHOW_TITLE "..dir)
  ss.l("1.2:"..dir.."'")
   local full_dir = root_dir
   if dir~="" then
     full_dir = root_dir.."\\"..dir
   end

   local f = fs:GetFiles(full_dir,"*.*",false)

  ss.l("2")
   for i=1,#f do 
    local filename = full_dir.."\\"..f[i]
    analiz:set("tmp1",filename)
    analiz:execute("GET_CRC32_FOR_FILE tmp $(tmp1)")
    local ff = analiz:get("tmp").."\t"..filename.."\n"
    out_stream:write(ff)
   end

   local dirs = fs:GetSubDirs(full_dir)
   local ldir = dir
   if dir~="" then ldir=dir.."\\" end
   for i=1,#dirs do
    writeCrc32(out_stream,root_dir,ldir..dirs[i],ignore_dir)
   end

end