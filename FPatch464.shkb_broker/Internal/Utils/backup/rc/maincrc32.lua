local fs = CreateFileServ()
local ss = analiz.libs.serv

local dir = analiz:get("dir")
local root_dir= analiz:get("root_dir")



local file = io.open(dir.."\\crc32_"..ss.b("$(DATETIME_FOR_LOG_FILE)")..".db", "w")
local ignore_dir={}


local function add_(n)
ignore_dir[n:lower()]=1
end

add_("OutBox")
add_("PTS")
add_("TXMInterface\\Dbs")
add_("TXMInterface\\Temp")
add_("TXMInterface\\Other")
add_("TXMInterface\\Logs")
add_("TXMInterface\\data")
add_("TXMInterface\\p.lib")
add_("TXMInterface\\services\\namechecking\\list\\tmp")
add_("Namechecking\\InBox")
add_("Namechecking\\Data")
local nc_h = "Namechecking\\p.Secco Namechecking\\help_"
add_(nc_h.."icheck")add_(nc_h.."ichecktuner")add_(nc_h.."namecheck")
add_(nc_h.."pfcheck")add_(nc_h.."plibadmin")add_(nc_h.."plibreport")
add_("Namechecking\\p.Secco Namechecking\\pf_service_dir")
add_("Namechecking\\p.Secco processing nodes\\std\\work")
add_("Namechecking\\p.Secco namechecking Web solution\\Services\\ScriptCache")

writeCrc32(file,root_dir,"",ignore_dir)
file:close()