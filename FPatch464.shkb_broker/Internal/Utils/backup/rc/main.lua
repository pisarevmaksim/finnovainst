local fs = CreateFileServ()
local ss = analiz.libs.serv

local pathes = analiz:get("patchs")
local root_dir= analiz:get("root_dir")



local bb =  fs:GetSubDirs(backup_dir)
local num = #bb
local bak_dir = backup_dir.."\\"..num.."_"..ss.ReplaceValue(pathes,";","_")




fs:ForceDir(bak_dir)

local meta = CreateIniFile(bak_dir.."\\meta.ini")
meta:write("common","patches",pathes)
meta:write("common","now",ss.b("$(now)"))
meta:write("common","host",ss.b("$(computername)"))
meta:write("common","user",ss.b("$(USER_NAME)"))
meta:write("common","ip",ss.b("$(LOCAL_IP)"))

local p = ss.mysplit(pathes,";")
local stat={all=0,files={},dir={}}

local sql_summary={pwds={},count=0,count_files =0,pwd="",warning_sql=""}

for i=1,#p do
  local patch = p[i]
  prepare_backup_sql(patch,sql_summary)
end 


for i=1,#p do 
  local patch = p[i]
  ss.l("backup for "..patch)
  backup(root_dir,bak_dir,patch,stat)
end

local function ll(f,msg)
 ss.l(msg)
 f:write(msg) f:write("\n")
end

if sql_summary.count>0 then
  backup_sql(bak_dir,sql_summary)
end

local summary = io.open(backup_dir.."\\".."summary.log","a")
ll(summary,"_________________________________________________________")
ll(summary,"num:\t"..num)
ll(summary,"now:\t"..ss.b("$(now)"))
ll(summary,"host:\t"..ss.b("$(computername)"))
ll(summary,"user:\t"..ss.b("$(USER_NAME)"))
ll(summary,"patches:\t"..pathes)
ll(summary,"backup.all "..stat.all .." files")
ll(summary,"extr:")
for k,v in pairs(stat.files) do
  ll(summary,"\t"..k.." "..v.." files")
end
ll(summary,"dirs:")
for k,v in pairs(stat.dir) do
  ll(summary,"\t"..k.." "..v.." files")
end

ll(summary,"sql.stat:")
ll(summary,"\tfiles:"..sql_summary.count_files)
ll(summary,"\tpwd:"..sql_summary.pwd)
ll(summary,"\tbackup.entities:"..sql_summary.count)
if sql_summary.warning_sql~="" then
 ll(summary,"\t warning.no information about backup:")
 ll(summary,"\t\t"..sql_summary.warning_sql)
end
summary:close()