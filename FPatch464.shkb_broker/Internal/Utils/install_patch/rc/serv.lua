local fs = CreateFileServ()
local ss = analiz.libs.serv

-- ----------------------------------------------------------------------
function CreateStrCache()
  local o ={values={},cache={}}

  function o:add(name)
    if self.cache[name]~=nil then return end
    self.cache[name]=name
    self.values[#self.values+1]=name
  end

  return o
end
-- ----------------------------------------------------------------------
function GetServicesList()
  local l={list_={},cache_={}}
   
  function l:add(s)
   for i=1,#s do
    local ss=s[i]
    if self.cache_[ss]==nil then
      self.cache_[ss]="1"
     self.list_[#self.list_+1]=ss
    end
   end
  end

  function l:execmd(cmd)
   for i=1,#self.list_ do
    local cmd1=ss.ReplaceValue(cmd,"[service]",self.list_[i])
    ss.e(cmd1)
   end
  end

  function l:show()
    local stat={cache_={},count_=0}
    for i=1,self:count() do
     local ii=self:get(i)
     ss.l(i.."\t"..ii.name..":"..ii.status)
     if stat.cache_[ii.status]==nil then stat.cache_[ii.status]=1 else stat.cache_[ii.status]=stat.cache_[ii.status]+1 end
     stat.count_=stat.count_+1
    end
    return stat
  end
  function l:stop()

--    ss.tprint(self.list_)
    self:execmd("STOP_WIN32_SERVICE . [service] 0")
    self:execmd("STOP_WIN32_SERVICE . [service] 1")
    self:show()
  end

  function l:start()
  ss.tprint(self.list_)

   if #self.list_==0 then 
     ss.l("start services skip. serice list empty")
    return
   end
   local ii=0
   while true do
    self:execmd("START_WIN32_SERVICE . [service] 0")
    self:execmd("START_WIN32_SERVICE . [service] 1")
    local stat = self:show()
    if stat.count_== stat.cache_["SERVICE_RUNNING"]then
     break
    end
   ss.tprint(stat)
    if ii>10 then 
     ss.l("failed start services")
     break
    end
    
    ss.e("SLEEP sec 20")
    ii=ii+1
   end
  end

  function l:count()
   return #self.list_
  end

  function l:get(i)
   local r={}
    r.name= self.list_[i]
   ss.e("QUERYSERVICESTATUS . "..r.name.." tmp")
    r.status =ss.g("tmp")
   return r
  end


  return l
end
-- ----------------------------------------------------------------------
function CreateCtx()
 local root_dir= analiz:get("root_dir")


 local ctx={}


 ctx.stat={all=0,files={},dir={}}

 ctx.root_dir = root_dir
 ctx.patch_dir = patch_dir

  ctx.serv=CreateInstallServ()

  ctx.moved_files = CreateStrCache()
  ctx.files       = CreateStrCache()
  ctx.local_options={}
  ctx.restart_services_list = GetServicesList()

  function ctx:title(val)
   ss.s("tmp",val)
  ss.e("show_title $(tmp)")
  end
  function ctx:get_services(mask)
   ss.e("GET_SERVICE_LIST_EX . tmp "..mask)
   return ss.mysplit(ss.g("tmp"),";")
  end
-- --------------------------------------------------
  function ctx:get_options(name,link,op)
   local o=self.local_options[name]
   if o~=nil then return o end
  local gfn = ss.ReplaceValue(link,"_root_",self.root_dir)
 
   o = CreateOptionsModificator(gfn,op)
   o:init()

if not( o:IsValid()) then
  ss.l("failed load :"..link)
  ss.l("failed:"..o:getLastError())
  ss.l("error.code:ProductInst.Patchs.Patch153.Err1")
  ss.l("fn:"..gfn)
  ss.a(1)
end


   self.local_options[name]=o
   return o
  end

-- --------------------------------------------------
  function ctx:add_summary(msg)
    ss.l(msg)

    local ssf=self.patch_dir.."\\".."summary.log"
    local sf = io.open(ssf,"a+")
    if sf==nil then 
     ss.l("failed.open file: "..ssf)
     ss.a("1")
    end
    sf:write(ss.b("[$(now)]").."\t"..msg.."\n")
    sf:close()
  end

 return ctx
end

function getPatchInfo(patch_dir,patch)
  local ret = { patch_name="", patch_script=""}
  local  i = ss.mysplit(patch,":")
  if #i==2 then 
     ret.patch_name=i[1] 
     ret.patch_script=i[2]
     return ret
  end
     ret.patch_name=patch 
     ret.patch_script=i[2]

  local ff = fs:GetFiles(patch_dir,"install*.lua",false)

  if #ff ==0 then return nil end

  if #ff ~=1 then
     -- пока не понятно что делать если больше одного install файла
     ss.l("failed:install*.lua:"..(#ff))
      return nil 
  end
 
  ret.patch_script=ff[1]  
  return ret
end
-- ------------------------------------------------------------------
function getInstallObj(patch_dir,patch)
 ss.l("load.installobject:"..patch)
  local pi = getPatchInfo(patch_dir,patch)
  if pi==nil then return nil end

  local dir = patch_dir.."\\"..pi.patch_name




  local fobj,emessage=dofile(dir.."\\"..pi.patch_script)
  if emessage~=nil then 
    ss.l("failed.install:"..patch)
    ss.l("emessage:"..emessage)
    ss.a("1")
  end

  local obj =fobj
  return obj
end

local function copy_(src_dir,from,to_dir,stat)

local function stat_inc(stat,name)
    local ff = stat[name]
    if ff==nil then stat[name]=1 else stat[name]=ff+1 end

end

--ss.l("1.from:"..from)
  fs:ForceDir(to_dir)
  local f = fs:GetFiles(from,"*.*",false)
  for i=1,#f do
--ss.l("1.1")
   local src =  src_dir.."\\"..f[i]
--ss.l("1.2")
   if fs:FileExists(src) then
    local to_fn = to_dir.."\\"..f[i]
    ss.l("copy.src:"..src.."\tto:"..to_fn )
    if fs:FileExists(src) then 
        fs:ForceDir(to_dir.."\\.bak")
         ss.s("tmp",to_fn ) 
        ss.s("tmp2",to_dir.."\\.bak\\"..f[i] ) 
        ss.e("MOVE_FILE_V2 $(tmp) $(tmp2)") 
    end
    fs:CopyFile(src,to_fn )
    stat.all = stat.all+1
    local extr = fs:ExtractExt(f[i])
    stat_inc(stat.files,extr)
    stat_inc(stat.dir,src_dir)
   end
  end
ss.l("2:"..from)
  local dirs = fs:GetSubDirs(from)
--  ss.tprint(dirs)
--  ss.a("a")

  for i=1,#dirs do
    ss.l("copy.dir:"..dirs[i])
    copy_(src_dir.."\\"..dirs[i],from.."\\"..dirs[i],to_dir.."\\"..dirs[i],stat)
  end

  return stat
end

-- ------------------------------------------------------------------
function CreateDefaultInstallObj(patch_dir,patch)
  local obj={}

  obj.patch={id=patch}

function obj:prepare(ctx)
end

function obj:install(ctx)
  ss.l(debug.traceback())	
  ctx.serv:copy_patch(ctx,self,ctx.root_dir,self.patch.full_dir)
end

  return obj
end
-- ------------------------------------------------------------------
function CreateInstallServ()
 local serv={}
-- ------------------------------
 function serv:get_stat(ctx)
  return "todo"
 end
-- ------------------------------
 function serv:add_files(ctx,patch_dir0,patch_dir,mask,obj)
--   ss.l("serv:add_moved.1")
   local ff = fs:GetFiles(patch_dir,mask,false)
--   ss.l("serv:add_moved.2")
   for i=1,#ff do
--    ss.l("add moved:"..patch_dir.."\\"..ff[i])
     local fff = patch_dir.."\\"..ff[i]
     local ii = fff:find(patch_dir0,1,true)
     if ii==1 then
        fff = "_root_"..fff:sub(patch_dir0:len()+1,fff:len())
      end
--ss.l("add.moved:"..fff)
     obj:add(fff)
   end
--   ss.l("serv:add_moved.3")
   local dirs = fs:GetSubDirs(patch_dir)
--   ss.l("serv:add_moved.4")
   for i=1,#dirs do
     self:add_files(ctx,patch_dir0,patch_dir.."\\"..dirs[i],mask,obj)
   end
 end

local function getShort(name)
 ss.s("tmp",name)
 ss.e("GET_SHORT_PATH tmp $(tmp)")
 return ss.g("tmp")
end
-- ------------------------------
function serv:merge(ctx,full_patch,op)
  local src_fn   =   getShort(ss.ReplaceValue(op.file,"_root_",ctx.root_dir))
  local patch_fn =   getShort(ss.ReplaceValue(op.file,"_root_",full_patch))
  local merge_cmd =  getShort(ss.ReplaceValue("_root_\\Internal\\Utils\\ex\\merge.exe","_root_",ctx.root_dir))
  analiz:execute('echo execute '..merge_cmd..' '..src_fn..' '..patch_fn..'')
  analiz:execute('execute '..merge_cmd..' '..src_fn..' '..patch_fn..'')
--  analiz:execute('execute "'..merge_cmd..'"  "'..src_fn..'" "'..patch_fn..'"')
end
-- ------------------------------
function serv:moved(ctx,moved_files,patch_id)
 for i=1,#moved_files do
   local f = moved_files[i]
   f=ss.ReplaceValue(f,"_root_",ctx.root_dir)
   ss.s("tmp_from",f)
   ss.s("tmp_to",f.."_"..patch_id)
   ss.e("echo MOVE_FILE2 $(tmp_from) $(tmp_to)")
 end
end
-- ------------------------------
function serv:copy_patch(ctx,patchObj,patch_dir)
  ss.l("install patch:"..patchObj.patch.id)
  copy_(patch_dir,patch_dir,ctx.root_dir,ctx.stat)
end
-- ------------------------------
 return serv
end
-- ------------------------------------------------------------------
