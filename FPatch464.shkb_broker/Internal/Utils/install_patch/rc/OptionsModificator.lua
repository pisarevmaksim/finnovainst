local ss = analiz.libs.serv
local fs = CreateFileServ()
local tick = CreateTimeServ()


-- first create source: F:\p\lua\modyfi.Local\
--local serialize = require 'ser'
---local serpent = require("serpent")
--ss.a("1")

-- ------------------------------------------------------------------------------------
function CreateOptionsModificator(fn,option)
 local m={fn_=fn,op_ = option}


 function m:getLastError()
   return ss.def(self.error_,"")
 end

 function m:dirty()
   self.isdirty_ =true
 end
 function m:init()
  if not fs:FileExists(self.fn_) then
     self.error_ = "not found file:"..self.fn_
      self.valid_=false
     return
  end
   ss.e("ANALIZ_SAFE_EXECUTE ret tmp_emessage LUA_EXECUTE_FILE lua "..self.fn_)
   local emess= ss.g("tmp_emessage")
   if emess~="" then 
     self.error_ = "error in load:"..emess
      self.valid_=false
      return
   end
  
   local cur_obj = _ENV[self.op_.global_name]
   if cur_obj==nil then
      self.error_ = "not found global object:"..self.op_.global_name
      self.valid_=false
      return
   end
   self.obj_= ss.deepcopy(cur_obj)

--   self.obj_= ss.deepcopy(global_oracle_setting_local)
   if self.obj_==nil then
     self.error_ = "error in deepcopy "..self.op_.global_name
      self.valid_=false
      return
   end

   self.isvalid_=true
 end

 function m:IsValid()
   return self.isvalid_==true
 end

 function m:getObj()
   return self.obj_
 end



 function m:add_default(name,val)
   local o= self.obj_[name]
   if o~=nil then return end
   self.obj_[name]=val
 end
 function m:patchMark(ex_params)
  local obj = self.obj_
  
  if obj.modifyInfo==nil then obj.modifyInfo={} end
  local mi = obj.modifyInfo
  if mi.counter==nil then mi.counter=1 else mi.counter=mi.counter+1 end
  mi.last={name=cur_pacthName,
         dt_str=ss.b("$(now)"),
         dt=tick.now() }
   for k,v in pairs(ex_params) do
     mi.last[k]=v
   end

 end
 function m:flush()
  if not (self.isdirty_==true) then  return end
  local sb = ss.CreateStringBuffer()

   ss.l("serialize obj..")
    sb:add('analiz:log("load '..self.op_.global_name..' --")')
    sb:add('\n')
    sb:add('\n')

    sb:add(table.show(self.obj_,self.op_.global_name))



    local dir = fs:ExtractPath(self.fn_)
    local name_bak=fs:ExtractName(self.fn_).."_"..tick.now()
    fs:ForceDir(dir.."\\bak")
    fs:CopyFile(self.fn_,dir.."\\bak\\"..name_bak)
--    ss.s("tmp",fn)
--    ss.e("DELETE_FILE $(tmp)")


    local w = io.open(self.fn_,"w+")
    w:write(sb:to_string())
    w:close()
 end
 return m
end

