local fs = CreateFileServ()
local ss = analiz.libs.serv

local pathes = analiz:get("patchs")
local root_dir= analiz:get("root_dir")

local p = ss.mysplit(pathes,";")

local iobjs={}

for i=1,#p do

  local obj = getInstallObj(patch_dir,p[i])
  local pi=  getPatchInfo(patch_dir,p[i])
  if pi==nil then 
     pi={}
     pi.patch_name=p[i]
     pi.patch_script=p[i]

  end
  if obj==nil then 
     obj=CreateDefaultInstallObj(p[i])
  end

  if obj.patch==nil then   obj.patch={} end
  obj.patch.full_dir=patch_dir.."\\"..pi.patch_name

   iobjs[#iobjs+1]=obj
end

local ctx=CreateCtx()

ctx:get_options("root","_root_\\Internal\\.Local\\global_prospero_setting.lua",{global_name="global_prospero_setting"})




ctx:title("prepare...")
for  i=1,#iobjs do
  local obj =iobjs[i]

  obj:prepare(ctx)
end
ctx:title("stop services..")
ctx.restart_services_list:stop()

ctx:title("install...")
for  i=1,#iobjs do
  local obj =iobjs[i]
  obj:install(ctx)
end

ss.l("ZZZZZZZZZZZ")
do --stat
local stat = ctx.stat
num =#p

local function ll(f,msg)
 ss.l(msg)
 f:write(msg) f:write("\n")
end


do
 for k,v in pairs(ctx.local_options) do
  v:flush()
 end
end

ctx:title("start services")
ctx.restart_services_list:start()

ctx:title("write summary...")

local summary = io.open(patch_dir.."\\".."summary.log","a")
ll(summary,"_________________________________________________________")
ll(summary,"num:\t"..num)
ll(summary,"now:\t"..ss.b("$(now)"))
ll(summary,"host:\t"..ss.b("$(computername)"))
ll(summary,"IP:\t"..ss.b("$(LOCAL_IP)"))
ll(summary,"user:\t"..ss.b("$(USER_NAME)"))
ll(summary,"patches:\t"..pathes)
ll(summary,"moved.all "..#ctx.moved_files.values .." files")
ll(summary,"copy.all "..#ctx.files.values .." files")
ll(summary,"real.copy.all "..stat.all .." files")
ll(summary,"restart services :")
do
  for i=1,ctx.restart_services_list:count() do
   local ii = ctx.restart_services_list:get(i)
    ll(summary,i.."\t"..ii.name..":"..ii.status)
  end
end
ll(summary,"extr:")
for k,v in pairs(stat.files) do
  ll(summary,"\t"..k.." "..v.." files")
end
ll(summary,"dirs:")
for k,v in pairs(stat.dir) do
  ll(summary,"\t"..k.." "..v.." files")
end

summary:close()

end

--p1