function full_restart_txm()

  analiz:log("restart mongodb...")
  if true then 
    rserv:stop_service(restart_options.services.mongoname)
    rserv:delete_mongo_db()
    rserv:start_service(restart_options.services.mongoname)
  end

  analiz:log("restart txm_workers...")  
  do 
    local pids = rserv:get_txm_worker_pids()
    rserv:kill_txm_workers(pids)
  end

   rserv:sleep(10)
  analiz:log("restart txm_proxy...")  
   rserv:kill_txm_proxy()

end