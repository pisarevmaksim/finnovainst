rserv={}
-- ----------------------------------------------
function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
                analiz:log("zz:"..str)
        end
        return t
end
-- ----------------------------------------------
function rserv:sleep(sec)
  analiz:execute("SLEEP sec "..sec)
end
-- ----------------------------------------------
function rserv:kill(name)
  analiz:execute("GET_SHORT_PATH tmp $(_ANALIZ_BIN)")
  analiz:execute("execute $(tmp)\\pskill.exe "..name)
end
-- ----------------------------------------------
function rserv:stop_service(name)
  analiz:execute("STOP_WIN32_SERVIVE . "..name.." 0")
end
-- ----------------------------------------------
function rserv:start_service(name)
  analiz:execute("START_WIN32_SERVIVE . "..name.." 0")
end
-- ----------------------------------------------
function rserv:kill_txm_workers(pids)
  local i = 1
  for i=1,#pids,1 do
    rserv:kill(pids[i])
  end
end
-- ----------------------------------------------
function rserv:kill_txm_proxy()
    rserv:kill("java_proxy")
end
-- ----------------------------------------------
function rserv:get_txm_worker_pids()
  analiz:set("tmp_mask","txm_worker_*.pid")
  analiz:execute("GET_FILE_LIST tmp $(txm_lock_v2) 0 $(tmp_mask) 0")
  analiz:execute("echo $(tmp)")

  local pids=analiz:get("tmp")
  local files=mysplit(pids,";")

  local res={}
  for i=1,#files,1 do 
     analiz:execute("calc_param tmp $(txm_lock_v2)\\"..files[i])
     analiz:execute("LOAD_PARAM tmp1 $(tmp)")
     res[i]=analiz:get("tmp1")
  end

--  analiz:execute("abort 0")
  return res
end
-- ----------------------------------------------
function rserv:delete_mongo_db()
 local dir = "$(txm_db_v2)\\txm_mongodb\\txm"
 analiz:execute("DELETE_DIR "..dir.." 1")
 analiz:execute("FORCEDIRECTORIES "..dir)
end
-- ----------------------------------------------