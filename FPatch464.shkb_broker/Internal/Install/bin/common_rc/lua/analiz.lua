analiz ={}
analiz.libs = {}


function analiz:log(str)
      analiz_to_log(str)
end

function analiz:execute(cmd)
  analiz_execute(cmd)
end

function analiz:tonumber(val)
  return analiz_tonumber(val)
end

function analiz:add_to_file(f,text)
  analiz_execute('ADD_TO_FILE '..f..' '..text)
end

 analiz_to_log('1x')

function analiz:coord()
  local c ={}
  c.x=-1
  c.y=-1

  function c:set(c)
     self.x = c.x
     self.y = c.y
      return self
  end

  function c:setxy(x,y)
     self.x = x
     self.y = y
     return self
  end
 return c
end


-- -----------------------------------
function analiz:double_table(name)
  local table ={}
  table.name = name

  function table:getxy(x,y)
      return analiz_double_get_value(self.name,x,y)
  end

  function table:get(c)
      return analiz_double_get_value(self.name,c.x,c.y)
  end

  function table:setxy(x,y,val)
      return analiz_double_set_value(self.name,x,y,val)
  end

  function table:set(c,val)
      return analiz_double_set_value(self.name,c.x,c.y,val)
  end

  function table:size()
     local c = analiz:coord()
     c.x,c.y=analiz_double_get_size(self.name)
     return c
  end

  function table:sizexy()
      return analiz_double_get_size(self.name)
  end


  function table:load(fn)
    analiz_execute('LOAD_DOUBLE_DATA '..self.name..' N '..fn)
  end

  
  function table:save(fn)
    analiz_execute("WRITE_DOUBLE "..self.name.." "..fn)
  end

  function table:find_col(column_name)
    analiz_execute("GET_INDEX "..self.name.." "..column_name.." tmp")
    return analiz:tonumber(analiz:get("tmp"))
  end


  return table
end
-- -----------------------------------
function analiz:string_table(name)
  local table ={}
  table.name = name

  function table:getxy(x,y)
      return analiz_stable_get_value(self.name,x,y)
  end

  function table:get(c)
      return analiz_stable_get_value(self.name,c.x,c.y)
  end

  function table:setxy(x,y,val)
      return analiz_stable_set_value(self.name,x,y,val)
  end

  function table:set(c,val)
      return analiz_stable_set_value(self.name,c.x,c.y,val)
  end

  function table:size()
     local c = analiz:coord()
     c.x,c.y=analiz_stable_get_size(self.name)
     return c
  end

  function table:sizexy()
      return analiz_stable_get_size(self.name)
  end

  function table:set_size(c)
    analiz_execute('SET_SIZE_STRING '..self.name..' '..c.x..' '..c.y)
  end

  function table:load(fn)
    analiz_execute('load_string_data '..self.name..' N '..fn)
  end

  function table:load_from_sql(pwd,sql)
    analiz:set("tmp",sql)
--    analiz_execute('echo LOAD_STRING_DATA_FROM_SQL '..self.name..' '..pwd.."..$(tmp)")
    analiz_execute('LOAD_STRING_DATA_FROM_SQL '..self.name..' '..pwd.." $(tmp)")
  end

  function table:save(fn)
    analiz_execute("WRITE_STRING "..self.name.." "..fn)
  end

  return table
end

function analiz:map_get(name)
  local mm ={}
  mm.name = name
--  analiz:log("3x")
  function mm:get(key)
      return analiz_map_get(self.name,key)
  end

  function mm:set(key,val)
--   analiz:log(self.name)
--   analiz:log(key)
--   analiz:log(val)

       analiz_map_set(self.name,key,val)
--      return analiz_map_set('test','a','aa')
  end

  function mm:keys()
      return analiz_map_keys(self.name)
  end
  function mm:clear()
  analiz_execute('CREATE_OR_CLEAR_STRING_MAP '..self.name)
  end

  function mm:show()
  analiz_execute('SHOW_STRING_MAP '..self.name)
  end

  return mm
end



function analiz:map_new(name)
  analiz_execute('CREATE_OR_CLEAR_STRING_MAP '..name)
  local mm = analiz:map_get(name)
  return mm
end


function analiz:set(name,val)
  analiz_set_param(name,val)
end

function analiz:load_from_file(name,fn)
   analiz_execute("LOAD_PARAM "..name.." "..fn)
end

function analiz:load_from_sql(name,db_field,pwd,sql)
   analiz_execute("SET_PARAM_FROM_SQL "..name.." "..pwd.." "..db_field.." "..sql)
end

function analiz:get(name)
  return analiz_get_param(name)
end

function analiz:IsStop()
   analiz_execute("IS_STOP tmp .")
   local is_ok = analiz:get("tmp")
    local is_service_stop = analiz:get("TAnalizImpl_Service_Signal_stop")
   return is_ok=="1" or is_service_stop=="1"
end

function analiz:SetDebugLevel(level)
  analiz:execute("set_param LOG_LEVEL "..level)
end

function analiz:raw_log(msg)
  analiz:set("tmp",msg)
  analiz:execute("TO_RAW_LOG $(tmp)")
end
function analiz:buildString(msg)
  analiz:set("tmp",msg)
  analiz:execute("calc_param tmp "..msg)
  return analiz:get("tmp")
end

