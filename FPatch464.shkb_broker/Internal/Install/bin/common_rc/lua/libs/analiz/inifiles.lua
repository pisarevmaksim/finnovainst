ss_ini2obj_serv_object={}
local ss = analiz.libs.serv

function CreateIniFile(fn_arg)
  local ini = {fn=fn_arg}
   analiz:execute("calc_param tmp ini_$(guid)")
   analiz:set("tmp1",fn_arg)
   analiz:execute("CREATE_INI_FILE $(tmp) $(tmp1)")
   ini.name = analiz:get("tmp")


  function ini:write(sec,key,val)
   analiz:set("tmp1",sec)
   analiz:set("tmp2",key)
   analiz:set("tmp3",val)
   analiz:execute("PARAM_TO_INI_FILE "..self.name.." $(tmp1) $(tmp2) $(tmp3)")
  end

  function ini:load(sec,key,def)
   if def==null then
     def=""
   end

   analiz:set("tmp","")
   analiz:set("tmp0",self.fn)
   analiz:set("tmp1",sec)
   analiz:set("tmp2",key)
   analiz:set("tmp3",def)
   analiz:execute("LOAD_PARAM_FROM_INI_FILE_DEFAULT $(tmp0) tmp $(tmp1) $(tmp2) $(tmp3)")

   return analiz:get("tmp")
  end

  function ini:getsession(session_name)
   analiz:execute("CREATE_OR_CLEAR_STRING_MAP tmp_ini_23425423523")
   analiz:set("tmp2",session_name)
   analiz:set("tmp",self.fn)
   analiz:set("tmp3","")
   analiz:execute("STRING_MAP_LOAD_FROM_INI_FILE2 tmp_ini_23425423523 $(tmp) $(tmp2) $(tmp3)")

    ss_ini2obj_serv_object={}
    analiz:execute("LUA_MAP_TO_OBJ lua ss_ini2obj_serv_object tmp_ini_23425423523")
    return ss.deepcopy(ss_ini2obj_serv_object)
  end

  function ini:close()
  -- todo
  end

  return ini
end