if analiz.libs.serv==nil then
analiz.libs.serv = {}
end

local ss = analiz.libs.serv
-- -------------------------
 function ss.b(msg)
  return analiz:buildString(msg)
end

-- ---------------------
 function ss.e(cmd)
 analiz:execute(cmd)
end
-- ---------------------
 function ss.s(n,val)
 analiz:set(n,val)
end

-- ---------------------
 function ss.g(n)
 return analiz:get(n)
end

-- ---------------------
 function ss.l(n)
 return analiz:log(n)
end

-- ---------------------
 function ss.a(n)
 if not (n == null) then
   ss.l(n)
 end
 ss.e("abort 1")
end
-- ---------------------



-- -------------------------------
function ss.tprint (tbl, indent)
--   ss.l("tprint.new")
    if tbl==nil then ss.l("nil") return end

  --  ss.l("tprint2")

  if not(type(tbl) == "table")  then
    if type(tbl) == 'boolean' then  ss.l( tostring(tbl))  else      ss.l( tbl) end
    return
  end
--  ss.l("tprint3")

  if not indent then indent = 0 end

  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
--  ss.l(type(v))
    if type(v) == "table" then
      ss.l(formatting)
      ss.tprint(v, indent+1)
    elseif type(v) == 'boolean' then
      ss.l(formatting .. tostring(v))      
    elseif type(v) == 'function' then
      ss.l(formatting .. "function")      
    else
      ss.l(formatting .. v)
    end
  end
end

-- -------------------------------
function ss.deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end



function ss.toJson_fast_sort (tbl,sb)
  if tbl==nil then
    sb:add("{}")
    return 
  end
  local j = ""
  sb:add("{")
  local first=true
  local keys = {}
  for k, v in pairs(tbl) do
    keys[#keys+1]=k
  end
  table.sort(keys)
  for i=1,#keys do
    local k = keys[i]
    local v = tbl[k]
    local vv=""
    if not first then
      sb:add(",")
    end
    if type(k)=="table" then
     sb:add("<table>=")
    else
     sb:add(k.."=")
    end
    if type(v) == "table" then
      ss.toJson_fast_sort(v,sb)
    elseif type(v) == 'boolean' then
      if v then vv="true" else vv="false"end
    elseif type(v) == 'string' then
      vv = "'"..v.."'"
    else
      vv = v
    end
    sb:add(vv)
   first = false
  end

  sb:add("}")
end

function ss.toJson_fast (tbl,sb)
  if tbl==nil then
    sb:add("{}")
    return 
  end
  local j = ""
  sb:add("{")
  local first=true

  for k, v in pairs(tbl) do
    local vv=""
    if not first then
      sb:add(",")
    end
    sb:add(k.."=")
    if type(v) == "table" then
      ss.toJson_fast(v,sb)
    elseif type(v) == 'boolean' then
      if v then vv="true" else vv="false"end
    elseif type(v) == 'string' then
      vv = "'"..v.."'"
    else
      vv = v
    end
    sb:add(vv)
   first = false
  end

  sb:add("}")
end
-- --------------------------------
function ss.toJson (tbl)
 local sb = ss.CreateStringBuffer()
 ss.toJson_fast(tbl,sb)
 return sb:to_string()
end

-- --------------------------------
function ss.toJson_sort(tbl)
 local sb = ss.CreateStringBuffer()
 ss.toJson_fast_sort(tbl,sb)
 return sb:to_string()
end

-- -------------------------------
-- --------------------------------
function ss.CreateStringBuffer ()
-- https://www.lua.org/pil/11.6.html
  local sb = {stack={""}}

  function sb:add(s)
      local stack = self.stack
      table.insert(stack, s)    -- push 's' into the the stack
      for i=#stack-1, 1, -1 do
        if string.len(stack[i]) > string.len(stack[i+1]) then
          break
        end
        stack[i] = stack[i] .. table.remove(stack)
      end
  end
-- -
  function sb:to_string()
    return table.concat(self.stack)
  end
  function sb:isempty()
   if #self.stack>1 then
      return false
   end
   return self.stack[1]==""
  end
-- -
  function sb:clear()
   self.stack={""}
  end
-- --------------------
 return sb
end
-- -------------------------------
function ss.def(a,b)
 if a==null then return b end
 return a
end
-- -------------------------------
-- -----------------------------------------------------------------------------------------
function ss.IntToStr(ii)
 local v= analiz:tonumber(ii)
 if v>0 and v<1 then
  return "0"
 end
 local i=""..ii
 return ss.ReplaceValue(i,".0","")
end
-- -------------------------------
function IntToStr(ii)
 local v= analiz:tonumber(ii)
 if v>0 and v<1 then
  return "0"
 end
 local i=""..ii
 return ss.ReplaceValue(i,".0","")
end

-- -----------------------------------------------------------------------------------------
function ss.ReplaceValue(str,t,n)
 analiz:set("tmp",str)
 analiz:set("tmp1",t)
 analiz:set("tmp2",n)
 analiz:execute("REPLACE_VALUE tmp $(tmp) $(tmp1) $(tmp2)")
 return analiz:get("tmp")
end
function ReplaceValue(str,t,n)
 analiz:set("tmp",str)
 analiz:set("tmp1",t)
 analiz:set("tmp2",n)
 analiz:execute("REPLACE_VALUE tmp $(tmp) $(tmp1) $(tmp2)")
 return analiz:get("tmp")
end

-- -----------------------------------------------------------------------------------------
function ss.UpperCase(str)
 analiz:set("tmp",str)
 analiz:execute("TO_UPPERCASE_PARAM tmp $(tmp)")
 return analiz:get("tmp")
end

-- ---------------------
function ss.mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
            --    analiz:log("zz:"..str)
        end
        return t
end
-- ---------------------
-- ---------------------
function ss.StrToFile(fn,str)
 analiz:set("tmp",fn)
 analiz:set("tmp1",str)
 analiz:execute("CLEAR_FILE $(tmp)")
 analiz:execute("ADD_TO_FILE $(tmp) $(tmp1)")
end
-- ---------------------
function ss.WriteToFile(fn,str)
 analiz:set("tmp",fn)
 analiz:set("tmp1",str)
 analiz:execute("ADD_TO_FILE $(tmp) $(tmp1)")

end
-- ---------------------
function ss.GetUnicName(pref)
  return  ss.b(pref.."_$(GUID_FOR_LOG)")
end
-- ---------------------
function ss.FileToStr(fn)
 analiz:set("tmp",fn)
 analiz:execute("LOAD_PARAM tmp $(tmp)")
 return analiz:get("tmp")
end
-- ---------------------

 function ss.GetFieldsList(fields)
  local res=""
  for i=1,#fields,1 do
   local f =fields[i]
   if i==1 then
     res = f
   else
     res = res..";"..f
   end
  end
  return res
 end
-- -----------------------------------------------------------------------------
function   ss.IsService()
 local l = analiz:get("_IS_SERVICE")
 return l=="1"
end
-- -----------------------------------------------------------------------------
function ss.ShowTitle(str)
  analiz:execute("SHOW_TITLE "..str)
end
-- -----------------------------------------------------------------------------------------
function ss.fix_(str)
  analiz:set("tmp",str)
  analiz:execute("FIX_STRING tmp $(tmp)")
  return analiz:get("tmp")
end

-- -----------------------------------------------------------------------------------------
function ss.CreateRandomGenerator(seed)
  local r ={}
  r.name_ = ss.b("random_$(guid)")
  ss.s("tmp",r.name_)
  ss.s("tmp1",""..seed)
  ss.e("CREATE_RANDOM_GENERATOR $(tmp) $(tmp1)")

  function r:close()
  ss.s("tmp",self.name_)
    ss.e("DESTROY_RANDOM_GENERATOR $(tmp)")
   self.name_ = ""
  end

  function r:Random(max_val)
    ss.s("tmp",self.name_)
    ss.s("tmp1",""..max_val)
    ss.e("RANDOM_VALUE_EX tmp $(tmp1) $(tmp)")
    return analiz:tonumber(ss.g("tmp"))
  end
  return r
end
-- -----------------------------------------------------------------------------------------
ss_map2obj_serv_object={}
function ss.map2obj(map_name)
    ss_map2obj_serv_object={}
    analiz:execute("LUA_MAP_TO_OBJ lua ss_map2obj_serv_object "..map_name)
    return ss.deepcopy(ss_map2obj_serv_object)
end
-- -----------------------------------------------------------------------------------------

