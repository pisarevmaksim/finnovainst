local ss = analiz.libs.serv

function CreateFileServ()
  local fileserv={}

-- ---------------------
function fileserv:ForceDir(dir)
  analiz:set("tmp",dir)
  analiz:execute("FORCEDIRECTORIES $(tmp) ")
end

-- --------------------------------
  function fileserv:FileExists(fn)
  analiz:set("tmp_fn",fn)
  analiz:execute("FILEEXISTS tmp $(tmp_fn)")
  return analiz:get("tmp")=="1"
 end
-- --------------------------------
  function fileserv:FileCount(dir,mask)
  analiz:set("tmp",dir)
  analiz:set("tmp1",mask)
  analiz:execute("echo GET_FILE_COUNT $(tmp) $(tmp1) tmp2")
  analiz:execute("GET_FILE_COUNT $(tmp) $(tmp1) tmp2")
  analiz:log(mask..":"..analiz:get("tmp2"))
  return analiz:tonumber(analiz:get("tmp2"))
 end
-- --------------------------------
  function fileserv:GetSubDirs(dir)
   analiz:set("tmp","") 
   analiz:set("tmp1",dir) 

   analiz:execute("GET_SUBDIRS_TO_FIELD_LIST tmp $(tmp1)")
   local files = ss.mysplit(analiz:get("tmp"),";")
   return files
  end

-- --------------------------------
  function fileserv:GetFiles(dir,mask,is_scan_sub_dir)
   analiz:set("tmp","") 
   analiz:set("tmp1",dir) 
   if  is_scan_sub_dir then
     analiz:set("tmp2","1") 
   else
     analiz:set("tmp2","0") 
   end
     analiz:set("tmp3",mask) 
   analiz:execute("GET_FILE_LIST tmp $(tmp1) $(tmp2) $(tmp3)")
   local files = ss.mysplit(analiz:get("tmp"),";")
   return files
  end
-- --------------------------------
  function fileserv:DeleteDir(dir,del_sub_dir)
   analiz:set("tmp1",dir) 
   if  del_sub_dir==false then
     analiz:set("tmp2","0") 
   else
     analiz:set("tmp2","1") 
   end


  analiz:execute("DELETE_DIR $(tmp1) $(tmp2)")
 end
-- --------------------------------
  function fileserv:CopyFile(from,to)
   analiz:set("tmp",from)
   analiz:set("tmp1",to)

  analiz:execute("COPY_FILE $(tmp) $(tmp1)")
  end
-- --------------------------------
  local function gocmd(cmd,param)
   analiz:set("tmp",param)

--  analiz:execute("echo "..cmd.." tmp $(tmp)")
  analiz:execute(cmd.." tmp $(tmp)")
   return analiz:get("tmp")
  end

  function fileserv:ExtractPath(fn)
   return gocmd("EXTRACT_FILE_PATH",fn)
  end
-- --------------------------------
  function fileserv:ExtractName(fn)
   return gocmd("EXTRACT_FILE_NAME",fn)
  end
-- --------------------------------
  function fileserv:ExtractExt(fn)
   return gocmd("EXTRACT_FILE_EXT",fn)
  end
-- --------------------------------

-- --------------------------------
  function fileserv:GetSrc32(fn)
   return gocmd("GET_CRC32_FOR_FILE",fn)
  end
-- --------------------------------
  function fileserv:GetSize(fn)
   return gocmd("GET_FILE_SIZE",fn)
  end
-- --------------------------------
  function fileserv:ChangeFileExt(fn,new_extr)
    analiz:set("tmp",fn)
    analiz:set("tmp1",new_extr)

    analiz:execute("CHANGE_FILE_EXT tmp $(tmp) $(tmp1)")

    return analiz:get("tmp")
  end
-- --------------------------------
-- --------------------------------
-- --------------------------------
  return fileserv
end
-- analiz:log("fileserv.end")