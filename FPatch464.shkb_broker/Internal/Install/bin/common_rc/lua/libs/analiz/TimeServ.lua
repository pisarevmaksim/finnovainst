local ss = analiz.libs.serv

if CreateDateTime==nil then
  ss.e("LUA_EXECUTE_FILE lua $(lua_analiz_libs)\\analiz\\init_dt.lua")
end 

local dserv  = CreateDateTime()

function CreateTimeServ()
 local t = {}
 local sec_const = (24.0*60.0*60.0)

 t.second=1.0/sec_const
-- ------------------------------ 
 function t.now()
   return analiz_now()
 end
-- ------------------------------
 function t.to_sec(delta)
   local d = math.floor(delta)
--   ss.l("d:"..d..",delta-d:"..delta-d)

   local sec = (delta-d)*sec_const
   return sec
 end
-- ------------------------------
 function t.to_msec(delta)
  local sec = t.to_sec(delta)
  return math.floor(sec*100000)/100
 end
-- --------convert double to DateTime object
 function t.get_dt(val)
   analiz:execute("DOUBLE_TO_DATETIME tmp "..val)
   local dt = CreateDateTime()
   dt:set(analiz:get("tmp"))
   return dt
 end


 function t.parse(dt)
  ss.s("tmp","")
  ss.s("tmp1",dt)
  ss.e("DOUBLE_TO_DATETIME tmp $(tmp1)")
  dserv:set(ss.g("tmp"))
  local ii={}
  
  ii.day =dserv:day()
  ii.month=dserv:month()
  ii.year=dserv:year()

  ii.hour=dserv:hour()
  ii.minute=dserv:min()
  ii.sec=dserv:sec()
  ii.msec =dserv:msec()
 
  return ii
 end
-- ------------------------------
 return t
end

-- ------------------------------

function CreateTimeDiap()
  local tt={}
  local t = CreateTimeServ()

  function tt:Start()
   self.start = t.now()
  end
  function tt:End()
   local e = t.now()
   self.delta_sec = t.to_sec(e-self.start)
  end
  function tt:to_string()
--   ss.l("self.delta_sec:"..self.delta_sec)
   local t = math.floor(self.delta_sec*1000)

   local msec = t%1000
   t= math.floor(t/1000)
     local sec = t%60;    t= math.floor(t/60)
     local min = t%60;    t= math.floor(t/60)
     local hour =t%24

   local res=""
  if(hour>0) then res=hour.." hour "  end
  if(min>0) then res=res..min.." min " end
  if(sec>0) then res=res..sec.." sec " end
  if(msec>0) then res=res..msec.." msec " end

  if res=="" then res = "0 msec" end

   return res
  end


  return tt
end