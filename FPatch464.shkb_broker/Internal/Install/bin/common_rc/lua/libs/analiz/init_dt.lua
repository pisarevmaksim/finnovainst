-- -----------------------------------------------------------------
function CreateDateTime()
  local dt={dt_ =""}
-- -----------------------------------------------------------------
 function dt:set_now()
  analiz:execute("calc_param tmp $(now)")
  self.dt_ = analiz:get("tmp")
  return self
 end
-- -----------------------------------------------------------------
 function dt:set(val)
  self.dt_ = val
  return self
 end
-- -----------------------------------------------------------------
 function dt:get_string()
  return self.dt_
 end
-- -----------------------------------------------------------------
 function dt:clone()
  local n = CreateDateTime()
--  analiz:log("clone:"..self.dt_)
  n:set(self.dt_)
  return n
 end


-- -----------------------------------------------------------------
 function dt:add_internal(command_str,val_sec)
  analiz:set("tmp",""..self.dt_)
  analiz:set("tmp1",""..val_sec)
  analiz:execute(command_str.." tmp $(tmp) $(tmp1)")
  local n = analiz:get("tmp")
  -- analiz:log(command_str..":before:"..self.dt_..",after:"..n)
  self.dt_ = n

  return self

 end
-- -----------------------------------------------------------------
 function dt:add_sec(val_sec)
  return self:add_internal("DATETIME_ADD_SEC",val_sec)
 end
-- -----------------------------------------------------------------
 function dt:add_min(val)
  return self:add_sec(val*60)
 end
-- -----------------------------------------------------------------
 function dt:add_day(val)
  return self:add_internal("DATETIME_ADD_DAY",val)
 end
-- -----------------------------------------------------------------
 function dt:add_month(val)
  return self:add_internal("DATETIME_ADD_MONTH",val)
 end

 function dt:decode_time(cmd)
   analiz:set("tmp2",self.dt_)
   analiz:execute("DATETIME_TO_DOUBLE2 tmp1 $(tmp2)")
   analiz:execute("DOUBLE_DATETIME_DECODE_TIME tmp $(tmp1) "..cmd)
   return analiz:tonumber(analiz:get("tmp"))
 end
-- -----------------------------------------------------------------
 function dt:to_double()
   analiz:set("tmp2",self.dt_)
   analiz:execute("DATETIME_TO_DOUBLE2 tmp1 $(tmp2)")
   return analiz:tonumber(analiz:get("tmp1"))
 end

-- -----------------------------------------------------------------
 function dt:decode_dt(cmd)
   analiz:set("tmp2",self.dt_)
   analiz:execute("DATETIME_TO_DOUBLE2 tmp1 $(tmp2)")
   analiz:execute(cmd.." tmp $(tmp1)")
   return analiz:tonumber(analiz:get("tmp"))
 end
-- -----------------------------------------------------------------
 function dt:year()
   return self:decode_dt("DOUBLE_DATETIME_TO_YEAR")
 end
-- -----------------------------------------------------------------
 function dt:month()
   return self:decode_dt("DOUBLE_DATETIME_TO_MONTH")
 end
-- -----------------------------------------------------------------
 function dt:day()
   return self:decode_dt("DOUBLE_DATETIME_TO_DAY_OF_MONTH")
 end
-- -----------------------------------------------------------------
 function dt:hour()
   return self:decode_time("hour")
 end
-- -----------------------------------------------------------------
 function dt:min()
   return self:decode_time("min")
 end
-- -----------------------------------------------------------------
 function dt:sec()
   return self:decode_time("sec")
 end
-- -----------------------------------------------------------------
 function dt:msec()
   return self:decode_time("msec")
 end

-- -----------------------------------------------------------------
 function dt:set_date(dd,mm,yyyy)
   
    analiz:set("tmp1",""..dd)
    analiz:set("tmp2",""..mm)
    analiz:set("tmp3",""..yyyy)
--   analiz:execute("echo DATETIME_SET_DATE tmp $(tmp1) $(tmp2) $(tmp3)")
    analiz:execute("DATETIME_SET_DATE tmp $(tmp1) $(tmp2) $(tmp3)")
    local t = analiz:get("tmp")
    analiz:log("set dt:"..t)
    self.dt_ = t
   return self
 end
 function dt:set_time(hh,mm,sec)
    analiz:set("tmp1",""..hh)
    analiz:set("tmp2",""..mm)
    analiz:set("tmp3",""..sec)
    analiz:set("tmp4",self.dt_)
  -- analiz:execute("echo DATETIME_SET_TIME tmp $(tmp4) $(tmp1) $(tmp2) $(tmp3)")
    analiz:execute("DATETIME_SET_TIME tmp $(tmp4) $(tmp1) $(tmp2) $(tmp3)")
    local t = analiz:get("tmp")
    self.dt_ = t
   return self

 end

-- -----------------------------------------------------------------
 function dt:diff_sec(val_dt)
  analiz:set("tmp",""..self.dt_)
  analiz:set("tmp1",""..val_dt:get_string())
--  analiz:execute("echo DATETIME_DIFF_SEC.. tmp $(tmp) $(tmp1)")

  analiz:execute("DATETIME_DIFF_SEC tmp $(tmp) $(tmp1)")
--analiz:execute("echo delta:$(tmp)")
  return analiz:tonumber(analiz:get("tmp"))
 end

-- -----------------------------------------------------------------




 return dt
end


