if analiz.libs.serv==nil then
   analiz.libs.serv = {}
end

local ss = analiz.libs.serv


-- ---------------------
function ss.CreatePwdServ()
  local pwd ={}
-- ----------------------------
  function pwd:load(file)
    self.file_ = file
    if file==null then 
--    ss.e("PWD $(system32)\\local.pwd")
      self.file_ = "$(system32)\\local.pwd"
    else
     self.file_ = ss.b(file)
     ss.e("PWD "..self.file_)
    end
  end
-- ----------------------------
  function pwd:add(name,p)
    ss.s("tmp",name)

    ss.s("tmp1",p.dsn)
    ss.s("tmp2",p.login)
    ss.s("tmp3",p.pwd)
    ss.s("tmp4",p.org)
    ss.e("ADD_TO_PWD $(tmp) $(tmp1) $(tmp2) $(tmp3) $(tmp4)")
  end
-- ----------------------------
  function pwd:get(name)
--   a("n:"..name)
   ss.s("tmp",name)

 ss.e("LOAD_PARAM_FROM_PWD $(tmp) tmp ALL")

  local c = { dsn = ss.g("tmp.DSN"),
              login = ss.g("tmp.LOGIN"),
              pwd = ss.g("tmp.PASSWORD"),
              org = ss.g("tmp.ORGCLIENT")
             }

    return c
  end

  function pwd:keys()
   ss.e("PWD_KEYS tmp")
   local k=ss.g("tmp")
   return ss.mysplit(k,";")
  end
--
  function pwd:save(file)
   if file ==nil then 
     file = self.file_
   else
    file = ss.b(file)
   end
   ss.e("SAVE_PWD "..file)
  end

--
  return pwd
end