html ={}
html.br='<BR>\n'

analiz:log("html.serv1")




function html:link_tag(tag)
  return  '<p><a name="'..tag..'"></a></p>'
end

function html:link_local_ref(tag,text)
  return    '<p><a href="#'..tag..'">'..text..'</a></p>'
end

function html:link_ref(tag,text)
  return    '<p><a href="'..tag..'">'..text..'</a></p>'
end

function html:get_html(header,body)
  return '<html><header>'..header..'</header>\n<body>'..body..'</body>\n</html>'
end

function html:bold(t)
  return '<b>'..t..'</b>'
end

function html:line(text)
--  return text
 return text..'<BR>'
end

function html:article(text)
--  return text
 return '<article>'..text..'</article>'
end


function html:header_ref(level,text, ref)
--  return text
 return '<h'..level..' href="'..ref..'">'..text..'</h'..level..'>'
end

function html:header(level,text)
--  return text
 return '<h'..level..'>'..text..'</h'..level..'>'
end

function html:get_table(t,max_x,max_y)
 local x,y = t:sizexy()
 if max_x<x then
  x=max_x
 end
 if max_y<y then
  y=max_y
 end
 local tt=''
  for yy = 0, y-1,1 do  
    local vv=''
    for xx = 0, x-1,1 do
       vv=vv..'<td>'..t:getxy(xx,yy)..'</td>'
    end
     tt=tt..'<tr>'..vv..'</tr>\n'
  end
  return '<table>'..tt..'</table>'
end

function html:get_table_tras(t,max_x,max_y)
 local x,y = t:sizexy()
 if max_x<x then
  x=max_x
 end
 if max_y<y then
  y=max_y
 end
 local tt=''
 for xx = 0, x-1,1 do
    local vv=''
    for yy = 0, y-1,1 do  
       vv=vv..'<td>'..t:getxy(xx,yy)..'</td>'
    end
     tt=tt..'<tr>'..vv..'</tr>\n'
  end
  return '<table>'..tt..'</table>'
end



analiz:log("html.serv.end")