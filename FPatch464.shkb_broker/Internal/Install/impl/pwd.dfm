object Form1: TForm
  Left = 264
  Top = 111
  BorderStyle = bsDialog
  Caption = 'Pwd'
  ClientHeight = 98
  ClientWidth = 260
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 57
    Width = 260
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object OKBtn: TButton
      Left = 32
      Top = 12
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 0
    end
    object CancelBtn: TButton
      Left = 128
      Top = 12
      Width = 75
      Height = 25
      Caption = '&Cancel'
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 260
    Height = 57
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 28
      Width = 21
      Height = 13
      Caption = 'Pwd'
    end
    object PwdEdt: TEdit
      Left = 52
      Top = 24
      Width = 149
      Height = 21
      PasswordChar = '*'
      TabOrder = 0
    end
  end
end