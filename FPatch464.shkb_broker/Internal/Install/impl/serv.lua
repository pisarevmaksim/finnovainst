-- ----------------------------------
function LoadFile(fn)
  analiz:log("LoadFile")
  analiz:set("tmp",fn)
  analiz:execute("LOAD_PARAM tmp1 $(tmp)")
  local res = analiz:get("tmp1")

--  analiz:log("LoadFile.end:"..res)
--  analiz:execute("abort 1")
  return res
end

-- ----------------------------------
function SaveFile(fn,t)
  analiz:set("tmp",fn)
  analiz:set("tmp1",t)
  analiz:execute("CLEAR_FILE $(tmp)")
  analiz:execute("ADD_TO_FILE $(tmp) $(tmp1)")
end
-- ----------------------------------
function BuildString(s)
  analiz:execute("calc_param tmp "..s)
  return analiz:get("tmp")
end
-- ----------------------------------
function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
                analiz:log("zz:"..str)
        end
        return t
end
-- ----------------------------------
function ReplaceValue(inputstr, temp,new_val)
  analiz:set("tmp",inputstr)
  analiz:set("tmp1",temp)
  analiz:set("tmp2",new_val)
  analiz:execute("REPLACE_VALUE tmp $(tmp) $(tmp1) $(tmp2)")
  local ret = analiz:get("tmp")
  return ret
end  
-- ----------------------------------
-- ----------------------------------
function extractDomenAndUser(str)
  local o = mysplit(str,"\\")
  local ret={domen="",user=""}
  if #o==0 then
     return ret
  end

  if #o==1 then
    ret.user=str
  elseif #o==2 then
    ret.domen=o[1]
    ret.user=o[2]
  else
   assert(false, "argument format not valid (domen\\user):"..str)
  end
  return ret
end

-- ----------------------------------
function showDomenUser(o)
 analiz:log("domen:\t"..o.domen)
 analiz:log("user:\t"..o.user)
end
-- ----------------------------------
function title(str)
 analiz:execute("SHOW_TITLE "..str)
end
-- ----------------------------------

