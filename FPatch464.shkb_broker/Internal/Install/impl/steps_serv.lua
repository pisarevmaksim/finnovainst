   function template_ctx(service_name)
     local t = {}






     t.service_name   =  service_name
     t.service_path   = BuildString("$(txm_s_root)\\services\\"..service_name)

     t.db_path        = BuildString("$(txm_s_root)\\DBs\\"..service_name)
     t.config_path        = BuildString("$(txm_s_root)\\Configs\\"..service_name)

     t.txm_s_root     = BuildString("$(txm_s_root)")

     local tmp = ReplaceValue(t.txm_s_root,"\\","[z1]")
     t.txm_s_root_ss =  ReplaceValue(tmp,"[z1]","\\\\")

     return t
   end


-- ----
   function template_to_setting(ctx,service_path,from,to)
    local t = LoadFile(service_path.."\\"..from)
    local val = ReplaceValue(t,"[[txm_s_root]]",ctx.txm_s_root)
          val = ReplaceValue(val,"[[txm_s_root_SS]]",ctx.txm_s_root_ss)
     SaveFile(service_path.."\\"..to,val)

  end

-- ----
