-- ---------------------------------
function Install_Namecheking(options)
   title("--Install_Namecheking--")
  install_nc_services(options)

  setting_restart_txm_services(options)

  settingPlibPriv(options)

  finasFix(options)
 
  FixOrgClient(options)
end
-- ---------------------------------
function Install_pf_txm(options)
   title("--Install_pf_txm--")
 SettingLocalPwd(options)


 SettingSystemEnvironment(options)
 SettingJavaProxy(options)
 SettingMongoDb(options)

 install_txm_services(options)

 setting_restart_txm_services(options)

 settingPlibPriv(options)

  finasFix(options)
 
  FixOrgClient(options)
-- txm_start(options)

end
-- ---------------------------------
   title("--start.install--")


local options = GetInstallOptions()
--   a(options.install_type)

-- namechecking                    
 if options.install_type=="namechecking" then
  Install_Namecheking(options)
 else
  Install_pf_txm(options)
 end
