serviceServ={}
analiz:log("init serviceServ.start")
-- -------------------------------------------------------------
function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
                analiz:log("zz:"..str)
        end
        return t
end
-- -------------------------------------------------------------
function serviceServ.getService(name_arg)
 local s = {name=name_arg}
 local function wait_to_str_(is_wait)
   if is_wait then
     return "1"
   end
   return "0"
 end

 function s:run(is_wait)
  analiz:execute("START_WIN32_SERVICE . "..self.name.." "..wait_to_str_(is_wait))
 
 end

 function s:stop(is_wait)
  analiz:execute("STOP_WIN32_SERVICE . "..self.name.." "..wait_to_str_(is_wait))
 end

 function s:status()
  analiz:execute("QUERYSERVICESTATUS . "..self.name.." tmp")
  return analiz:get("tmp")
 end
 return s

end
-- -------------------------------------------------------------
function serviceServ.getServiceList(filter)
 analiz:set("tmp1",filter)
 analiz:execute("GET_SERVICE_LIST_EX . tmp $(tmp1)")
 local list = mysplit(analiz:get("tmp"),";")

 local ret={}
 for i=1,#list,1 do
  ret[#ret+1]= serviceServ.getService(list[i])
  analiz:log("add:"..list[i])
 end

 return ret
end
-- -------------------------------------------------------------
function serviceServ.changeUserPwd(service_name,usr,pwd)
  analiz:set("tmp1",service_name)
  analiz:set("tmp2",usr)
  analiz:set("tmp3",pwd)
  analiz:execute("SERVICE_CONFIG_SETTING_USER_PWD tmp . $(tmp1) $(tmp2) $(tmp3)")
  return analiz:get("tmp")
end

-- -------------------------------------------------------------
function serviceServ.SettingRestartOptions(service_name,time_sec)
  analiz:set("tmp1",service_name)
  analiz:set("tmp2",time_sec)
  analiz:execute("SERVICE_CONFIG_FAILURE_ACTIONS_V2 . $(tmp1) $(tmp2)")
end

-- -------------------------------------------------------------
function serviceServ.uninstall_service(service_name)
  analiz:set("tmp1",service_name)
  analiz:execute("UNINSTALL_WIN32_SERVICE . $(tmp1)")
end

-- -------------------------------------------------------------
function serviceServ.SettingStartType(service_name,start_type)
  analiz:set("tmp1",service_name)
  analiz:set("tmp2",start_type)
  analiz:execute("SERVICE_CONFIG_SETTING_START_MODE tmp . $(tmp1) $(tmp2)")
end

-- -------------------------------------------------------------
function serviceServ.echo(name)
  analiz:execute("echo serviceServ.echo:"..name)
end

function serviceServ.init()
  analiz:execute("SET_COMMAND_LOG_LEVEL SET_COMMAND_LOG_LEVEL 5")
  analiz:execute("SET_COMMAND_LOG_LEVEL SERVICE_CONFIG_SETTING_USER_PWD 5")
  analiz:execute("SET_COMMAND_LOG_LEVEL QUERYSERVICESTATUS 5")
  analiz:execute("SET_COMMAND_LOG_LEVEL GET_SERVICE_LIST_EX 5")
  analiz:execute("SET_COMMAND_LOG_LEVEL START_WIN32_SERVICE 5")
  analiz:execute("SET_COMMAND_LOG_LEVEL STOP_WIN32_SERVICE 5")
  analiz:execute("SET_COMMAND_LOG_LEVEL SERVICE_CONFIG_FAILURE_ACTIONS_V2 5")
end

serviceServ.init()
analiz:log("init serviceServ.end")