

function show_status(filter)

local services = serviceServ.getServiceList(filter)

  analiz:log("services.count: "..#services)
for i=1,#services,1 do
  local name = services[i].name
  local status = services[i]:status()
  analiz:log("service: "..name.." status:"..status)
end

end
-- --------------------------------------
function run(filter)
local services = serviceServ.getServiceList(filter)

  analiz:log("services.count: "..#services)
for i=1,#services,1 do
  local name = services[i].name
  services[i]:run(false)
  analiz:log("send start to "..name)
end

end
-- --------------------------------------
function SettingStartType(filter,t)
local services = serviceServ.getServiceList(filter)


  analiz:log("services.count: "..#services)
  for i=1,#services,1 do
    local name = services[i].name
    serviceServ.SettingStartType(name,t);
  analiz:log("setting restart options for service: "..name)
  end
end
-- --------------------------------------


function stop(filter)
local services = serviceServ.getServiceList(filter)

  analiz:log("services.count: "..#services)
for i=1,#services,1 do
  local name = services[i].name
  services[i]:stop(false)
  analiz:log("send stop to "..name)
end
end

-- --------------------------------------

function change_user_pwd(filter,usr,pwd)
local services = serviceServ.getServiceList(filter)


  analiz:log("services.count: "..#services)
for i=1,#services,1 do
  local name = services[i].name
  local ret =serviceServ.changeUserPwd(name,usr,pwd);
  analiz:log("service: "..name.." result:"..ret)
end
end
-- --------------------------------------

function SettingRestartOptions(filter,time)
local services = serviceServ.getServiceList(filter)


  analiz:log("services.count: "..#services)
for i=1,#services,1 do
  local name = services[i].name
  serviceServ.SettingRestartOptions(name,time);
  analiz:log("setting restart options for service: "..name)
end
end

-- --------------------------------------

function unistall_services(filter)
local services = serviceServ.getServiceList(filter)


  analiz:log("services.count: "..#services)
for i=1,#services,1 do
  local name = services[i].name
  serviceServ.uninstall_service(name,time);
  analiz:log("unistall: "..name)
end
end
-- --------------------------------------