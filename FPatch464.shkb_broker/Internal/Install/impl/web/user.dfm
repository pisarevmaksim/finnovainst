object Form1: TForm
  Left = 264
  Top = 111
  Width = 262
  Height = 177
  Caption = 'New web user'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 22
    Top = 16
    Width = 22
    Height = 13
    Caption = 'User'
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object Edit1: TEdit
    Left = 79
    Top = 12
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'new_user'
  end
  object Edit2: TEdit
    Left = 79
    Top = 44
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object OKBtn: TButton
    Left = 24
    Top = 96
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 2
  end
  object CancelBtn: TButton
    Left = 128
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 3
  end
end