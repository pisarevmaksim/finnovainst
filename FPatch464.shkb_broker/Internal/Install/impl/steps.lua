analiz:log("steps")
-- --------------------------------------------------------
function SettingSystemEnvironment(options)
   title("--SettingSystemEnvironment--")
  analiz:execute("echo step1:set TXMInterface")

  analiz:execute("calc_param tmp $(_ANALIZ_BIN)")
  analiz:execute("calc_param TXMInterface_dir "..options.txm_dir)
  analiz:execute("EXPAND_FILENAME TXMInterface_dir $(TXMInterface_dir)")
  analiz:execute("GET_SHORT_PATH TXMInterface_dir $(TXMInterface_dir)")

  analiz:execute("echo TXMInterface_dir $(TXMInterface_dir)")

  analiz:execute("execute setx.exe TXM_SYSTEM_DIR $(TXMInterface_dir) /M")

end

-- --------------------------------------------------------
  function add_to_pwd(name,cp)
    analiz:log("add to pwd: "..name)
    analiz:set("tmp1",cp.dsn)
    analiz:set("tmp2",cp.user)
    analiz:set("tmp3",cp.pwd)
    analiz:set("tmp4",cp.org)
    analiz:execute("ADD_TO_PWD "..name.." $(tmp1) $(tmp2) $(tmp3) $(tmp4)")
  end

-- --------------------------------------------------------
function SettingLocalPwd(options)
   title("--SettingLocalPwd--")

  analiz:execute("PWD $(system32)\\local.pwd")


  add_to_pwd("plib_aml",options.plib_aml)
  add_to_pwd("txm_db2",options.aml_db)



  analiz:execute("SAVE_PWD $(system32)\\local.pwd")

end

-- --------------------------------------------------------


-- --------------------------------------------------------
function SettingMongoDb(options)
   title("--SettingMongoDb--")
  local ctx = template_ctx("txm_mongodb")

  analiz:log("txm_s_root:"..ctx.txm_s_root)

  template_to_setting(ctx,ctx.config_path.."\\txm",
                     "mongodb.config.txm.template",
                     "mongodb.config.txm")


  template_to_setting(ctx,ctx.service_path.."\\txm",
                     "install_service.template",
                     "install_service.cmd")

  
-- create temp dir
 analiz:execute("FORCEDIRECTORIES $(txm_s_root)\\Temp\\Logs\\txm_mongodb\\txm")

-- create db dir
 analiz:execute("FORCEDIRECTORIES $(txm_s_root)\\DBs\\txm_mongodb\\txm")


-- analiz:execute("abort 0")

end

-- --------------------------------------------------------
function SettingJavaProxy(options)
   title("--SettingJavaProxy--")
analiz:log("SetJavaProxy")

analiz:log("p1")
  analiz:set("tmp",options.java)
  analiz:execute("echo COPY_FILE $(tmp)\\java.exe $(tmp)\\java_proxy.exe")
  analiz:execute("COPY_FILE $(tmp)\\java.exe $(tmp)\\java_proxy.exe")
--  analiz:analiz("abort 0")

-- ----
analiz:log("p1.1")

  local ctx = template_ctx("txm_proxy")
 
analiz:log("p1.2")
-- ----
  local function setting_(from,to)
    template_to_setting(ctx,ctx.service_path,from,to)
  end

-- ----

-- ---- setting go.cmd
 analiz:log("z0")
  local go_template = LoadFile(ctx.service_path.."\\go.template")
 analiz:log("z1")
  local go_cmd = ReplaceValue(go_template,"[[java_path]]",options.java)
 analiz:log("z1.1")
        go_cmd = ReplaceValue(go_cmd,"[[TXM_SYSTEM_DIR]]",ctx.txm_s_root)
 analiz:log("z1.2")
        go_cmd = ReplaceValue(go_cmd,"[[TXM_SYSTEM_DIR_SS]]",ctx.txm_s_root_ss)
 analiz:log("z2")
  SaveFile(ctx.service_path.."\\go.cmd",go_cmd)
 analiz:log("z31")

-- ---- setting properties
  setting_("analiz.template","analiz.properties")

  setting_("profd_server_test4.template","profd_server_test4.properties")

  setting_("log4j.template","log4j.properties")



-- create temp dir
 analiz:execute("FORCEDIRECTORIES $(txm_s_root)\\Temp\\Logs\\txm_proxy")

-- analiz:execute("abort 0")

analiz:log("end")
end
-- --------------------------------------------------------
function install_nc_services(options)
  if options.service.run_install_cmd==false then
      title("--install_nc_services.skip--")
      return
  end
   title("--install_nc_services--")
 local function exe_(cmd)
 analiz:execute("echo execute "..cmd)
analiz:execute("execute "..cmd)
 end
    analiz:set("tmp",options.prospero_root.."\\Namechecking")
    analiz:execute("echo GET_SHORT_PATH tmp_cmd $(tmp)")
    analiz:execute("GET_SHORT_PATH tmp_cmd $(tmp)")
    local cmd = analiz:get("tmp_cmd").."\\install_all.cmd"
  exe_(cmd)
--  a("debug")
end

-- --------------------------------------------------------
function install_txm_services(options)
  if options.service.run_install_cmd==false then
      title("--install_txm_services.skip--")
      return
  end

   title("--install_txm_services--")
 analiz:log("install_txm_services...")
 analiz:execute("SET_CURRENT_DIR $(_ANALIZ_BIN)")
--   analiz:execute("SET_CURRENT_DIR "..options.txm_dir.."\\TXMServer\\bin")

 local function exe_(cmd)
 analiz:execute("echo execute "..cmd)
analiz:execute("execute "..cmd)
 end

 exe_("install.cmd")
 exe_("install_workers.cmd")
 exe_("install_batch_All.cmd")
 exe_("install_pf_tr_monitoring_all.cmd")
 exe_("install_namechecking.cmd")

 exe_("..\\..\\services\\txm_mongodb\\txm\\install_service.cmd")


-- analiz:execute("SET_CURRENT_DIR $(_ANALIZ_BIN)\\install_txm")
 analiz:execute("SET_CURRENT_DIR $(dir)\\..")
end
-- --------------------------------------------------------
function setting_restart_txm_services(options)
   title("--setting_restart_txm_services--")
  local mask = options.service.mask
  for i=1,#mask,1 do 
   SettingRestartOptions(mask[i],"0")
  end
--  SettingRestartOptions("txm_","0")
--  SettingRestartOptions("pf_tr_monitoring_","0")
--  SettingRestartOptions("namecheck_ListUpdate","0")

  SettingRestartOptions("txm_mongodb_v2","120")
  do 
    local o = options.service.login
    if o.enabled then
      for i=1,#mask,1 do 
         change_user_pwd(mask[i],o.user,o.pwd)
       end

--      change_user_pwd("txm_",o.user,o.pwd)
--      change_user_pwd("pf_tr_monitoring_",o.user,o.pwd)
--      change_user_pwd("namecheck_ListUpdate",o.user,o.pwd)
    end
  end

  SettingStartType("txm_mo","CUSTOM")
end
-- --------------------------------------------------------

function txm_start(options)
   title("--txm_start--")
      run("txm_")
      run("pf_tr_monitoring_")
      run("namecheck_ListUpdate")

end
-- --------------------------------------------------------
function txm_deinstall_services(options,type)
   title("--txm_deinstall_services:"..type.."--")
  if type=="namechecking" then
     unistall_services("Analiz","0")
     unistall_services("ProcessFinas","0")
     unistall_services("namecheck_","0")
  else
    unistall_services("txm_","0")
    unistall_services("pf_tr_monitoring_","0")
    unistall_services("namecheck_ListUpdate","0")
  end
end
-- --------------------------------------------------------
function setPlibPriv(pwd,user)
 local u = extractDomenAndUser(user)
 analiz:log("setPlibPriv:"..pwd.dsn)
 add_to_pwd("tmp",pwd)
  analiz:load_from_sql("tmp","C","tmp","select count(*) as C from [Accounts] where login='"..u.user.."'")
 local check = analiz:get("tmp")
 analiz:log("check:"..check)
 local sql = ""
 if check=="0" then
   sql = "INSERT INTO [dbo].[Accounts]([Login],[ClientID],[OrgAdmin],[SuperUser],[UserAdmin],[Publisher],[Reviser],[Releaser],[BackupOperator],[MaintenanceOperator],[User])"
   sql = sql.." VALUES('"..u.user.."',1,0,1,0,0,0,0,0,0,1)"
 else

   sql = "update [Accounts] set [user]=1,[SuperUser]=1  where login='"..u.user.."'"
 end
 analiz:set("tmp1",sql)
 analiz:execute("echo EXEC_SQL tmp $(tmp1)")
 analiz:execute("EXEC_SQL tmp $(tmp1)")

end
-- --------------------------------------------------------
function settingPlibPriv(options)
   title("--settingPlibPriv--")
 local is_txm =  isInstallTxm(options.install_type)

 if options.service.login.enabled then
   setPlibPriv(options.plib,options.service.login.user)
   if is_txm then 
     setPlibPriv(options.plib_aml,options.service.login.user)
   end
 end

 for i=1,#options.plib_users,1 do
  title("--settingPlibPriv.for user:"..options.plib_users[i])
   setPlibPriv(options.plib,options.plib_users[i])
   if is_txm then 
     setPlibPriv(options.plib_aml,options.plib_users[i])
   end
 end
   title("--settingPlibPriv.set amlweb orgclient")

  add_to_pwd("tmp",options.namecheck)
  analiz:set("tmp1","update Accounts set OrgClient='"..options.plib.org.."' where Login = 'sa'")
  analiz:execute("EXEC_SQL tmp $(tmp1)")
end
-- --------------------------------------------------------
function finasFix(options)
   title("--finasFix--")

   add_to_pwd("tmp",options.plib)

   analiz:set("tmp1", "update [Model] set IsShared = 1 where name = 'Std sanction list'")
  
   analiz:execute("echo EXEC_SQL tmp $(tmp1)")
   analiz:execute("EXEC_SQL tmp $(tmp1)")
end
-- --------------------------------------------------------
function FixOrgClient(options)
   title("--FixOrgClient--")
 local function fix_(pwd)
   add_to_pwd("tmp",pwd)

   analiz:set("tmp1", "update OrgClients set ClientName='"..pwd.org.."' where ClientID=1")

   analiz:execute("echo EXEC_SQL tmp $(tmp1)")
   analiz:execute("EXEC_SQL tmp $(tmp1)")

 end
-- -------------------------------
 local is_txm =  isInstallTxm(options.install_type)
 fix_(options.plib)
 if is_txm then 
   fix_(options.plib_aml)
 end
end
-- --------------------------------------------------------
function a(str)
  if not(str==null) then
   analiz:log(str)
  end
  analiz:execute("ABORT 0")
end

-- --------------------------------------------------------
function isInstallTxm(type)
  if type=="namechecking" then
   return false
  end
   return true
end
-- --------------------------------------------------------
function isServiceInstall(type)
   title("--isServiceInstall--")
  local check_service ="txm_"
  if not(isInstallTxm(type)) then
   check_service ="namecheck_single"
  end
  local services = serviceServ.getServiceList(check_service)
  local count = #services
  analiz:log("count for"..type..":"..count)
  return #services==0
end
-- --------------------------------------------------------

