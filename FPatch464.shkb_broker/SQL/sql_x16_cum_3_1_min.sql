--patch:sql_x16_cum_3.sql.start
--patch:sqlx16_1.sql.start
-- 
use plib_namecheck
GO


declare @bank_id int
declare @profile_name varchar(255)
declare @orgclient_name varchar(255)
declare @mros_report_mode varchar(255)
declare @mros_report_dsn varchar(255)
declare @mros_report_schema varchar(255)

set @bank_id=942
set @orgclient_name='DEFAULT_BANK'
set @profile_name='BANK_942'
set @mros_report_mode='mssql'
set @mros_report_dsn='FDMARTORA'
--set @mros_report_schema='FAQMRSDM' --'FAF_MROS_DM'
set @mros_report_schema='FAF' --'FAF_MROS_DM'


IF not EXISTS (SELECT * FROM OrgClients WHERE F_BANK_ID=@bank_id) BEGIN
  INSERT INTO [dbo].[OrgClients]
           ([ClientName]
           ,[ShareDefault]
           ,[Description]
           ,[F_BANK_ID], 
            ProfileName)
     VALUES
           (@orgclient_name
           ,1
           ,''
           ,@bank_id
           ,@profile_name
)
END

exec ADD_ORG_PARAM @profile_name,'SEND_TO','default_profile'
exec ADD_ORG_PARAM @profile_name,'MROS_REPORT_MODE',@mros_report_mode
exec ADD_ORG_PARAM @profile_name,'MROS_REPORT_PWD',@mros_report_dsn
exec ADD_ORG_PARAM @profile_name,'MROS_REPORT_SCHEMA',@mros_report_schema --ora_mros


GO

