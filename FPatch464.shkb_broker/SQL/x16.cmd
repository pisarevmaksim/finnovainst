echo off


echo sql_x16_cum_2_1.sql
call "%prospero_root%\Internal\Utils\sqlcmd_.cmd" sql_x16_cum_2_1.sql
echo %ERRORLEVEL%
if %ERRORLEVEL% NEQ 0 goto failed


echo sql_x16_cum_2_2.sql...
call "%prospero_root%\Internal\Utils\sqlcmd_.cmd" sql_x16_cum_2_2.sql
echo %ERRORLEVEL%
if %ERRORLEVEL% NEQ 0 goto failed

echo sql_x16_cum_3_1_min.sql ...
call "%prospero_root%\Internal\Utils\sqlcmd_.cmd" sql_x16_cum_3_1_min.sql
echo %ERRORLEVEL%
if %ERRORLEVEL% NEQ 0 goto failed



echo install ok
exit /B 0
:failed
echo failed

exit /B 1





