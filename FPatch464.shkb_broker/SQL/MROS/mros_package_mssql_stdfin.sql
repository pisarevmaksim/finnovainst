/*
 *++
 * --------------------------------------------------------------------
 * HEADER : Copyright (c) Finnova AG
 * PROJECT: FAF DATAMART MROS- FINNOVA ANALYTICAL FRAMEWORK
 * PACKAGE: SQL SERVER MROS
 *
 * $Workfile   mros_package_mssql_stdfin.sql
 *
 * Description:
 * This script contains the functions and procedures used for generating MROS reports
 *
 *
 * --------------------------------------------------------------------
 *
 * $Log: mros_package_mssql_stdfin.sql,v $
 * Revision 1.2  2020/06/19 19:05:24  ja
 * syncronizing changes with an oracle version
 *        the input sets hierarchy has been added into set_of_payments
 *        amount limitation has been added into historical report in payments (additionally to transactions)
 *        sitz_nr_bez is taken from accounts instead of payments now
 *        balance has been added
 *        reference tables have been added
 *        RP_ input variables have been added
 *        betrag_ and low_amount >= 0.01
 *        function faf_MROS_char_process has been added
 *        version with simplified script text
 *        function trans_node_from_payments has been added
 *        withdrawal/deposit have been added
 *        simple error checking has been added
 *        function node_deposit_withdrawal has been added
 *        function head_part_of_report has been added
 *        indicators checking has been added
 *        IDRsn cleaning has been added in Activity report
 *        We have added the case when a person deposits money into someone else's account 
 *        comments processing has been added
 *        edit_kt_nr instead of kt_lnr as "account/account" node
 *        edit_kd_nr instead of kd_lnr as "account/client_number" node
 *        business_closed has been altered
 *        transmode_comment has been added
 *        varchar(8000) in "reason" and "action"
 *        version without ID numbers
 *        Source_id has been removed
 *        @max_items has been added
 *        empty set of accounts is filled from set of clients now (activity report)
 *        hierarchy in historical report between accounts and clients has been added
 *        saldo and beneficiary have been added in activity too
 *        obj_stat_cd in (120, 130, 140, 800, 900) now
 *        char_processing works differently for variables from Nikita
 *        tables and attributes names have been changed according to Finnova policy
 *        address_type depends on kd_htyp_cd now
 *        DF_ variables have been added into set_of_payments
 *        09.07.2020 default values for tax_reg_number,incorporation_country_code,addresses,currency_code,iban in activity report too (see new buisiness rules)
 *        15.07.2020 changed char_processing for input & output comments and df_comments
 *        17.07.2020 we take kt_id instead of bic when len(bic)<=3
 *        20.08.2020 added bk_ attributes (for input and outgoing transactions)
 *        26.08.2020 added improvement for the case of the kt_lnr/kd_lnr absence
 *        26.08.2020 dbo --> FAF, staging_namecheck --> FAF_MBDATA_T
 *        28.08.2020 call signatory from KD_KD when entity owns account, call counterparties from KD_KD in activity (with depth)
 *        09.09.2020 we don't use trak_ref_typ limitation for withdrawal/deposit
 *        29.09.2020 problem with duplicating edit_kt_nr is fixed in activity report
 *        22.10.2020 improvement in text_rec parsing
 *        22.10.2020 call signatory from VT-link when entity owns account  
 *        23.10.2020 call signatory from VT-link in activity 
 *        23.10.2020 depth of links in activity is 1 and 2 for doppelperson
 *        23.10.2020 special_mark instead of comments when it exists
 *        23.10.2020 text_rec in transaction_description
 *        23.10.2020 edit_kd_nr + partner_flag in comments for clients in activity
 *        23.10.2020 Actual country in comments for all clients
 *        26.10.2020 grouping of signature in account
 *        28.10.2020 hardcoded searched string (for type 90) in text_rec parsing
 *        20.11.2020 new comment for cancelations
 *        22.11.2020 some hints were deleted
 *
 * Revision 1.1  2020/06/12 09:12:34  ja
 * first Version of the package based on Finnova standard naming conventions
 *
 *
 * --------------------------------------------------------------------
 */


use FAF_MBDATA_T;
go

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
go

--we have to drop functions to drop types
IF OBJECT_ID('[FAF].[report_activity]', 'FN') IS NOT NULL
	DROP function [FAF].[report_activity]; 
go
IF OBJECT_ID('[FAF].[report_client_trans]', 'FN') IS NOT NULL
	DROP function [FAF].[report_client_trans]; 
go
IF OBJECT_ID('[FAF].[historical_report]', 'FN') IS NOT NULL
	DROP function [FAF].[historical_report]; 
go
IF OBJECT_ID('[FAF].[booking_report]', 'FN') IS NOT NULL
	DROP function [FAF].[booking_report]; 
go
IF OBJECT_ID('[FAF].[set_of_payments]', 'FN') IS NOT NULL
	DROP function [FAF].[set_of_payments]; 
go
IF OBJECT_ID('[FAF].[one_in_payment]', 'FN') IS NOT NULL
	DROP function [FAF].[one_in_payment]; 
go
IF OBJECT_ID('[FAF].[one_out_payment]', 'FN') IS NOT NULL
	DROP function [FAF].[one_out_payment]; 
go
IF OBJECT_ID('[FAF].[one_payment]', 'FN') IS NOT NULL
	DROP function [FAF].[one_payment]; 
go
IF OBJECT_ID('[FAF].[trans_node_from_payments]', 'FN') IS NOT NULL
	  DROP function [FAF].[trans_node_from_payments]; 
go
IF OBJECT_ID('[FAF].[node_deposit_withdrawal]', 'FN') IS NOT NULL
	  DROP function [FAF].[node_deposit_withdrawal]; 
go
IF OBJECT_ID('[FAF].[head_part_of_report]', 'FN') IS NOT NULL
	  DROP function [FAF].[head_part_of_report]; 
go
IF EXISTS (SELECT 1 FROM sys.types WHERE is_table_type = 1 AND name ='FAF_MROS_IdRsn_t') 
	DROP TYPE FAF.FAF_MROS_IdRsn_t;
go
IF EXISTS (SELECT 1 FROM sys.types WHERE is_table_type = 1 AND name ='FAF_MROS_charID_t') 
	DROP TYPE FAF.FAF_MROS_charID_t;
go
IF EXISTS (SELECT 1 FROM sys.types WHERE is_table_type = 1 AND name ='FAF_MROS_numID_t') 
	DROP TYPE FAF.FAF_MROS_numID_t;
go
IF EXISTS (SELECT 1 FROM sys.types WHERE is_table_type = 1 AND name ='FAF_MROS_techn_t') 
	DROP TYPE FAF.FAF_MROS_techn_t;
go
IF EXISTS (SELECT 1 FROM sys.types WHERE is_table_type = 1 AND name ='FAF_MROS_techn1_t') 
	DROP TYPE FAF.FAF_MROS_techn1_t;
go

create TYPE FAF.FAF_MROS_IdRsn_t as table (chID varchar(50),partner_flag decimal(1,0),rsn varchar(4000));
create TYPE FAF.FAF_MROS_charID_t as table (chID varchar(50));
create TYPE FAF.FAF_MROS_numID_t as table (nID decimal(24,0));
create TYPE FAF.FAF_MROS_techn_t as table (ID decimal(24,0),significance int,comments varchar(4000),lvl int);
create TYPE FAF.FAF_MROS_techn1_t as table (ID0 decimal(24,0),ID1 decimal(24,0));
go


----------------------------------------------------------------------------------------------------------
--technical function to process varchar variables
IF OBJECT_ID('[FAF].[faf_MROS_char_process]', 'FN') IS NOT NULL
	  DROP function [FAF].[faf_MROS_char_process]; 
go
create function FAF.faf_MROS_char_process (@ch varchar(max),@mv varchar(max),@n int)
returns varchar(max)
as
begin
	declare @s varchar(max);
	set @s = replace(rtrim(ltrim(replace(replace(replace(@ch,char(10),' '),char(9),' '),char(13),' '))),'  ',' ');
	if @s is null or @s='' 
		set @s = @mv;
	if @s is not null and @n>0
		set @s = left(@s,@n);

	return(@s)
end;
go

--version  of technical function without replacement
IF OBJECT_ID('[FAF].[faf_MROS_char_process_]', 'FN') IS NOT NULL
	  DROP function [FAF].[faf_MROS_char_process_]; 
go
create function FAF.faf_MROS_char_process_ (@ch varchar(max),@mv varchar(max),@n int)
returns varchar(max)								 
as
begin
	declare @s varchar(max);					   
	set @s = rtrim(ltrim(@ch));
	if @s is null or @s='' 
		set @s = @mv;
	if @s is not null and @n>0
		set @s = left(@s,@n);

	return(@s)
end;
go						 


---------------------------------------------------------------------------------------------------


--Transaction node from payments (technical function)
IF OBJECT_ID('[FAF].[trans_node_from_payments]', 'FN') IS NOT NULL
	  DROP function [FAF].[trans_node_from_payments]; 
go
create function FAF.trans_node_from_payments (
	@F_BANK_ID int,
	@SET_OF_PAYMENT_ID FAF.FAF_MROS_numID_t READONLY,
	@SET_OF_ORDER_NR FAF.FAF_MROS_charID_t READONLY)
returns xml
as
begin
	declare @xml_t xml;
	declare @inst_name varchar(255);
	declare @swif varchar(11);
	declare @max_items int;
	set @max_items = 1000;

	--Bank name and swift code from the technical table
	set @inst_name = (select top 1 FAF.faf_MROS_char_process(institution_name,'n/a',255) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);
	set @swif = (select top 1 FAF.faf_MROS_char_process(swift,'n/a',11) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);
	
	--collect payments from set of orders if it's necessary (we have the hierarchy now)
	declare @SET_OF_PAYMENT_ID1 FAF.FAF_MROS_numID_t;
	insert into @SET_OF_PAYMENT_ID1 
		select top (@max_items) zv_zlg_sys_lnr as nID 
		from 
			FAF_MBDATA_T.FAF.FA_MROS_ZV_ZLG_T s3
		--acceptable transaction types only
		join
			FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
		on s3.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=@F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
		--acceptable trak_ref types only
		join
			FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ref_tp
		on s3.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=@F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1				
		where s3.userbk_nr=@F_BANK_ID and s3.zv_zlg_sys_lnr in (select nid from @SET_OF_PAYMENT_ID)
		order by AUF_DT_ERT desc;
	if (select count(*) from @SET_OF_PAYMENT_ID1)=0 
		insert into @SET_OF_PAYMENT_ID1 
			select top (@max_items) zv_zlg_sys_lnr as nID 
			from 
				FAF_MBDATA_T.FAF.FA_MROS_ZV_ZLG_T s3
			--acceptable transaction types only
			join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
			on s3.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=@F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
			--acceptable trak_ref types only
			join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ref_tp
			on s3.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=@F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1				
			where s3.userbk_nr=@F_BANK_ID and s3.auf_id in (select chid from @SET_OF_ORDER_NR)
			order by AUF_DT_ERT desc;

	--The main request
	if (select count(*) from @SET_OF_PAYMENT_ID1)>0
	begin		
		set @xml_t = 
			(select 
				s3.zv_zlg_sys_lnr as "transactionnumber",
				FAF.faf_MROS_char_process(s3.auf_id,NULL,50) as "internal_ref_number",
				case 
					when s3.SITZ_NR_AUSF is not null
					then 'Branch '+ cast(s3.SITZ_NR_AUSF as varchar) + ' (' + FAF.faf_MROS_char_process(s3.sitz_nr_bez,'',235) + ')'
					else
						case tr_tp.MROS_value
							when 1 then 'n/a'
							when 2 then 'n/a'
							else null 
						end
				end as "transaction_location",
				FAF.faf_MROS_char_process_(
					case 
						when FAF.faf_MROS_char_process(s3.gf_art_bez,NULL,0) is not null
						then 'Transaction type: '+FAF.faf_MROS_char_process(s3.gf_art_bez,'',4000)
						else ''
					end +
					case 
						when FAF.faf_MROS_char_process(s3.trak_ref_typ_bez,NULL,0) is not null
						then 
							case 
								when FAF.faf_MROS_char_process(s3.gf_art_bez,NULL,0) is not null
								then '; ' + char(13) + char(10)
								else ''
							end + 'Business type: '+FAF.faf_MROS_char_process(s3.trak_ref_typ_bez,'',4000)
						else ''
					end +
					case 
						when FAF.faf_MROS_char_process(s3.gf_art_bez,NULL,0) is not null or FAF.faf_MROS_char_process(s3.trak_ref_typ_bez,NULL,0) is not null
						then '; ' + char(13) + char(10)
						else ''
					end + FAF.faf_MROS_char_process_(s20.text_rec,'',4000)
				,'n/a',4000) as "transaction_description",
				left(
					case 
						when 
							s3.ausf_dt_ist is not null and (s3.ausf_dt_soll is null or s3.ausf_dt_ist<=s3.ausf_dt_soll) and (s3.AUF_DT_ERT is null or s3.ausf_dt_ist<=s3.AUF_DT_ERT) 
						then convert(char,s3.ausf_dt_ist,126)
						when 
							s3.ausf_dt_soll is not null and (s3.ausf_dt_ist is null or s3.ausf_dt_soll<=s3.ausf_dt_ist) and (s3.AUF_DT_ERT is null or s3.ausf_dt_soll<=s3.AUF_DT_ERT) 
						then convert(char,s3.ausf_dt_soll,126)
						when 
							s3.AUF_DT_ERT is not null and (s3.ausf_dt_soll is null or s3.AUF_DT_ERT<=s3.ausf_dt_soll) and (s3.ausf_dt_ist is null or s3.AUF_DT_ERT<=s3.ausf_dt_ist) 
						then convert(char,s3.AUF_DT_ERT,126)
						else convert(char,SYSDATETIME(),126)	
					end
				,19) as "date_transaction",
				left(convert(char,s3.valuta,126),19) as "value_date",
				isnull(tr_tp.MROS_value,15) as "transmode_code", 
				case 
					when tr_tp.MROS_value=13 
					then FAF.faf_MROS_char_process(tr_tp.comments,'n/a',50)
					else NULL
				end as "transmode_comment",
				case 
					when tr_tp.MROS_value=16 then 0 
					else 
						case
							when abs(s3.BETRAG_BWRG)<0.01 then 0.01
							else abs(isnull(s3.BETRAG_BWRG,0.01))
						end 
				end as "amount_local",
				
				--OM_AUF.KT_LNR as sender

				case 
					when s3.ZV_EING_AUSG_CD='A' and (s3.ZV_ADR_EFF_LNR is NOT NULL or s3.sh_cd=2) or s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NULL and s3.sh_cd=2 
					then 
						(select
							1 as "from_funds_code",
							isnull(cur.iso_wrg_cd,'CHF') as "from_foreign_currency/foreign_currency_code",
							abs(isnull(s3.BETRAG_ZWRG,isnull(s3.BETRAG_BWRG,0.01))) as "from_foreign_currency/foreign_amount",
							1 as "from_foreign_currency/foreign_exchange_rate",
							--t_account_my_client
							isnull(
								(select 
									@inst_name as institution_name,
									@swif as swift,
									isnull(stz.MROS_value,'ZH') as branch,
									FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as account,
									isnull(wrg.iso_wrg_cd,'CHF') as currency_code,
									FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as account_name,
									FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as iban,
									FAF.faf_MROS_char_process(cl.edit_kd_nr,'n/a',30) as client_number,
									isnull(ac_tp.MROS_value,16) as personal_account_type,
									--account owner
									isnull(
										(select
											--entity
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "t_entity/name",
											case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "t_entity/incorporation_legal_form",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "t_entity/incorporation_number",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "t_entity/business",
											case when se.kd_htyp_cd=3 then 
												isnull(sea.addresses,
													(select
														4 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "t_entity/addresses",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "t_entity/url",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "t_entity/incorporation_state",
											case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "t_entity/incorporation_country_code",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/incorporation_date",
											case when se.kd_htyp_cd=3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1 else NULL
												end 
												else NULL 
											end as "t_entity/business_closed",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/date_business_closed",
											case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "t_entity/tax_reg_number",
											case 
												when se.kd_htyp_cd=3 
												then 
													FAF.faf_MROS_char_process_(
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else ''
														end + 
														case 
															when lf.MROS_value=4 
															then 
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then '; ' + char(13) + char(10)
																	else ''
																end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
															else ''
														end
													,NULL,4000) 
												else NULL 
											end as "t_entity/comments",
											--person
											case when se.kd_htyp_cd<3 then 
												case when se.PARTNER_FLAG=0 then 1 else Null end
												else NULL 
											end as "signatory/is_primary",
											case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "signatory/t_person/gender",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "signatory/t_person/title",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "signatory/t_person/first_name",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "signatory/t_person/last_name",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then '1900-01-01T01:00:00' 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end
												else NULL 
											end as "signatory/t_person/birthdate",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "signatory/t_person/birth_place",
											case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "signatory/t_person/nationality1",
											case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "signatory/t_person/nationality2",
											case when se.kd_htyp_cd<3 then spp.phones else NULL end as "signatory/t_person/phones",
											case when se.kd_htyp_cd<3 then 
												isnull(sea.addresses,
													(select
														3 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/addresses",
											--t_person_identification
											case when se.kd_htyp_cd<3 then 
												isnull(spi.identification, 
													(select
														4 as "type",
														'n/a' as "number",
														'UNK' as "issue_country",
														'n/a' as "comments"
													FOR XML PATH (''),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/identification",
											case when se.kd_htyp_cd<3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
														or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1
													else NULL
												end 
												else NULL 
											end as "signatory/t_person/deceased",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "signatory/t_person/date_deceased",
											case 
												when se.kd_htyp_cd<3 
												then 
													FAF.faf_MROS_char_process(														
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else NULL
														end
													,NULL,4000)
												else NULL 
											end as "signatory/t_person/comments",
											case when se.kd_htyp_cd<3 then 1 else NULL end as "signatory/role" 								
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
										left join 
											FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
										on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
										on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
										on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
										on cntr1.iso_land_cd=se.iso_land_code_nat
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
										on cntr2.iso_land_cd=se.iso_land_code_nat_2
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
										on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
										on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
										on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
										where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and (se.partner_flag=0 or se.kd_htyp_cd=2)
										FOR XML PATH (''),type, ELEMENTS)
										,(select 
											'n/a' as "t_entity/name",
											1 as "t_entity/incorporation_legal_form",
												4 as "t_entity/addresses/address/address_type",
												'n/a' as "t_entity/addresses/address/address",
												'n/a' as "t_entity/addresses/address/city",
												'UNK' as "t_entity/addresses/address/country_code",
											'UNK' as "t_entity/incorporation_country_code",
											'yes' as "t_entity/tax_reg_number"
										FOR XML PATH (''),type, ELEMENTS)
									),
									--t_person (connected with the entity in KD_KD)
									(select 
										isnull(gd.MROS_value,'U') as "t_person/gender",
										FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
										FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
										FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
										case 
											when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
											then 
												case 
													when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
													then '1900-01-01T01:00:00' 
													else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.GEBURT_DAT,126),19)
										end as "t_person/birthdate",
										FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
										isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
										cntr2.iso_land_cd as "t_person/nationality2",
										--t_phone
										spp.phones as "t_person/phones",
										--t_address
										isnull(spa.addresses,
											(select
												3 as "address_type",
												'n/a' as "address",
												'n/a' as "city",
												'UNK' as "country_code"
											FOR XML PATH ('address'),type, ELEMENTS)
										) as "t_person/addresses",
										--t_person_identification
										isnull(spi.identification, 
											(select
												4 as "type",
												'n/a' as "number",
												'UNK' as "issue_country",
												'n/a' as "comments"
											FOR XML PATH (''),type, ELEMENTS)
										) as "t_person/identification",
										case 
											when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
												or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
											then 1
											else NULL
										end as "t_person/deceased",
										case 
											when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
											then 
												case 
													when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
													then NULL 
													else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.TOD_DAT,126),19)
										end as "t_person/date_deceased",
										FAF.faf_MROS_char_process_(
											case
												when isnull(kdkd.significance,6)=6
												then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
												else ''
											end +
											case
												when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
												then 
													case
														when isnull(kdkd.significance,6)=6
														then '; ' + char(13) + char(10)
														else ''
													end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
												else ''
											end
										,NULL,4000) as "t_person/comments",
										isnull(kdkd.significance,6) as "role" 
									from 
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
									join
										(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
											case significance
												when 101 then 1
												when 107 then 7
												when 113 then 13
												when 114 then 14
												when 115 then 15
												when 117 then 17
												when 118 then 18
												when 119 then 19
												when 120 then 20
												when 121 then 21
												when 122 then 22
												when 123 then 23
												when 124 then 24
												when 125 then 25
												when 12882 then 19
												when 12198 then 20
												when 13110 then 21
												when 12995 then 22
												when 12305 then 23
												when 1481430 then 24
												when 13685 then 24
												when 13673 then 24
												when 13908 then 24
												when 1402770 then 25
												when 13800 then 25
												when 14022 then 25
												when 12947 then 25
												else 6
											end as significance
										from
											(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
												round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
												STUFF(
													(SELECT ', ' + t2.comments 
													FROM 
														(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
															case 
																when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
																then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
																else 6 
															end as significance
														from
															(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
																case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
															on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
															where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
															union all
															select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
																case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
															on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
															where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
															union all
															select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
																case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
															on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
															on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
															union all
															select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
																case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
														   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
															on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
														) t2 
													WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
													FOR XML PATH(''))
												, 1, LEN(', '), '') as comments
											from 
												(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
													case 
														when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
														then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
														else 6 
													end as significance
												from
													(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
														case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
													on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
													where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
													union all
													select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
														case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
													on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
													where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
													union all
													select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
														case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
													on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
													on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
													union all
													select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
														case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
												   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
													on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
												) t0
											group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
										) kdkd
									on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
									join
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
									on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
									on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
									on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
									on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
									on cntr1.iso_land_cd=sp.iso_land_code_nat
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
									on cntr2.iso_land_cd=sp.iso_land_code_nat_2
									left join
										FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
									on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
									where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0 and se.kd_htyp_cd=3
									FOR XML PATH ('signatory'),type, ELEMENTS),									
									case 
										when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
										then '1900-01-01T01:00:00'
										else left(convert(char,s5.EROEFF_DAT,126),19) 
									end as opened,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then null 
										else left(convert(char,s5.AUFHEB_DAT,126),19)
									end as closed,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then isnull(sld.saldo_bwrg,0) 
										else NULL
									end as balance,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then left(convert(char,SYSDATETIME(),126),19)
										else NULL
									end as date_balance,
									case 
										when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
										then 2
										else 1
									end as status_code,
									case 
										when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
										then NULL
										else isnull(sld.saldo_kwrg,0)
									end as beneficiary,
									FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as beneficiary_comment,
									FAF.faf_MROS_char_process(
										case 
											when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
											then
												'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
													else ''
												end
											else
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
													else 'n/a'
												end
										end
									,'n/a',4000) as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM s5 WITH (FORCESEEK)
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM cl WITH (FORCESEEK)
								on s5.kd_lnr=cl.kd_lnr and s5.userbk_nr=cl.userbk_nr and cl.PARTNER_FLAG=0
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KT_SALDO sld WITH (FORCESEEK)
								on s5.kt_lnr=sld.kt_lnr and s5.userbk_nr=sld.userbk_nr
								left join
									FAF_MBDATA_T.FAF.FA_MROS_INT_WRG wrg
								on s5.iso_wrg_cd=wrg.iso_wrg_cd
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE stz
								on s5.userbk_nr=stz.userbk_nr and s5.sitz_nr=stz.finnova_value and stz.ref_type=1 and stz.activ_MROS=1
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ac_tp
								on s5.userbk_nr=ac_tp.userbk_nr and s5.kt_art_ber_cd=ac_tp.finnova_value and ac_tp.ref_type=9 and ac_tp.activ_MROS=1
								where s5.kt_lnr=s3.kt_lnr and s5.userbk_nr=s3.userbk_nr
								FOR XML PATH (''),type, ELEMENTS)
								,(select 
									'n/a' as "institution_name",
									'n/a' as "swift",
									'ZH' as "branch",
									'n/a' as "account",
									'CHF' as "currency_code",
									'n/a' as "iban",
									'n/a' as "client_number",
									16 as "personal_account_type",
										'n/a' as "t_entity/name",
										1 as "t_entity/incorporation_legal_form",
											4 as "t_entity/addresses/address/address_type",
											'n/a' as "t_entity/addresses/address/address",
											'n/a' as "t_entity/addresses/address/city",
											'UNK' as "t_entity/addresses/address/country_code",
										'UNK' as "t_entity/incorporation_country_code",
										'yes' as "t_entity/tax_reg_number",
									'1900-01-01T00:00:00' as "opened",
									0 as "balance",
									left(convert(char,sysdatetime(),126),19) as "date_balance",
									4 as "status_code",
									'n/a' as "beneficiary_comment"
								FOR XML PATH (''),type, ELEMENTS)
							) as "from_account"
						FOR XML PATH (''),type, ELEMENTS)
					else NULL 
				end as "t_from_my_client",
				
				--ZV_ZLG_T.KT_LNR_B as sender
				
				case 
					when (s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NULL or s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NULL) and s3.sh_cd=1 
					then 
						(select 
							1 as "from_funds_code",
							isnull(cur.iso_wrg_cd,'CHF') as "from_foreign_currency/foreign_currency_code",
							abs(isnull(s3.BETRAG_ZWRG,isnull(s3.BETRAG_BWRG,0.01))) as "from_foreign_currency/foreign_amount",
							1 as "from_foreign_currency/foreign_exchange_rate",
							--t_account_my_client
							isnull(
								(select 
									@inst_name as institution_name,
									@swif as swift,
									isnull(stz.MROS_value,'ZH') as branch,
									FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as account,
									isnull(wrg.iso_wrg_cd,'CHF') as currency_code,
									FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as account_name,
									FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as iban,
									FAF.faf_MROS_char_process(cl.edit_kd_nr,'n/a',30) as client_number,
									isnull(ac_tp.MROS_value,16) as personal_account_type,
									--account owner
									isnull(
										(select
											--entity
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "t_entity/name",
											case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "t_entity/incorporation_legal_form",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "t_entity/incorporation_number",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "t_entity/business",
											case when se.kd_htyp_cd=3 then 
												isnull(sea.addresses,
													(select
														4 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "t_entity/addresses",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "t_entity/url",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "t_entity/incorporation_state",
											case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "t_entity/incorporation_country_code",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/incorporation_date",
											case when se.kd_htyp_cd=3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1 else NULL
												end 
												else NULL 
											end as "t_entity/business_closed",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/date_business_closed",
											case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "t_entity/tax_reg_number",
											case 
												when se.kd_htyp_cd=3 
												then 
													FAF.faf_MROS_char_process_(
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else ''
														end + 
														case 
															when lf.MROS_value=4 
															then 
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then '; ' + char(13) + char(10)
																	else ''
																end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
															else ''
														end
													,NULL,4000) 
												else NULL 
											end as "t_entity/comments",
											--person
											case when se.kd_htyp_cd<3 then 
												case when se.PARTNER_FLAG=0 then 1 else Null end
												else NULL 
											end as "signatory/is_primary",
											case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "signatory/t_person/gender",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "signatory/t_person/title",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "signatory/t_person/first_name",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "signatory/t_person/last_name",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then '1900-01-01T01:00:00' 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end
												else NULL 
											end as "signatory/t_person/birthdate",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "signatory/t_person/birth_place",
											case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "signatory/t_person/nationality1",
											case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "signatory/t_person/nationality2",
											case when se.kd_htyp_cd<3 then spp.phones else NULL end as "signatory/t_person/phones",
											case when se.kd_htyp_cd<3 then 
												isnull(sea.addresses,
													(select
														3 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/addresses",
											--t_person_identification
											case when se.kd_htyp_cd<3 then 
												isnull(spi.identification, 
													(select
														4 as "type",
														'n/a' as "number",
														'UNK' as "issue_country",
														'n/a' as "comments"
													FOR XML PATH (''),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/identification",
											case when se.kd_htyp_cd<3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
														or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1
													else NULL
												end 
												else NULL 
											end as "signatory/t_person/deceased",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "signatory/t_person/date_deceased",
											case 
												when se.kd_htyp_cd<3 
												then 
													FAF.faf_MROS_char_process(														
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else NULL
														end
													,NULL,4000)
												else NULL 
											end as "signatory/t_person/comments",
											case when se.kd_htyp_cd<3 then 1 else NULL end as "signatory/role" 								
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
										left join 
											FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
										on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
										on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
										on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
										on cntr1.iso_land_cd=se.iso_land_code_nat
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
										on cntr2.iso_land_cd=se.iso_land_code_nat_2
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
										on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
										on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
										on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
										where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and (se.partner_flag=0 or se.kd_htyp_cd=2)
										FOR XML PATH (''),type, ELEMENTS)
										,(select 
											'n/a' as "t_entity/name",
											1 as "t_entity/incorporation_legal_form",
												4 as "t_entity/addresses/address/address_type",
												'n/a' as "t_entity/addresses/address/address",
												'n/a' as "t_entity/addresses/address/city",
												'UNK' as "t_entity/addresses/address/country_code",
											'UNK' as "t_entity/incorporation_country_code",
											'yes' as "t_entity/tax_reg_number"
										FOR XML PATH (''),type, ELEMENTS)
									),
									--t_person (connected with the entity in KD_KD)
									(select 
										isnull(gd.MROS_value,'U') as "t_person/gender",
										FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
										FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
										FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
										case 
											when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
											then 
												case 
													when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
													then '1900-01-01T01:00:00' 
													else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.GEBURT_DAT,126),19)
										end as "t_person/birthdate",
										FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
										isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
										cntr2.iso_land_cd as "t_person/nationality2",
										--t_phone
										spp.phones as "t_person/phones",
										--t_address
										isnull(spa.addresses,
											(select
												3 as "address_type",
												'n/a' as "address",
												'n/a' as "city",
												'UNK' as "country_code"
											FOR XML PATH ('address'),type, ELEMENTS)
										) as "t_person/addresses",
										--t_person_identification
										isnull(spi.identification, 
											(select
												4 as "type",
												'n/a' as "number",
												'UNK' as "issue_country",
												'n/a' as "comments"
											FOR XML PATH (''),type, ELEMENTS)
										) as "t_person/identification",
										case 
											when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
												or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
											then 1
											else NULL
										end as "t_person/deceased",
										case 
											when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
											then 
												case 
													when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
													then NULL 
													else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.TOD_DAT,126),19)
										end as "t_person/date_deceased",
										FAF.faf_MROS_char_process_(
											case
												when isnull(kdkd.significance,6)=6
												then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
												else ''
											end +
											case
												when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
												then 
													case
														when isnull(kdkd.significance,6)=6
														then '; ' + char(13) + char(10)
														else ''
													end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
												else ''
											end
										,NULL,4000) as "t_person/comments",
										isnull(kdkd.significance,6) as "role" 
									from 
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
									join
										(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
											case significance
												when 101 then 1
												when 107 then 7
												when 113 then 13
												when 114 then 14
												when 115 then 15
												when 117 then 17
												when 118 then 18
												when 119 then 19
												when 120 then 20
												when 121 then 21
												when 122 then 22
												when 123 then 23
												when 124 then 24
												when 125 then 25
												when 12882 then 19
												when 12198 then 20
												when 13110 then 21
												when 12995 then 22
												when 12305 then 23
												when 1481430 then 24
												when 13685 then 24
												when 13673 then 24
												when 13908 then 24
												when 1402770 then 25
												when 13800 then 25
												when 14022 then 25
												when 12947 then 25
												else 6
											end as significance
										from
											(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
												round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
												STUFF(
													(SELECT ', ' + t2.comments 
													FROM 
														(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
															case 
																when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
																then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
																else 6 
															end as significance
														from
															(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
																case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
															on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
															where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
															union all
															select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
																case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
															on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
															where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
															union all
															select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
																case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
															on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
															on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
															union all
															select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
																case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
														   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
															on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
														) t2 
													WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
													FOR XML PATH(''))
												, 1, LEN(', '), '') as comments
											from 
												(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
													case 
														when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
														then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
														else 6 
													end as significance
												from
													(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
														case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
													on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
													where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
													union all
													select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
														case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
													on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
													where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
													union all
													select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
														case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
													on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
													on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
													union all
													select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
														case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
												   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
													on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
												) t0
											group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
										) kdkd
									on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
									join
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
									on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
									on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
									on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
									on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
									on cntr1.iso_land_cd=sp.iso_land_code_nat
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
									on cntr2.iso_land_cd=sp.iso_land_code_nat_2
									left join
										FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
									on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
									where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0 and se.kd_htyp_cd=3
									FOR XML PATH ('signatory'),type, ELEMENTS),	
									case 
										when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
										then '1900-01-01T01:00:00'
										else left(convert(char,s5.EROEFF_DAT,126),19) 
									end as opened,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then null 
										else left(convert(char,s5.AUFHEB_DAT,126),19)
									end as closed,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then isnull(sld.saldo_bwrg,0) 
										else NULL
									end as balance,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then left(convert(char,SYSDATETIME(),126),19) 
										else NULL
									end as date_balance,
									case 
										when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
										then 2
										else 1
									end as status_code,
									case 
										when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
										then NULL
										else isnull(sld.saldo_kwrg,0)
									end as beneficiary,
									FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as beneficiary_comment,
									FAF.faf_MROS_char_process(
										case 
											when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
											then
												'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
													else ''
												end
											else
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
													else 'n/a'
												end
										end
									,'n/a',4000) as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM s5 WITH (FORCESEEK)
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM cl WITH (FORCESEEK)
								on s5.kd_lnr=cl.kd_lnr and s5.userbk_nr=cl.userbk_nr and cl.PARTNER_FLAG=0
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KT_SALDO sld WITH (FORCESEEK)
								on s5.kt_lnr=sld.kt_lnr and s5.userbk_nr=sld.userbk_nr
								left join
									FAF_MBDATA_T.FAF.FA_MROS_INT_WRG wrg
								on s5.iso_wrg_cd=wrg.iso_wrg_cd
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE stz
								on s5.userbk_nr=stz.userbk_nr and s5.sitz_nr=stz.finnova_value and stz.ref_type=1 and stz.activ_MROS=1
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ac_tp
								on s5.userbk_nr=ac_tp.userbk_nr and s5.kt_art_ber_cd=ac_tp.finnova_value and ac_tp.ref_type=9 and ac_tp.activ_MROS=1
								where s5.kt_lnr=s3.kt_lnr_b and s5.userbk_nr=s3.userbk_nr
								FOR XML PATH (''),type, ELEMENTS)
								,(select 
									'n/a' as "institution_name",
									'n/a' as "swift",
									'ZH' as "branch",
									'n/a' as "account",
									'CHF' as "currency_code",
									'n/a' as "iban",
									'n/a' as "client_number",
									16 as "personal_account_type",
										'n/a' as "t_entity/name",
										1 as "t_entity/incorporation_legal_form",
											4 as "t_entity/addresses/address/address_type",
											'n/a' as "t_entity/addresses/address/address",
											'n/a' as "t_entity/addresses/address/city",
											'UNK' as "t_entity/addresses/address/country_code",
										'UNK' as "t_entity/incorporation_country_code",
										'yes' as "t_entity/tax_reg_number",
									'1900-01-01T00:00:00' as "opened",
									0 as "balance",
									left(convert(char,sysdatetime(),126),19) as "date_balance",
									4 as "status_code",
									'n/a' as "beneficiary_comment"
								FOR XML PATH (''),type, ELEMENTS)
							) as "from_account"
						FOR XML PATH (''),type, ELEMENTS)
					else NULL 
				end as "t_from_my_client",
				
				--OM_TEXT.TEXT_REC as sender
				
				case 
					when s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NOT NULL 
					then 
						(select 
							1 as "from_funds_code",
							FAF.faf_MROS_char_process(isnull(s25.bk_name,case when patindex('%' + char(13) + '%',s25.adr_zeilen)>0 then substring(s25.adr_zeilen,1,patindex('%' + char(13) + '%',s25.adr_zeilen)-1) else s25.adr_zeilen end),'n/a',255) as "from_account/institution_name",
							FAF.faf_MROS_char_process(isnull(s25.bic_cp,
								case 
									when s25.bic is null or len(s25.bic)<=3
									then s25.kt_id
									else s25.bic
								end
							),'n/a',11) as "from_account/swift",
							FAF.faf_MROS_char_process(
								isnull(s25.KT_NR, isnull(s25.IBAN_ID, isnull(s25.QR_IBAN_ID,
									replace(
										case 
											when charindex('Kontonummer Zahler bei eingehenden MT-Meldungen',s20.text_rec)>0 
											then substring(substring(s20.text_rec,charindex('Kontonummer Zahler bei eingehenden MT-Meldungen',s20.text_rec)+47,len(s20.text_rec)),charindex(':',substring(s20.text_rec,charindex('Kontonummer Zahler bei eingehenden MT-Meldungen',s20.text_rec)+47,len(s20.text_rec)))+1,len(s20.text_rec))																			
											when charindex(char(10)+'6/',char(10)+s20.text_rec)>0 
											then substring(char(10)+s20.text_rec,charindex(char(10)+'6/',char(10)+s20.text_rec)+3,charindex(char(10),substring(char(10)+s20.text_rec,charindex(char(10)+'6/',char(10)+s20.text_rec)+3,len(s20.text_rec))+char(10))-1)
											else substring(s20.text_rec,charindex(': ',s20.text_rec)+2,charindex(char(10),substring(s20.text_rec,charindex(': ',s20.text_rec)+2,len(s20.text_rec))+char(10))-1)
										end
									,'/C/','')
								)))
							,'n/a',50) as "from_account/account",
							FAF.faf_MROS_char_process_(isnull(s25.COUNTERPARTY_DETAILS,s20.text_rec),'n/a',4000) as "from_account/comments"
						FOR XML PATH (''),type, ELEMENTS)
					else NULL 
				end as "t_from",
				
				--OM_AUF.KT_LNR as recipient

				case 
					when (s3.ZV_EING_AUSG_CD='E' and (s3.AUF_ZV_ADR_EFF_LNR is NOT NULL or s3.sh_cd=1)) or (s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NULL and s3.sh_cd=1) 
					then 
						(select 
							1 as "to_funds_code",
							isnull(cur.iso_wrg_cd,'CHF') as "to_foreign_currency/foreign_currency_code",
							abs(isnull(s3.BETRAG_ZWRG,isnull(s3.BETRAG_BWRG,0.01))) as "to_foreign_currency/foreign_amount",
							1 as "to_foreign_currency/foreign_exchange_rate",
							--t_account_my_client
							isnull(
								(select 
									@inst_name as institution_name,
									@swif as swift,
									isnull(stz.MROS_value,'ZH') as branch,
									FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as account,
									isnull(wrg.iso_wrg_cd,'CHF') as currency_code,
									FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as account_name,
									FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as iban,
									FAF.faf_MROS_char_process(cl.edit_kd_nr,'n/a',30) as client_number,
									isnull(ac_tp.MROS_value,16) as personal_account_type,
									--account owner
									isnull(
										(select
											--entity
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "t_entity/name",
											case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "t_entity/incorporation_legal_form",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "t_entity/incorporation_number",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "t_entity/business",
											case when se.kd_htyp_cd=3 then 
												isnull(sea.addresses,
													(select
														4 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "t_entity/addresses",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "t_entity/url",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "t_entity/incorporation_state",
											case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "t_entity/incorporation_country_code",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/incorporation_date",
											case when se.kd_htyp_cd=3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1 else NULL
												end 
												else NULL 
											end as "t_entity/business_closed",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/date_business_closed",
											case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "t_entity/tax_reg_number",
											case 
												when se.kd_htyp_cd=3 
												then 
													FAF.faf_MROS_char_process_(
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else ''
														end + 
														case 
															when lf.MROS_value=4 
															then 
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then '; ' + char(13) + char(10)
																	else ''
																end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
															else ''
														end
													,NULL,4000) 
												else NULL 
											end as "t_entity/comments",
											--person
											case when se.kd_htyp_cd<3 then 
												case when se.PARTNER_FLAG=0 then 1 else Null end
												else NULL 
											end as "signatory/is_primary",
											case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "signatory/t_person/gender",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "signatory/t_person/title",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "signatory/t_person/first_name",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "signatory/t_person/last_name",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then '1900-01-01T01:00:00' 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end
												else NULL 
											end as "signatory/t_person/birthdate",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "signatory/t_person/birth_place",
											case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "signatory/t_person/nationality1",
											case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "signatory/t_person/nationality2",
											case when se.kd_htyp_cd<3 then spp.phones else NULL end as "signatory/t_person/phones",
											case when se.kd_htyp_cd<3 then 
												isnull(sea.addresses,
													(select
														3 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/addresses",
											--t_person_identification
											case when se.kd_htyp_cd<3 then 
												isnull(spi.identification, 
													(select
														4 as "type",
														'n/a' as "number",
														'UNK' as "issue_country",
														'n/a' as "comments"
													FOR XML PATH (''),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/identification",
											case when se.kd_htyp_cd<3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
														or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1
													else NULL
												end 
												else NULL 
											end as "signatory/t_person/deceased",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "signatory/t_person/date_deceased",
											case 
												when se.kd_htyp_cd<3 
												then 
													FAF.faf_MROS_char_process(														
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else NULL
														end
													,NULL,4000)
												else NULL 
											end as "signatory/t_person/comments",
											case when se.kd_htyp_cd<3 then 1 else NULL end as "signatory/role" 								
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
										left join 
											FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
										on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
										on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
										on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
										on cntr1.iso_land_cd=se.iso_land_code_nat
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
										on cntr2.iso_land_cd=se.iso_land_code_nat_2
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
										on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
										on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
										on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
										where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and (se.partner_flag=0 or se.kd_htyp_cd=2)
										FOR XML PATH (''),type, ELEMENTS)
										,(select 
											'n/a' as "t_entity/name",
											1 as "t_entity/incorporation_legal_form",
												4 as "t_entity/addresses/address/address_type",
												'n/a' as "t_entity/addresses/address/address",
												'n/a' as "t_entity/addresses/address/city",
												'UNK' as "t_entity/addresses/address/country_code",
											'UNK' as "t_entity/incorporation_country_code",
											'yes' as "t_entity/tax_reg_number"
										FOR XML PATH (''),type, ELEMENTS)
									),
									--t_person (connected with the entity in KD_KD)
									(select 
										isnull(gd.MROS_value,'U') as "t_person/gender",
										FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
										FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
										FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
										case 
											when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
											then 
												case 
													when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
													then '1900-01-01T01:00:00' 
													else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.GEBURT_DAT,126),19)
										end as "t_person/birthdate",
										FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
										isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
										cntr2.iso_land_cd as "t_person/nationality2",
										--t_phone
										spp.phones as "t_person/phones",
										--t_address
										isnull(spa.addresses,
											(select
												3 as "address_type",
												'n/a' as "address",
												'n/a' as "city",
												'UNK' as "country_code"
											FOR XML PATH ('address'),type, ELEMENTS)
										) as "t_person/addresses",
										--t_person_identification
										isnull(spi.identification, 
											(select
												4 as "type",
												'n/a' as "number",
												'UNK' as "issue_country",
												'n/a' as "comments"
											FOR XML PATH (''),type, ELEMENTS)
										) as "t_person/identification",
										case 
											when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
												or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
											then 1
											else NULL
										end as "t_person/deceased",
										case 
											when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
											then 
												case 
													when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
													then NULL 
													else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.TOD_DAT,126),19)
										end as "t_person/date_deceased",
										FAF.faf_MROS_char_process_(
											case
												when isnull(kdkd.significance,6)=6
												then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
												else ''
											end +
											case
												when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
												then 
													case
														when isnull(kdkd.significance,6)=6
														then '; ' + char(13) + char(10)
														else ''
													end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
												else ''
											end
										,NULL,4000) as "t_person/comments",
										isnull(kdkd.significance,6) as "role" 
									from 
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
									join
										(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
											case significance
												when 101 then 1
												when 107 then 7
												when 113 then 13
												when 114 then 14
												when 115 then 15
												when 117 then 17
												when 118 then 18
												when 119 then 19
												when 120 then 20
												when 121 then 21
												when 122 then 22
												when 123 then 23
												when 124 then 24
												when 125 then 25
												when 12882 then 19
												when 12198 then 20
												when 13110 then 21
												when 12995 then 22
												when 12305 then 23
												when 1481430 then 24
												when 13685 then 24
												when 13673 then 24
												when 13908 then 24
												when 1402770 then 25
												when 13800 then 25
												when 14022 then 25
												when 12947 then 25
												else 6
											end as significance
										from
											(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
												round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
												STUFF(
													(SELECT ', ' + t2.comments 
													FROM 
														(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
															case 
																when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
																then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
																else 6 
															end as significance
														from
															(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
																case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
															on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
															where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
															union all
															select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
																case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
															on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
															where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
															union all
															select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
																case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
															on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
															on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
															union all
															select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
																case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
														   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
															on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
														) t2 
													WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
													FOR XML PATH(''))
												, 1, LEN(', '), '') as comments
											from 
												(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
													case 
														when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
														then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
														else 6 
													end as significance
												from
													(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
														case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
													on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
													where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
													union all
													select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
														case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
													on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
													where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
													union all
													select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
														case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
													on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
													on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
													union all
													select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
														case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
												   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
													on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
												) t0
											group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
										) kdkd
									on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
									join
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
									on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
									on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
									on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
									on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
									on cntr1.iso_land_cd=sp.iso_land_code_nat
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
									on cntr2.iso_land_cd=sp.iso_land_code_nat_2
									left join
										FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
									on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
									where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0 and se.kd_htyp_cd=3
									FOR XML PATH ('signatory'),type, ELEMENTS),	
									case 
										when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
										then '1900-01-01T01:00:00'
										else left(convert(char,s5.EROEFF_DAT,126),19) 
									end as opened,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then null 
										else left(convert(char,s5.AUFHEB_DAT,126),19)
									end as closed,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then isnull(sld.saldo_bwrg,0) 
										else NULL
									end as balance,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then left(convert(char,SYSDATETIME(),126),19)  
										else NULL
									end as date_balance,
									case 
										when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
										then 2
										else 1
									end as status_code,
									case 
										when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
										then NULL
										else isnull(sld.saldo_kwrg,0)
									end as beneficiary,
									FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as beneficiary_comment,
									FAF.faf_MROS_char_process(
										case 
											when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
											then
												'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
													else ''
												end
											else
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
													else 'n/a'
												end
										end
									,'n/a',4000) as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM s5 WITH (FORCESEEK)
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM cl WITH (FORCESEEK)
								on s5.kd_lnr=cl.kd_lnr and s5.userbk_nr=cl.userbk_nr and cl.PARTNER_FLAG=0
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KT_SALDO sld
								on s5.kt_lnr=sld.kt_lnr and s5.userbk_nr=sld.userbk_nr
								left join
									FAF_MBDATA_T.FAF.FA_MROS_INT_WRG wrg
								on s5.iso_wrg_cd=wrg.iso_wrg_cd
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE stz
								on s5.userbk_nr=stz.userbk_nr and s5.sitz_nr=stz.finnova_value and stz.ref_type=1 and stz.activ_MROS=1
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ac_tp
								on s5.userbk_nr=ac_tp.userbk_nr and s5.kt_art_ber_cd=ac_tp.finnova_value and ac_tp.ref_type=9 and ac_tp.activ_MROS=1
								where s5.kt_lnr=s3.kt_lnr and s5.userbk_nr=s3.userbk_nr
								FOR XML PATH (''),type, ELEMENTS)
								,(select 
									'n/a' as "institution_name",
									'n/a' as "swift",
									'ZH' as "branch",
									'n/a' as "account",
									'CHF' as "currency_code",
									'n/a' as "iban",
									'n/a' as "client_number",
									16 as "personal_account_type",
										'n/a' as "t_entity/name",
										1 as "t_entity/incorporation_legal_form",
											4 as "t_entity/addresses/address/address_type",
											'n/a' as "t_entity/addresses/address/address",
											'n/a' as "t_entity/addresses/address/city",
											'UNK' as "t_entity/addresses/address/country_code",
										'UNK' as "t_entity/incorporation_country_code",
										'yes' as "t_entity/tax_reg_number",
									'1900-01-01T00:00:00' as "opened",
									0 as "balance",
									left(convert(char,sysdatetime(),126),19) as "date_balance",
									4 as "status_code",
									'n/a' as "beneficiary_comment"
								FOR XML PATH (''),type, ELEMENTS)
							) as "to_account"
						FOR XML PATH (''),type, ELEMENTS)
					else NULL 
				end as "t_to_my_client",

				--ZV_ZLG_T.KT_LNR_B as recipient

				case 
					when (s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NULL or s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NULL) and s3.sh_cd=2 
					then 
						(select 
							1  as "to_funds_code",
							isnull(cur.iso_wrg_cd,'CHF') as "to_foreign_currency/foreign_currency_code",
							abs(isnull(s3.BETRAG_ZWRG,isnull(s3.BETRAG_BWRG,0.01))) as "to_foreign_currency/foreign_amount",
							1 as "to_foreign_currency/foreign_exchange_rate",
							--t_account_my_client
							isnull(
								(select 
									@inst_name as institution_name,
									@swif as swift,
									isnull(stz.MROS_value,'ZH') as branch,
									FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as account,
									isnull(wrg.iso_wrg_cd,'CHF') as currency_code,
									FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as account_name,
									FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as iban,
									FAF.faf_MROS_char_process(cl.edit_kd_nr,'n/a',30) as client_number,
									isnull(ac_tp.MROS_value,16) as personal_account_type,
									--account owner
									isnull(
										(select
											--entity
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "t_entity/name",
											case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "t_entity/incorporation_legal_form",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "t_entity/incorporation_number",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "t_entity/business",
											case when se.kd_htyp_cd=3 then 
												isnull(sea.addresses,
													(select
														4 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "t_entity/addresses",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "t_entity/url",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "t_entity/incorporation_state",
											case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "t_entity/incorporation_country_code",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/incorporation_date",
											case when se.kd_htyp_cd=3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1 else NULL
												end 
												else NULL 
											end as "t_entity/business_closed",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/date_business_closed",
											case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "t_entity/tax_reg_number",
											case 
												when se.kd_htyp_cd=3 
												then 
													FAF.faf_MROS_char_process_(
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else ''
														end + 
														case 
															when lf.MROS_value=4 
															then 
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then '; ' + char(13) + char(10)
																	else ''
																end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
															else ''
														end
													,NULL,4000) 
												else NULL 
											end as "t_entity/comments",
											--person
											case when se.kd_htyp_cd<3 then 
												case when se.PARTNER_FLAG=0 then 1 else Null end
												else NULL 
											end as "signatory/is_primary",
											case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "signatory/t_person/gender",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "signatory/t_person/title",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "signatory/t_person/first_name",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "signatory/t_person/last_name",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then '1900-01-01T01:00:00' 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end
												else NULL 
											end as "signatory/t_person/birthdate",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "signatory/t_person/birth_place",
											case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "signatory/t_person/nationality1",
											case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "signatory/t_person/nationality2",
											case when se.kd_htyp_cd<3 then spp.phones else NULL end as "signatory/t_person/phones",
											case when se.kd_htyp_cd<3 then 
												isnull(sea.addresses,
													(select
														3 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/addresses",
											--t_person_identification
											case when se.kd_htyp_cd<3 then 
												isnull(spi.identification, 
													(select
														4 as "type",
														'n/a' as "number",
														'UNK' as "issue_country",
														'n/a' as "comments"
													FOR XML PATH (''),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/identification",
											case when se.kd_htyp_cd<3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
														or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1
													else NULL
												end 
												else NULL 
											end as "signatory/t_person/deceased",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "signatory/t_person/date_deceased",
											case 
												when se.kd_htyp_cd<3 
												then 
													FAF.faf_MROS_char_process(														
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else NULL
														end
													,NULL,4000)
												else NULL 
											end as "signatory/t_person/comments",
											case when se.kd_htyp_cd<3 then 1 else NULL end as "signatory/role" 								
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
										left join 
											FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
										on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
										on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
										on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
										on cntr1.iso_land_cd=se.iso_land_code_nat
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
										on cntr2.iso_land_cd=se.iso_land_code_nat_2
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
										on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
										on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
										on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
										where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and (se.partner_flag=0 or se.kd_htyp_cd=2)
										FOR XML PATH (''),type, ELEMENTS)
										,(select 
											'n/a' as "t_entity/name",
											1 as "t_entity/incorporation_legal_form",
												4 as "t_entity/addresses/address/address_type",
												'n/a' as "t_entity/addresses/address/address",
												'n/a' as "t_entity/addresses/address/city",
												'UNK' as "t_entity/addresses/address/country_code",
											'UNK' as "t_entity/incorporation_country_code",
											'yes' as "t_entity/tax_reg_number"
										FOR XML PATH (''),type, ELEMENTS)
									),
									--t_person (connected with the entity in KD_KD)
									(select 
										isnull(gd.MROS_value,'U') as "t_person/gender",
										FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
										FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
										FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
										case 
											when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
											then 
												case 
													when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
													then '1900-01-01T01:00:00' 
													else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.GEBURT_DAT,126),19)
										end as "t_person/birthdate",
										FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
										isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
										cntr2.iso_land_cd as "t_person/nationality2",
										--t_phone
										spp.phones as "t_person/phones",
										--t_address
										isnull(spa.addresses,
											(select
												3 as "address_type",
												'n/a' as "address",
												'n/a' as "city",
												'UNK' as "country_code"
											FOR XML PATH ('address'),type, ELEMENTS)
										) as "t_person/addresses",
										--t_person_identification
										isnull(spi.identification, 
											(select
												4 as "type",
												'n/a' as "number",
												'UNK' as "issue_country",
												'n/a' as "comments"
											FOR XML PATH (''),type, ELEMENTS)
										) as "t_person/identification",
										case 
											when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
												or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
											then 1
											else NULL
										end as "t_person/deceased",
										case 
											when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
											then 
												case 
													when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
													then NULL 
													else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.TOD_DAT,126),19)
										end as "t_person/date_deceased",
										FAF.faf_MROS_char_process_(
											case
												when isnull(kdkd.significance,6)=6
												then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
												else ''
											end +
											case
												when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
												then 
													case
														when isnull(kdkd.significance,6)=6
														then '; ' + char(13) + char(10)
														else ''
													end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
												else ''
											end
										,NULL,4000) as "t_person/comments",
										isnull(kdkd.significance,6) as "role" 
									from 
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
									join
										(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
											case significance
												when 101 then 1
												when 107 then 7
												when 113 then 13
												when 114 then 14
												when 115 then 15
												when 117 then 17
												when 118 then 18
												when 119 then 19
												when 120 then 20
												when 121 then 21
												when 122 then 22
												when 123 then 23
												when 124 then 24
												when 125 then 25
												when 12882 then 19
												when 12198 then 20
												when 13110 then 21
												when 12995 then 22
												when 12305 then 23
												when 1481430 then 24
												when 13685 then 24
												when 13673 then 24
												when 13908 then 24
												when 1402770 then 25
												when 13800 then 25
												when 14022 then 25
												when 12947 then 25
												else 6
											end as significance
										from
											(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
												round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
												STUFF(
													(SELECT ', ' + t2.comments 
													FROM 
														(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
															case 
																when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
																then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
																else 6 
															end as significance
														from
															(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
																case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
															on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
															where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
															union all
															select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
																case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
															on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
															where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
															union all
															select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
																case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
															on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
															on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
															union all
															select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
																case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
														   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
															on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
														) t2 
													WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
													FOR XML PATH(''))
												, 1, LEN(', '), '') as comments
											from 
												(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
													case 
														when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
														then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
														else 6 
													end as significance
												from
													(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
														case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
													on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
													where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
													union all
													select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
														case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
													on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
													where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
													union all
													select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
														case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
													on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
													on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
													union all
													select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
														case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
												   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
													on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
												) t0
											group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
										) kdkd
									on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
									join
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
									on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
									on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
									on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
									on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
									on cntr1.iso_land_cd=sp.iso_land_code_nat
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
									on cntr2.iso_land_cd=sp.iso_land_code_nat_2
									left join
										FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
									on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
									where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0 and se.kd_htyp_cd=3
									FOR XML PATH ('signatory'),type, ELEMENTS),	
									case 
										when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
										then '1900-01-01T01:00:00'
										else left(convert(char,s5.EROEFF_DAT,126),19) 
									end as opened,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then null 
										else left(convert(char,s5.AUFHEB_DAT,126),19)
									end as closed,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then isnull(sld.saldo_bwrg,0) 
										else NULL
									end as balance,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then left(convert(char,SYSDATETIME(),126),19) 
										else NULL
									end as date_balance,
									case 
										when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
										then 2
										else 1
									end as status_code,
									case 
										when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
										then NULL
										else isnull(sld.saldo_kwrg,0)
									end as beneficiary,
									FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as beneficiary_comment,
									FAF.faf_MROS_char_process(
										case 
											when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
											then
												'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
													else ''
												end
											else
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
													else 'n/a'
												end
										end
									,'n/a',4000) as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM s5 WITH (FORCESEEK)
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM cl WITH (FORCESEEK)
								on s5.kd_lnr=cl.kd_lnr and s5.userbk_nr=cl.userbk_nr and cl.PARTNER_FLAG=0
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KT_SALDO sld WITH (FORCESEEK)
								on s5.kt_lnr=sld.kt_lnr and s5.userbk_nr=sld.userbk_nr
								left join
									FAF_MBDATA_T.FAF.FA_MROS_INT_WRG wrg
								on s5.iso_wrg_cd=wrg.iso_wrg_cd
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE stz
								on s5.userbk_nr=stz.userbk_nr and s5.sitz_nr=stz.finnova_value and stz.ref_type=1 and stz.activ_MROS=1
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ac_tp
								on s5.userbk_nr=ac_tp.userbk_nr and s5.kt_art_ber_cd=ac_tp.finnova_value and ac_tp.ref_type=9 and ac_tp.activ_MROS=1
								where s5.kt_lnr=s3.kt_lnr_b and s5.userbk_nr=s3.userbk_nr
								FOR XML PATH (''),type, ELEMENTS)
								,(select 
									'n/a' as "institution_name",
									'n/a' as "swift",
									'ZH' as "branch",
									'n/a' as "account",
									'CHF' as "currency_code",
									'n/a' as "iban",
									'n/a' as "client_number",
									16 as "personal_account_type",
										'n/a' as "t_entity/name",
										1 as "t_entity/incorporation_legal_form",
											4 as "t_entity/addresses/address/address_type",
											'n/a' as "t_entity/addresses/address/address",
											'n/a' as "t_entity/addresses/address/city",
											'UNK' as "t_entity/addresses/address/country_code",
										'UNK' as "t_entity/incorporation_country_code",
										'yes' as "t_entity/tax_reg_number",
									'1900-01-01T00:00:00' as "opened",
									0 as "balance",
									left(convert(char,sysdatetime(),126),19) as "date_balance",
									4 as "status_code",
									'n/a' as "beneficiary_comment"
								FOR XML PATH (''),type, ELEMENTS)
							) as "to_account"
						FOR XML PATH (''),type, ELEMENTS)
					else NULL 
				end as "t_to_my_client",

				--ZV_ADR_EFF.adr_zeilen as recipient
				
				case 
					when s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NOT NULL 
					then 
						(select 
							1 as "to_funds_code",
							FAF.faf_MROS_char_process(isnull(ss1.bk_name,
								case
									when ss1.EMPF_ART_CD=1 then case when patindex('%' + char(13) + '%',ss1.auf_adr_zeilen)>0 then substring(ss1.auf_adr_zeilen,1,patindex('%' + char(13) + '%',ss1.auf_adr_zeilen)-1) else ss1.auf_adr_zeilen end
									when ss1.EMPF_ART_CD=2 then case when patindex('%' + char(13) + '%',ss1.adr_zeilen)>0 then substring(ss1.adr_zeilen,1,patindex('%' + char(13) + '%',ss1.adr_zeilen)-1) else ss1.adr_zeilen end
									else 'n/a'
								end
							),'n/a',255)  as "to_account/institution_name",
							FAF.faf_MROS_char_process(isnull(ss1.bic_cp,
								case
									when ss1.EMPF_ART_CD=1 
									then 										
										isnull(
											case 
												when ss1.auf_bic is null or len(ss1.auf_bic)<=3
												then ss1.auf_kt_id
												else ss1.auf_bic
											end
										,ss1.bic)										
									when ss1.EMPF_ART_CD=2 
									then 
										case 
											when ss1.bic is null or len(ss1.bic)<=3
											then ss1.kt_id
											else ss1.bic
										end
									else 'n/a'
								end
							),'n/a',11) as "to_account/swift",
							FAF.faf_MROS_char_process(isnull(ss1.KT_NR, isnull(ss1.IBAN_ID, isnull(ss1.QR_IBAN_ID,ss1.ext_kt_nr))),'n/a',50) as "to_account/account",
							case when ss1.EMPF_ART_CD=1 then FAF.faf_MROS_char_process(substring(ss1.adr_zeilen,1,case when charindex(char(10),ss1.adr_zeilen)<=3 or charindex(char(10),ss1.adr_zeilen)>255 then 255 else charindex(char(10),ss1.adr_zeilen)-1 end),'n/a',255) else NULL end as "to_account/t_entity/name",
							FAF.faf_MROS_char_process_(isnull(ss1.COUNTERPARTY_DETAILS,ss1.adr_zeilen),'n/a',4000) as "to_account/comments"
						FOR XML PATH (''),type, ELEMENTS)
					else NULL 
				end as "t_to",
				case 
					when s3.BETRAG_BWRG<0
					then 'Cancellation / Refund'					 
					else NULL
				end as "comments"
			from 
				FAF_MBDATA_T.FAF.FA_MROS_ZV_ZLG_T s3 
			--counterparty's information from input messages
			left join				 
				FAF_MBDATA_T.FAF.FA_MROS_OM_TEXT s20 
			on s3.userbk_nr=s20.userbk_nr and s3.om_text_lnr=s20.om_text_lnr
			left join
				FAF_MBDATA_T.FAF.FA_MROS_ZV_ADR_EFF s25 
			on s3.userbk_nr=s25.userbk_nr and s3.auf_zv_adr_eff_lnr=s25.zv_adr_eff_lnr
			--counterparty's information from output messages
			left join
				FAF_MBDATA_T.FAF.FA_MROS_ZV_ADR_EFF ss1 
			on s3.userbk_nr=ss1.userbk_nr and s3.zv_adr_eff_lnr=ss1.zv_adr_eff_lnr
			left join
				FAF_MBDATA_T.FAF.FA_MROS_INT_WRG cur
			on s3.iso_wrg_cd=cur.iso_wrg_cd
			left join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
			on s3.userbk_nr=tr_tp.userbk_nr and s3.buchtyp_cd=tr_tp.finnova_value and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
			where s3.userbk_nr=@F_BANK_ID and s3.zv_zlg_sys_lnr in (select nID from @SET_OF_PAYMENT_ID1)
			FOR XML PATH ('transaction'),type, ELEMENTS);
	end;

	return(@xml_t);
end;
go	


---------------------------------------------------------------------------------------------------

--transaction nodes from deposit_withdrawal (technical function)
IF OBJECT_ID('[FAF].[node_deposit_withdrawal]', 'FN') IS NOT NULL
	  DROP function [FAF].[node_deposit_withdrawal]; 
go
create function FAF.node_deposit_withdrawal (
	@F_BANK_ID int,
	@SET_OF_TR_ID FAF.FAF_MROS_numID_t READONLY) 
returns xml
as
begin
	declare @xml_d xml;
	declare @inst_name varchar(255);
	declare @swif varchar(11);

	if (select count(*) from @SET_OF_TR_ID)>0
	begin
		--Bank name and swift code from the technical table
		set @inst_name = (select top 1 FAF.faf_MROS_char_process(institution_name,'n/a',255) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);
		set @swif = (select top 1 FAF.faf_MROS_char_process(swift,'n/a',11) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);

		--main request
		set @xml_d = 
			(select 
				s3.kt_buch_lnr as "transactionnumber",
				case 
					when s3.SITZ_NR is not null
					then 'Branch '+ cast(s3.SITZ_NR as varchar) + ' (' + FAF.faf_MROS_char_process(s3.sitz_nr_bez,'',235) + ')'
					else
						case tr_tp.MROS_value
							when 1 then 'n/a'
							when 2 then 'n/a'
							else null 
						end
				end as "transaction_location",
				FAF.faf_MROS_char_process_(
					case
						when FAF.faf_MROS_char_process(s3.trak_ref_typ_bez,NULL,0) is not null
						then 'Business type: ' + FAF.faf_MROS_char_process(s3.trak_ref_typ_bez,'',0)
						else ''
					end +
					case
						when FAF.faf_MROS_char_process(s3.text_ext,NULL,0) is not null 
						then 
							case
								when FAF.faf_MROS_char_process(s3.trak_ref_typ_bez,NULL,0) is not null
								then '; ' + char(13) + char(10)
								else ''
							end + 'Text: ' + FAF.faf_MROS_char_process_(s3.text_ext,'',0)
						else ''
					end +
					case
						when FAF.faf_MROS_char_process(s3.text_zus,NULL,0) is not null 
						then 
							case
								when FAF.faf_MROS_char_process(s3.trak_ref_typ_bez,NULL,0) is not null or FAF.faf_MROS_char_process(s3.text_ext,NULL,0) is not null
								then '; ' + char(13) + char(10)
								else ''
							end + 
							case
								when FAF.faf_MROS_char_process(s3.text_ext,NULL,0) is not null 
								then ''
								else 'Text: '
							end + FAF.faf_MROS_char_process_(s3.text_zus,'',0)
						else ''
					end
				,'n/a',4000) as "transaction_description",
				left(convert(char,s3.buch_dat_a,126),19) as "date_transaction",
				left(convert(char,s3.valuta,126),19) as "value_date",
				isnull(tr_tp.MROS_value,15) as "transmode_code", 
				case 
					when tr_tp.MROS_value=13 
					then FAF.faf_MROS_char_process(tr_tp.comments,'n/a',50)
					else NULL
				end as "transmode_comment",
				case 
					when tr_tp.MROS_value=16 then 0 
					else 
						case
							when abs(s3.betrag_bwrg)<0.01 then 0.01
							else abs(isnull(s3.betrag_bwrg,0.01)) 
						end
				end as "amount_local",	
				case 
					when FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,0) is null
					then
						case 
							when s3.withdrawal_flag=1
							then 
								(select
									1 as "from_funds_code",
									isnull(cur.iso_wrg_cd,'CHF') as "from_foreign_currency/foreign_currency_code",
									abs(isnull(s3.betrag_kwrg,isnull(s3.betrag_bwrg,0.01))) as "from_foreign_currency/foreign_amount",
									1 as "from_foreign_currency/foreign_exchange_rate",
									--t_account_my_client
									isnull(
										(select 
											@inst_name as institution_name,
											@swif as swift,
											isnull(stz.MROS_value,'ZH') as branch,
											FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as account,
											isnull(wrg.iso_wrg_cd,'CHF') as currency_code,
											FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as account_name,
											FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as iban,
											FAF.faf_MROS_char_process(cl.edit_kd_nr,'n/a',30) as client_number,
											isnull(ac_tp.MROS_value,16) as personal_account_type,
											--account owner
											isnull(
												(select
													--entity
													case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "t_entity/name",
													case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "t_entity/incorporation_legal_form",
													case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "t_entity/incorporation_number",
													case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "t_entity/business",
													case when se.kd_htyp_cd=3 then 
														isnull(sea.addresses,
															(select
																4 as "address_type",
																'n/a' as "address",
																'n/a' as "city",
																'UNK' as "country_code"
															FOR XML PATH ('address'),type, ELEMENTS)
														) 
														else NULL 
													end as "t_entity/addresses",
													case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "t_entity/url",
													case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "t_entity/incorporation_state",
													case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "t_entity/incorporation_country_code",
													case when se.kd_htyp_cd=3 then 
														case 
															when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
															then 
																case 
																	when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
																	then NULL 
																	else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
																end 
															else left(convert(char,se.GEBURT_DAT,126),19)
														end 
														else NULL 
													end as "t_entity/incorporation_date",
													case when se.kd_htyp_cd=3 then 
														case 
															when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
															then 1 else NULL
														end 
														else NULL 
													end as "t_entity/business_closed",
													case when se.kd_htyp_cd=3 then 
														case 
															when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
															then 
																case 
																	when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
																	then NULL 
																	else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
																end 
															else left(convert(char,se.TOD_DAT,126),19)
														end 
														else NULL 
													end as "t_entity/date_business_closed",
													case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "t_entity/tax_reg_number",
													case 
														when se.kd_htyp_cd=3 
														then 
															FAF.faf_MROS_char_process_(
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
																	else ''
																end + 
																case 
																	when lf.MROS_value=4 
																	then 
																		case
																			when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																			then '; ' + char(13) + char(10)
																			else ''
																		end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
																	else ''
																end
															,NULL,4000) 
														else NULL 
													end as "t_entity/comments",
													--person
													case when se.kd_htyp_cd<3 then 
														case when se.PARTNER_FLAG=0 then 1 else Null end
														else NULL 
													end as "signatory/is_primary",
													case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "signatory/t_person/gender",
													case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "signatory/t_person/title",
													case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "signatory/t_person/first_name",
													case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "signatory/t_person/last_name",
													case when se.kd_htyp_cd<3 then 
														case 
															when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
															then 
																case 
																	when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
																	then '1900-01-01T01:00:00' 
																	else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
																end 
															else left(convert(char,se.GEBURT_DAT,126),19)
														end
														else NULL 
													end as "signatory/t_person/birthdate",
													case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "signatory/t_person/birth_place",
													case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "signatory/t_person/nationality1",
													case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "signatory/t_person/nationality2",
													case when se.kd_htyp_cd<3 then spp.phones else NULL end as "signatory/t_person/phones",
													case when se.kd_htyp_cd<3 then 
														isnull(sea.addresses,
															(select
																3 as "address_type",
																'n/a' as "address",
																'n/a' as "city",
																'UNK' as "country_code"
															FOR XML PATH ('address'),type, ELEMENTS)
														) 
														else NULL 
													end as "signatory/t_person/addresses",
													--t_person_identification
													case when se.kd_htyp_cd<3 then 
														isnull(spi.identification, 
															(select
																4 as "type",
																'n/a' as "number",
																'UNK' as "issue_country",
																'n/a' as "comments"
															FOR XML PATH (''),type, ELEMENTS)
														) 
														else NULL 
													end as "signatory/t_person/identification",
													case when se.kd_htyp_cd<3 then 
														case 
															when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
																or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
															then 1
															else NULL
														end 
														else NULL 
													end as "signatory/t_person/deceased",
													case when se.kd_htyp_cd<3 then 
														case 
															when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
															then 
																case 
																	when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
																	then NULL 
																	else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
																end 
															else left(convert(char,se.TOD_DAT,126),19)
														end 
														else NULL 
													end as "signatory/t_person/date_deceased",
													case 
														when se.kd_htyp_cd<3 
														then 
															FAF.faf_MROS_char_process(														
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
																	else NULL
																end
															,NULL,4000)
														else NULL 
													end as "signatory/t_person/comments",
													case when se.kd_htyp_cd<3 then 1 else NULL end as "signatory/role" 								
												from 
													FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
												left join 
													FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
												on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
												left join
													FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
												on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
												left join
													FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
												on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
												left join
													FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
												on cntr1.iso_land_cd=se.iso_land_code_nat
												left join
													FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
												on cntr2.iso_land_cd=se.iso_land_code_nat_2
												left join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
												on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
												left join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
												on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
												left join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
												on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
												where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and (se.partner_flag=0 or se.kd_htyp_cd=2)
												FOR XML PATH (''),type, ELEMENTS)
												,(select 
													'n/a' as "t_entity/name",
													1 as "t_entity/incorporation_legal_form",
														4 as "t_entity/addresses/address/address_type",
														'n/a' as "t_entity/addresses/address/address",
														'n/a' as "t_entity/addresses/address/city",
														'UNK' as "t_entity/addresses/address/country_code",
													'UNK' as "t_entity/incorporation_country_code",
													'yes' as "t_entity/tax_reg_number"
												FOR XML PATH (''),type, ELEMENTS)
											),
											--t_person (connected with the entity in KD_KD)
											(select 
												isnull(gd.MROS_value,'U') as "t_person/gender",
												FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
												FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
												FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
												case 
													when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
													then 
														case 
															when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
															then '1900-01-01T01:00:00' 
															else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,sp.GEBURT_DAT,126),19)
												end as "t_person/birthdate",
												FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
												isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
												cntr2.iso_land_cd as "t_person/nationality2",
												--t_phone
												spp.phones as "t_person/phones",
												--t_address
												isnull(spa.addresses,
													(select
														3 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) as "t_person/addresses",
												--t_person_identification
												isnull(spi.identification, 
													(select
														4 as "type",
														'n/a' as "number",
														'UNK' as "issue_country",
														'n/a' as "comments"
													FOR XML PATH (''),type, ELEMENTS)
												) as "t_person/identification",
												case 
													when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
														or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
													then 1
													else NULL
												end as "t_person/deceased",
												case 
													when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
													then 
														case 
															when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,sp.TOD_DAT,126),19)
												end as "t_person/date_deceased",
												FAF.faf_MROS_char_process_(
													case
														when isnull(kdkd.significance,6)=6
														then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
														else ''
													end +
													case
														when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
														then 
															case
																when isnull(kdkd.significance,6)=6
																then '; ' + char(13) + char(10)
																else ''
															end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
														else ''
													end
												,NULL,4000) as "t_person/comments",
												isnull(kdkd.significance,6) as "role" 
											from 
												FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
											join
												(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
													case significance
														when 101 then 1
														when 107 then 7
														when 113 then 13
														when 114 then 14
														when 115 then 15
														when 117 then 17
														when 118 then 18
														when 119 then 19
														when 120 then 20
														when 121 then 21
														when 122 then 22
														when 123 then 23
														when 124 then 24
														when 125 then 25
														when 12882 then 19
														when 12198 then 20
														when 13110 then 21
														when 12995 then 22
														when 12305 then 23
														when 1481430 then 24
														when 13685 then 24
														when 13673 then 24
														when 13908 then 24
														when 1402770 then 25
														when 13800 then 25
														when 14022 then 25
														when 12947 then 25
														else 6
													end as significance
												from
													(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
														round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
														STUFF(
															(SELECT ', ' + t2.comments 
															FROM 
																(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
																	case 
																		when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
																		then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
																		else 6 
																	end as significance
																from
																	(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
																		case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
																	from 
																		FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
																	join
																		FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
																	on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
																	where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
																	union all
																	select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
																		case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
																	from 
																		FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
																	join
																		FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
																	on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
																	where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
																	union all
																	select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
																		case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
																	from 
																		FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
																	join
																		FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
																	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
																	join
																		FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
																	on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
																	where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
																	union all
																	select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
																		case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
																	from 
																		FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
																	join
																		FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
																   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
																	join
																		FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
																	on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
																	where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
																) t2 
															WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
															FOR XML PATH(''))
														, 1, LEN(', '), '') as comments
													from 
														(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
															case 
																when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
																then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
																else 6 
															end as significance
														from
															(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
																case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
															on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
															where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
															union all
															select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
																case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
															on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
															where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
															union all
															select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
																case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
															on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
															on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
															union all
															select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
																case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
														   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
															on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
														) t0
													group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
												) kdkd
											on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
											join
												FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
											on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
											left join
												FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
											on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
											left join
												FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
											on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
											left join
												FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
											on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
											left join
												FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
											on cntr1.iso_land_cd=sp.iso_land_code_nat
											left join
												FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
											on cntr2.iso_land_cd=sp.iso_land_code_nat_2
											left join
												FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
											on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
											where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0 and se.kd_htyp_cd=3
											FOR XML PATH ('signatory'),type, ELEMENTS),	
											case 
												when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
												then '1900-01-01T01:00:00'
												else left(convert(char,s5.EROEFF_DAT,126),19) 
											end as opened,
											case 
												when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
												then null 
												else left(convert(char,s5.AUFHEB_DAT,126),19)
											end as closed,
											case 
												when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
												then isnull(sld.saldo_bwrg,0) 
												else NULL
											end as balance,
											case 
												when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
												then left(convert(char,SYSDATETIME(),126),19) 
												else NULL
											end as date_balance,
											case 
												when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
												then 2
												else 1
											end as status_code,
											case 
												when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
												then NULL
												else isnull(sld.saldo_kwrg,0)
											end as beneficiary,
											FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as beneficiary_comment,
											FAF.faf_MROS_char_process(
												case 
													when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
													then
														'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
														case
															when ac_tp.MROS_value=14
															then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
															else ''
														end
													else
														case
															when ac_tp.MROS_value=14
															then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
															else 'n/a'
														end
												end
											,'n/a',4000) as comments
										FOR XML PATH (''),type, ELEMENTS)
										,(select 
											'n/a' as "institution_name",
											'n/a' as "swift",
											'ZH' as "branch",
											'n/a' as "account",
											'CHF' as "currency_code",
											'n/a' as "iban",
											'n/a' as "client_number",
											16 as "personal_account_type",
												'n/a' as "t_entity/name",
												1 as "t_entity/incorporation_legal_form",
													4 as "t_entity/addresses/address/address_type",
													'n/a' as "t_entity/addresses/address/address",
													'n/a' as "t_entity/addresses/address/city",
													'UNK' as "t_entity/addresses/address/country_code",
												'UNK' as "t_entity/incorporation_country_code",
												'yes' as "t_entity/tax_reg_number",
											'1900-01-01T00:00:00' as "opened",
											0 as "balance",
											left(convert(char,sysdatetime(),126),19) as "date_balance",
											4 as "status_code",
											'n/a' as "beneficiary_comment"
										FOR XML PATH (''),type, ELEMENTS)
									) as "from_account"
								FOR XML PATH (''),type, ELEMENTS)
							else 
								(select
									1 as "from_funds_code",
									isnull(cur.iso_wrg_cd,'CHF') as "from_foreign_currency/foreign_currency_code",
									abs(isnull(s3.betrag_kwrg,isnull(s3.betrag_bwrg,0.01))) as "from_foreign_currency/foreign_amount",
									1 as "from_foreign_currency/foreign_exchange_rate",
									--account owner
									isnull(
										(select
											--t_entity
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "from_entity/name",
											case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "from_entity/incorporation_legal_form",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "from_entity/incorporation_number",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "from_entity/business",
											case when se.kd_htyp_cd=3 then 
												isnull(sea.addresses,
													(select
														4 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "from_entity/addresses",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "from_entity/url",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "from_entity/incorporation_state",
											case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "from_entity/incorporation_country_code",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end 
												else NULL 
											end as "from_entity/incorporation_date",
											case when se.kd_htyp_cd=3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1 else NULL
												end 
												else NULL 
											end as "from_entity/business_closed",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "from_entity/date_business_closed",
											case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "from_entity/tax_reg_number",
											case 
												when se.kd_htyp_cd=3 
												then 
													FAF.faf_MROS_char_process_(
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else ''
														end + 
														case 
															when lf.MROS_value=4 
															then 
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then '; ' + char(13) + char(10)
																	else ''
																end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
															else ''
														end
													,NULL,4000) 
												else NULL 
											end as "from_entity/comments",
											--t_person 
											case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "from_person/gender",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "from_person/title",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "from_person/first_name",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "from_person/last_name",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then '1900-01-01T01:00:00' 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end 
												else NULL 
											end as "from_person/birthdate",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "from_person/birth_place",
											case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "from_person/nationality1",
											case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "from_person/nationality2",
											case when se.kd_htyp_cd<3 then spp.phones else NULL end as "from_person/phones",
											case when se.kd_htyp_cd<3 then 
												isnull(sea.addresses,
													(select
														3 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "from_person/addresses",
											case when se.kd_htyp_cd<3 then 
												isnull(spi.identification, 
													(select
														4 as "type",
														'n/a' as "number",
														'UNK' as "issue_country",
														'n/a' as "comments"
													FOR XML PATH (''),type, ELEMENTS)
												) 
												else NULL 
											end as "from_person/identification",
											case when se.kd_htyp_cd<3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
														or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1
													else NULL
												end 
												else NULL 
											end as "from_person/deceased",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "from_person/date_deceased",
											case 
												when se.kd_htyp_cd<3 
												then 
													FAF.faf_MROS_char_process(														
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else NULL
														end
													,NULL,4000)
												else NULL 
											end as "from_person/comments"
										from
											FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
										on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
										on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
										on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
										on cntr1.iso_land_cd=se.iso_land_code_nat
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
										on cntr2.iso_land_cd=se.iso_land_code_nat_2
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
										on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
										on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
										on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
										where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0									
										FOR XML PATH (''),type, ELEMENTS)
										,(select 
											'n/a' as "from_entity/name",
											1 as "from_entity/incorporation_legal_form",
												4 as "from_entity/addresses/address/address_type",
												'n/a' as "from_entity/addresses/address/address",
												'n/a' as "from_entity/addresses/address/city",
												'UNK' as "from_entity/addresses/address/country_code",
											'UNK' as "from_entity/incorporation_country_code",
											'yes' as "from_entity/tax_reg_number"
										FOR XML PATH (''),type, ELEMENTS)
									)
								FOR XML PATH (''),type, ELEMENTS)
						end
					else NULL
				end as "t_from_my_client",
				case 
					when FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,0) is not null
					then 
						(select
							1 as "from_funds_code",
							isnull(cur.iso_wrg_cd,'CHF') as "from_foreign_currency/foreign_currency_code",
							abs(isnull(s3.betrag_kwrg,isnull(s3.betrag_bwrg,0.01))) as "from_foreign_currency/foreign_amount",
							1 as "from_foreign_currency/foreign_exchange_rate",
							case
								when patindex('%[a-Z][^a-Z]%',FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,100))>0
								then left(FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,100),patindex('%[a-Z][^a-Z]%',FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,100)))
								else 'n/a'
							end "from_person/first_name",
							case
								when patindex('%[a-Z][^a-Z]%',FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,100))>0
								then FAF.faf_MROS_char_process(right(FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,100),len(FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,100))-patindex('%[a-Z][^a-Z]%',FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,100))),'n/a',100)
								else FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,'n/a',100)
							end "from_person/last_name",
							FAF.faf_MROS_char_process(
								FAF.faf_MROS_char_process(s3.adr_zeile_1_ez,NULL,4000) +
								case 
									when FAF.faf_MROS_char_process(s3.adr_zeile_2_ez,NULL,0) is not NULL
									then '; ' + FAF.faf_MROS_char_process(s3.adr_zeile_2_ez,NULL,4000)
									else ''
								end +
								case 
									when FAF.faf_MROS_char_process(s3.adr_zeile_3_ez,NULL,0) is not NULL
									then '; ' + FAF.faf_MROS_char_process(s3.adr_zeile_3_ez,NULL,4000)
									else ''
								end +
								case 
									when FAF.faf_MROS_char_process(s3.adr_zeile_4_ez,NULL,0) is not NULL
									then '; ' + FAF.faf_MROS_char_process(s3.adr_zeile_4_ez,NULL,4000)
									else ''
								end +
								case 
									when FAF.faf_MROS_char_process(s3.adr_zeile_5_ez,NULL,0) is not NULL
									then '; ' + FAF.faf_MROS_char_process(s3.adr_zeile_5_ez,NULL,4000)
									else ''
								end
							,NULL,4000) as "from_person/comments"
						FOR XML PATH (''),type, ELEMENTS)
					else NULL
				end as "t_from",
				case 
					when s3.withdrawal_flag=1
					then 
						(select
							1 as "to_funds_code",
							isnull(cur.iso_wrg_cd,'CHF') as "to_foreign_currency/foreign_currency_code",
							abs(isnull(s3.betrag_kwrg,isnull(s3.betrag_bwrg,0.01))) as "to_foreign_currency/foreign_amount",
							1 as "to_foreign_currency/foreign_exchange_rate",
							--account owner
							isnull(
								(select
									--t_entity
									case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "to_entity/name",
									case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "to_entity/incorporation_legal_form",
									case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "to_entity/incorporation_number",
									case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "to_entity/business",
									case when se.kd_htyp_cd=3 then 
										isnull(sea.addresses,
											(select
												4 as "address_type",
												'n/a' as "address",
												'n/a' as "city",
												'UNK' as "country_code"
											FOR XML PATH ('address'),type, ELEMENTS)
										) 
										else NULL 
									end as "to_entity/addresses",
									case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "to_entity/url",
									case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "to_entity/incorporation_state",
									case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "to_entity/incorporation_country_code",
									case when se.kd_htyp_cd=3 then 
										case 
											when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
											then 
												case 
													when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
													then NULL 
													else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,se.GEBURT_DAT,126),19)
										end 
										else NULL 
									end as "to_entity/incorporation_date",
									case when se.kd_htyp_cd=3 then 
										case 
											when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
											then 1 else NULL
										end 
										else NULL 
									end as "to_entity/business_closed",
									case when se.kd_htyp_cd=3 then 
										case 
											when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
											then 
												case 
													when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
													then NULL 
													else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,se.TOD_DAT,126),19)
										end 
										else NULL 
									end as "to_entity/date_business_closed",
									case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "to_entity/tax_reg_number",
									case 
										when se.kd_htyp_cd=3 
										then 
											FAF.faf_MROS_char_process_(
												case
													when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
													then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
													else ''
												end + 
												case 
													when lf.MROS_value=4 
													then 
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then '; ' + char(13) + char(10)
															else ''
														end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
													else ''
												end
											,NULL,4000) 
										else NULL 
									end as "to_entity/comments",
									--t_person 
									case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "to_person/gender",
									case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "to_person/title",
									case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "to_person/first_name",
									case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "to_person/last_name",
									case when se.kd_htyp_cd<3 then 
										case 
											when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
											then 
												case 
													when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
													then '1900-01-01T01:00:00' 
													else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,se.GEBURT_DAT,126),19)
										end 
										else NULL 
									end as "to_person/birthdate",
									case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "to_person/birth_place",
									case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "to_person/nationality1",
									case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "to_person/nationality2",
									case when se.kd_htyp_cd<3 then spp.phones else NULL end as "to_person/phones",
									case when se.kd_htyp_cd<3 then 
										isnull(sea.addresses,
											(select
												3 as "address_type",
												'n/a' as "address",
												'n/a' as "city",
												'UNK' as "country_code"
											FOR XML PATH ('address'),type, ELEMENTS)
										) 
										else NULL 
									end as "to_person/addresses",
									case when se.kd_htyp_cd<3 then 
										isnull(spi.identification, 
											(select
												4 as "type",
												'n/a' as "number",
												'UNK' as "issue_country",
												'n/a' as "comments"
											FOR XML PATH (''),type, ELEMENTS)
										) 
										else NULL 
									end as "to_person/identification",
									case when se.kd_htyp_cd<3 then 
										case 
											when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
												or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
											then 1
											else NULL
										end 
										else NULL 
									end as "to_person/deceased",
									case when se.kd_htyp_cd<3 then 
										case 
											when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
											then 
												case 
													when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
													then NULL 
													else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,se.TOD_DAT,126),19)
										end 
										else NULL 
									end as "to_person/date_deceased",
									case 
										when se.kd_htyp_cd<3 
										then 
											FAF.faf_MROS_char_process(														
												case
													when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
													then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
													else NULL
												end
											,NULL,4000)
										else NULL 
									end as "to_person/comments"
								from
									FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
								on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
								on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
								left join
									FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
								on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
								left join
									FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
								on cntr1.iso_land_cd=se.iso_land_code_nat
								left join
									FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
								on cntr2.iso_land_cd=se.iso_land_code_nat_2
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
								on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
								on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
								left join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
								on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
								where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0										
								FOR XML PATH (''),type, ELEMENTS)
								,(select 
									'n/a' as "to_entity/name",
									1 as "to_entity/incorporation_legal_form",
										4 as "to_entity/addresses/address/address_type",
										'n/a' as "to_entity/addresses/address/address",
										'n/a' as "to_entity/addresses/address/city",
										'UNK' as "to_entity/addresses/address/country_code",
									'UNK' as "to_entity/incorporation_country_code",
									'yes' as "to_entity/tax_reg_number"
								FOR XML PATH (''),type, ELEMENTS)
							)
						FOR XML PATH (''),type, ELEMENTS)
					else
						(select
							1 as "to_funds_code",
							isnull(cur.iso_wrg_cd,'CHF') as "to_foreign_currency/foreign_currency_code",
							abs(isnull(s3.betrag_kwrg,isnull(s3.betrag_bwrg,0.01))) as "to_foreign_currency/foreign_amount",
							1 as "to_foreign_currency/foreign_exchange_rate",
							--t_account_my_client
							isnull(
								(select 
									@inst_name as institution_name,
									@swif as swift,
									isnull(stz.MROS_value,'ZH') as branch,
									FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as account,
									isnull(wrg.iso_wrg_cd,'CHF') as currency_code,
									FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as account_name,
									FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as iban,
									FAF.faf_MROS_char_process(cl.edit_kd_nr,'n/a',30) as client_number,
									isnull(ac_tp.MROS_value,16) as personal_account_type,
									--account owner
									isnull(
										(select
											--entity
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "t_entity/name",
											case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "t_entity/incorporation_legal_form",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "t_entity/incorporation_number",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "t_entity/business",
											case when se.kd_htyp_cd=3 then 
												isnull(sea.addresses,
													(select
														4 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "t_entity/addresses",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "t_entity/url",
											case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "t_entity/incorporation_state",
											case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "t_entity/incorporation_country_code",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/incorporation_date",
											case when se.kd_htyp_cd=3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1 else NULL
												end 
												else NULL 
											end as "t_entity/business_closed",
											case when se.kd_htyp_cd=3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "t_entity/date_business_closed",
											case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "t_entity/tax_reg_number",
											case 
												when se.kd_htyp_cd=3 
												then 
													FAF.faf_MROS_char_process_(
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else ''
														end + 
														case 
															when lf.MROS_value=4 
															then 
																case
																	when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
																	then '; ' + char(13) + char(10)
																	else ''
																end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
															else ''
														end
													,NULL,4000) 
												else NULL 
											end as "t_entity/comments",
											--person
											case when se.kd_htyp_cd<3 then 
												case when se.PARTNER_FLAG=0 then 1 else Null end
												else NULL 
											end as "signatory/is_primary",
											case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "signatory/t_person/gender",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "signatory/t_person/title",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "signatory/t_person/first_name",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "signatory/t_person/last_name",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
													then 
														case 
															when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
															then '1900-01-01T01:00:00' 
															else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.GEBURT_DAT,126),19)
												end
												else NULL 
											end as "signatory/t_person/birthdate",
											case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "signatory/t_person/birth_place",
											case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "signatory/t_person/nationality1",
											case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "signatory/t_person/nationality2",
											case when se.kd_htyp_cd<3 then spp.phones else NULL end as "signatory/t_person/phones",
											case when se.kd_htyp_cd<3 then 
												isnull(sea.addresses,
													(select
														3 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													FOR XML PATH ('address'),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/addresses",
											--t_person_identification
											case when se.kd_htyp_cd<3 then 
												isnull(spi.identification, 
													(select
														4 as "type",
														'n/a' as "number",
														'UNK' as "issue_country",
														'n/a' as "comments"
													FOR XML PATH (''),type, ELEMENTS)
												) 
												else NULL 
											end as "signatory/t_person/identification",
											case when se.kd_htyp_cd<3 then 
												case 
													when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
														or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
													then 1
													else NULL
												end 
												else NULL 
											end as "signatory/t_person/deceased",
											case when se.kd_htyp_cd<3 then 
												case 
													when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
													then 
														case 
															when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
															then NULL 
															else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
														end 
													else left(convert(char,se.TOD_DAT,126),19)
												end 
												else NULL 
											end as "signatory/t_person/date_deceased",
											case 
												when se.kd_htyp_cd<3 
												then 
													FAF.faf_MROS_char_process(														
														case
															when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
															then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
															else NULL
														end
													,NULL,4000)
												else NULL 
											end as "signatory/t_person/comments",
											case when se.kd_htyp_cd<3 then 1 else NULL end as "signatory/role" 								
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
										left join 
											FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
										on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
										on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
										on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
										on cntr1.iso_land_cd=se.iso_land_code_nat
										left join
											FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
										on cntr2.iso_land_cd=se.iso_land_code_nat_2
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
										on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
										on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
										left join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
										on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
										where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and (se.partner_flag=0 or se.kd_htyp_cd=2)
										FOR XML PATH (''),type, ELEMENTS)
										,(select 
											'n/a' as "t_entity/name",
											1 as "t_entity/incorporation_legal_form",
												4 as "t_entity/addresses/address/address_type",
												'n/a' as "t_entity/addresses/address/address",
												'n/a' as "t_entity/addresses/address/city",
												'UNK' as "t_entity/addresses/address/country_code",
											'UNK' as "t_entity/incorporation_country_code",
											'yes' as "t_entity/tax_reg_number"
										FOR XML PATH (''),type, ELEMENTS)
									),
									--t_person (connected with the entity in KD_KD)
									(select 
										isnull(gd.MROS_value,'U') as "t_person/gender",
										FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
										FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
										FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
										case 
											when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
											then 
												case 
													when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
													then '1900-01-01T01:00:00' 
													else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.GEBURT_DAT,126),19)
										end as "t_person/birthdate",
										FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
										isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
										cntr2.iso_land_cd as "t_person/nationality2",
										--t_phone
										spp.phones as "t_person/phones",
										--t_address
										isnull(spa.addresses,
											(select
												3 as "address_type",
												'n/a' as "address",
												'n/a' as "city",
												'UNK' as "country_code"
											FOR XML PATH ('address'),type, ELEMENTS)
										) as "t_person/addresses",
										--t_person_identification
										isnull(spi.identification, 
											(select
												4 as "type",
												'n/a' as "number",
												'UNK' as "issue_country",
												'n/a' as "comments"
											FOR XML PATH (''),type, ELEMENTS)
										) as "t_person/identification",
										case 
											when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
												or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
											then 1
											else NULL
										end as "t_person/deceased",
										case 
											when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
											then 
												case 
													when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
													then NULL 
													else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
												end 
											else left(convert(char,sp.TOD_DAT,126),19)
										end as "t_person/date_deceased",
										FAF.faf_MROS_char_process_(
											case
												when isnull(kdkd.significance,6)=6
												then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
												else ''
											end +
											case
												when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
												then 
													case
														when isnull(kdkd.significance,6)=6
														then '; ' + char(13) + char(10)
														else ''
													end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
												else ''
											end
										,NULL,4000) as "t_person/comments",
										isnull(kdkd.significance,6) as "role" 
									from 
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
									join
										(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
											case significance
												when 101 then 1
												when 107 then 7
												when 113 then 13
												when 114 then 14
												when 115 then 15
												when 117 then 17
												when 118 then 18
												when 119 then 19
												when 120 then 20
												when 121 then 21
												when 122 then 22
												when 123 then 23
												when 124 then 24
												when 125 then 25
												when 12882 then 19
												when 12198 then 20
												when 13110 then 21
												when 12995 then 22
												when 12305 then 23
												when 1481430 then 24
												when 13685 then 24
												when 13673 then 24
												when 13908 then 24
												when 1402770 then 25
												when 13800 then 25
												when 14022 then 25
												when 12947 then 25
												else 6
											end as significance
										from
											(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
												round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
												STUFF(
													(SELECT ', ' + t2.comments 
													FROM 
														(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
															case 
																when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
																then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
																else 6 
															end as significance
														from
															(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
																case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
															on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
															where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
															union all
															select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
																case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
															on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
															where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
															union all
															select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
																case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
															on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
															on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
															union all
															select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
																case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
															from 
																FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
															join
																FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
														   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
															join
																FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
															on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
															where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
														) t2 
													WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
													FOR XML PATH(''))
												, 1, LEN(', '), '') as comments
											from 
												(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
													case 
														when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
														then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
														else 6 
													end as significance
												from
													(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
														case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
													on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
													where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
													union all
													select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
														case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
													on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
													where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
													union all
													select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
														case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
													on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
													on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
													union all
													select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
														case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
													from 
														FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
													join
														FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
												   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
													join
														FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
													on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
													where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
												) t0
											group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
										) kdkd
									on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
									join
										FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
									on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
									on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
									on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
									on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
									on cntr1.iso_land_cd=sp.iso_land_code_nat
									left join
										FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
									on cntr2.iso_land_cd=sp.iso_land_code_nat_2
									left join
										FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
									on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
									where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0 and se.kd_htyp_cd=3
									FOR XML PATH ('signatory'),type, ELEMENTS),	
									case 
										when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
										then '1900-01-01T01:00:00'
										else left(convert(char,s5.EROEFF_DAT,126),19) 
									end as opened,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then null 
										else left(convert(char,s5.AUFHEB_DAT,126),19)
									end as closed,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then isnull(sld.saldo_bwrg,0) 
										else NULL
									end as balance,
									case 
										when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
										then left(convert(char,SYSDATETIME(),126),19)  
										else NULL
									end as date_balance,
									case 
										when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
										then 2
										else 1
									end as status_code,
									case 
										when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
										then NULL
										else isnull(sld.saldo_kwrg,0)
									end as beneficiary,
									FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as beneficiary_comment,
									FAF.faf_MROS_char_process(
										case 
											when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
											then
												'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
													else ''
												end
											else
												case
													when ac_tp.MROS_value=14
													then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
													else 'n/a'
												end
										end
									,'n/a',4000) as comments
								FOR XML PATH (''),type, ELEMENTS)
								,(select 
									'n/a' as "institution_name",
									'n/a' as "swift",
									'ZH' as "branch",
									'n/a' as "account",
									'CHF' as "currency_code",
									'n/a' as "iban",
									'n/a' as "client_number",
									16 as "personal_account_type",
										'n/a' as "t_entity/name",
										1 as "t_entity/incorporation_legal_form",
											4 as "t_entity/addresses/address/address_type",
											'n/a' as "t_entity/addresses/address/address",
											'n/a' as "t_entity/addresses/address/city",
											'UNK' as "t_entity/addresses/address/country_code",
										'UNK' as "t_entity/incorporation_country_code",
										'yes' as "t_entity/tax_reg_number",
									'1900-01-01T00:00:00' as "opened",
									0 as "balance",
									left(convert(char,sysdatetime(),126),19) as "date_balance",
									4 as "status_code",
									'n/a' as "beneficiary_comment"
								FOR XML PATH (''),type, ELEMENTS)
							) as "to_account"
						FOR XML PATH (''),type, ELEMENTS)
				end as "t_to_my_client",
				case 
					when s3.betrag_bwrg<0
					then 'Cancellation / Refund'				 
					else NULL
				end as "comments"
			from 
				FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH_EA s3 
			left join
				FAF_MBDATA_T.FAF.FA_MROS_INT_WRG cur
			on s3.iso_wrg_cd=cur.iso_wrg_cd
			left join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
			on s3.userbk_nr=tr_tp.userbk_nr and s3.buchtyp_cd=tr_tp.finnova_value and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
			left join
				FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM s5 WITH (FORCESEEK)
			on s5.kt_lnr=s3.kt_lnr and s5.userbk_nr=s3.userbk_nr
			left join
				FAF_MBDATA_T.FAF.FA_MROS_KT_SALDO sld WITH (FORCESEEK)
			on s5.kt_lnr=sld.kt_lnr and s5.userbk_nr=sld.userbk_nr
			left join
				FAF_MBDATA_T.FAF.FA_MROS_INT_WRG wrg
			on s5.iso_wrg_cd=wrg.iso_wrg_cd
			left join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE stz
			on s5.userbk_nr=stz.userbk_nr and s5.sitz_nr=stz.finnova_value and stz.ref_type=1 and stz.activ_MROS=1
			left join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ac_tp
			on s5.userbk_nr=ac_tp.userbk_nr and s5.kt_art_ber_cd=ac_tp.finnova_value and ac_tp.ref_type=9 and ac_tp.activ_MROS=1
			left join
				FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM cl WITH (FORCESEEK)
			on s5.kd_lnr=cl.kd_lnr and s5.userbk_nr=cl.userbk_nr and cl.PARTNER_FLAG=0
			where s3.userbk_nr=@F_BANK_ID and s3.kt_buch_lnr in (select nID from @SET_OF_TR_ID)
			FOR XML PATH ('transaction'),type, ELEMENTS);
	end;	
	
	return(@xml_d);
end;
go	


---------------------------------------------------------------------------------------------------

--caption part of report (technical function)
IF OBJECT_ID('[FAF].[head_part_of_report]', 'FN') IS NOT NULL
	  DROP function [FAF].[head_part_of_report]; 
go
create function FAF.head_part_of_report (
	@F_BANK_ID int,
	@RP_NAME varchar(100),
	@RP_SURNAME varchar(100),
	@RP_PHONE varchar(50),
	@RP_EMAIL varchar(50),
	@RP_OCCUPATION varchar(255),
	@RP_ADDRESS varchar(100),
	@RP_CITY varchar(255),
	@FIU_REF_NUMBER varchar(255),
	@ENTITY_REFERENCE varchar(255),	
	@REPORT_REASON varchar(4000),
	@R_ACTION varchar(4000),
	@REPORT_INDICATORS FAF.FAF_MROS_charID_t READONLY,
	@report xml,
	@STR_flag tinyint) 
returns xml
as
begin
	declare @xml_r xml;

	if @STR_flag=1
	begin
		--check if there is a transaction here
		if isnull(@report.exist('transaction'),0)=0
			set @xml_r= (select 'There are no transactions that meet the conditions; ' FOR XML PATH ('mros_report_error'),type, ELEMENTS);
	end;
	else
	begin
		--check if there is a report_party here
		if isnull(@report.exist('activity/report_parties/report_party'),0)=0
			set @xml_r= (select 'There are no records that meet the conditions; ' FOR XML PATH ('mros_report_error'),type, ELEMENTS);
	end;

	if @xml_r is null
	begin
		set @xml_r = 
			(select 
				s4.rentity_id as "rentity_id",
				'E' as "submission_code",
				case
					when @STR_flag=1 then 'STR'
					else 'SAR'
				end as "report_code",
				FAF.faf_MROS_char_process_(@ENTITY_REFERENCE,'n/a',255) as "entity_reference",
				FAF.faf_MROS_char_process_(@FIU_REF_NUMBER,NULL,255) as "fiu_ref_number",
				left(convert(char,sysdatetime(),126),19) as "submission_date",
				'CHF' as "currency_code_local",
				case
					when FAF.faf_MROS_char_process(@RP_NAME,NULL,0) is not null and FAF.faf_MROS_char_process(@RP_SURNAME,NULL,0) is not null
					then FAF.faf_MROS_char_process_(@RP_NAME,'n/a',100)
					else FAF.faf_MROS_char_process(s4.first_name,'n/a',100) 
				end as "reporting_person/first_name",
				case
					when FAF.faf_MROS_char_process(@RP_NAME,NULL,0) is not null and FAF.faf_MROS_char_process(@RP_SURNAME,NULL,0) is not null
					then FAF.faf_MROS_char_process_(@RP_SURNAME,'n/a',100)
					else FAF.faf_MROS_char_process(s4.last_name,'n/a',100) 
				end as "reporting_person/last_name",
				(select
					case 
						when FAF.faf_MROS_char_process(@RP_PHONE,NULL,0) is not null
						then 
							(select 
								4 as "tph_contact_type",
								1 as "tph_communication_type",
								FAF.faf_MROS_char_process_(@RP_PHONE,'n/a',50) as "tph_number"
							FOR XML PATH ('phone'),type, ELEMENTS)
						else NULL
					end,
					case 
						when FAF.faf_MROS_char_process(@RP_EMAIL,NULL,0) is not null
						then 
							(select 
								4 as "tph_contact_type",
								3 as "tph_communication_type",
								FAF.faf_MROS_char_process_(@RP_EMAIL,'n/a',50) as "tph_number"
							FOR XML PATH ('phone'),type, ELEMENTS)
						else NULL
					end,
					case 
						when FAF.faf_MROS_char_process(@RP_PHONE,NULL,0) is null and FAF.faf_MROS_char_process(@RP_EMAIL,NULL,0) is null
						then 
							(select 
								4 as "tph_contact_type",
								isnull(s4.tph_communication_type,6) as "tph_communication_type",
								FAF.faf_MROS_char_process(s4.tph_number,'n/a',50) as "tph_number"
							FOR XML PATH ('phone'),type, ELEMENTS)
						else NULL
					end
				FOR XML PATH (''),type, ELEMENTS) as "reporting_person/phones",
				case 
					when FAF.faf_MROS_char_process(@RP_ADDRESS,NULL,0) is not null and FAF.faf_MROS_char_process(@RP_CITY,NULL,0) is not NULL
					then 
						(select 
							4 as "address_type",
							FAF.faf_MROS_char_process_(@RP_ADDRESS,'n/a',100) as "address",
							FAF.faf_MROS_char_process_(@RP_CITY,'n/a',255) as "city",
							'CH' as "country_code"
						FOR XML PATH ('address'),type, ELEMENTS)
					else NULL
				end as "reporting_person/addresses",
				FAF.faf_MROS_char_process_(@RP_OCCUPATION,NULL,255) as "reporting_person/occupation",
				4 as "location/address_type",
				FAF.faf_MROS_char_process(s4.[address],'n/a',100) as "location/address",
				FAF.faf_MROS_char_process(s4.city,'n/a',255) as "location/city",
				'CH' as "location/country_code",
				FAF.faf_MROS_char_process_(@REPORT_REASON,'n/a',8000) as "reason",
				FAF.faf_MROS_char_process_(@R_ACTION,'n/a',8000) as "action",
				@report,
				(select chID as "indicator" from @REPORT_INDICATORS FOR XML PATH (''),type, ELEMENTS) as "report_indicators"
			from 
				FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE s4
			where s4.userbk_nr_home=@F_BANK_ID
			FOR XML PATH ('report'),type, ELEMENTS);
	end;
		
	return(@xml_r);
end;
go	


---------------------------------------------------------------------------------------------------

--transactions report from ktwbuch_det (transaction number collection)
IF OBJECT_ID('[FAF].[booking_report]', 'FN') IS NOT NULL
	  DROP function [FAF].[booking_report]; 
go
create function FAF.booking_report (
	@F_BANK_ID int,
	@RP_NAME varchar(100),
	@RP_SURNAME varchar(100),
	@RP_PHONE varchar(50),
	@RP_EMAIL varchar(50),
	@RP_OCCUPATION varchar(255),
	@RP_ADDRESS varchar(100),
	@RP_CITY varchar(255),
	@FIU_REF_NUMBER varchar(255),
	@ENTITY_REFERENCE varchar(255),	
	@REPORT_REASON varchar(4000),
	@R_ACTION varchar(4000),
	@REPORT_INDICATORS FAF.FAF_MROS_charID_t READONLY,
	@SET_OF_TR_ID FAF.FAF_MROS_numID_t READONLY,
	@AC_NR varchar(50)) 
returns xml
as
begin
	declare @xml_r xml,
		@xml_p xml,
		@xml_t xml,
		@xml_d xml;
	declare @SET_OF_PAYMENT_ID FAF.FAF_MROS_numID_t;
	declare @TR_ID FAF.FAF_MROS_numID_t;
	declare @SET_OF_ORDER_NR FAF.FAF_MROS_charID_t;
	declare @ind FAF.FAF_MROS_charID_t;
	declare @str varchar(4000);
	declare @n int;
	declare @max_items int;
	set @max_items = 1000;
	
	--indicators checking
	insert into @ind
		select indicator from FAF_MBDATA_T.FAF.FA_MROS_INT_INDIZ where indicator in (select ltrim(rtrim(chID)) from @REPORT_INDICATORS);
	set @str = '';
	if (select count(*) from @ind where right(chID,1)='M')<>1 
		set @str=@str + 'the report must include only one indicator with type Report_Type (M)';
	if (select count(*) from @ind where right(chID,1)='V')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Predicate_Offenses (V)';
	if (select count(*) from @ind where right(chID,1)='G')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Reason (G)';
	if len(@str)>0
		set @str = 'Indicators error: ' + @str + '; ';

	--table FA_MROS_INT_PERS_CHARGE checking
	if (select count(*) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID)=0
		set @str = @str + 'The table FAF_MROS_persons_in_charge does not have the record with userbk_nr_home = ' + cast(@F_BANK_ID as varchar) + '; ';

	--account ID checking
	if (select count(*) from FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM where userbk_nr=@F_BANK_ID and EDIT_KT_NR=@AC_NR)=0
		set @str = @str + 'Account number not found; ';

	--transactions set checking
	if (select count(*) from @SET_OF_TR_ID)=0
		set @str = @str + 'At least one transaction number should be provided; ';

	--Error processing
	if len(@str)>0
		set @xml_r= (select @str as "mros_report_error" FOR XML PATH (''),type, ELEMENTS);
	
	--if there is no error then start the main request
	if @xml_r is null
	begin			
			--list of relevant payments
			insert into @SET_OF_PAYMENT_ID 
			select top(@max_items) zv_zlg_sys_lnr 
			from
				(select distinct pm.zv_zlg_sys_lnr, pm.AUF_DT_ERT
				from 
					FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH tr
				join
					FAF_MBDATA_T.FAF.FA_MROS_ZV_ZLG_T pm
				on tr.userbk_nr=pm.userbk_nr and tr.auf_lnr=pm.auf_lnr and (tr.kt_lnr=pm.KT_LNR or tr.kt_lnr=pm.KT_LNR_B)
				--acceptable transaction types only
				join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
				on pm.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=@F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
				--acceptable trak_ref types only
				join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ref_tp
				on pm.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=@F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
				where tr.userbk_nr=@F_BANK_ID and tr.kt_buch_lnr in (select nID from @SET_OF_TR_ID)
					and tr.edit_kt_nr=@AC_NR) ss
			order by AUF_DT_ERT desc;
			set @n = (select count(*) from @SET_OF_PAYMENT_ID);

			--payment nodes
			if @n>0
				set @xml_p = FAF.trans_node_from_payments(@F_BANK_ID,@SET_OF_PAYMENT_ID,@SET_OF_ORDER_NR);
	
			--list of relevant transactions			
			if (@max_items>@n)
			begin
				insert into @TR_ID 
				select top (@max_items-@n) tr.kt_buch_lnr from 
					FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH_EA tr
				--acceptable transaction types only
				join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
				on tr.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=@F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1 and tr_tp.special_mark in ('withdrawal','deposit')
				--acceptable trak_ref types only
				--join
				--	FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ref_tp
				--on tr.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=@F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
				where tr.userbk_nr=@F_BANK_ID and tr.kt_buch_lnr in (select nID from @SET_OF_TR_ID)
					and tr.edit_kt_nr=@AC_NR
				order by buch_dat_a_1 desc;

				--withdrawals/deposits
				set @xml_d = FAF.node_deposit_withdrawal (@F_BANK_ID,@TR_ID);
			end;
	
		--remain part of the report
		set @xml_t = (select @xml_p,@xml_d FOR XML PATH (''),type, ELEMENTS);
		set @xml_r =FAF.head_part_of_report (@F_BANK_ID,@RP_NAME,@RP_SURNAME,@RP_PHONE,@RP_EMAIL,@RP_OCCUPATION,@RP_ADDRESS,@RP_CITY,@FIU_REF_NUMBER,@ENTITY_REFERENCE,@REPORT_REASON,@R_ACTION,@ind,@xml_t,1);
	end;

	return(@xml_r);
end;
go	


---------------------------------------------------------------------------------------------------


--transactions report from ktwbuch_det (clients/accounts + filter)
IF OBJECT_ID('[FAF].[historical_report]', 'FN') IS NOT NULL
	  DROP function [FAF].[historical_report]; 
go
create function FAF.historical_report (
	@F_BANK_ID int,
	@RP_NAME varchar(100),
	@RP_SURNAME varchar(100),
	@RP_PHONE varchar(50),
	@RP_EMAIL varchar(50),
	@RP_OCCUPATION varchar(255),
	@RP_ADDRESS varchar(100),
	@RP_CITY varchar(255),
	@FIU_REF_NUMBER varchar(255),
	@ENTITY_REFERENCE varchar(255),	
	@REPORT_REASON varchar(4000),
	@R_ACTION varchar(4000),
	@REPORT_INDICATORS FAF.FAF_MROS_charID_t READONLY,
	@SET_OF_CL_NR FAF.FAF_MROS_charID_t READONLY,
	@SET_OF_AC_NR FAF.FAF_MROS_charID_t READONLY,	
	@DATE_FROM int,
	@DATE_TO int,
	@LOW_AMOUNT decimal(20,2),
	@DEBIT tinyint,
	@CREDIT tinyint,
	@N_TRANS_LIMIT int) 
returns xml
as
begin
	declare @xml_r xml,
		@xml_t xml,
		@xml_p xml,
		@xml_d xml;
	declare @tr_lim int;
	declare @am_lim decimal(20,2);
	declare @d1 int;
	declare @d2 int;
	declare @SET_OF_PAYMENT_ID FAF.FAF_MROS_numID_t;
	declare @SET_OF_TR_ID0 FAF.FAF_MROS_numID_t,
		@SET_OF_TR_ID1 FAF.FAF_MROS_numID_t;
	declare @SET_OF_ORDER_NR FAF.FAF_MROS_charID_t;
	declare @ind FAF.FAF_MROS_charID_t;
	declare @str varchar(4000);
	declare @max_items int;
	set @max_items = 1000;
	
	--indicators checking
	insert into @ind
		select indicator from FAF_MBDATA_T.FAF.FA_MROS_INT_INDIZ where indicator in (select ltrim(rtrim(chID)) from @REPORT_INDICATORS);
	set @str = '';
	if (select count(*) from @ind where right(chID,1)='M')<>1 
		set @str=@str + 'the report must include only one indicator with type Report_Type (M)';
	if (select count(*) from @ind where right(chID,1)='V')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Predicate_Offenses (V)';
	if (select count(*) from @ind where right(chID,1)='G')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Reason (G)';
	if len(@str)>0
		set @str = 'Indicators error: ' + @str + '; ';

	--table FA_MROS_INT_PERS_CHARGE checking
	if (select count(*) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID)=0
		set @str = @str + 'The table FAF_MROS_persons_in_charge does not have the record with userbk_nr_home = ' + cast(@F_BANK_ID as varchar) + '; ';

	--debit/credit checking
	if isnull(@DEBIT,1)=0 and isnull(@CREDIT,1)=0
		set @str = @str + 'At least one from Debit/Credit should be nonzero; ';

	--Error processing
	if len(@str)>0
		set @xml_r= (select @str as "mros_report_error" FOR XML PATH (''),type, ELEMENTS);

	--if there is no error	then start the main request
	if @xml_r is null
	begin
		--input dates checking
		set @d1 = @DATE_FROM;
		if @d1 is null or @d1<19000101 or @d1>cast(replace(convert(char,sysdatetime(),102),'.','') as int) 
			set @d1 = 17530101;
		set @d2 = @DATE_TO;
		if @d2 is null or @d2<19000101 or @d1>@d2
			set @d2 = 22000101;

		--N_TRANS_LIMIT checking
		set @tr_lim = @N_TRANS_LIMIT;
		if @tr_lim is null or @tr_lim<=0
			set @tr_lim = @max_items;

		--amount limit checking
		set @am_lim = @LOW_AMOUNT;
		if @am_lim is null or @am_lim<0.01
			set @am_lim = 0.01;

		--list of relevant transactions	
		if (select count(*) from @SET_OF_AC_NR)>0
		begin
			insert into @SET_OF_TR_ID0 select top (@tr_lim) kt_buch_lnr from FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH 
				where userbk_nr=@F_BANK_ID 
					and edit_kt_nr in (select chID from @SET_OF_AC_NR)
					and buch_dat_a_1>=@d1
					and buch_dat_a_1<=@d2
					and abs(betrag_bwrg)>=@am_lim
					and (sh_cd = case when @DEBIT=0 then '' else 'D' end
						or sh_cd = case when @CREDIT=0 then '' else 'C' end)
				order by buch_dat_a_1 desc;
		end;
		else
		begin
			if (select count(*) from @SET_OF_CL_NR)>0
				insert into @SET_OF_TR_ID0 select top (@tr_lim) kt_buch_lnr from FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH 
					where userbk_nr=@F_BANK_ID 
						and edit_kd_nr in (select chID from @SET_OF_CL_NR)
						and buch_dat_a_1>=@d1
						and buch_dat_a_1<=@d2
						and abs(betrag_bwrg)>=@am_lim
						and (sh_cd = case when @DEBIT=0 then '' else 'D' end
							or sh_cd = case when @CREDIT=0 then '' else 'C' end)
					order by buch_dat_a_1 desc;
		end;

		--list of relevant payments	
		if (select count(*) from @SET_OF_TR_ID0)>0
		begin
			insert into @SET_OF_PAYMENT_ID 
			select top (@tr_lim) pm.zv_zlg_sys_lnr from 
				FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH tr
			join
				FAF_MBDATA_T.FAF.FA_MROS_ZV_ZLG_T pm
			on tr.userbk_nr=pm.userbk_nr and tr.auf_lnr=pm.auf_lnr and (tr.kt_lnr=pm.KT_LNR or tr.kt_lnr=pm.KT_LNR_B)
			--acceptable transaction types only
			join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
			on pm.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=@F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
			--acceptable trak_ref types only
			join
				FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ref_tp
			on pm.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=@F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
			where tr.userbk_nr=@F_BANK_ID and tr.kt_buch_lnr in (select nID from @SET_OF_TR_ID0)
				and abs(pm.betrag_bwrg)>=@am_lim
			order by pm.AUF_DT_ERT desc;

			--payment nodes
			if (select count(*) from @SET_OF_PAYMENT_ID)>0
				set @xml_p = FAF.trans_node_from_payments(@F_BANK_ID,@SET_OF_PAYMENT_ID,@SET_OF_ORDER_NR);
		end;
	
		--withdrawals/deposits
		set @tr_lim = (select @tr_lim - count(*) from @SET_OF_PAYMENT_ID);
		if @tr_lim>0
		begin	
			--list of relevant transactions	
			if (select count(*) from @SET_OF_AC_NR)>0
			begin
				insert into @SET_OF_TR_ID1 select top (@tr_lim) kt_buch_lnr 
					from FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH_EA  tr
					--acceptable transaction types only
					join
						FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
					on tr.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=@F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1 and tr_tp.special_mark in ('withdrawal','deposit')
					--acceptable trak_ref types only
					--join
					--	FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ref_tp
					--on tr.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=@F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
					where tr.userbk_nr=@F_BANK_ID 
						and tr.edit_kt_nr in (select chID from @SET_OF_AC_NR)
						and tr.buch_dat_a_1>=@d1
						and tr.buch_dat_a_1<=@d2
						and abs(tr.betrag_bwrg)>=@am_lim
						and (tr.sh_cd = case when @DEBIT=0 then '' else 'D' end
							or tr.sh_cd = case when @CREDIT=0 then '' else 'C' end)
					order by tr.buch_dat_a_1 desc;
			end;
			else
			begin	
				if (select count(*) from @SET_OF_CL_NR)>0
					insert into @SET_OF_TR_ID1 select top (@tr_lim) kt_buch_lnr 
						from FAF_MBDATA_T.FAF.FA_MROS_KT_BUCH_EA tr
						--acceptable transaction types only
						join
							FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tr_tp
						on tr.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=@F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1 and tr_tp.special_mark in ('withdrawal','deposit')
						--acceptable trak_ref types only
						--join
						--	FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ref_tp
						--on tr.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=@F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
						where tr.userbk_nr=@F_BANK_ID 
							and tr.edit_kd_nr in (select chID from @SET_OF_CL_NR)
							and tr.buch_dat_a_1>=@d1
							and tr.buch_dat_a_1<=@d2
							and abs(tr.betrag_bwrg)>=@am_lim
							and (tr.sh_cd = case when @DEBIT=0 then '' else 'D' end
								or tr.sh_cd = case when @CREDIT=0 then '' else 'C' end)
						order by tr.buch_dat_a_1 desc;
			end;

			if (select count(*) from @SET_OF_TR_ID1)>0
			begin
				set @xml_d = FAF.node_deposit_withdrawal (@F_BANK_ID,@SET_OF_TR_ID1);
			end;
		end;
	
		--remain part of the report
		set @xml_t = (select @xml_p,@xml_d FOR XML PATH (''),type, ELEMENTS);
		set @xml_r =FAF.head_part_of_report (@F_BANK_ID,@RP_NAME,@RP_SURNAME,@RP_PHONE,@RP_EMAIL,@RP_OCCUPATION,@RP_ADDRESS,@RP_CITY,@FIU_REF_NUMBER,@ENTITY_REFERENCE,@REPORT_REASON,@R_ACTION,@ind,@xml_t,1);
	end;

	return(@xml_r);
end;
go	


---------------------------------------------------------------------------------------------------



--report with several arbitrary payments
IF OBJECT_ID('[FAF].[set_of_payments]', 'FN') IS NOT NULL
	  DROP function [FAF].[set_of_payments]; 
go
create function FAF.set_of_payments (
	@F_BANK_ID int,
	@RP_NAME varchar(100),
	@RP_SURNAME varchar(100),
	@RP_PHONE varchar(50),
	@RP_EMAIL varchar(50),
	@RP_OCCUPATION varchar(255),
	@RP_ADDRESS varchar(100),
	@RP_CITY varchar(255),
	@FIU_REF_NUMBER varchar(255),
	@ENTITY_REFERENCE varchar(255),	
	@REPORT_REASON varchar(4000),
	@R_ACTION varchar(4000),
	@REPORT_INDICATORS FAF.FAF_MROS_charID_t READONLY,
	@SET_OF_PAYMENT_ID FAF.FAF_MROS_numID_t READONLY,
	@SET_OF_ORDER_NR FAF.FAF_MROS_charID_t READONLY,
	@DF_PAYMENT_ID numeric(22,0),
	@DF_ORDER_NR varchar(50),
	@DF_DESCRIPTION varchar(4000),
	@DF_DATE datetime,
	@DF_CURRENCY varchar(4),
	@DF_AMOUNT numeric(24,4),
	@DF_DEBIT_CREDIT varchar(1),
	@DF_ACCOUNT_ID numeric(22,0),
	@DF_CUSTOMER_ID numeric(22,0),
	@DF_CUSTOMER_NR varchar(50),
	@DF_BANK_NAME varchar(255),
	@DF_SWIFT varchar(11),
	@DF_ACCOUNT varchar(50),		
	@DF_COMMENTS varchar(4000))
returns xml
as
begin
	declare @xml_t xml,
			@xml_r xml,
			@xml_cl xml,
			@xml_cl1 xml,
			@xml_ac xml;
	declare @ind FAF.FAF_MROS_charID_t;
	declare @str varchar(4000);
	declare @inst_name varchar(255);
	declare @swif varchar(11);
	declare @cur varchar(4);

	
	--indicators checking
	insert into @ind
		select indicator from FAF_MBDATA_T.FAF.FA_MROS_INT_INDIZ where indicator in (select ltrim(rtrim(chID)) from @REPORT_INDICATORS);
	set @str = '';
	if (select count(*) from @ind where right(chID,1)='M')<>1 
		set @str=@str + 'the report must include only one indicator with type Report_Type (M)';
	if (select count(*) from @ind where right(chID,1)='V')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Predicate_Offenses (V)';
	if (select count(*) from @ind where right(chID,1)='G')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Reason (G)';
	if len(@str)>0
		set @str = 'Indicators error: ' + @str + '; ';

	--table FA_MROS_INT_PERS_CHARGE checking
	if (select count(*) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID)=0
		set @str = @str + 'The table FAF_MROS_persons_in_charge does not have the record with userbk_nr_home = ' + cast(@F_BANK_ID as varchar) + '; ';

	--Error processing
	if len(@str)>0
		set @xml_r= (select @str as "mros_report_error" FOR XML PATH (''),type, ELEMENTS);

	--if there is no error then start the main request
	if @xml_r is null
	begin
		set @xml_t = FAF.trans_node_from_payments (@F_BANK_ID,@SET_OF_PAYMENT_ID,@SET_OF_ORDER_NR);

		--Direct forwarding
		if @xml_t is null and (select count(*) from @SET_OF_PAYMENT_ID)=1 and @DF_PAYMENT_ID is not null and @DF_ACCOUNT_ID is not null
		begin
			--Bank name and swift code from the technical table
			set @inst_name = (select top 1 FAF.faf_MROS_char_process(institution_name,'n/a',255) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);
			set @swif = (select top 1 FAF.faf_MROS_char_process(swift,'n/a',11) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);
			
			--currency
			set @cur = (select top 1 iso_wrg_cd from FAF_MBDATA_T.FAF.FA_MROS_INT_WRG where iso_wrg_cd=@DF_CURRENCY);


			set @xml_cl = 
				(select
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else null end as "t_entity/name",
					case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else null end as "t_entity/incorporation_legal_form",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else null end as "t_entity/incorporation_number",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else null end as "t_entity/business",
					case when se.kd_htyp_cd=3 then 
						isnull(sea.addresses,
							(select
								4 as "address_type",
								'n/a' as "address",
								'n/a' as "city",
								'UNK' as "country_code"
							FOR XML PATH ('address'),type, ELEMENTS)
						) 
						else null 
					end as "t_entity/addresses",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else null end as "t_entity/url",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else null end as "t_entity/incorporation_state",
					case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else null end as "t_entity/incorporation_country_code",
					case when se.kd_htyp_cd=3 then 
						case 
							when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
							then 
								case 
									when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
									then NULL 
									else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
								end 
							else left(convert(char,se.GEBURT_DAT,126),19)
						end 
						else null 
					end as "t_entity/incorporation_date",
					case when se.kd_htyp_cd=3 then 
						case 
							when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
							then 1 else NULL
						end 
						else null 
					end as "t_entity/business_closed",
					case when se.kd_htyp_cd=3 then 
						case 
							when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
							then 
								case 
									when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
									then NULL 
									else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
								end 
							else left(convert(char,se.TOD_DAT,126),19)
						end 
						else null 
					end as "t_entity/date_business_closed",
					case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else null end as "t_entity/tax_reg_number",
					case 
						when se.kd_htyp_cd=3 
						then 
							FAF.faf_MROS_char_process_(
								case
									when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
									then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
									else ''
								end + 
								case 
									when lf.MROS_value=4 
									then 
										case
											when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
											then '; ' + char(13) + char(10)
											else ''
										end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
									else ''
								end
							,NULL,4000) 
						else NULL 
					end as "t_entity/comments",
					case when se.kd_htyp_cd<3 then case when se.PARTNER_FLAG=0 then 1 else Null end else null end as "signatory/is_primary",
				case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else null end as "signatory/t_person/gender",
				case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else null end as "signatory/t_person/title",
				case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else null end as "signatory/t_person/first_name",
				case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else null end as "signatory/t_person/last_name",
				case when se.kd_htyp_cd<3 then 
					case 
						when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
						then 
							case 
								when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
								then '1900-01-01T01:00:00' 
								else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
							end 
						else left(convert(char,se.GEBURT_DAT,126),19)
					end 
					else null 
				end as "signatory/t_person/birthdate",
				case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else null end as "signatory/t_person/birth_place",
				case when se.kd_htyp_cd<3 then isnull(cntr1.iso_land_cd,'UNK') else null end as "signatory/t_person/nationality1",
				case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else null end as "signatory/t_person/nationality2",
				case when se.kd_htyp_cd<3 then spp.phones else null end as "signatory/t_person/phones",
				case when se.kd_htyp_cd<3 then 
					isnull(sea.addresses,
						(select
							3 as "address_type",
							'n/a' as "address",
							'n/a' as "city",
							'UNK' as "country_code"
						FOR XML PATH ('address'),type, ELEMENTS)
					) 
					else null 
				end as "signatory/t_person/addresses",
				case when se.kd_htyp_cd<3 then 
					isnull(spi.identification, 
						(select
							4 as "type",
							'n/a' as "number",
							'UNK' as "issue_country",
							'n/a' as "comments"
						FOR XML PATH (''),type, ELEMENTS)
					) 
					else null 
				end as "signatory/t_person/identification",
				case when se.kd_htyp_cd<3 then 
					case 
						when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
							or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
						then 1
						else NULL
					end 
					else null 
				end as "signatory/t_person/deceased",
				case when se.kd_htyp_cd<3 then 
					case 
						when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
						then 
							case 
								when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
								then NULL 
								else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
							end 
						else left(convert(char,se.TOD_DAT,126),19)
					end 
					else null 
				end as "signatory/t_person/date_deceased",
				case 
					when se.kd_htyp_cd<3 
					then 
						FAF.faf_MROS_char_process(														
							case
								when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
								then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
								else NULL
							end
						,NULL,4000)
					else NULL 
				end as "signatory/t_person/comments",
				case when se.kd_htyp_cd<3 then 1 else null end as "signatory/role" 
				from 
					FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
				left join 
					FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
				on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
				left join
					FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
				on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
				left join
					FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
				on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
				left join
					FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
				on cntr1.iso_land_cd=se.iso_land_code_nat
				left join
					FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
				on cntr2.iso_land_cd=se.iso_land_code_nat_2
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
				on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
				on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
				on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
				where se.kd_lnr=@DF_CUSTOMER_ID and se.userbk_nr=@F_BANK_ID and (se.partner_flag=0 or se.kd_htyp_cd=2)
				FOR XML PATH (''),type, ELEMENTS);

			if @xml_cl is null
				begin
					set @xml_cl = 
						(select
							'n/a' as "name",
							1 as "incorporation_legal_form",
							(select
								4 as "address_type",
								'n/a' as "address",
								'n/a' as "city",
								'UNK' as "country_code"
							FOR XML PATH ('address'),type, ELEMENTS) as "addresses",
							'UNK' as "incorporation_country_code",
							'yes' as "tax_reg_number"
						FOR XML PATH ('t_entity'),type, ELEMENTS);
				end;
			else
				begin
					--t_person (connected with the entity in KD_KD)
					set @xml_cl1 = 
						(select 
							isnull(gd.MROS_value,'U') as "t_person/gender",
							FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
							FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
							FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
							case 
								when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
								then 
									case 
										when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
										then '1900-01-01T01:00:00' 
										else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,sp.GEBURT_DAT,126),19)
							end as "t_person/birthdate",
							FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
							isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
							cntr2.iso_land_cd as "t_person/nationality2",
							--t_phone
							spp.phones as "t_person/phones",
							--t_address
							isnull(spa.addresses,
								(select
									3 as "address_type",
									'n/a' as "address",
									'n/a' as "city",
									'UNK' as "country_code"
								FOR XML PATH ('address'),type, ELEMENTS)
							) as "t_person/addresses",
							--t_person_identification
							isnull(spi.identification, 
								(select
									4 as "type",
									'n/a' as "number",
									'UNK' as "issue_country",
									'n/a' as "comments"
								FOR XML PATH (''),type, ELEMENTS)
							) as "t_person/identification",
							case 
								when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
									or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
								then 1
								else NULL
							end as "t_person/deceased",
							case 
								when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
								then 
									case 
										when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
										then NULL 
										else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,sp.TOD_DAT,126),19)
							end as "t_person/date_deceased",
							FAF.faf_MROS_char_process_(
								case
									when isnull(kdkd.significance,6)=6
									then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
									else ''
								end +
								case
									when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
									then 
										case
											when isnull(kdkd.significance,6)=6
											then '; ' + char(13) + char(10)
											else ''
										end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
									else ''
								end
							,NULL,4000) as "t_person/comments",
							isnull(kdkd.significance,6) as "role" 
						from 
							FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
						join
							(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
								case significance
									when 101 then 1
									when 107 then 7
									when 113 then 13
									when 114 then 14
									when 115 then 15
									when 117 then 17
									when 118 then 18
									when 119 then 19
									when 120 then 20
									when 121 then 21
									when 122 then 22
									when 123 then 23
									when 124 then 24
									when 125 then 25
									when 12882 then 19
									when 12198 then 20
									when 13110 then 21
									when 12995 then 22
									when 12305 then 23
									when 1481430 then 24
									when 13685 then 24
									when 13673 then 24
									when 13908 then 24
									when 1402770 then 25
									when 13800 then 25
									when 14022 then 25
									when 12947 then 25
									else 6
								end as significance
							from
								(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
									round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
									STUFF(
										(SELECT ', ' + t2.comments 
										FROM 
											(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
												case 
													when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
													then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
													else 6 
												end as significance
											from
												(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
													case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
												on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
												where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
												union all
												select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
													case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
												on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
												where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
												union all
												select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
													case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
												join
													FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
												on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
												on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
												where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
												union all
												select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
													case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
												join
													FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
											   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
												on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
												where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
											) t2 
										WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
										FOR XML PATH(''))
									, 1, LEN(', '), '') as comments
								from 
									(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
										case 
											when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
											then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
											else 6 
										end as significance
									from
										(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
											case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
										on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
										where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
										union all
										select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
											case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
										on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
										where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
										union all
										select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
											case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
										join
											FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
										on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
										on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
										where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
										union all
										select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
											case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
										join
											FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
									   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
										on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
										where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
									) t0
								group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
							) kdkd
						on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
						join
							FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
						on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
						left join
							FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
						on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
						left join
							FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
						on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
						left join
							FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
						on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
						left join
							FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
						on cntr1.iso_land_cd=sp.iso_land_code_nat
						left join
							FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
						on cntr2.iso_land_cd=sp.iso_land_code_nat_2
						left join
							FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
						on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
						where se.kd_lnr=@DF_CUSTOMER_ID and se.userbk_nr=@F_BANK_ID and se.partner_flag=0 and se.kd_htyp_cd=3
						FOR XML PATH ('signatory'),type, ELEMENTS);
				end;	
					
			set @xml_ac = 
				(select 
					@inst_name as institution_name,
					@swif as swift,
					isnull(stz.MROS_value,'ZH') as branch,
					FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as account,
					isnull(wrg.iso_wrg_cd,'CHF') as currency_code,
					FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as account_name,
					FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as iban,
					FAF.faf_MROS_char_process(@DF_CUSTOMER_NR,'n/a',30) as client_number,
					isnull(ac_tp.MROS_value,16) as personal_account_type,
					@xml_cl,@xml_cl1,
					case 
						when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
						then '1900-01-01T01:00:00'
						else left(convert(char,s5.EROEFF_DAT,126),19) 
					end as opened,
					case 
						when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
						then null 
						else left(convert(char,s5.AUFHEB_DAT,126),19)
					end as closed,
					case 
						when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
						then isnull(sld.saldo_bwrg,0) 
						else NULL
					end as balance,
					case 
						when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
						then left(convert(char,SYSDATETIME(),126),19)
						else NULL
					end as date_balance,
					case 
						when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
						then 2
						else 1
					end as status_code,
					case 
						when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
						then NULL
						else isnull(sld.saldo_kwrg,0)
					end as beneficiary,
					FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as beneficiary_comment,
					FAF.faf_MROS_char_process(
						case 
							when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
							then
								'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
								case
									when ac_tp.MROS_value=14
									then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
									else ''
								end
							else
								case
									when ac_tp.MROS_value=14
									then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
									else 'n/a'
								end
						end
					,'n/a',4000) as comments
				from 
					FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM s5 WITH (FORCESEEK)
				left join
					FAF_MBDATA_T.FAF.FA_MROS_KT_SALDO sld WITH (FORCESEEK)
				on s5.kt_lnr=sld.kt_lnr and s5.userbk_nr=sld.userbk_nr
				left join
					FAF_MBDATA_T.FAF.FA_MROS_INT_WRG wrg
				on s5.iso_wrg_cd=wrg.iso_wrg_cd
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE stz
				on s5.userbk_nr=stz.userbk_nr and s5.sitz_nr=stz.finnova_value and stz.ref_type=1 and stz.activ_MROS=1
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ac_tp
				on s5.userbk_nr=ac_tp.userbk_nr and s5.kt_art_ber_cd=ac_tp.finnova_value and ac_tp.ref_type=9 and ac_tp.activ_MROS=1
				where s5.kt_lnr=@DF_ACCOUNT_ID and s5.userbk_nr=@F_BANK_ID
				FOR XML PATH (''),type, ELEMENTS);

			if @xml_ac is null
			begin
				set @xml_ac = 
					(select 
						@inst_name as institution_name,
						@swif as swift,
						'ZH' as branch,
						'n/a' as account,
						'CHF' as currency_code,
						'n/a' as iban,
						FAF.faf_MROS_char_process(@DF_CUSTOMER_NR,'n/a',30) as client_number,
						16 as personal_account_type,
						@xml_cl,
						'1900-01-01T01:00:00'as opened,
						0 as "balance",
						left(convert(char,sysdatetime(),126),19) as "date_balance",
						4 as "status_code",
						'n/a' as beneficiary_comment						
					FOR XML PATH (''),type, ELEMENTS);
			end;

			set @xml_t = 
				(select 
					@DF_PAYMENT_ID as "transactionnumber",
					FAF.faf_MROS_char_process(@DF_ORDER_NR,NULL,50) as "internal_ref_number",
					FAF.faf_MROS_char_process_(@DF_DESCRIPTION,'n/a',4000) as "transaction_description",
					left(convert(char,isnull(@DF_DATE,SYSDATETIME()),126),19) as "date_transaction",
					'15' as "transmode_code", 
					case
						when abs(isnull(@DF_AMOUNT,0))<0.01 then 0.01
						else abs(isnull(@DF_AMOUNT,0.01))
					end as "amount_local",
					case 
						when isnull(@DF_DEBIT_CREDIT,'D')<>'C'
						then 
							(select
								1 as "from_funds_code",
								FAF.faf_MROS_char_process(@cur,'CHF',4) as "from_foreign_currency/foreign_currency_code",
								case
									when abs(isnull(@DF_AMOUNT,0))<0.01 then 0.01
									else abs(isnull(@DF_AMOUNT,0.01))
								end as "from_foreign_currency/foreign_amount",
								1 as "from_foreign_currency/foreign_exchange_rate",
								@xml_ac as "from_account"
							FOR XML PATH (''),type, ELEMENTS)
						else NULL 
					end as "t_from_my_client",
					
					case 
						when @DF_DEBIT_CREDIT='C' 
						then 
							(select 
								1 as "from_funds_code",
								FAF.faf_MROS_char_process(@DF_BANK_NAME,'n/a',255) as "from_account/institution_name",
								FAF.faf_MROS_char_process(@DF_SWIFT,'n/a',11) as "from_account/swift",
								FAF.faf_MROS_char_process(@DF_ACCOUNT,'n/a',50) as "from_account/account",
								FAF.faf_MROS_char_process_(@DF_COMMENTS,'n/a',4000) as "from_account/comments"
							FOR XML PATH (''),type, ELEMENTS)
						else NULL 
					end as "t_from",
					
					case 
						when @DF_DEBIT_CREDIT='C'
						then 
							(select 
								1 as "to_funds_code",
								FAF.faf_MROS_char_process(@cur,'CHF',4) as "to_foreign_currency/foreign_currency_code",
								case
									when abs(isnull(@DF_AMOUNT,0))<0.01 then 0.01
									else abs(isnull(@DF_AMOUNT,0.01))
								end as "to_foreign_currency/foreign_amount",
								1 as "to_foreign_currency/foreign_exchange_rate",
								@xml_ac as "to_account"
							FOR XML PATH (''),type, ELEMENTS)
						else NULL 
					end as "t_to_my_client",

					case 
						when isnull(@DF_DEBIT_CREDIT,'D')<>'C' 
						then 
							(select 
								1 as "to_funds_code",
								FAF.faf_MROS_char_process(@DF_BANK_NAME,'n/a',255) as "to_account/institution_name",
								FAF.faf_MROS_char_process(@DF_SWIFT,'n/a',11) as "to_account/swift",
								FAF.faf_MROS_char_process(@DF_ACCOUNT,'n/a',50) as "to_account/account",
								FAF.faf_MROS_char_process_(@DF_COMMENTS,'n/a',4000) as "to_account/comments"
							FOR XML PATH (''),type, ELEMENTS)
						else NULL 
					end as "t_to",

					case 
						when @DF_AMOUNT<0
						then 'Cancellation / Refund'					 
						else NULL
					end as "comments"
				FOR XML PATH ('transaction'),type, ELEMENTS);
		end;

		--remain part of the report
		set @xml_r =FAF.head_part_of_report (@F_BANK_ID,@RP_NAME,@RP_SURNAME,@RP_PHONE,@RP_EMAIL,@RP_OCCUPATION,@RP_ADDRESS,@RP_CITY,@FIU_REF_NUMBER,@ENTITY_REFERENCE,@REPORT_REASON,@R_ACTION,@ind,@xml_t,1);
	end;

	return(@xml_r);
end;
go	


---------------------------------------------------------------------------------------------------


--SAR report (collection of accounts and clients)
IF OBJECT_ID('[FAF].[report_activity]', 'FN') IS NOT NULL
	  DROP function [FAF].[report_activity]; 
go
create function FAF.report_activity (
	@F_BANK_ID int,
	@RP_NAME varchar(100),
	@RP_SURNAME varchar(100),
	@RP_PHONE varchar(50),
	@RP_EMAIL varchar(50),
	@RP_OCCUPATION varchar(255),
	@RP_ADDRESS varchar(100),
	@RP_CITY varchar(255),
	@FIU_REF_NUMBER varchar(255),
	@ENTITY_REFERENCE varchar(255),	
	@REPORT_REASON varchar(4000),
	@R_ACTION varchar(4000),
	@REPORT_INDICATORS FAF.FAF_MROS_charID_t READONLY,
	@SET_OF_ACCOUNTS FAF.FAF_MROS_IdRsn_t READONLY,
	@SET_OF_CLIENTS FAF.FAF_MROS_IdRsn_t READONLY) 
returns xml
as
begin
	declare @xml_p xml;
	declare @xml_e xml;
	declare @xml_a xml;
	declare @xml_t xml;
	declare @xml_r xml;
	declare @inst_name varchar(255);
	declare @swif varchar(11);
	declare @n_id int;
	declare @s_reason varchar(4000);
	declare @ind FAF.FAF_MROS_charID_t;
	declare @str varchar(4000);
	declare @n int;
	declare @m int;
	declare @AC_NR FAF.FAF_MROS_IdRsn_t;
	declare @Cl_NR FAF.FAF_MROS_IdRsn_t;
	declare @thn FAF.FAF_MROS_techn1_t;
	declare @thn1 FAF.FAF_MROS_techn_t;
	declare @thn2 FAF.FAF_MROS_techn_t;
	declare @max_items int;
	set @max_items = 1000;
	
	--indicators checking
	insert into @ind
		select indicator from FAF_MBDATA_T.FAF.FA_MROS_INT_INDIZ where indicator in (select ltrim(rtrim(chID)) from @REPORT_INDICATORS);
	set @str = '';
	if (select count(*) from @ind where right(chID,1)='M')<>1 
		set @str=@str + 'the report must include only one indicator with type Report_Type (M)';
	if (select count(*) from @ind where right(chID,1)='V')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Predicate_Offenses (V)';
	if (select count(*) from @ind where right(chID,1)='G')<1
		set @str = case when len(@str)=0 then '' else @str+', ' end + 'the report must include at least one indicator with type Reason (G)';
	if len(@str)>0
		set @str = 'Indicators error: ' + @str + '; ';

	--table FA_MROS_INT_PERS_CHARGE checking
	if (select count(*) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID)=0
		set @str = @str + 'The table FAF_MROS_persons_in_charge does not have the record with userbk_nr_home = ' + cast(@F_BANK_ID as varchar) + '; ';

	--input sets checking
	set @m = (select count(*) from @SET_OF_ACCOUNTS);
	set @n = (select count(*) from @SET_OF_CLIENTS);
	if @m+@n=0
		set @str = @str + 'At least one account/client number should be provided; ';

	--Error processing
	if len(@str)>0
		set @xml_r= (select @str as "mros_report_error" FOR XML PATH (''),type, ELEMENTS);

	--if there is no error then start the main request
	if @xml_r is null
	begin
		--Bank name and swift code from the technical table
		set @inst_name = (select top 1 FAF.faf_MROS_char_process(institution_name,'n/a',255) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);
		set @swif = (select top 1 FAF.faf_MROS_char_process(swift,'n/a',11) from FAF_MBDATA_T.FAF.FA_MROS_INT_PERS_CHARGE where userbk_nr_home=@F_BANK_ID);

		--accounts
		if @m=0
			begin
				insert into @AC_NR
				select top (@max_items) ac.edit_kt_nr as chID,NULL as partner_flag, s.rsn
				from 
					@SET_OF_CLIENTS s
				join
					FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM cl
				on cl.edit_kd_nr=s.chID and cl.userbk_nr=@F_BANK_ID and cl.partner_flag=0
				join
					FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM ac				
				on ac.userbk_nr=cl.userbk_nr and ac.kd_lnr=cl.kd_lnr;
			end;
		else
			insert into @AC_NR select top (@max_items) chID,NULL as partner_flag, rsn from @SET_OF_ACCOUNTS;

		set @m = (select count(*) from @AC_NR);
		if @m>0
		begin
			set @xml_a = 
				(select
					(select
						@inst_name as "institution_name",
						@swif as "swift",
						isnull(stz.MROS_value,'ZH') as "branch",
						FAF.faf_MROS_char_process(s5.edit_kt_nr,'n/a',50) as "account",
						isnull(wrg.iso_wrg_cd,'CHF') as "currency_code",
						FAF.faf_MROS_char_process(s5.kt_art_nr_bez,NULL,255) as "account_name",
						FAF.faf_MROS_char_process(s5.iban_id,'n/a',34) as "iban",
						FAF.faf_MROS_char_process(cl.edit_kd_nr,'n/a',30) as "client_number",
						isnull(ac_tp.MROS_value,16) as "personal_account_type",
						--account owner
						isnull(
							(select
								--t_entity
								case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else NULL end as "t_entity/name",
								case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else NULL end as "t_entity/incorporation_legal_form",
								case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else NULL end as "t_entity/incorporation_number",
								case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else NULL end as "t_entity/business",
								case when se.kd_htyp_cd=3 then 
									isnull(sea.addresses,
										(select
											4 as "address_type",
											'n/a' as "address",
											'n/a' as "city",
											'UNK' as "country_code"
										FOR XML PATH ('address'),type, ELEMENTS)
									) 
									else NULL 
								end as "t_entity/addresses",
								case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else NULL end as "t_entity/url",
								case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "t_entity/incorporation_state",
								case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else NULL end as "t_entity/incorporation_country_code",
								case when se.kd_htyp_cd=3 then 
									case 
										when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
										then 
											case 
												when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
												then NULL 
												else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
											end 
										else left(convert(char,se.GEBURT_DAT,126),19)
									end 
									else NULL 
								end as "t_entity/incorporation_date",
								case when se.kd_htyp_cd=3 then 
									case 
										when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
										then 1 else NULL
									end 
									else NULL 
								end as "t_entity/business_closed",
								case when se.kd_htyp_cd=3 then 
									case 
										when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
										then 
											case 
												when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
												then NULL 
												else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
											end 
										else left(convert(char,se.TOD_DAT,126),19)
									end 
									else NULL 
								end as "t_entity/date_business_closed",
								case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else NULL end as "t_entity/tax_reg_number",
								case 
									when se.kd_htyp_cd=3 
									then 
										FAF.faf_MROS_char_process_(
											case
												when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
												then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
												else ''
											end + 
											case 
												when lf.MROS_value=4 
												then 
													case
														when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
														then '; ' + char(13) + char(10)
														else ''
													end + FAF.faf_MROS_char_process(lf.comments,'n/a',4000)
												else ''
											end
										,NULL,4000) 
									else NULL 
								end as "t_entity/comments",
								--t_person
								case when se.kd_htyp_cd<3 then case when se.PARTNER_FLAG=0 then 1 else Null end else NULL end as "signatory/is_primary",
								case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else NULL end as "signatory/t_person/gender",
								case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else NULL end as "signatory/t_person/title",
								case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else NULL end as "signatory/t_person/first_name",
								case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else NULL end as "signatory/t_person/last_name",
								case when se.kd_htyp_cd<3 then 
									case 
										when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
										then 
											case 
												when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
												then null 
												else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
											end 
										else left(convert(char,se.GEBURT_DAT,126),19)
									end 
									else NULL 
								end as "signatory/t_person/birthdate",
								case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else NULL end as "signatory/t_person/birth_place",
								case when se.kd_htyp_cd<3 then cntr1.iso_land_cd else NULL end as "signatory/t_person/nationality1",
								case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else NULL end as "signatory/t_person/nationality2",
								case when se.kd_htyp_cd<3 then spp.phones else NULL end as "signatory/t_person/phones",
								case when se.kd_htyp_cd<3 then sea.addresses else NULL end as "signatory/t_person/addresses",
								case when se.kd_htyp_cd<3 then spi.identification else NULL end as "signatory/t_person/identification",
								case when se.kd_htyp_cd<3 then 
									case 
										when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
											or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
										then 1
										else NULL
									end 
									else NULL 
								end as "signatory/t_person/deceased",
								case when se.kd_htyp_cd<3 then 
									case 
										when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
										then 
											case 
												when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
												then NULL 
												else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
											end 
										else left(convert(char,se.TOD_DAT,126),19)
									end 
									else NULL 
								end as "signatory/t_person/date_deceased",
								case 
									when se.kd_htyp_cd<3 
									then 
										FAF.faf_MROS_char_process(														
											case
												when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
												then 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
												else NULL
											end
										,NULL,4000)
									else NULL 
								end as "signatory/t_person/comments",
								case when se.kd_htyp_cd<3 then 1 else NULL end as "signatory/role" 
							from 
								FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
							left join 
								FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
							on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
							left join
								FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
							on spp.kd_lnr=se.kd_lnr and spp.userbk_nr=se.userbk_nr
							left join
								FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
							on spi.kd_lnr=se.kd_lnr and spi.userbk_nr=se.userbk_nr
							left join
								FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
							on cntr1.iso_land_cd=se.iso_land_code_nat
							left join
								FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
							on cntr2.iso_land_cd=se.iso_land_code_nat_2
							left join
								FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
							on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
							left join
								FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
							on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
							left join
								FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
							on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
							where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and (se.partner_flag=0 or se.kd_htyp_cd=2)
							FOR XML PATH (''),type, ELEMENTS)
							,(select 
								'n/a' as "t_entity/name",
								1 as "t_entity/incorporation_legal_form",
									4 as "t_entity/addresses/address/address_type",
									'n/a' as "t_entity/addresses/address/address",
									'n/a' as "t_entity/addresses/address/city",
									'UNK' as "t_entity/addresses/address/country_code",
								'UNK' as "t_entity/incorporation_country_code",
								'yes' as "t_entity/tax_reg_number"
							FOR XML PATH (''),type, ELEMENTS)
						),
						--t_person (connected with the entity in KD_KD)
						(select 
							isnull(gd.MROS_value,'U') as "t_person/gender",
							FAF.faf_MROS_char_process(sp.titel,NULL,30) as "t_person/title",
							FAF.faf_MROS_char_process(sp.VORNAME,'n/a',100) as "t_person/first_name",
							FAF.faf_MROS_char_process(sp.name_1,'n/a',100) as "t_person/last_name",
							case 
								when sp.GEBURT_DAT is null or sp.GEBURT_DAT < cast('1753-01-01' as date) or sp.GEBURT_DAT>sysdatetime()
								then 
									case 
										when sp.GEBURT_JAHR is null or sp.GEBURT_JAHR<1753 or sp.GEBURT_JAHR>year(sysdatetime())
										then '1900-01-01T01:00:00' 
										else cast(sp.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,sp.GEBURT_DAT,126),19)
							end as "t_person/birthdate",
							FAF.faf_MROS_char_process(sp.geburt_ort,NULL,255) as "t_person/birth_place",
							isnull(cntr1.iso_land_cd,'UNK') as "t_person/nationality1",
							cntr2.iso_land_cd as "t_person/nationality2",
							--t_phone
							spp.phones as "t_person/phones",
							--t_address
							isnull(spa.addresses,
								(select
									3 as "address_type",
									'n/a' as "address",
									'n/a' as "city",
									'UNK' as "country_code"
								FOR XML PATH ('address'),type, ELEMENTS)
							) as "t_person/addresses",
							--t_person_identification
							isnull(spi.identification, 
								(select
									4 as "type",
									'n/a' as "number",
									'UNK' as "issue_country",
									'n/a' as "comments"
								FOR XML PATH (''),type, ELEMENTS)
							) as "t_person/identification",
							case 
								when (sp.TOD_DAT >= cast('1753-01-01' as date) and sp.TOD_DAT<=sysdatetime())
									or (sp.TOD_JAHR>=1753 and sp.TOD_JAHR<=year(sysdatetime()))
								then 1
								else NULL
							end as "t_person/deceased",
							case 
								when sp.TOD_DAT is null or sp.TOD_DAT < cast('1753-01-01' as date) or sp.TOD_DAT>sysdatetime()
								then 
									case 
										when sp.TOD_JAHR is null or sp.TOD_JAHR<1753 or sp.TOD_JAHR>year(sysdatetime())
										then NULL 
										else cast(sp.TOD_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,sp.TOD_DAT,126),19)
							end as "t_person/date_deceased",
							FAF.faf_MROS_char_process_(
								case
									when isnull(kdkd.significance,6)=6
									then 'Role of the client is ' + FAF.faf_MROS_char_process(kdkd.comments,'n/a',4000)
									else ''
								end +
								case
									when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(sp.iso_land_code_nat,NULL,4000) is not null
									then 
										case
											when isnull(kdkd.significance,6)=6
											then '; ' + char(13) + char(10)
											else ''
										end + 'Actual country is ' + FAF.faf_MROS_char_process(sp.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(sp.iso_land_code_nat,'',4000) + ')'
									else ''
								end
							,NULL,4000) as "t_person/comments",
							isnull(kdkd.significance,6) as "role" 
						from 
							FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
						join
							(select userbk_nr,kd_lnr,kd_lnr_zuw,comments,
								case significance
									when 101 then 1
									when 107 then 7
									when 113 then 13
									when 114 then 14
									when 115 then 15
									when 117 then 17
									when 118 then 18
									when 119 then 19
									when 120 then 20
									when 121 then 21
									when 122 then 22
									when 123 then 23
									when 124 then 24
									when 125 then 25
									when 12882 then 19
									when 12198 then 20
									when 13110 then 21
									when 12995 then 22
									when 12305 then 23
									when 1481430 then 24
									when 13685 then 24
									when 13673 then 24
									when 13908 then 24
									when 1402770 then 25
									when 13800 then 25
									when 14022 then 25
									when 12947 then 25
									else 6
								end as significance
							from
								(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,
									round(exp(sum(log(cast(significance as decimal(38,20))+100))),0) as significance,
									STUFF(
										(SELECT ', ' + t2.comments 
										FROM 
											(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
												case 
													when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
													then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
													else 6 
												end as significance
											from
												(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
													case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
												on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
												where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
												union all
												select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
													case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
												on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
												where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
												union all
												select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
													case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
												join
													FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
												on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
												on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
												where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
												union all
												select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
													case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
												from 
													FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
												join
													FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
											   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
												join
													FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
												on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
												where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
											) t2 
										WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw 
										FOR XML PATH(''))
									, 1, LEN(', '), '') as comments
								from 
									(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
										case 
											when isnumeric(FAF.faf_MROS_char_process(mros_value,'6',2))>0 
											then cast(FAF.faf_MROS_char_process(mros_value,'6',2) as int) 
											else 6 
										end as significance
									from
										(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
											case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
										on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=14 and rt1.activ_MROS=1
										where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
										union all
										select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
											case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
										on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=15 and rt2.activ_MROS=1
										where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
										union all
										select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
											case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
										join
											FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
										on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
										on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
										where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
										union all
										select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
											case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
										from 
											FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
										join
											FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
									   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
										join
											FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
										on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
										where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1
									) t0
								group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd0
							) kdkd
						on se.kd_lnr=kdkd.kd_lnr and se.userbk_nr=kdkd.userbk_nr
						join
							FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
						on sp.kd_lnr=kdkd.kd_lnr_zuw and sp.userbk_nr=kdkd.userbk_nr and sp.kd_htyp_cd<3
						left join
							FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
						on spp.kd_lnr=sp.kd_lnr and spp.userbk_nr=sp.userbk_nr
						left join
							FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML spa WITH (FORCESEEK)
						on spa.kd_lnr=sp.kd_lnr and spa.userbk_nr=sp.userbk_nr
						left join
							FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML spi WITH (FORCESEEK)
						on spi.kd_lnr=sp.kd_lnr and spi.userbk_nr=sp.userbk_nr
						left join
							FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
						on cntr1.iso_land_cd=sp.iso_land_code_nat
						left join
							FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
						on cntr2.iso_land_cd=sp.iso_land_code_nat_2
						left join
							FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
						on sp.userbk_nr=gd.userbk_nr and sp.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
						where s5.kd_lnr=se.kd_lnr and s5.userbk_nr=se.userbk_nr and se.partner_flag=0 and se.kd_htyp_cd=3
						FOR XML PATH ('signatory'),type, ELEMENTS),
						case 
							when s5.EROEFF_DAT is null or s5.EROEFF_DAT<cast('1753-01-01' as date) or s5.EROEFF_DAT>sysdatetime()
							then null
							else left(convert(char,s5.EROEFF_DAT,126),19) 
						end as "opened",
						case 
							when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
							then null 
							else left(convert(char,s5.AUFHEB_DAT,126),19)
						end as "closed",
						case 
							when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
							then isnull(sld.saldo_bwrg,0) 
							else NULL
						end as "balance",
						case 
							when s5.AUFHEB_DAT is null or s5.AUFHEB_DAT<s5.EROEFF_DAT or s5.AUFHEB_DAT<cast('1753-01-01' as date) or s5.AUFHEB_DAT>sysdatetime()
							then left(convert(char,SYSDATETIME(),126),19)
							else NULL
						end as "date_balance",
						case 
							when (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime()) or s5.SPERR_CD=97 or s5.aufheb_cd='ZZ'
							then 2
							else 1
						end as "status_code",
						case 
							when s5.iso_wrg_cd='CHF' or (s5.AUFHEB_DAT is not null and s5.AUFHEB_DAT>=isnull(s5.EROEFF_DAT,cast('1753-01-01' as date)) and s5.AUFHEB_DAT>=cast('1753-01-01' as date) and s5.AUFHEB_DAT<=sysdatetime())
							then NULL
							else isnull(sld.saldo_kwrg,0)
						end as "beneficiary",
						FAF.faf_MROS_char_process(s5.sitz_nr_bez,'n/a',255) as "beneficiary_comment",
						FAF.faf_MROS_char_process(
							case 
								when wrg.iso_wrg_cd is null and FAF.faf_MROS_char_process(s5.wrg_bez,NULL,4000) is not null and FAF.faf_MROS_char_process(s5.iso_wrg_cd,NULL,4000) is not null
								then
									'Actual currency is ' + FAF.faf_MROS_char_process(s5.wrg_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(s5.iso_wrg_cd,'',4000) + '); ' +
									case
										when ac_tp.MROS_value=14
										then FAF.faf_MROS_char_process(ac_tp.comments,'',4000)
										else ''
									end
								else
									case
										when ac_tp.MROS_value=14
										then FAF.faf_MROS_char_process(ac_tp.comments,'n/a',4000)
										else 'n/a'
									end
							end
						,'n/a',4000) as "comments"
					from 
						(select * from
							(select *, row_number() over (partition by edit_kt_nr,userbk_nr order by kt_lnr) as rn from FAF_MBDATA_T.FAF.FA_MROS_KT_STAMM WITH (FORCESEEK) where userbk_nr=@F_BANK_ID and edit_kt_nr=s0.chID) kt
						where rn=1) s5 
					left join
						FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM cl WITH (FORCESEEK)
					on s5.kd_lnr=cl.kd_lnr and s5.userbk_nr=cl.userbk_nr and cl.PARTNER_FLAG=0
					left join
						FAF_MBDATA_T.FAF.FA_MROS_KT_SALDO sld WITH (FORCESEEK)
					on s5.kt_lnr=sld.kt_lnr and s5.userbk_nr=sld.userbk_nr
					left join
						FAF_MBDATA_T.FAF.FA_MROS_INT_WRG wrg
					on s5.iso_wrg_cd=wrg.iso_wrg_cd
					left join
						FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE stz
					on s5.userbk_nr=stz.userbk_nr and s5.sitz_nr=stz.finnova_value and stz.ref_type=1 and stz.activ_MROS=1
					left join
						FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE ac_tp
					on s5.userbk_nr=ac_tp.userbk_nr and s5.kt_art_ber_cd=ac_tp.finnova_value and ac_tp.ref_type=9 and ac_tp.activ_MROS=1					
					FOR XML PATH ('account'),type, ELEMENTS),
					10 as "significance",
					FAF.faf_MROS_char_process(s0.rsn,'n/a',4000) as "reason"
				from 
					@AC_NR s0
				FOR XML PATH ('report_party'),type, ELEMENTS);
		end;

		--clients
		if @n>0 and @max_items>@m
		begin
			insert into @CL_NR select top (@max_items-@m) chID,partner_flag,rsn from @SET_OF_CLIENTS;
				
			set @xml_e = 
				(select top (@max_items-@m)
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else null end as "entity/name",
					case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else null end  as "entity/incorporation_legal_form",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else null end as "entity/incorporation_number",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else null end as "entity/business",
					case when se.kd_htyp_cd=3 then 
						isnull(sea.addresses,
							(select
								4 as "address_type",
								'n/a' as "address",
								'n/a' as "city",
								'UNK' as "country_code"
							FOR XML PATH ('address'),type, ELEMENTS)
						) 
						else null 
					end as "entity/addresses",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else null end as "entity/url",
					case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else null end as "entity/incorporation_state",
					case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else null end as "entity/incorporation_country_code",
					case when se.kd_htyp_cd=3 then 
						case 
							when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
							then 
								case 
									when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
									then NULL 
									else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
								end 
							else left(convert(char,se.GEBURT_DAT,126),19)
						end 
						else null 
					end as "entity/incorporation_date",
					case when se.kd_htyp_cd=3 then 
						case 
							when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
							then 1 else NULL
						end 
						else null 
					end as "entity/business_closed",
					case when se.kd_htyp_cd=3 then 
						case 
							when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
							then 
								case 
									when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
									then NULL 
									else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
								end 
							else left(convert(char,se.TOD_DAT,126),19)
						end 
						else null 
					end as "entity/date_business_closed",
					case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else null end as "entity/tax_reg_number",
					case 
						when se.kd_htyp_cd=3 
						then 
							FAF.faf_MROS_char_process_(
								'Client number: ' + se.edit_kd_nr + 
								case
									when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
									then '; ' + char(13) + char(10) + 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
									else ''
								end +
								case 
									when lf.MROS_value=4 
									then '; ' + char(13) + char(10) + FAF.faf_MROS_char_process(lf.comments,'',4000)
									else ''
								end
							,NULL,4000) 
						else null 
					end as "entity/comments",
					case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else null end as "person/gender",
					case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else null end as "person/title",
					case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else null end as "person/first_name",
					case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else null end as "person/last_name",
					case when se.kd_htyp_cd<3 then 
						case 
							when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
							then 
								case 
									when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
									then null 
									else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
								end 
							else left(convert(char,se.GEBURT_DAT,126),19)
						end 
						else null 
					end as "person/birthdate",
					case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else null end as "person/birth_place",
					case when se.kd_htyp_cd<3 then cntr1.iso_land_cd else null end as "person/nationality1",
					case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else null end as "person/nationality2",
					case when se.kd_htyp_cd<3 then sp.phones else null end as "person/phones",
					case when se.kd_htyp_cd<3 then sea.addresses else null end as "person/addresses",
					case when se.kd_htyp_cd<3 then si.identification else null end as "person/identification",
					case 
						when se.kd_htyp_cd<3 
						then 
							case 
								when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
									or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
								then 1
								else NULL
							end 
						else null 
					end as "person/deceased",
					case 
						when se.kd_htyp_cd<3 
						then 
							case 
								when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
								then 
									case 
										when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
										then NULL 
										else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,se.TOD_DAT,126),19)
							end 
						else null 
					end as "person/date_deceased",
					case 
						when se.kd_htyp_cd<3 
						then 
							FAF.faf_MROS_char_process_(
								'Client number: ' + se.edit_kd_nr + 
								case 
									when se.kd_htyp_cd=2 
									then 
										case
											when se.partner_flag = 0
											then ', 1st partner'  
											else ', 2nd partner'
										end
									else '' 
								end +
								case
									when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
									then '; ' + char(13) + char(10) + 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
									else ''
								end
							,NULL,4000)
						else NULL 
					end as "person/comments",
					10 as "significance",
					FAF.faf_MROS_char_process(s0.rsn,'n/a',4000) as "reason"
				from 
					@CL_NR s0
				join 
					FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
				on se.edit_kd_nr=s0.chID and se.userbk_nr=@F_BANK_ID and (s0.partner_flag is null or se.kd_htyp_cd<>2 or se.partner_flag=s0.partner_flag)
				left join 
					FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
				on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
				left join	
					FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML si WITH (FORCESEEK)
				on se.kd_lnr=si.kd_lnr and se.userbk_nr=si.userbk_nr
				left join
					FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML sp WITH (FORCESEEK)
				on se.kd_lnr=sp.kd_lnr and se.userbk_nr=sp.userbk_nr
				left join
					FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
				on cntr1.iso_land_cd=se.iso_land_code_nat
				left join
					FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
				on cntr2.iso_land_cd=se.iso_land_code_nat_2
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
				on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
				on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
				left join
					FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
				on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
				FOR XML PATH ('report_party'),type, ELEMENTS);

			set @n = @m+@n;
			if @max_items>@n
			begin
				with
					KD_KD as
						(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
							case 
								when isnumeric(FAF.faf_MROS_char_process(mros_value,'10',2))>0 
								then cast(FAF.faf_MROS_char_process(mros_value,'10',2) as int) 
								else 10 
							end as significance
						from
							(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
								case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
							from 
								FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
							join
								FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
							on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=12 and rt1.activ_MROS=1
							where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
							union all
							select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
								case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
							from 
								FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
							join
								FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
							on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=13 and rt2.activ_MROS=1
							where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
							union all
							select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
								case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
							from 
								FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
							join
								FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
						   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
							join
								FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
							on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
							where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
							union all
							select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
								case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
							from 
								FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
							join
								FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
						   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
							join
								FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
							on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
							where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1)
				insert into @thn1 
				select kdkd.kd_lnr_zuw as ID,
					case when kdkd.cnt=1 then kdkd.significance else 10 end as significance,
					'The client is related to the client ' + sp.kd_bez + ', role: ' + kdkd.comments as comments,
					1 as lvl
				from 
					@CL_NR s0
				join 
					FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
				on sp.edit_kd_nr=s0.chID and sp.userbk_nr=@F_BANK_ID and sp.partner_flag=0
				join
					(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,count(*) as cnt,max(significance) as significance,
						STUFF((SELECT ', ' + t2.comments FROM KD_KD t2 WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw FOR XML PATH('')), 1, LEN(', '), '') as comments
					from KD_KD t0
					group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd
				on sp.kd_lnr=kdkd.kd_lnr and sp.userbk_nr=kdkd.userbk_nr
				join
					FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
				on se.kd_lnr=kdkd.kd_lnr_zuw and se.userbk_nr=kdkd.userbk_nr and se.partner_flag=0;

				insert into @thn
				select kdkd.kd_lnr ID0,kdkd.kd_lnr_zuw as ID1
				from 
					@CL_NR s0
				join 
					FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
				on sp.edit_kd_nr=s0.chID and sp.userbk_nr=@F_BANK_ID and sp.partner_flag=0 and sp.kd_htyp_cd=2
				join
					(select distinct userbk_nr,kd_lnr,kd_lnr_zuw from
						(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw
						from 
							FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
						join
							FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
						on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=12 and rt1.activ_MROS=1 and rt1.special_mark='DOPPELPERSON'
						where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
						union all
						select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw
						from 
							FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
						join
							FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
						on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=13 and rt2.activ_MROS=1 and rt2.special_mark='DOPPELPERSON'
						where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw) k1
					) kdkd
				on sp.kd_lnr=kdkd.kd_lnr and sp.userbk_nr=kdkd.userbk_nr;

				if (select count(*) from @thn)>0
				begin
					with
						KD_KD as
							(select distinct userbk_nr,kd_lnr,kd_lnr_zuw,comments,
								case 
									when isnumeric(FAF.faf_MROS_char_process(mros_value,'10',2))>0 
									then cast(FAF.faf_MROS_char_process(mros_value,'10',2) as int) 
									else 10 
								end as significance
							from
								(select kdkd1.userbk_nr,kdkd1.kd_lnr,kdkd1.kd_lnr_zuw,rt1.mros_value,
									case when FAF.faf_MROS_char_process(rt1.special_mark,NULL,4000) is null then rt1.comments else rt1.special_mark end as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd1
								join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt1
								on kdkd1.kd_kd_verb_cd=rt1.Finnova_value and rt1.userbk_nr=kdkd1.userbk_nr and rt1.ref_type=12 and rt1.activ_MROS=1
								where kdkd1.valid_bis_dat>=sysdatetime() and kdkd1.kd_lnr<>kdkd1.kd_lnr_zuw
								union all
								select kdkd2.userbk_nr,kdkd2.kd_lnr_zuw as kd_lnr,kdkd2.kd_lnr as kd_lnr_zuw,rt2.mros_value,
									case when FAF.faf_MROS_char_process(rt2.special_mark,NULL,4000) is null then rt2.comments else rt2.special_mark end as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_KD_KD kdkd2
								join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt2
								on kdkd2.kd_kd_verb_cd=rt2.Finnova_value and rt2.userbk_nr=kdkd2.userbk_nr and rt2.ref_type=13 and rt2.activ_MROS=1
								where kdkd2.valid_bis_dat>=sysdatetime() and kdkd2.kd_lnr<>kdkd2.kd_lnr_zuw
								union all
								select vts.userbk_nr,vts.kd_lnr as kd_lnr,vtz.kd_lnr as kd_lnr_zuw,rt3.mros_value,
									case when FAF.faf_MROS_char_process(rt3.special_mark,NULL,4000) is null then rt3.comments else rt3.special_mark end as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
								join
									FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
							   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
								join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt3
								on vts.vt_typ_cd=rt3.Finnova_value and rt3.userbk_nr=vts.userbk_nr and rt3.ref_type=18 and rt3.activ_MROS=1
								where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr
								union all
								select vts.userbk_nr,vtz.kd_lnr as kd_lnr,vts.kd_lnr as kd_lnr_zuw,rt4.mros_value,
									case when FAF.faf_MROS_char_process(rt4.special_mark,NULL,4000) is null then rt4.comments else rt4.special_mark end as comments
								from 
									FAF_MBDATA_T.FAF.FA_MROS_VT_STAMM vts
								join
									FAF_MBDATA_T.FAF.FA_MROS_VT_ZUORD vtz
							   	on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr=vtz.vt_lnr
								join
									FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE rt4
								on vts.vt_typ_cd=rt4.Finnova_value and rt4.userbk_nr=vts.userbk_nr and rt4.ref_type=19 and rt4.activ_MROS=1
								where vts.ende_dat>=sysdatetime() and vts.kd_lnr<>vtz.kd_lnr) k1)
					insert into @thn1 
					select kdkd.kd_lnr_zuw as ID,
						case when kdkd.cnt=1 then kdkd.significance else 10 end as significance,
						'The client is related to the client ' + sp.kd_bez + ', role: ' + kdkd.comments as comments,
						2 as lvl
					from 
						@thn s0
					join 
						FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM sp WITH (FORCESEEK)
					on sp.kd_lnr=s0.ID1 and sp.userbk_nr=@F_BANK_ID and sp.partner_flag=0
					join
						(select t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw,count(*) as cnt,max(significance) as significance,
							STUFF((SELECT ', ' + t2.comments FROM KD_KD t2 WHERE t0.userbk_nr=t2.userbk_nr and t0.kd_lnr=t2.kd_lnr and t0.kd_lnr_zuw=t2.kd_lnr_zuw FOR XML PATH('')), 1, LEN(', '), '') as comments
						from KD_KD t0
						group by t0.userbk_nr,t0.kd_lnr,t0.kd_lnr_zuw) kdkd
					on sp.kd_lnr=kdkd.kd_lnr and sp.userbk_nr=kdkd.userbk_nr
					join
						FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
					on se.kd_lnr=kdkd.kd_lnr_zuw and se.userbk_nr=kdkd.userbk_nr and se.partner_flag=0
					where kdkd.kd_lnr_zuw<>s0.ID0;
				end;
		
				insert into @thn2
				select top (@max_items-@n) ID,
					case when cnt=1 then significance else 10 end as significance,
					comments, lvl	
				from
					(select t0.ID,count(*) as cnt,max(significance) as significance,min(lvl) as lvl,
						STUFF((SELECT '; ' + t2.comments FROM @thn1 t2 WHERE t2.ID=t0.ID FOR XML PATH('')), 1, LEN('; '), '') as comments
					from @thn1 t0
					group by t0.ID) s;	

				set @xml_p = 
					(select 
						case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.kd_bez,'n/a',255) else null end as "entity/name",
						case when se.kd_htyp_cd=3 then isnull(lf.MROS_value,1) else null end  as "entity/incorporation_legal_form",
						case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.UID_NR,NULL,50) else null end as "entity/incorporation_number",
						case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.noga_bez,NULL,255) else null end as "entity/business",
						case when se.kd_htyp_cd=3 then 
							isnull(sea.addresses,
								(select
									4 as "address_type",
									'n/a' as "address",
									'n/a' as "city",
									'UNK' as "country_code"
								FOR XML PATH ('address'),type, ELEMENTS)
							) 
							else null 
						end as "entity/addresses",
						case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.EMAIL_ADR,NULL,255) else null end as "entity/url",
						case when se.kd_htyp_cd=3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else null end as "entity/incorporation_state",
						case when se.kd_htyp_cd=3 then isnull(cntr1.iso_land_cd,'UNK') else null end as "entity/incorporation_country_code",
						case when se.kd_htyp_cd=3 then 
							case 
								when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
								then 
									case 
										when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
										then NULL 
										else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,se.GEBURT_DAT,126),19)
							end 
							else null 
						end as "entity/incorporation_date",
						case when se.kd_htyp_cd=3 then 
							case 
								when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime()) or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
								then 1 else NULL
							end 
							else null 
						end as "entity/business_closed",
						case when se.kd_htyp_cd=3 then 
							case 
								when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
								then 
									case 
										when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
										then NULL 
										else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,se.TOD_DAT,126),19)
							end 
							else null 
						end as "entity/date_business_closed",
						case when se.kd_htyp_cd=3 then isnull(tax.MROS_value,'yes') else null end as "entity/tax_reg_number",
						case 
							when se.kd_htyp_cd=3 
							then 
								FAF.faf_MROS_char_process_(
									'Client number: ' + se.edit_kd_nr + 
									case
										when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
										then '; ' + char(13) + char(10) + 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
										else ''
									end +
									case 
										when lf.MROS_value=4 
										then '; ' + char(13) + char(10) + FAF.faf_MROS_char_process(lf.comments,'',4000)
										else ''
									end
								,NULL,4000) 
							else null 
						end as "entity/comments",
						case when se.kd_htyp_cd<3 then isnull(gd.MROS_value,'U') else null end as "person/gender",
						case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.titel,NULL,30) else null end as "person/title",
						case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.VORNAME,'n/a',100) else null end as "person/first_name",
						case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.name_1,'n/a',100) else null end as "person/last_name",
						case when se.kd_htyp_cd<3 then 
							case 
								when se.GEBURT_DAT is null or se.GEBURT_DAT < cast('1753-01-01' as date) or se.GEBURT_DAT>sysdatetime()
								then 
									case 
										when se.GEBURT_JAHR is null or se.GEBURT_JAHR<1753 or se.GEBURT_JAHR>year(sysdatetime())
										then null 
										else cast(se.GEBURT_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,se.GEBURT_DAT,126),19)
							end 
							else null 
						end as "person/birthdate",
						case when se.kd_htyp_cd<3 then FAF.faf_MROS_char_process(se.geburt_ort,NULL,255) else null end as "person/birth_place",
						case when se.kd_htyp_cd<3 then cntr1.iso_land_cd else null end as "person/nationality1",
						case when se.kd_htyp_cd<3 then cntr2.iso_land_cd else null end as "person/nationality2",
						case when se.kd_htyp_cd<3 then spp.phones else null end as "person/phones",
						case when se.kd_htyp_cd<3 then sea.addresses else null end as "person/addresses",
						case when se.kd_htyp_cd<3 then si.identification else null end as "person/identification",
						case when se.kd_htyp_cd<3 then 
							case 
								when (se.TOD_DAT >= cast('1753-01-01' as date) and se.TOD_DAT<=sysdatetime())
									or (se.TOD_JAHR>=1753 and se.TOD_JAHR<=year(sysdatetime()))
								then 1
								else NULL
							end 
							else null 
						end as "person/deceased",
						case when se.kd_htyp_cd<3 then 
							case 
								when se.TOD_DAT is null or se.TOD_DAT < cast('1753-01-01' as date) or se.TOD_DAT>sysdatetime()
								then 
									case 
										when se.TOD_JAHR is null or se.TOD_JAHR<1753 or se.TOD_JAHR>year(sysdatetime())
										then NULL 
										else cast(se.TOD_JAHR as varchar) + '-01-01T01:00:00' 
									end 
								else left(convert(char,se.TOD_DAT,126),19)
							end 
							else null 
						end as "person/date_deceased",
						case 
							when se.kd_htyp_cd<3 
							then 
								FAF.faf_MROS_char_process_(
									'Client number: ' + se.edit_kd_nr + 
									case 
										when se.kd_htyp_cd=2 
										then 
											case
												when se.partner_flag = 0
												then ', 1st partner'  
												else ', 2nd partner'
											end
										else '' 
									end +
									case
										when cntr1.iso_land_cd is null and FAF.faf_MROS_char_process(se.land_lnr_nat_bez,NULL,4000) is not NULL and FAF.faf_MROS_char_process(se.iso_land_code_nat,NULL,4000) is not null
										then '; ' + char(13) + char(10) + 'Actual country is ' + FAF.faf_MROS_char_process(se.land_lnr_nat_bez,'',4000) + ' (' + FAF.faf_MROS_char_process(se.iso_land_code_nat,'',4000) + ')'
										else ''
									end
								,NULL,4000)
							else NULL 
						end as "person/comments",
						s0.significance as "significance",
						FAF.faf_MROS_char_process(s0.comments,'n/a',4000) as "reason"
					from 
						@thn2 s0
					join
						FAF_MBDATA_T.FAF.FA_MROS_KD_STAMM se WITH (FORCESEEK)
					on se.kd_lnr=s0.ID and se.userbk_nr=@F_BANK_ID
					left join 
						FAF_MBDATA_T.FAF.FA_MROS_KD_ADR_XML sea WITH (FORCESEEK)
					on sea.kd_lnr=se.kd_lnr and sea.userbk_nr=se.userbk_nr
					left join	
						FAF_MBDATA_T.FAF.FA_MROS_KD_VSB_XML si WITH (FORCESEEK)
					on se.kd_lnr=si.kd_lnr and se.userbk_nr=si.userbk_nr
					left join
						FAF_MBDATA_T.FAF.FA_MROS_KD_EMAIL_XML spp WITH (FORCESEEK)
					on se.kd_lnr=spp.kd_lnr and se.userbk_nr=spp.userbk_nr
					left join
						FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr1
					on cntr1.iso_land_cd=se.iso_land_code_nat
					left join
						FAF_MBDATA_T.FAF.FA_MROS_INT_LAND cntr2
					on cntr2.iso_land_cd=se.iso_land_code_nat_2
					left join
						FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE gd
					on se.userbk_nr=gd.userbk_nr and se.GESCHLECHT_CD=gd.finnova_value and gd.ref_type=4 and gd.activ_MROS=1
					left join
						FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE lf
					on se.userbk_nr=lf.userbk_nr and se.kd_rstat_cd=lf.finnova_value and lf.ref_type=8 and lf.activ_MROS=1
					left join
						FAF_MBDATA_T.FAF.FA_MROS_REFERENCE_TABLE tax
					on se.userbk_nr=tax.userbk_nr and se.KD_SITZ_GSCH_CD=tax.finnova_value and tax.ref_type=3 and tax.activ_MROS=1
					order by s0.lvl
					FOR XML PATH ('report_party'),type, ELEMENTS);					
			end;		
		end;


		--remain part of the report
		set @xml_t = 
			(select 
				(select 
					@xml_a,@xml_e,@xml_p
				FOR XML PATH ('report_parties'),type, ELEMENTS) 
			FOR XML PATH ('activity'),type, ELEMENTS);
		set @xml_r =FAF.head_part_of_report (@F_BANK_ID,@RP_NAME,@RP_SURNAME,@RP_PHONE,@RP_EMAIL,@RP_OCCUPATION,@RP_ADDRESS,@RP_CITY,@FIU_REF_NUMBER,@ENTITY_REFERENCE,@REPORT_REASON,@R_ACTION,@ind,@xml_t,0);
	end;

	return(@xml_r);
end;
go