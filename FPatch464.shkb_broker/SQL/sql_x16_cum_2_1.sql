--patch:sql_x16_cum_2_1.sql.start
--patch:sql270_0_broker.sql.start
USE MASTER;

DECLARE @is_broker_enabled BIT = 0;

SELECT
  @is_broker_enabled = is_broker_enabled
FROM sys.databases
WHERE NAME = 'staging_namecheck';

IF @is_broker_enabled = 0
BEGIN
  DECLARE @kill VARCHAR(8000) = '';
  SELECT
    @kill = @kill + 'kill ' + CONVERT(VARCHAR(5), session_id) + ';'
  FROM sys.dm_exec_sessions
  WHERE database_id = DB_ID('staging_namecheck');

  EXEC (@kill);

  ALTER DATABASE staging_namecheck
  SET ENABLE_BROKER;
END;

SELECT
  NAME + ':' +
  CASE
    WHEN is_broker_enabled = 1 THEN 'ok'
    ELSE 'failed'
  END
FROM sys.databases
WHERE NAME = 'staging_namecheck';
                   
--patch:sql270_0_broker.sql.finish
--patch:sql_x16_cum_2_1.sql.finish