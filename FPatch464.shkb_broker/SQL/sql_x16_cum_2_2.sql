--patch:sql_x16_cum_2_2.sql.start
--patch:sql270_1.sql.start
use staging_namecheck
GO

--from patch255.start
IF not EXISTS (SELECT * FROM syscolumns WHERE id = OBJECT_ID('NAMECHECK') AND name = 'PROFILE_NAME')
  ALTER TABLE NAMECHECK ADD PROFILE_NAME VARCHAR(255) NULL

GO

--from patch255.end
IF EXISTS(select 1  where   Object_id('dbo.[NC_Log]') is not NULL) drop table [dbo].[NC_Log]

CREATE TABLE [dbo].[NC_Log](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](1000) NULL,
	[DT] [datetime] NULL
) ON [PRIMARY]

GO
--patch:sql270_1.sql.finish
--patch:sql270_2_clear_old_mros.sql.start
use [staging_namecheck]
GO

IF EXISTS(select * from sys.services where name='ContractMROS_reciveService')   drop service ContractMROS_reciveService
IF EXISTS(select * from sys.services where name='ContractMROS_sendService')  drop service ContractMROS_sendService

IF EXISTS(select * from sys.services where name='ContractMROS_v2_reciveService') drop service ContractMROS_v2_reciveService
IF EXISTS(select * from sys.services where name='ContractMROS_v2_sendService') drop service ContractMROS_v2_sendService



IF EXISTS(select * from sys.services where name='broker_queue_recv') drop service broker_queue_recv
IF EXISTS(select * from sys.services where name='broker_queue_send') drop service broker_queue_send
IF EXISTS(select * from sys.services where name='ContractMROS_v2_recv') drop service ContractMROS_v2_recv
IF EXISTS(select * from sys.services where name='ContractMROS_v2_send') drop service ContractMROS_v2_send

IF EXISTS(select * from sys.services where name='ContractMROS_v2_reciveService') drop service ContractMROS_v2_reciveService
IF EXISTS(select * from sys.services where name='ContractMROS_v2_sendService') drop service ContractMROS_v2_sendService

IF EXISTS(select * from sys.service_queues where name='ContractMROS_reciveQueue')   drop queue ContractMROS_reciveQueue
IF EXISTS(select * from sys.service_queues where name='ContractMROS_sendQueue')   drop queue ContractMROS_sendQueue
IF EXISTS(select * from sys.service_queues where name='ContractMROS_v2_reciveQueue')   drop queue ContractMROS_v2_reciveQueue
IF EXISTS(select * from sys.service_queues where name='ContractMROS_v2_sendQueue')   drop queue [dbo].[ContractMROS_v2_sendQueue]
IF EXISTS(select * from sys.service_queues where name='ContractMROS_v2')   drop queue [dbo].ContractMROS_v2
IF EXISTS(select * from sys.service_queues where name='ContractMROS_v2_sendQueue')   drop queue [dbo].ContractMROS_v2_sendQueue
IF EXISTS(select * from sys.service_queues where name='ContractMROS_v2_recvQueue')   drop queue [dbo].ContractMROS_v2_recvQueue



IF EXISTS(select 1  where   Object_id('broker_queue') is not NULL) drop queue broker_queue



IF EXISTS(select * from sys.service_contracts where name='ContractMROS')  DROP CONTRACT [ContractMROS]
IF EXISTS(select * from sys.service_contracts where name='ContractMROS_v2') DROP CONTRACT [ContractMROS_v2]



IF  EXISTS(select * from sys.service_message_types WHERE name='TypeMROS_DATA') DROP MESSAGE TYPE TypeMROS_DATA
IF EXISTS(select * from sys.service_message_types WHERE name='TypeMROS_DATA_v2')  DROP MESSAGE TYPE TypeMROS_DATA_v2


IF EXISTS(select 1  where   Object_id('msBrokerServ') is not NULL) drop table [dbo].[msBrokerServ]


--Send_TypeMROS_DATA_v2Example 'hello word'

--patch:sql270_2_clear_old_mros.sql.finish

--patch:sql270_3_1_broker.sql.start

USE [staging_namecheck]
GO

/****** Object:  MessageType [TypeMROS_DATA_v2]    Script Date: 17.01.2020 11:37:37 ******/
CREATE MESSAGE TYPE [TypeMROS_DATA_v2] VALIDATION = NONE
GO



/****** Object:  ServiceContract [ContractMROS_v2]    Script Date: 17.01.2020 11:37:09 ******/
CREATE CONTRACT [ContractMROS_v2] ([TypeMROS_DATA_v2] SENT BY ANY)
GO



/****** Object:  ServiceQueue [ContractMROS_v2_reciveQueue]    Script Date: 17.01.2020 12:07:08 ******/
CREATE QUEUE [dbo].[ContractMROS_v2_recvQueue] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
GO

/****** Object:  ServiceQueue [ContractMROS_v2_sendQueue]    Script Date: 17.01.2020 12:04:35 ******/
CREATE QUEUE [dbo].[ContractMROS_v2_sendQueue] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
GO


/****** Object:  BrokerService [ContractMROS_v2_sendService]    Script Date: 17.01.2020 12:03:47 ******/
CREATE SERVICE [ContractMROS_v2_sendService]  ON QUEUE [dbo].[ContractMROS_v2_sendQueue] ([ContractMROS_v2])
GO




GO
CREATE SERVICE [ContractMROS_v2_reciveService]  ON QUEUE [dbo].ContractMROS_v2_recvQueue ([ContractMROS_v2])






--patch:sql270_3_1_broker.sql.finish

--patch:sql270_3_brokerServ.lua.start

USE [staging_namecheck]
GO


-- ----------------------------------------------MROS_unique_task_id
IF OBJECT_ID('[dbo].[MROS_unique_task_id]', 'SO') IS  NULL
 create sequence MROS_unique_task_id as bigint
	start with 1
	increment by 1;
go



-- ----------------------------------------------MROS_data_status
IF OBJECT_ID('MROS_data_status') IS  NULL BEGIN
CREATE TABLE [dbo].[MROS_data_status](
	[TaskID] [bigint] NOT NULL,
	[Status] [varchar](100) NULL,
	[open_dt] [datetime] NULL,
	[close_dt] [datetime] NULL,
	[filename] [varchar](521) NULL,
	[Comment] [varchar](4000) NULL,
	[Valid] [int] NULL,
 CONSTRAINT [PK_MROS_data_status] PRIMARY KEY CLUSTERED 
(
	[TaskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END


-- ----------------------------------------------BROKER_data_extractionStatus
IF not exists (SELECT * FROM sys.objects WHERE  name = 'BROKER_data_extractionStatus') BEGIN
CREATE TABLE [dbo].[BROKER_data_extractionStatus](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TaskID] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[dt] [datetime] NULL,
	[Comment] [varchar](100) NULL,
	[Valid] [int] NULL
) ON [PRIMARY]

/****** Object:  Index [BROKER_data_extractionStatus_index]    Script Date: 04.12.2019 13:42:05 ******/
CREATE NONCLUSTERED INDEX [BROKER_data_extractionStatus_index] ON [dbo].[BROKER_data_extractionStatus]
(
	[TaskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END


-- ----------------------------------------------BROKER_run_data_extraction_task
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'BROKER_run_data_extraction_task')
DROP PROCEDURE BROKER_run_data_extraction_task
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BROKER_run_data_extraction_task]
	
	@task varchar(4000),
	@taskid varchar(100)
AS
BEGIN

if @taskid is not NULL 
 INSERT INTO [dbo].[BROKER_data_extractionStatus]
           ([TaskID]
           ,[Status]
           ,[dt],
		   Valid)
     VALUES
           (@taskid
           ,'OPEN'
           ,GetDate(),1   )

exec [dbo].Send_TypeMROS_DATA_v2Example @task
END

GO

-- ----------------------------------------------BROKER_check_data_extraction_task_status
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'BROKER_check_data_extraction_task_status')
DROP PROCEDURE BROKER_check_data_extraction_task_status
GO

CREATE Procedure [dbo].[BROKER_check_data_extraction_task_status]
	
	@TaskId varchar(100)
AS
BEGIN
declare @status varchar(100)
select top 1 @status=STATUS from BROKER_data_extractionStatus where TaskID=@TaskId and Valid=1 order by dt desc

IF @status is NULL BEGIN
 set @status ='NOT FOUND TaskID'
end 
select @status as STATUS 
END



GO

-- ----------------------------------------------MROS_check_data_extraction_task_statusF
/****** Object:  UserDefinedFunction [dbo].[MROS_check_data_extraction_task_statusF]    Script Date: 04.12.2019 13:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
IF EXISTS (SELECT * FROM sys.objects WHERE  name = 'MROS_check_data_extraction_task_statusF')
DROP FUNCTION MROS_check_data_extraction_task_statusF
GO

CREATE FUNCTION [dbo].[MROS_check_data_extraction_task_statusF]
(	
	@TaskId varchar(100)
)
RETURNs varchar(100) 
	
AS
BEGIN

declare @status varchar(100)

select top 1 @status=STATUS from MROS_data_extractionStatus 
where TaskID=@TaskId and Valid=1 order by dt desc

IF @status is NULL BEGIN
 set @status ='NOT FOUND TaskID'
end 


RETURN @status
end


GO

-- ----------------------------------------------MROS_data_request_v2
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'MROS_data_request_v2')
DROP PROCEDURE MROS_data_request_v2
GO

CREATE PROCEDURE [dbo].[MROS_data_request_v2]
	
	@task varchar(max),  
    @task_id bigint OUTPUT  
AS
BEGIN

    declare @task_out varchar(max);
	set @task_id = next value for staging_namecheck.dbo.MROS_unique_task_id;

       set @task_out = '{task_id="'+cast(@task_id as varchar(255))+'",task_type="mros",real_user_task_='+@task+'
	}--end task'


 
 	--put new record in the result table with "open" status
	INSERT INTO staging_namecheck.dbo.MROS_data_status 		([TaskID],[Status],[open_dt],Valid)	VALUES (@task_id,'Open',GetDate(),1);

   exec BROKER_run_data_extraction_task @task_out,@task_id
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'BROKER_data_request')
DROP PROCEDURE BROKER_data_request
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'BROKER_check_data_extraction_task_statusF')
drop FUNCTION BROKER_check_data_extraction_task_statusF
GO

CREATE PROCEDURE [dbo].[BROKER_data_request]
	
	@task varchar(max)
AS
BEGIN

    declare @task_out varchar(max);
	declare @task_id bigint;
	set @task_id = next value for staging_namecheck.dbo.MROS_unique_task_id;

	set @task_out = '{task_id="'+cast(@task_id as varchar(255))+'",real_user_task_='+@task+'
	}--end task'


 
 	--put new record in the result table with "open" status
	--INSERT INTO staging_namecheck.dbo.MROS_data_status 		([TaskID],[Status],[open_dt],Valid)	VALUES (@task_id,'Open',GetDate(),1);

   exec BROKER_run_data_extraction_task @task_out,@task_id
   select @task_id as TaskID;

END


IF EXISTS (SELECT * FROM sys.objects WHERE  name = 'BROKER_check_data_extraction_task_statusF')
DROP FUNCTION BROKER_check_data_extraction_task_statusF
GO


CREATE FUNCTION [dbo].[BROKER_check_data_extraction_task_statusF]
(	
	@TaskId varchar(100)
)
RETURNs varchar(100) 
	
AS
BEGIN

declare @status varchar(100)

select top 1 @status=STATUS from BROKER_data_extractionStatus 
where TaskID=@TaskId and Valid=1 order by dt desc

IF @status is NULL BEGIN
 set @status ='NOT FOUND TaskID'
end 


RETURN @status
end

--patch:sql270_3_brokerServ.lua.start
--patch:sql270_4_ADD_ORG_PARAM.sql.start
GO
USE [plib_namecheck]
GO

IF OBJECT_ID('[dbo].[ADD_ORG_PARAM]') IS NOT NULL
      DROP PROCEDURE [dbo].[ADD_ORG_PARAM]; 
go




CREATE PROCEDURE [dbo].[ADD_ORG_PARAM](@ProfileName varchar(100),@paramName varchar(256), @value varchar(256))
AS
BEGIN

IF @value is NULL delete OrgClientParams where ProfileName=@ProfileName and NAME=@paramName 
else IF EXISTS (SELECT * FROM OrgClientParams WHERE ProfileName=@ProfileName and NAME=@paramName )
   update OrgClientParams set value=@value where ProfileName=@ProfileName and NAME=@paramName 
else
  INSERT INTO [dbo].[OrgClientParams] ([ProfileName],[Name],[Value])  VALUES  (@ProfileName,@paramName,@value)

END

--patch:sql270_4_ADD_ORG_PARAM.sql.finish
--patch:sql270_5_Profile949.sql.start
GO
USE [plib_namecheck]
GO

declare @bank_id int
declare @profile_name varchar(255)
declare @mros_report_mode varchar(255)

set @bank_id=949
set @profile_name='BANK_949'
set @mros_report_mode='mssql'
--set @mros_report_mode='oracle'

IF not EXISTS (SELECT * FROM OrgClients WHERE F_BANK_ID=@bank_id) BEGIN
  INSERT INTO [dbo].[OrgClients]
           ([ClientName]
           ,[ShareDefault]
           ,[Description]
           ,[F_BANK_ID], 
            ProfileName)
     VALUES
           ('DEFAULT_BANK'
           ,1
           ,'for test.system'
           ,@bank_id
           ,@profile_name
)
END

exec ADD_ORG_PARAM @profile_name,'SEND_TO','default_profile'
exec ADD_ORG_PARAM @profile_name,'MROS_REPORT_MODE',@mros_report_mode
exec ADD_ORG_PARAM @profile_name,'MROS_REPORT_PWD','AmlWeb'
--exec ADD_ORG_PARAM @profile_name,'MROS_REPORT_DSN','ora_mq' --ora_mros







--select * from OrgClientParams where name = 'MROS_REPORT_MODE'


--patch:sql270_5_Profile949.sql.finish

--patch:sql270_6.sql.start
USE [staging_namecheck]
GO
/****** Object:  StoredProcedure [dbo].[UploadFile]    Script Date: 17.12.2019 15:43:37 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'UploadFile')
DROP PROCEDURE UploadFile
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'UploadFile2')
DROP PROCEDURE UploadFile2
GO

IF EXISTS (SELECT * FROM sys.objects WHERE  name = 'mros_guid2fn')
DROP TABLE mros_guid2fn
GO


/****** Object:  Table [dbo].[mros_guid2fn]    Script Date: 18.02.2020 14:06:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[mros_guid2fn](
	[guid] [varchar](500) NULL,
	[fn] [varchar](521) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UploadFile]
@name varchar(max), --имя файла в аттаче
@data varbinary(max) --содержимое файла в аттаче
AS
BEGIN



      declare @commentid bigint
	  declare @filename varchar(500)

	  BEGIN TRANSACTION
       INSERT INTO [dbo].[file_storage] (name, data) VALUES (@name, @data)



	   select top 1 @filename=filename from file_storage order by id desc 


	   COMMIT TRANSACTION

	   select  @filename as FN


END
GO
CREATE PROCEDURE [dbo].[UploadFile2]
@name varchar(max), --имя файла в аттаче
@data varbinary(max), --содержимое файла в аттаче
@file_name varchar(521) OUTPUT  
AS
BEGIN



      declare @commentid bigint
	  declare @filename varchar(500)

	  BEGIN TRANSACTION
       INSERT INTO [dbo].[file_storage] (name, data) VALUES (@name, @data)



	   select top 1 @filename=filename from file_storage order by id desc 


	   COMMIT TRANSACTION

--	   select  @filename as FN
set @file_name =@filename


END

--patch:sql270_6.sql.finish
--patch:sql_x16_cum_2_2.sql.finish