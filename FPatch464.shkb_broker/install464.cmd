set datamart_dsn=FDMARTORA
set datamart_host=(local)
set datamart_db=FAF_MBDATA_T
set datamart_Trusted_Connection=Yes
rem set datamart_Trusted_Connection=No

net stop ProcessFinas
echo fpatch464:step1.1.Internal...
xcopy /a /r /y /I /E /Q .\Internal "C:\Program Files (x86)\Prospero\Internal"
call :CheckErrorLevel %ERRORLEVEL% "failed:copy internal"
if %ERRORLEVEL% NEQ 0 goto failed


echo fpatch464:step1.3.Namechecking...
xcopy /a /r /y /I /E /Q .\Namechecking "C:\Program Files (x86)\Prospero\Namechecking"
call :CheckErrorLevel %ERRORLEVEL% "failed:copy Namechecking"
if %ERRORLEVEL% NEQ 0 goto failed


echo add odbc connection
powershell.exe -file install\addOdbc.ps1  -dsn %datamart_dsn% -hostname %datamart_host% -database %datamart_db% -Trusted_Connection %datamart_Trusted_Connection%
echo ERRORLEVEL:%ERRORLEVEL%
if %ERRORLEVEL% NEQ 0 goto failed

echo run installLocalPwd.cmd 
pushd
cd /d "C:\Program Files\prospero\Internal\Install"\
call installLocalPwd.cmd
popd
echo ERRORLEVEL:%ERRORLEVEL%
if %ERRORLEVEL% NEQ 0 goto failed

echo install_ver262:step 11.MROS install sender procedure...
cd /d "C:\Program Files (x86)\Prospero\Namechecking\p.Secco processing nodes\std.oracle\lexEngine\AnalizServices\broker\install"\
call go.cmd
echo ERRORLEVEL:%ERRORLEVEL%
popd
if %ERRORLEVEL% NEQ 0 goto failed

pushd
cd "C:\Program Files (x86)\Prospero\Namechecking\p.Secco processing nodes\std.oracle\lexEngine"\
call install_v2_broker.cmd
popd 

cd /d "%~dp0"
call :EchoColor "fpatch464:install ok" Green


exit /b 0

:failed
cd /d %~dp0
call :EchoColor "fpatch464:failed" Red

exit /b 1



:EchoColor [text] [color]
powershell "'%~1'.GetEnumerator()|%%{Write-Host $_ -NoNewline -ForegroundColor %~2}"
exit /B



:CheckErrorLevel [errlevel] [emessage] 
echo ELEVEL:%~1
rem if %~1 NEQ 0 goto :failed
if %~1 EQU  0 exit /b
call :EchoColor "%~2" Red
echo .
exit /b 1
