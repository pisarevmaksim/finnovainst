local ss = analiz.libs.serv


local template_path_old ="%prospero_root%\\Namechecking\\p.Secco processing nodes\\std.oracle\\lexEngine\\AnalizNT_lexEngine.exe"


--  local template_path ="%prospero_root%\\Namechecking\\p.Secco processing nodes\\std.oracle\\lexEngine\\AnalizNT_lexEngine.exe"

--[[
18.03.2020 
переделано из за требований Андрея избежать перезагрузок при автоматической инсталяции
так как делается в авральном режиме на фоне многочисленых задач то вписываем путь явный
--]]

  local template_root = "C:\\Program Files (x86)\\Prospero\\Namechecking"
  local template_path       =template_root.."\\p.Secco processing nodes\\std.oracle\\lexEngine\\AnalizNT_lexEngine.exe"
  local template_path_std   =template_root.."\\p.Secco processing nodes\\std\\lexEngine\\AnalizNT_lexEngine.exe"
  local template_path_std_pf=template_root.."\\p.Secco processing nodes\\std\\pfservice\\pfServiceNT.exe"
  local  template_listimport=template_root.."\\p.Secco processing nodes\\std\\ListImporter\\bin\\AnalizNT.exe"
  local  template_analiz    =template_root.."\\p.Secco Namechecking\\AnalizNT.exe"
  local  template_finalize  =template_root.."\\p.Secco processing nodes\\FinalizingSession\\AnalizNT_FinalizingSession.exe"



  local service_x = ss.mysplit("proxy;post;finas;release;deadline;dynprofiling;mros;broker;listimport;analiz;finalizingsession",";")

--stub .start
function isServiceInstall(type)
 return true
end


serviceServ={}
function serviceServ.getServiceList()
 return {}
end
--stub .end

function get_exe_patch(task,i)
  local paths = global_prospero_setting.paths
  if paths==nil then paths={} end

--  ss.tprint(task)
--ss.a(1)
  if type(task)=="string" then 
   local val = task


     task={ttype=val} 
  end


  if task.ttype=="listimport" then return ss.def(paths.listimport,template_listimport) end
  if task.ttype=="analiz" then return ss.def(paths.analiz,template_analiz) end
                  
  if task.ttype=="finalizingsession"    then return ss.def(paths.finalizingSession,template_finalize) end


--ss.l(ss.def(task.ttype,"nul"))
--      ss.l(debug.traceback())	
--ss.a(1)

  if task.ttype == "oracle" then return ss.def(paths.std_oracle,template_path) end
  if task.ttype == "mssql_single" then 
   local o= ss.def(paths.std,template_path_std) 
--ss.l(o)
--       ss.l(debug.traceback())	
--ss.a(1)
   return o
end
  if task.ttype == "mssql_batch" then
    if i== "pf_service" then 
     return ss.def(paths.std_pf,template_path_std_pf)
    end 
    return ss.def(paths.std,template_path_std)
  end


   return ss.def(paths.std_oracle,template_path) 
end

function GetArg()
 function get_(i)
  ss.e("PARAM_FROM_ARG tmp "..i)
  return ss.g("tmp")
 end
 local o = {}
 o.arg0 = get_(0)
 o.arg1 = get_(1)
 o.arg2 = get_(2)
 o.arg3 = get_(3)
 o.arg4 = get_(4)
 o.arg5 = get_(5)
 o.arg6 = get_(6)
 o.arg7 = get_(7)
 return o
end

local function get_host(comp_name)
 return comp_name --stub
end
local function  int_(i)
 return math.floor(i)
end


function GetServicesRaw( host,name)

  analiz:set("tmp_mask",name)
--  analiz:execute("echo GET_SERVICE_LIST_EX ".. host.." tmp $(tmp_mask)")
  analiz:execute("GET_SERVICE_LIST_EX ".. host.." tmp $(tmp_mask)")
  local val = analiz:get("tmp")
--  ss.e("echo list:$(tmp)")
  local ret= ss.mysplit(val, ";")
  return ret
end

local function get_single_count(comp_name,name,prifix)
if prifix==nil then prifix="namecheck_single_" end

--   print(debug.traceback("Stack trace"))
local mask = prifix..name.."_"

 local r = GetServicesRaw(comp_name,mask)
--ss.l(mask)
--ss.tprint(r)
--ss.a(1)
 local count =#r-1
 if prifix=="namecheck_batch_" then 
  count=count-1
 end


 return count
end


local function failure_service(task,login,pwd)
  local host=""
  if task.host~="." then host=task.host end
  local exe = "sc "..host.." failure "..task.servicename..' actions= restart/0/restart/0/restart/0 reset= 86400  '
  ss.e("echo "..exe)
  ss.e("execute "..exe)
end

local function install_service(task)
  local host=""
  if task.host~="." then host=task.host end

  local exe = "sc "..host.." create "..task.servicename..' binpath= "'..task.bin..'" displayname= '..task.displayname.." start= auto"
  ss.e("echo "..exe)
--ss.l("afafraerfewqarf")
--ss.a(11)
  ss.e("execute "..exe)
  failure_service(task)
end

local function uninstall_service(task)
  local host=""
  if task.host~="." then host=task.host end
  local exe = "sc "..host.." delete "..task.servicename
  ss.e("echo "..exe)
  ss.e("execute "..exe)
end

local function login_service(task,login,pwd)
  local host=""
  if task.host~="." then host=task.host end
  local exe = "sc "..host.." config "..task.servicename..' obj= "'..login..'" password= "'..pwd..'"'
  ss.e("echo "..exe)
  ss.e("execute "..exe)
end

local function start_service(task,is_wait)
  local l = GetServicesRaw(task.host,task.servicename)
  for i=1,#l do
   ss.e("START_WIN32_SERVICE "..task.host.." "..l[i].." "..ss.def(is_wait,"0"))
  end
end

local function stop_service(task,is_wait)
  local l = GetServicesRaw(task.host,task.servicename)
  for i=1,#l do
   ss.e("STOP_WIN32_SERVICE "..task.host.." "..l[i].." "..ss.def(is_wait,"0"))
  end
end

local function get_status_service(task)
  ss.l("get_status_service.start:"..task.servicename)

  local l = GetServicesRaw(task.host,task.servicename)
  ss.l("found:"..(#l))
  local r = {}
  for i=1,#l do
   ss.e("QUERY_SERVICESTATUS tmp "..task.host.." "..l[i])
   r[l[i]]=ss.g("tmp")
  end
  ss.l("get_status_service.end")
  return r
end
local function print_status_service(r)
  ss.l("print_status_service.start")
  local rr={}
  for k,v in pairs(r) do rr[#rr+1]=k end
  table.sort(rr)
  ss.l("____________________________________\n\n\n")
  for i=1,#rr do
   local key = rr[i]
    ss.l(key..":"..r[key])
  end
end

 local function get_single(task,i,prefix)
  ss.e("ERASE_PARAM UNIQUE_IDENTIFIER")
  local path =get_exe_patch(task,i)

  if prefix==nil then 
   prefix="namecheck_single_"
  end

  ss.e("ERASE_PARAM UNIQUE_IDENTIFIER")
  local path =get_exe_patch(task,i)
  if i=="pf_service" then 
    local ret = {}
     ret.bin = path
     ret.bin=ret.bin.." UNIQUE_IDENTIFIER="..task.name
     ret.bin=ret.bin.." SERVICE_NAME=namecheck_batch_$(UNIQUE_IDENTIFIER)_pf" 
     ret.bin=ret.bin.." DISPLAY_NAME=namecheck_batch_$(UNIQUE_IDENTIFIER)_pf"  

     ret.displayname = "namecheck_batch_"..task.name.."_pf"
     ret.servicename = "namecheck_batch_"..task.name.."_pf"
     ret.host = task.host
  return ret

  end

  local bin_str = path.." UNIQUE_IDENTIFIER="..task.name.."_"..i.."_"..task.count
  bin_str=bin_str.." SERVICE_NAME="..prefix.."$(UNIQUE_IDENTIFIER)"
  bin_str=bin_str.." DISPLAY_NAME="..prefix.."$(UNIQUE_IDENTIFIER)"
  bin_str=bin_str.." SERVICE_SCRIPT=lexEngineService INSTALL_START_TYPE=AUTO"
  bin_str=bin_str.." SERVICE_LOG_TAG=start_lex_$(UNIQUE_IDENTIFIER)"..ss.b("$(guid")
  bin_str=bin_str.." family_id="..i.." family_count="..task.count
  if prefix=="namecheck_single_" then
    bin_str=bin_str.." type=SINGLE"
  else
    bin_str=bin_str.." type=BATCH"
  end

  local uname = task.name.."_"..i.."_"..task.count

  local ret = {}
  ret.bin = bin_str
  ret.displayname = prefix..uname
  ret.servicename = prefix..uname
  ret.host = task.host

--ss.tprint(ret)
--       ss.l(debug.traceback())	
--ss.a(1)
  return ret
 end

 local function get_kundenstamm(task,i)
  local r = get_single(task,i)
  r.bin=r.bin.." TYPE2=Kundenstamm" 
  return r
 end


function install_single(comp_name,name,count,ttype)




 local task={}
 task.host = get_host(comp_name)
 task.name=name
 task.count=int_(count)
 if ttype==nil or ttype=="" then ttype="oracle" end
 task.ttype = ttype

 local prefix = nil 
 if task.ttype=="mssql_batch" then 
  prefix="namecheck_batch_"
 end

 install_service(get_single(task,"management",prefix))
 if task.ttype=="mssql_batch" then 
    install_service(get_single(task,"pf_service",prefix))
 end


 for i=0,count-1 do
   install_service(get_single(task,i,prefix))
 end
end

function install_kundenstamm(comp_name,name,count)
 local task={}
 task.host = get_host(comp_name)
 task.name=name
 task.count=int_(count)
 if ttype==nil or ttype=="" then ttype="oracle" end
 task.ttype = ttype



 install_service(get_kundenstamm(task,"management"))

 for i=0,count-1 do
   install_service(get_kundenstamm(task,i))
 end

end


function stop_single(comp_name,name,ttype)
 local task={}
 task.host = get_host(comp_name)
 task.name=name

 task.count=get_single_count(comp_name,name)
 ss.l(name..":"..task.count)

 if ttype==nil or ttype=="" then ttype="oracle" end
 task.ttype = ttype



 stop_service(get_single(task,"management"))
 for i=0,task.count-1 do
   stop_service(get_single(task,i))
 end

end

function uninstall_single(comp_name,name,ttype)
 local task={}


 task.host = get_host(comp_name)
 task.name=name

 if ttype==nil or ttype=="" then ttype="oracle" end
 task.ttype = ttype



 task.count=get_single_count(comp_name,name)
-- ss.tprint(task)
-- ss.l("name:"..name)
-- ss.a(1)


 uninstall_service(get_single(task,"management"))
 if task.count>0 then
  for i=0,task.count-1 do
    uninstall_service(get_single(task,i))
  end
 end
end

function  login_single(comp_name,name,login,pwd,ttype)
ss.l("login_single.start")
--ss.l("comp_name:"..comp_name)
--ss.l("ttype:"..ttype)
--ss.a(1)
ss.tprint(name)
ss.l("_____________________")          
 local task={}
 task.host = get_host(comp_name)
 task.name=name

 local prefix = nil 
 if ttype=="mssql_batch" then 
  prefix="namecheck_batch_"
 end        


 task.count=get_single_count(comp_name,name,prefix)




 login_service(get_single(task,"management",prefix),login,pwd)

 if ttype=="mssql_batch" then 
    login_service(get_single(task,"pf_service",prefix),login,pwd)
 end

 for i=0,task.count-1 do
   login_service(get_single(task,i,prefix),login,pwd)
 end
ss.l("login_single.end")
end

 local function  get_x_listimport_(arg1)
  local path =get_exe_patch("listimport")
    local ret={}
     if ss.def(arg1,"")=="" then 
        ret.bin = path.." namechecking_ListImporter"
        ret.displayname = "namechecking_ListImporter"
        ret.servicename = "namechecking_ListImporter"
      else           
        ret.bin = " get_x_listimport_:unsupport:"..arg1
        ret.displayname = "namechecking_ListImporter_"
        ret.servicename = "namechecking_ListImporter_"
      end
   return ret
 end

 local function get_x_(ttype,serv_name,arg1)
   if serv_name=="displayname" then
      if ttype=="proxy"    then return "namechecking_finnova_proxy_" end
      if ttype=="finas"    then return "namecheck_ora_finas_" end
      if ttype=="post"     then return "namechecking_finnova_post_" end
      if ttype=="release"  then return "namechecking_finnova_release_" end
      if ttype=="deadline"  then return "namechecking_deadline_" end
      if ttype=="dynprofiling" then return "namechecking_dynprofiling_" end
      if ttype=="mros" then return "namechecking_finnova_mros_" end
      if ttype=="broker" then return "namechecking_broker_" end
      if ttype=="listimport" then return get_x_listimport_("zz").displayname end
      if ttype=="analiz" then return "Analiz" end
      if ttype=="finalizingsession" then return "namecheck_FinalizingSession" end

   end
   if serv_name=="servicename" then
     if ttype=="proxy" then return "namechecking_finnova_proxy_" end
     if ttype=="finas"    then return "namecheck_ora_finas_" end
     if ttype=="post"     then return "namechecking_finnova_post_" end
     if ttype=="release"  then return "namechecking_finnova_release_" end
     if ttype=="deadline"  then return "namechecking_deadline_" end
     if ttype=="dynprofiling" then return "namechecking_dynprofiling_" end
     if ttype=="mros" then return "namechecking_finnova_mros_" end
     if ttype=="broker" then return "namechecking_broker_" end
     if ttype=="listimport" then return get_x_listimport_("zz").servicename end
      if ttype=="analiz" then return "Analiz" end
      if ttype=="finalizingsession" then return "namecheck_FinalizingSession" end
   end
   if serv_name=="script" then
     if ttype=="proxy"    then return "namechecking_finnova_proxy" end
     if ttype=="finas"    then return "finasServMQ" end
     if ttype=="post"     then return "namechecking_finnova_post" end
     if ttype=="release"  then return "namechecking_finnova_releaseServ" end
     if ttype=="deadline"  then return "namechecking_deadline" end
     if ttype=="dynprofiling" then return "namechecking_DynProfiling" end
     if ttype=="mros" then return "namechecking_finnova_MROS" end
     if ttype=="broker" then return "namechecking_broker" end
     if ttype=="listimport" then return "namechecking_ListImporter" end
     if ttype=="analiz" then return "Analiz" end
     if ttype=="finalizingsession" then return "namecheck_FinalizingSession" end
   end
   ss.a("get_x_:unknown:"..serv_name..":"..ttype)
   ss.a(1)
   return "get_x_:unknown:"..serv_name..":"..ttype
 end


 local function get_task_for_x(ttype,name,arg1)
  ss.e("ERASE_PARAM UNIQUE_IDENTIFIER")
  local path =get_exe_patch(ttype) --template_path
  ss.l("get_task_for_x")
  ss.l("ttype:"..ttype)
  ss.l("name:"..name)
  ss.l("arg1:"..ss.def(arg1,""))



  if ttype=="listimport" and name=="namechecking_ListImporter" then 
    return get_x_listimport_("") --old logic
  end


  if ttype=="finalizingsession" then 
     local ret = {}
     ret.bin = path.." namecheck_FinalizingSession"
     ret.displayname = "namecheck_FinalizingSession"
     ret.servicename = "namecheck_FinalizingSession"
    return ret
  end
--ss.l(ttype)
--ss.l("_____________")
--   print(debug.traceback("Stack trace"))
--    ss.a(1)


  if ttype=="analiz" then 
  local ret = {}
     ret.bin = path
     ret.displayname = "Analiz"
     ret.servicename = "Analiz"
    return ret
  end
  if ttype=="finalizingsession" then 
  local ret = {}
     ret.bin = path.." namecheck_FinalizingSession"
     ret.displayname = "namecheck_FinalizingSession"
     ret.servicename = "namecheck_FinalizingSession"
    return ret
  end

                        
  local bin_str = path.." UNIQUE_IDENTIFIER="..name
  bin_str=bin_str.." SERVICE_NAME="..get_x_(ttype,"servicename",name).."$(UNIQUE_IDENTIFIER)"
  bin_str=bin_str.." DISPLAY_NAME="..get_x_(ttype,"displayname").."$(UNIQUE_IDENTIFIER)"
  bin_str=bin_str.." SERVICE_SCRIPT="..get_x_(ttype,"script").." INSTALL_START_TYPE=AUTO"
  bin_str=bin_str.." SERVICE_LOG_TAG="..get_x_(ttype,"displayname").."$(UNIQUE_IDENTIFIER)"


  local uname = name
  local ret = {}
  ret.bin = bin_str
  ret.displayname = get_x_(ttype,"displayname")..uname
  ret.servicename = get_x_(ttype,"servicename")..uname
--  ret.host = task.host
  return ret
 end

-- ---------------------------------------------------------------
function  install_x(comp_name,ttype,name,arg1)
   local task = get_task_for_x(ttype,name,arg1)
   task.host = get_host(comp_name)
   install_service(task)
end

-- ---------------------------------------------------------------
function  uninstall_x(comp_name,ttype,name,arg1)
   local task = get_task_for_x(ttype,name,arg1)

   task.host = get_host(comp_name)

   uninstall_service(task)
end

-- ---------------------------------------------------------------
function  login_x(comp_name,ttype,name,user,pwd,arg1)
   local task = get_task_for_x(ttype,name,arg1)
   task.host = get_host(comp_name)

   login_service(task,user,pwd)
end

function  run_x(comp_name,ttype,name,arg1)
   local task = {}                  
   task.servicename = get_x_(ttype,"servicename")

   if name~="all" then task.servicename=task.servicename..name end
   task.host = get_host(comp_name)

   start_service(task)
end

function  stop_x(comp_name,ttype,name)
   local task = {}
   task.servicename = get_x_(ttype,"servicename")
   if name~="all" then task.servicename=task.servicename..name end
   task.host = get_host(comp_name)

   stop_service(task)
end


function  status_x(comp_name,ttype,name)
   local task = {}
   task.servicename = get_x_(ttype,"servicename")
   if name~="all" then task.servicename=task.servicename..name end
   task.host = get_host(comp_name)

  print_status_service(get_status_service(task))
end


function go_script(script_lua)
  ss.e("LUA_EXECUTE_FILE lua "..script_lua)
end



-- ----------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------
function install_host(option)

 local single_cmd = commands["INSTALL_SINGLE"]
 local kundenstamm_cmd= commands["INSTALL_KUNDENSTAMM"]
 local login_cmd = commands["LOGIN_SINGLE"]
 local install_x_cmd = commands["INSTALL_X"]
 local login_x   = commands["LOGIN_X"]

local function install_workers(cmd,services)
ss.l("install_workers.start")
   if services==nil then return end

   for i=1,#services do
     local s = services[i]
     cmd:doit({arg2=option.host,arg3=s.name,arg4=s.count,arg5=s.ttype})
     if option.login.enabled then
       login_cmd:doit({arg2=option.host,
                       arg3=s.name,
                       arg4=option.login.user,
                       arg5=option.login.pwd,
                       arg6=s.ttype

                       })
     end
   end
ss.l("install_workers.end")
end

local function install_x(ttype)
 ss.l("install_x:"..ttype)
   local services = option[ttype]
   if services==nil then ss.l(ttype.." not found") return end
  ss.l("count:"..#services)
   for i=1,#services do
     local s = services[i]
     install_x_cmd:doit({arg2=option.host,arg3=ttype,arg4=s.name,arg5=s.arg1})
     if option.login.enabled then
       login_x:doit({arg2=option.host,arg3=ttype,arg4=s.name,arg5=option.login.user,arg6=option.login.pwd,arg7=s.arg1})
     end
   end
end



 install_workers(single_cmd,option.single)
 install_workers(kundenstamm_cmd,option.kundenstamm)

 for i=1,#service_x do
   install_x(service_x[i])
 end
end

-- ---------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------
function uninstall_host(option)
 local single_cmd = commands["UNINSTALL_SINGLE"]
 local stop_cmd    = commands["STOP_SINGLE"]
 local uninstall_x_cmd = commands["UNINSTALL_X"]
 local stop_X_cmd  = commands["STOP_X"]


local function uninstall_single(ttype)
  local services=option[ttype]
  if services==nil then ss.l("uninstall_single:"..ttype.." not found") return end

   for i=1,#services do
     local s = services[i]
     local arg_= {arg2=option.host,arg3=s.name}
     stop_cmd:doit(arg_)
     single_cmd:doit(arg_)
   end
end
local function uninstall_x__(ttype)
   local services = option[ttype]
   if services==nil then ss.l("uninstall_x__:"..ttype.." not found") return end
   for i=1,#services do
     local s = services[i]
     local arg_= {arg2=option.host,arg3=ttype,arg4=s.name,arg5=s.arg1}
     stop_X_cmd:doit(arg_)
     uninstall_x_cmd:doit(arg_)
   end
end


 uninstall_single("single")
 uninstall_single("kundenstamm")

 for i=1,#service_x do
   uninstall_x__(service_x[i])
 end

end

-- ----------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------
function  broker_init()
ss.l("broker_init.start")
local broker_option=global_oracle_setting.broker.broker_option

local serv = ss.factory.msbroker.CreateServ(broker_option)

ss.l("____________________________________________broker_init.step1.initBroker..")
--serv:initBroker()
ss.l("____________________________________________broker_init.step2.CreateType..")
serv:CreateType(broker_option.typeinfo)
ss.l("____________________________________________broker_init.step2.CreateService..")
serv:FullCreateService(broker_option.contract,broker_option,broker_option.typeinfo)

ss.l("____________________________________________broker_init.step2.CreateProcedure..")
local example_send   = serv:CreateProcedureSend(broker_option,"Send_"..broker_option.typeinfo.typename.."Example")
--not support local example_recv = serv:CreateProcedureRecv(broker_option,"Recv_"..broker_option.typeinfo.typename.."Example")
ss.l("____________________________________________broker_init.end")

ss.l("send example")
ss.l(example_send)
--ss.l("recive example")
--ss.l(example_recv)

end