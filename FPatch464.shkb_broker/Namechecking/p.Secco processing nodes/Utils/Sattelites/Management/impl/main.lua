local ss = analiz.libs.serv



local params = GetArg()

local cmd_name = params.arg1:upper()

local cmd = commands[cmd_name]

if cmd==nil then cmd = commands["HELP"] end

cmd:doit(params)
