local ss = analiz.libs.serv
commands={}

local type_x = "PROXY,FINAS,POST,RELEASE,DEADLINE,DynProfiling,MROS,BROKER"
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

local help_cmd={}
function help_cmd:name() return "HELP" end
function help_cmd:group() return "help" end

function help_cmd:short_help() return " short help" end
function help_cmd:doit(params)
 local kk ={}
 local groups ={}
 for a,b in pairs(commands) do 
  local g = b:group()
  local gg = kk[g]    if gg ==nil then gg={} kk[g]=gg groups[#groups+1]=g end
    gg[#gg+1]=a 
 end
 table.sort(groups)

 for k,v in pairs(kk) do 
  table.sort(v) 
 end
 ss.l("______________________________________________________")
 ss.l("______________________________________________________")
 ss.l("______________________________________________________")
 for i=1,#groups do
  ss.l("___________"..groups[i]..":")
  local k = kk[groups[i]]

  for i=1,#k do

    local cmd = commands[k[i]]
    local str = cmd:name().."-"..cmd:short_help()
    ss.l(str)
   end
  ss.l("\n")
 end
 ss.l("______________________________________________________")
 ss.l("______________________________________________________")
 ss.l("______________________________________________________")
end

-- --------------------------------------------------------
-- --------------------------------------------------------
-- --------------------------------------------------------
  local login_single_cmd={}

  function login_single_cmd:name() return "LOGIN_SINGLE" end
  function login_single_cmd:group() return "single" end
  function login_single_cmd:short_help() return " <comp> name login pwd" end
  function login_single_cmd:doit(params)
     local comp_name = params.arg2
     local name      = params.arg3
     local login      = params.arg4
     local pwd       = params.arg5
     local ttype     = params.arg6 --oracle,mssql_single,mssql_batch
--  ss.tprint(params)
--ss.a(1)
     login_single(comp_name,name,login,pwd,ttype)
  end

  local install_single_cmd={}
  function install_single_cmd:name() return "INSTALL_SINGLE" end
  function install_single_cmd:group() return "single" end
  function install_single_cmd:short_help() return " <comp> name count" end
  function install_single_cmd:doit(params)
     local comp_name = params.arg2
     local name      = params.arg3
     local count     = analiz:tonumber(params.arg4)
     local ttype     = params.arg5 --oracle,mssql_single,mssql_batch
     install_single(comp_name,name,count,ttype)
  end

  local install_kundenstamm_cmd={}
  function install_kundenstamm_cmd:name() return "INSTALL_KUNDENSTAMM" end
  function install_kundenstamm_cmd:group() return "single" end
  function install_kundenstamm_cmd:short_help() return " <comp> name count" end
  function install_kundenstamm_cmd:doit(params)
     local comp_name = params.arg2
     local name      = params.arg3
     local count     = analiz:tonumber(params.arg4)
     install_kundenstamm(comp_name,name,count)
  end

  local uninstall_single_cmd={}
  function uninstall_single_cmd:name() return "UNINSTALL_SINGLE" end
  function uninstall_single_cmd:group() return "single" end
  function uninstall_single_cmd:short_help() return " <comp> name" end
  function uninstall_single_cmd:doit(params)
     local comp_name = params.arg2
     local name      = params.arg3
     local ttype     = params.arg4 --oracle,mssql_single,mssql_batch
     uninstall_single(comp_name,name,ttype)
  end

-- --------------------------------------------------------
-- --------------------------------------------------------
-- --------------------------------------------------------

  local install_X_cmd={}
  function install_X_cmd:name() return "INSTALL_X" end
  function install_X_cmd:group() return "other services" end
  function install_X_cmd:short_help() return " <comp> <type:"..type_x.."> name arg1" end
  function install_X_cmd:doit(params)
     local comp_name = params.arg2
     local ttype      = params.arg3:lower()
     local name      = params.arg4
     local arg1      = params.arg5
--    ss.tprint(params)
     install_x(comp_name,ttype,name,arg1)
  end
-- --------------------------------------------------------
-- --------------------------------------------------------
-- --------------------------------------------------------

  local uninstall_X_cmd={}
  function uninstall_X_cmd:name() return "UNINSTALL_X" end
  function uninstall_X_cmd:group() return "other services" end
  function uninstall_X_cmd:short_help() return " <comp> <type:"..type_x.."> name" end
  function uninstall_X_cmd:doit(params)
     local comp_name = params.arg2
     local ttype      = params.arg3:lower()
     local name      = params.arg4
     local arg1      = params.arg5
     uninstall_x(comp_name,ttype,name,arg1)
  end


  local login_X_cmd={}
  function login_X_cmd:name() return "LOGIN_X" end
  function login_X_cmd:group() return "other services" end
  function login_X_cmd:short_help() return " <comp> <type:"..type_x.."> name" end
  function login_X_cmd:doit(params)
     local comp_name = params.arg2
     local ttype      = params.arg3:lower()
     local name      = params.arg4
     local login      = params.arg5
     local pwd      = params.arg6
     local arg1      = params.arg7
     login_x(comp_name,ttype,name,login,pwd,arg1)
  end

  local run_X_cmd={}
  function run_X_cmd:name() return "RUN_X" end
  function run_X_cmd:group() return "other services" end
  function run_X_cmd:short_help() return " <comp> <type:"..type_x.."> <name or all>" end
  function run_X_cmd:doit(params)
     local comp_name = params.arg2
     local ttype      = params.arg3:lower()
     local name      = params.arg4
     run_x(comp_name,ttype,name)
  end
  local stop_X_cmd={}
  function stop_X_cmd:name() return "STOP_X" end
  function stop_X_cmd:group() return "other services" end
  function stop_X_cmd:short_help() return " <comp> <type:"..type_x.."> <name or all>" end
  function stop_X_cmd:doit(params)
     local comp_name = params.arg2
     local ttype      = params.arg3:lower()
     local name      = params.arg4
     stop_x(comp_name,ttype,name)
  end
  local status_X_cmd={}
  function status_X_cmd:name() return "STATUS_X" end
  function status_X_cmd:group() return "other services" end
  function status_X_cmd:short_help() return " <comp> <type:"..type_x.."> <name or all>" end
  function status_X_cmd:doit(params)
     local comp_name = params.arg2
     local ttype      = params.arg3:lower()
     local name      = params.arg4
     status_x(comp_name,ttype,name)
  end


  local stop_SINGLE_cmd={}
  function stop_SINGLE_cmd:name() return "STOP_SINGLE" end
  function stop_SINGLE_cmd:group() return "single" end
  function stop_SINGLE_cmd:short_help() return " <comp>  <name>" end
  function stop_SINGLE_cmd:doit(params)
     local comp_name = params.arg2
     local name      = params.arg3
     local ttype     = params.arg4 --oracle,mssql_single,mssql_batch
     stop_single(comp_name,name,ttype)
  end

-- --------------------------------------------------------
-- --------------------------------------------------------
-- --------------------------------------------------------

  local EXECUTE_SCRIPT_cmd={}
  function EXECUTE_SCRIPT_cmd:name() return "EXECUTE_SCRIPT" end
  function EXECUTE_SCRIPT_cmd:group() return "install support" end
  function EXECUTE_SCRIPT_cmd:short_help() return " <script name>" end
  function EXECUTE_SCRIPT_cmd:doit(params)
     ss.l("user.params:")
     ss.tprint(params)
     ss.l("_____________")
     local script_lua = params.arg2
     go_script(script_lua)
  end



  local IMPORT_LOCAL_TO_SQL_cmd={}
  function IMPORT_LOCAL_TO_SQL_cmd:name() return "IMPORT_LOCAL_TO_SQL" end
  function IMPORT_LOCAL_TO_SQL_cmd:group() return "local" end
  function IMPORT_LOCAL_TO_SQL_cmd:short_help() return " <script.sql>" end
  function IMPORT_LOCAL_TO_SQL_cmd:doit(params)
     local script_lua = params.arg2
     import_local_to_sql(script_lua)
  end

  local BROKER_INIT_cmd={}
  function BROKER_INIT_cmd:name() return "BROKER_INIT" end
  function BROKER_INIT_cmd:group() return "local" end
  function BROKER_INIT_cmd:short_help() return " - init object for broker" end
  function BROKER_INIT_cmd:doit(params)
     broker_init()
  end


-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
local function add_(cmd)
 commands[cmd:name()]=cmd
end


add_(help_cmd)
add_(install_single_cmd)
add_(uninstall_single_cmd)
add_(install_X_cmd)
add_(login_single_cmd)
add_(uninstall_X_cmd)
add_(login_X_cmd)
add_(run_X_cmd)
add_(stop_X_cmd)
add_(status_X_cmd)
add_(EXECUTE_SCRIPT_cmd)
add_(stop_SINGLE_cmd)
add_(install_kundenstamm_cmd)

add_(IMPORT_LOCAL_TO_SQL_cmd)
add_(BROKER_INIT_cmd)
