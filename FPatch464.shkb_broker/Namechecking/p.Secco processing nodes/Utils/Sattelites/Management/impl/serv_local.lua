local ss = analiz.libs.serv
local fs = CreateFileServ()


local function i_(t,tagname,objname) return {type_ = t,tagname_=tagname,objname_=objname} end

local gl_main = {name="global_prospero_setting",
                path= "Internal\\.Local\\global_prospero_setting.lua" ,
                mapping = {i_("STRING","INSTANCE.FAMILY","instance_info.family"),
                           i_("STRING","INSTANCE.SPACE_TYPE","instance_info.space_type"),
                           i_("BOOL","OPTIONS.WRITE_RECORDSTATUS","options.write_recordstatus"),
                           i_("BOOL","WRITE_EMPTY_IN_NAMECHECK","options.write_empty_in_namecheck"),
                           i_("STRING","LISTIMPORT.MAIL_TO","options.listimport.mail_to"),
                           i_("STRING","LISTIMPORT.WGET","options.listimport.wget_cmd"),
                           i_("STRING","LISTIMPORT.LIST.BLACK","options.listimport.lists.black"),
                           i_("STRING","LISTIMPORT.LIST.PEP","options.listimport.lists.pep"),
                           i_("STRING","LISTIMPORT.LIST.LIST","options.listimport.lists.list"),
                           i_("STRING","LISTIMPORT.WAIT.MODE","options.listimport.wait_options.mode"),
                           i_("STRING","LISTIMPORT.WAIT.FROM","options.listimport.wait_options.time.from_"),
                           i_("STRING","LISTIMPORT.WAIT.TO","options.listimport.wait_options.time.end_"),
                          }

  }


local gl_lex = {name="global_oracle_setting_local",
                path= "Namechecking\\p.Secco processing nodes\\std.oracle\\lexEngine\\AnalizServices\\.Local\\global_setting_local.lua" ,
                mapping = {i_("STRING","ORACLE.SCHEMA_VERSION","prefix_ex.actual_version"),
                           i_("STRING","LEX.LIST.default_profile.Kundenstamm.THRESHOLD","lex.default_profile.Kundenstamm.threshold"),
                           i_("STRING","LEX.LIST.default_profile.Kundenstamm.LIST",     "lex.default_profile.Kundenstamm.list"),
                           i_("STRING","LEX.LIST.default_profile.std.THRESHOLD","lex.default_profile.std.threshold"),
                           i_("STRING","LEX.LIST.default_profile.std.LIST",     "lex.default_profile.std.list"),

                           i_("STRING","LEX.LIST.WorldchecklistProfile.Kundenstamm.THRESHOLD","lex.WorldchecklistProfile.Kundenstamm.threshold"),
                           i_("STRING","LEX.LIST.WorldchecklistProfile.Kundenstamm.LIST",     "lex.WorldchecklistProfile.Kundenstamm.list"),
                           i_("STRING","LEX.LIST.WorldchecklistProfile.std.THRESHOLD",        "lex.WorldchecklistProfile.std.threshold"),
                           i_("STRING","LEX.LIST.WorldchecklistProfile.std.LIST",             "lex.WorldchecklistProfile.std.list"),


                           i_("STRING","ORACLE.NC_SCHEMA",             "nc_schema"),
                           i_("DOUBLE","LEX.SCORE_THRESHOLD",             "score_threshold"),
                           i_("STRING","ORACLE.MROS_SCHEMA",             "mros.schema"),

                           i_("STRING","ORACLE.IN_QUEUE",             "finnova.out"),
                           i_("STRING","ORACLE.OUT_QUEUE",             "finnova.from"),
                          }
  }


local templates={}
function templates:insert(obj)
 return "INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'"..obj.key.."', N'"..obj.val.."') --"..obj.comment.."\n"
end

local function load_(file,objname)
 local root = ss.b("$(_ANALIZ_BIN)\\..\\..\\..\\..\\")
 local fn = root..file

  if not fs:FileExists(fn) then
     local emessage = "not found file:"..fn_
     ss.l("failed:"..emessage)
     ss.a(1)
  end
   ss.e("ANALIZ_SAFE_EXECUTE ret tmp_emessage LUA_EXECUTE_FILE lua "..fn)
   local emess= ss.g("tmp_emessage")
   if emess~="" then 
     local emessage = "error in load:"..emess
     ss.l("failed:"..emessage)
     ss.a(1)
   end
  
   local cur_obj = _ENV[objname]
   if cur_obj==nil then
      local emessage =  "not found global object:"..objname
      ss.l("failed:"..emessage)
      ss.a(1)
   end
   return cur_obj
end

local saver={}
function saver:Begin()
 self.global_prospero_setting = global_prospero_setting
 global_prospero_setting ={}
end

function saver:End()
  global_prospero_setting = self.global_prospero_setting
end


local function bool_to_string(val)
 if val then return "YES" end
 return "NO"
end

local function write_(writer,v,p,prop)
 if p.type_ =="STRING" or p.type_=="DOUBLE" then
   writer:write(templates:insert({key=p.tagname_,val=v,comment=prop.name.."."..p.objname_}))
   return
 end
 if p.type_ =="BOOL" then
   writer:write(templates:insert({key=p.tagname_,val=bool_to_string(v),comment=prop.name.."."..p.objname_}))
   return
 end


 ss.l("write_.failed. not found type:"..p.type_)
 ss.a(1)
end

local function write_obj(writer,obj)
  local sb = ss.CreateStringBuffer()

   ss.l("serialize obj..")
    sb:add('analiz:log("load '..self.op_.global_name..' --")')
    sb:add('\n')
    sb:add('\n')

    sb:add(table.show(self.obj_,self.op_.global_name))



    local dir = fs:ExtractPath(self.fn_)
    local name_bak=fs:ExtractName(self.fn_).."_"..tick.now()
    fs:ForceDir(dir.."\\bak")
    fs:CopyFile(self.fn_,dir.."\\bak\\"..name_bak)
--    ss.s("tmp",fn)
--    ss.e("DELETE_FILE $(tmp)")


 
    writer:write(sb:to_string())
end

local function get_element(obj,objname,prop)
 local p = ss.mysplit(objname,".")
 local v = obj
 for i=1,#p do
   v=v[p[i]]
   if v==nil then 
    ss.l("not found:"..objname.." in "..prop.name.." index:"..i)
    ss.l("obj:")
    ss.tprint(obj)
    ss.a(1)
   end
 end
 return v
end

local function  write_obj_to_sql(writer,obj,prop)
 local function wln(msg) writer:write(msg.."\n") end
 wln("")
 wln("-- "..prop.name)
 wln("-- "..prop.path)

 for i=1,#prop.mapping do
  local p =prop.mapping[i]
  local val = get_element(obj,p.objname_,prop)
  write_(writer,val,p,prop)
 end
 wln("")
end


function  import_local_to_sql(script_lua)
 saver:Begin()
 local writer = io.open(script_lua,"w+")

 local g1    = load_(gl_main.path,gl_main.name)
 local g_lex = load_(gl_lex.path ,gl_lex.name)



 write_obj_to_sql(writer,g1,gl_main)
 write_obj_to_sql(writer,g_lex,gl_lex)


 writer:close()
 saver:End()
end