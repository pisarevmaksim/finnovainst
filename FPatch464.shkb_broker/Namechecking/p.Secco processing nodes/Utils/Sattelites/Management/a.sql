
-- global_prospero_setting
-- Internal\.Local\global_prospero_setting.lua
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'INSTANCE.FAMILY', N'DB_PREF:STRING:INSTANCE.FAMILY') --global_prospero_setting.instance_info.family
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'INSTANCE.SPACE_TYPE', N'DB_PREF:STRING:INSTANCE.SPACE_TYPE') --global_prospero_setting.instance_info.space_type
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'OPTIONS.WRITE_RECORDSTATUS', N'YES') --global_prospero_setting.options.write_recordstatus
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'WRITE_EMPTY_IN_NAMECHECK', N'YES') --global_prospero_setting.options.write_empty_in_namecheck
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.MAIL_TO', N'DB_PREF:STRING:LISTIMPORT.MAIL_TO') --global_prospero_setting.options.listimport.mail_to
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.WGET', N'DB_PREF:STRING:LISTIMPORT.WGET') --global_prospero_setting.options.listimport.wget_cmd
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.LIST.BLACK', N'DB_PREF:STRING:LISTIMPORT.LIST.BLACK') --global_prospero_setting.options.listimport.lists.black
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.LIST.PEP', N'DB_PREF:STRING:LISTIMPORT.LIST.PEP') --global_prospero_setting.options.listimport.lists.pep
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.LIST.LIST', N'DB_PREF:STRING:LISTIMPORT.LIST.LIST') --global_prospero_setting.options.listimport.lists.list
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.WAIT.MODE', N'DB_PREF:STRING:LISTIMPORT.WAIT.MODE') --global_prospero_setting.options.listimport.wait_options.mode
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.WAIT.FROM', N'DB_PREF:STRING:LISTIMPORT.WAIT.FROM') --global_prospero_setting.options.listimport.wait_options.time.from_
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LISTIMPORT.WAIT.TO', N'DB_PREF:STRING:LISTIMPORT.WAIT.TO') --global_prospero_setting.options.listimport.wait_options.time.end_


-- global_oracle_setting_local
-- Namechecking\p.Secco processing nodes\std.oracle\lexEngine\AnalizServices\.Local\global_setting_local.lua
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'ORACLE.SCHEMA_VERSION', N'DB_PREF:STRING:ORACLE.SCHEMA_VERSION') --global_oracle_setting_local.prefix_ex.actual_version
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.default_profile.Kundenstamm.THRESHOLD', N'DB_PREF:STRING:LEX.LIST.default_profile.Kundenstamm.THRESHOLD') --global_oracle_setting_local.lex.default_profile.Kundenstamm.threshold
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.default_profile.Kundenstamm.LIST', N'DB_PREF:STRING:LEX.LIST.default_profile.Kundenstamm.LIST') --global_oracle_setting_local.lex.default_profile.Kundenstamm.list
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.default_profile.std.THRESHOLD', N'DB_PREF:STRING:LEX.LIST.default_profile.std.THRESHOLD') --global_oracle_setting_local.lex.default_profile.std.threshold
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.default_profile.std.LIST', N'DB_PREF:STRING:LEX.LIST.default_profile.std.LIST') --global_oracle_setting_local.lex.default_profile.std.list
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.WorldchecklistProfile.Kundenstamm.THRESHOLD', N'DB_PREF:STRING:LEX.LIST.WorldchecklistProfile.Kundenstamm.THRESHOLD') --global_oracle_setting_local.lex.WorldchecklistProfile.Kundenstamm.threshold
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.WorldchecklistProfile.Kundenstamm.LIST', N'DB_PREF:STRING:LEX.LIST.WorldchecklistProfile.Kundenstamm.LIST') --global_oracle_setting_local.lex.WorldchecklistProfile.Kundenstamm.list
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.WorldchecklistProfile.std.THRESHOLD', N'DB_PREF:STRING:LEX.LIST.WorldchecklistProfile.std.THRESHOLD') --global_oracle_setting_local.lex.WorldchecklistProfile.std.threshold
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.LIST.WorldchecklistProfile.std.LIST', N'DB_PREF:STRING:LEX.LIST.WorldchecklistProfile.std.LIST') --global_oracle_setting_local.lex.WorldchecklistProfile.std.list
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'ORACLE.NC_SCHEMA', N'DB_PREF:STRING:ORACLE.NC_SCHEMA') --global_oracle_setting_local.nc_schema
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'LEX.SCORE_THRESHOLD', N'DB_PREF:DOUBLE:LEX.SCORE_THRESHOLD') --global_oracle_setting_local.score_threshold
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'ORACLE.MROS_SCHEMA', N'DB_PREF:STRING:ORACLE.MROS_SCHEMA') --global_oracle_setting_local.mros.schema
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'ORACLE.IN_QUEUE', N'DB_PREF:STRING:ORACLE.IN_QUEUE') --global_oracle_setting_local.finnova.out
INSERT [dbo].[OrgClientParams] ( [ProfileName], [Name], [Value]) VALUES ( N'SYSTEM', N'ORACLE.OUT_QUEUE', N'DB_PREF:STRING:ORACLE.OUT_QUEUE') --global_oracle_setting_local.finnova.from

