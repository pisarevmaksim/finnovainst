USE [staging_namecheck]
GO

DECLARE @RC int
DECLARE @taskid varchar(max)
DECLARE @task varchar(max)
DECLARE @status varchar(max)

-- TODO: Set parameter values here.
set @taskid = 'Task'+cast(newid() as varchar(100))
set @task = '{task_id="'+@taskid+'",task_name = "PING_PONG"}'
print @task 
EXECUTE @RC = BROKER_run_data_extraction_task @task,@taskid

  print  'wait result'
  set @status='start'
while @status<>'COMPLETE' begin
  EXECUTE @status= BROKER_check_data_extraction_task_statusF @taskid
  print  @status
  WAITFOR DELAY '00:00:03'
end
print  'finish'
