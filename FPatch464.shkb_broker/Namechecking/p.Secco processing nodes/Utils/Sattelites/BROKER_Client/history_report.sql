use staging_namecheck

DECLARE @task varchar(max)
DECLARE @status varchar(max)
DECLARE @task_id bigint 




set @task = '{task_name="mros.history_report",
--mros_mode="mssql",--oracle
              f_bank_id=949,
              source_id=1,
              entity_reference="ENTITY_REFERENCE",
              report_reason="REPORT_REASON",
              r_action="R_ACTION",
              report_indicators={"65125080736040"},
              set_of_transactions={}, --@tr  set_of_transactions={{id = 12345}}
              set_of_customers={{id=10232},--cl1
                                   {chid="549.616"}}, --cl2
              set_of_accounts={{id=50689291617793},--ac1
                                  {chid="40.024.3727"}--ac2
                                 },
                date_from=20170101,
                date_to=20180101,
                low_amount=120000,
                debit=nil,
                credit=nil,
                n_trans_limit=5
}'



EXEC  MROS_data_request_v2 @task,@task_id OUTPUT

select @task_id as TASK_ID

select @status=status from MROS_data_status  where TaskID=@task_id
  print  'task.id:'+cast(@task_id as varchar(255))
  print  'wait result'
  set @status='start'
while not (@status='COMPLETE' or  @status ='ERROR') begin
 select @status=status from MROS_data_status  where TaskID=@task_id
  print  @status
  WAITFOR DELAY '00:00:01'
end
print  'finish:'+@status

select * from MROS_data_status  where TaskID=@task_id

