local ss = analiz.libs.serv


local function fix_q(str)
  if str:find("'")==nil then return str end
  return ss.ReplaceValue(str,"'","''")
end
function CreateBrokerClient(op)
  local c={}
  c.option =op

  function c:send_task(task)
--    local sqltask=[['{TaskID="]]..task.task_id..[[",process_id = "]]..task.process_id..[["}']]
    local sqltask = table.show(task,"zz")
    sqltask=sqltask:sub(5,sqltask:len())-- remove zz =
ss.l(sqltask)
--ss.a(1)
--failed.tast    local sqltask=[['{TaskID="]]..task.task_id..[[",process_id = "]]..task.process_id..[["']]

    local sql = "EXEC [dbo].[BROKER_run_data_extraction_task] '"..fix_q(sqltask).."','"..fix_q(task.task_id).."'"
    ss.s("tmp",sql)
    ss.e("echo EXEC_SQL "..self.option.pwd.." $(tmp)")
    ss.e("EXEC_SQL "..self.option.pwd.." $(tmp)")
--   ss.a("11")
  end

  function c:get_status(task_id)
    ss.e("SET_PARAM_FROM_SQL tmp "..self.option.pwd.." STATUS EXECUTE  [dbo].[BROKER_check_data_extraction_task_status] '"..fix_q(task_id).."'")
    return ss.g("tmp")
  end

  return c
end