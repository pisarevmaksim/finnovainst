local ss = analiz.libs.serv

local tick = CreateTimeServ()

local mros_client = CreateBrokerClient({pwd="AmlWeb"})


local function CreatePingPongTask(taskid)
 return {task_id =taskid ,task_name="PING_PONG"}
end


local tid = "BrokerClient_v2_'"..ss.b("$(now)")
--local tid = "process_db_201811261435045682"
local task = CreatePingPongTask(tid)


local start = tick.now()
mros_client:send_task(task)

local start_w = tick.now()

ss.l("task_id:"..task.task_id)
local old_status=""
local start_s = tick.now()
while true do
  local status = mros_client:get_status(task.task_id)
  if old_status~=status then
    local start_s1 = tick.now()
    ss.l(status..":"..tick.to_msec(start_s1-start_s))
    start_s=start_s1
    old_status=status
  end
  if (status=="COMPLETE") then break end
  if (status=="ERROR")   then break end
end

local finish = tick.now()

ss.l("total time(msec):\t"..tick.to_msec(finish-start))
ss.l("send task time(msec):\t"..tick.to_msec(start_w-start))