use staging_namecheck

DECLARE @task varchar(max)
DECLARE @status varchar(max)
DECLARE @task_id bigint 



set @task = '{task_name="mros.set_of_payments",
              f_bank_id=949,
              source_id=0,
             rp_name= "John",
	rp_surname = "Smith",
	rp_phone = "+123",
	rp_email = "ya@gmail.com",
	rp_occupation = "the important person",
	rp_address = "Common street, b.1",
	rp_city = "Zürich",
	fiu_ref_number= "STR 1/01.01.2020",
              entity_reference="ent.ref.",
              report_reason="rep.reas.",
              r_action="rep.act.",
              report_indicators={"0001M","1004V","2012G"},
              set_of_payments={{id="58130244425175"},{id="58130244425108"},{id="59426249919664"},{id="58357283301609"},{id="58185780398462"}},
              set_of_orders={{id="58277119821140"},{chid="ZV20160620/066787"}},
              
}'



exec  MROS_data_request_v2 @task,@task_id OUTPUT
select @task_id as TASK_ID

select @status=status from MROS_data_status  where TaskID=@task_id
  print  'task.id:'+cast(@task_id as varchar(255))
  print  'wait result'
  set @status='start'
while not (@status='COMPLETE' or  @status ='ERROR') begin
 select @status=status from MROS_data_status  where TaskID=@task_id
  print  @status
  WAITFOR DELAY '00:00:01'
end
print  'finish:'+@status

select * from MROS_data_status  where TaskID=@task_id

