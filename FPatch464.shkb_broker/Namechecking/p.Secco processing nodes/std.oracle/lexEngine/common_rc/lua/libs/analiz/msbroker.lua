local ss = analiz.libs.serv
local tick = CreateTimeServ()

if ss.factory==nil then ss.factory={} end
if ss.factory.msbroker==nil then ss.factory.msbroker={ver_id=1,ver="0.001"} end
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- option.dsn - 
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------

ss.e("GET_CURRENT_PROCESS_ID txm_pid")
local host_name = ss.b("$(COMPUTERNAME)")
local pid = ss.g("txm_pid")


local function GetDialogID(option)
--[[,pwd,contract_name,
                              recive_servicename,
                         recive_queuename,
                              send_servicename,sub_name)
--]]
ss.l("GetDialogID.start")
ss.tprint(option)
ss.l("_____________")
ss.l(debug.traceback())	
--ss.a(1)
local contract_name =ss.def(option.contract,"")
local recive_service=ss.def(option.recive_service,"")
local send_service=ss.def(option.send_service,"")
local sub_name=ss.def(option.name,"")
local queue_name = option.queue

--ss.tprint(option)
--ss.l("__:"..queue_name)


local and_lock_where = ""
local recive_service_uniq = recive_service.."_"..ss.b("$(GUID_FOR_LOG2)")
local recive_queue_uniq = recive_service_uniq.."_queue"


if ss.def(option.clienttype,"")=="request/response" then
  and_lock_where = "and (LockDT is NULL or LockDT<GetDate())"
end
 
ss.l("and_lock_where:"..and_lock_where)
--ss.tprint(option)

  local sql1 = [[
  BEGIN TRANSACTION

-- INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('start'       ,GetDate())  
  DECLARE @DialogID uniqueidentifier
  DECLARE @curID bigint
  DECLARE @dtID uniqueidentifier
  DECLARE @session_id uniqueidentifier
  DECLARE @reciver_name varchar(500)
  DECLARE @reciver_family varchar(500)
  DECLARE @reciver_N int
  DECLARE @QUEUE_NAME varchar(500)
-- INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('check'       ,GetDate())  
IF OBJECT_ID(N'msBrokerServ','U') IS NULL BEGIN
CREATE TABLE [dbo].[msBrokerServ](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReciveServiceFamily] [varchar](255) NULL,
	[ReciveServiceName] [varchar](255) NULL,
	[SendServiceName] [varchar](255) NULL,
        QUEUE_NAME varchar(255) NULL,
	[Name] [varchar](255) NULL,
	[DialogID] [varchar](255) NULL,
	[Comment] [varchar](255) NULL,
        LockDT datetime NULL
) ON [PRIMARY]

END
-- INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('set'       ,GetDate())  
      set @reciver_family=']]..recive_service..[['
      set @reciver_name  =']]..recive_service..[['
      set @QUEUE_NAME  =']] ..queue_name.. [['
-- INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('read '       ,GetDate())  

      select top 1 @DialogID=DialogID,@curID=ID,@reciver_name=ReciveServiceName,@QUEUE_NAME = [QUEUE_NAME]  from msBrokerServ WITH (NOLOCK) where ReciveServiceFamily=']]..recive_service..
       "' and SendServiceName='"..send_service..
           "' and Name='"..sub_name.."' "..and_lock_where.." \n ORDER BY ID \n"


 
sql1 = sql1..[[
-- INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('read.end '       ,GetDate()) 
    if @DialogID is NULL BEGIN
-- INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('create.start '       ,GetDate())  
         select @reciver_N = count(*) from  msBrokerServ where ReciveServiceFamily=@reciver_family

         if @reciver_N<>0 begin
--           INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('create.create QUEUE'       ,GetDate())  
           set @reciver_name = ']]..recive_service_uniq..[['
           set @QUEUE_NAME = ']]..recive_queue_uniq..[['
           CREATE QUEUE ]]..recive_queue_uniq.."\n"..[[

  --         INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('create.create service'       ,GetDate())  
           CREATE SERVICE ]]..recive_service_uniq..[[   ON QUEUE ]].. recive_queue_uniq..[[(]]..contract_name..[[)

    --       INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('create.create @DialogID'       ,GetDate())  

         BEGIN DIALOG CONVERSATION @DialogID
         FROM SERVICE ]]..send_service.. "\n"..[[
         TO SERVICE ']]..recive_service_uniq..[['
         ON CONTRACT ]]..contract_name.."\n"..[[
         WITH ENCRYPTION = OFF
         end
         ELSE BEGIN
           BEGIN DIALOG CONVERSATION @DialogID
           FROM SERVICE ]]..send_service.. "\n"..[[
           TO SERVICE ']]..recive_service..[['
           ON CONTRACT ]]..contract_name.."\n"..[[
           WITH ENCRYPTION = OFF

         END


]]


sql1 = sql1..[[
--           INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('insert msBrokerServ'       ,GetDate())  
INSERT INTO [dbo].[msBrokerServ]
           ([ReciveServiceFamily]
           ,[ReciveServiceName]
           ,[SendServiceName]
           ,QUEUE_NAME
           ,[Name]
           ,[DialogID]
           ,[Comment],
            LockDT)
     VALUES
           (]]

sql1 = sql1.."@reciver_family,@reciver_name,'"..send_service.."',@QUEUE_NAME,'"..ss.def(sub_name,"")
sql1 = sql1.."',@DialogID,'"..host_name.." pid:"..pid.."',NULL  )"


sql1 = sql1..[[
           INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('get  dialog'       ,GetDate())  
      select top 1 @DialogID=DialogID,@curID=ID  from msBrokerServ where ReciveServiceName=@reciver_name
        and SendServiceName=']]..send_service..
           "' and Name='"..sub_name.."' "..and_lock_where.." \n ORDER BY ID \n"


sql1 = sql1..[[
   END
]]


sql1 = sql1..[[
--           INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('update lock'       ,GetDate())  
                 update msBrokerServ set lockDT=dateadd(minute,10,GetDate()),comment =']]..host_name.." pid:"..pid..[[' where ID=@curID
           ]]

sql1 = sql1..[[
   COMMIT TRANSACTION
/*
  select @session_id = conversation_id 
  from sys.conversation_endpoints
    where conversation_handle=@DialogID
*/
    INSERT INTO NC_Log ([NAME],  DT)     VALUES           ('end'       ,GetDate())  
   select @DialogID as C,@curID as ID, @reciver_name as RECIVER_NAME,@QUEUE_NAME AS QUEUE_NAME


]]


 if option.ID~=nil then
   sql1= "select DialogID as C,ID as ID, ReciveServiceName as RECIVER_NAME,QUEUE_NAME  AS QUEUE_NAME from msBrokerServ where id= "..option.ID
 end

 if option.use_qname==true then
   sql1= "select top 1 DialogID as C,ID as ID, ReciveServiceName as RECIVER_NAME,QUEUE_NAME  AS QUEUE_NAME from msBrokerServ where QUEUE_NAME= '"..option.qname.."' order by ID"
 end

 ss.s("sql",sql1)
 ss.s("tmp","")

if option.debug_==true then
--  ss.l(sql1)
--  ss.a(1)
end

-- ss.e("SET_PARAM_FROM_SQL tmp "..option.pwd.." C $(sql)")
ss.e("CREATE_OR_CLEAR_STRING_MAP tmp_broker_map")
ss.e("ADD_MAP_FROM_SQL_SILEN tmp_broker_map "..option.pwd.." $(sql)")

 local m = analiz:map_get("tmp_broker_map")
 local ret = {}

 ret.dialogID= m:get("C")
 ret.ID= m:get("ID")
 ret.reciver = m:get("RECIVER_NAME")
 ret.queue = m:get("QUEUE_NAME")

-- ss.tprint(ret)
-- ss.a(1)

-- return ss.g("tmp")
 return ret

end

-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------

local function CreateSendSql_(option,dialogID)
local sql_send_str = [[
DECLARE @Example1Dialog uniqueidentifier
DECLARE @Message varchar(max)
DECLARE @bMessage VARBINARY(max)

set @Example1Dialog = ']]..dialogID..[['


SET @bMessage =  :message_val;
SEND ON CONVERSATION @Example1Dialog 
   MESSAGE TYPE ]].. option.typeinfo.typename..[[ (@bMessage)

]]

 return sql_send_str
end

local function CreateSendSql_str(option,dialogID)
local sql_send_str = [[
DECLARE @Example1Dialog uniqueidentifier
DECLARE @Message varchar(max)
DECLARE @sessionID uniqueidentifier
--DECLARE @bMessage VARBINARY(max)

set @Example1Dialog = ']]..dialogID..[['


SET @Message =  :message_val;
SEND ON CONVERSATION @Example1Dialog 
   MESSAGE TYPE ]].. option.typeinfo.typename..[[ (@Message)

/*select @sessionID=conversation_handle
  from sys.conversation_endpoints
  where conversation_id=@Example1Dialog and is_initiator=0

select  @sessionID as SID
*/
]]

 return sql_send_str
end

local function check_(option)
--  ss.l("__________________________")
--  ss.tprint(  debug.getinfo(2))        
--  ss.l("__________________________")
  option.recive= ss.def(option.recive,option.contract.."_recvQueue")
  option.recive_service=ss.def(option.recive_service,option.contract.."_reciveService")

  option.send= ss.def(option.send,option.contract.."_sendQueue")
  option.send_service=ss.def(option.send_service,option.contract.."_sendService")

  option.name= ss.def(option.name,"default")
end


local function common_init(option)
 local s = {}
 check_(option)

  s.mem = ss.CreateMemItem(1000)
  local r = GetDialogID(option)
  s.dialogID = r.dialogID
  s.ID = r.ID
  s.reciver = r.reciver
  s.queue = r.queue
--  ss.tprint(r)
--  ss.l("common_init.abort")ss.a(1)
--[[,option.contract,
                           option.reciveservice,option.recive,
                           option.send,option.name)
--]]
--  ss.l("dialogID:"..s.dialogID)
  s.sql=ss.CreateKmpSQL()
--  s.engine =  createSqlEngine({pwd=option.pwd,sql="."})
  s.engine =  createSqlEngine({pwd=option.pwd,ENGINE_TYPE="ado",sql="."})
  s.option=option


  function s:updateLockDT(options)
    local datepart = ss.def(options.datepart,"day")
    local number = ss.def(options.number,"1")
    local sql = "update msBrokerServ set lockDT=dateadd("..datepart..","..number..",GetDate()),comment ='"..host_name.." pid:"..pid.."' where ID="..self.ID
  end

  function s:Lock_finallize(options)
    local datepart = ss.def(options.datepart,"day")
    local number = ss.def(options.number,"1")
    local sql = "update msBrokerServ set lockDT=NULL where ID="..self.ID
  end
  return s
end

function ss.factory.msbroker.CreateSender(option)
 local s=common_init(option)
--  ss.l("reciver")
--  ss.l(s.reciver)
--  ss.a(11)
  s.send_sql =CreateSendSql_(option,s.dialogID)
  s.send_sql_str =CreateSendSql_str(option,s.dialogID)

-- ---------------------------
  function s:sendval(val)
    self.sql:setSql(self.send_sql_str)
    self.sql:Clear()
    self.sql:add_string("message_val",val)
--    ss.l(self.send_sql_str)
    self.engine:ExecSqlEx(self.sql)

    --???
    sql = "select conversation_handle AS SID  from sys.conversation_endpoints  where conversation_id='"..self.dialogID.."' and is_initiator=0"

    ss.s("sql",sql)
    ss.e("echo SET_PARAM_FROM_SQL tmp "..self.option.pwd.." SID $(sql)")
    ss.e("SET_PARAM_FROM_SQL tmp "..self.option.pwd.." SID $(sql)")
    local r = {}
    r.SID = ss.g("tmp")
    return r

  end

-- ---------------------------
  function s:sendobj(obj)
-- ss.l("____________________________________sendobj.object")
-- ss.l(self.send_sql)
    self.mem:truncate()
    self.mem:write_obj(obj)
-- ss.l("sendobj.1")
    self.sql:setSql(self.send_sql)
--   ss.l(self.send_sql)
-- ss.l("sendobj.2")
    self.sql:Clear()
    self.sql:add_blob("message_val",self.mem)
-- ss.l("sendobj.3")
    self.engine:ExecSqlEx(self.sql)
-- ss.l("sendobj.end")



  end

  function s:logout()
    if self.engine~=nil then    return end
    self.engine:Destroy()
    self.engine=nil
  end
  function s:login()
    self.engine =  createSqlEngine({pwd=self.option.pwd,sql="."})
  end

  function s:close()
    if self.engine~=nil then    self.engine:Destroy() end
    self.sql:Destroy()
    self.mem:destroy()
  end

 return s
end

-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------

function ss.factory.msbroker.CreateReciver(option)
ss.l("___________________")
--ss.tprint(option)
--ss.a(11)
 local r=common_init(option)
-- ss.tprint(r)
-- ss.a(1)


-- ------------------------------------
  function r:recivevals(option)
--  ss.l(" r:recivevals")
  local objs={}
  local recive_sql =[[waitfor (
RECEIVE top(]]..option.limit..[[)  message_body AS message
   FROM ]]..self.queue..[[
   ),timeout ]]..option.timeout..[[	]]
--   ss.l("recive_sql:"..recive_sql)

--ss.l("___")
--   ss.tprint(self.option)
--   ss.a("1")
   self.engine:SetSql(recive_sql,"0")
   local ii=0
   local isNext,emessage = self.engine:Next()
   while isNext do
    if emessage~="" then
      break
    end
    local r = self.engine:Record()
    ss.l("print:")
    ss.tprint(r)
    objs[#objs+1]=r.message
    ii=ii+1
    isNext,emessage = self.engine:Next()
   end
 return objs,emessage
end

-- ------------------------------------
  function r:reciveobj(option)
--  ss.l(" r:reciveobj")
  local objs={}
  local recive_sql =[[waitfor (
RECEIVE top(]]..option.limit..[[)  message_body AS message
   FROM ]]..self.queue..[[
   ),timeout ]]..option.timeout..[[	]]

  --ss.l(recive_sql)
   self.engine:SetSql(recive_sql,"0")
   local ii=0
   local isNext,emessage = self.engine:Next()
   while isNext do
    if emessage~="" then
      break
    end
    local r = self.engine:Record()
    
    self.mem:truncate()
    self.engine:debug_get_blob(1,self.mem);
    local res = self.mem:read_obj()
    objs[#objs+1]=res
    ii=ii+1
    isNext,emessage = self.engine:Next()
   end
 return objs,emessage
end

-- ------------------------------------
  function r:logout()
    if self.engine~=nil then    return end
    self.engine:Destroy()
    self.engine=nil
  end
  function r:login()
    self.engine =  createSqlEngine({pwd=self.option.pwd,sql="."})
  end

-- ------------------------------------
  function r:close()
    if self.engine~=nil then    self.engine:Destroy() end
    self.sql:Destroy()
    self.mem:destroy()

  end
-- ------------------------------------  


-- ------------------------------------
  return r

end


-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- initBroker
-- createType
-- 
-- option={pwd='xxx'}
function ss.factory.msbroker.CreateServ(option)
  local s={}
  s.option = option

  function s:exeSql_(sql)
    ss.s("tmp",sql)
    ss.e("echo EXEC_SQL "..self.option.pwd.." $(tmp)")
    ss.e("EXEC_SQL "..self.option.pwd.." $(tmp)")
  end
-- ---------------------------------------------  
  function s:initBroker()
    ss.e("SET_PARAM_FROM_SQL tmp "..self.option.pwd.." C select db_name() as C")
    local database=ss.g("tmp")
    ss.l("msbroker:initBroker:"..database)

    self:exeSql_( [[--Enabling service broker
USE master
ALTER DATABASE  ]] ..database..[[  
 SET ENABLE_BROKER; 
]]
)

  end

-- ---------------------------------------------
--validate_type:"NONE","WELL_FORMED_XML"
 function s:CreateType(typeinfo)
     ss.l("msbroker:create type "..typeinfo.typename.." validation:"..typeinfo.validate_type)
      self:exeSql_("CREATE MESSAGE TYPE "..typeinfo.typename.." VALIDATION = "..typeinfo.validate_type)
 end

 function s:CreateContract(type_name,contractName)
     ss.l("msbroker:create contract "..contractName.." type:"..type_name)
      self:exeSql_("CREATE CONTRACT "..contractName.." ("..type_name.." SENT BY any) ")
 end

 function s:CreateQueue(QueueName)
   ss.l("CREATE QUEUE "..QueueName)
--   ss.a(1)
   self:exeSql_("CREATE QUEUE "..QueueName)
 end

 function s:CreateService(QueueName,ServiceName,contractName,comment)
   ss.l("msbroker:create service:"..ServiceName.." Queue:"..QueueName.."contractName:"..contractName.." comment:"..ss.def(comment,"none"))
--   self:exeSql_("CREATE QUEUE "..QueueName)
   self:exeSql_("CREATE SERVICE "..ss.def(ServiceName,"!!ServceName_is_null!!").." ON QUEUE "..ss.def(QueueName,"!!QueueName_is_NULL!!").."("..ss.def(contractName,"!contractName_is_null!")..")")
 end




  function s:FullCreateService(name,option,typeOption)
--   ss.a(1467)
    check_(option)
    ss.l("______________________________________________________")
    ss.tprint(option)
    ss.l("______________________________________________________")
    self:CreateContract(typeOption.typename,option.contract)
    ss.l("______________________________________________________")
    local sendQ=name.."_sendQueue"
    local recvQ=name.."_recvQueue"
    self:CreateQueue(sendQ)
    self:CreateQueue(recvQ)
    ss.l("______________________________________________________")

  


--    self:CreateService(name,name.."_send",option.contract,"send")
    self:CreateService(sendQ,option.send_service,option.contract,"send")
    ss.l("______________________________________________________")
--    self:CreateService(name,name.."_recv",option.contract,"recive")
    self:CreateService(recvQ,option.recive_service,option.contract,"recive")
    ss.l("______________________________________________________")  
  end

-- ----------------------------------------------------------------------

  function s:CreateProcedureSend(option,name)
   check_(option)
  local dialogID = GetDialogID(option).dialogID
  ss.l("send.dialogID:"..dialogID)
--  ss.a(11)

local sql0=[[
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[]]..name..[[]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN 
 DROP PROCEDURE [dbo].[]]..name..[[]
end
]]


-- =============================================
-- Author:		analiz.msbroker.serv
-- Create date:]]..ss.b("$(now)").. [[ 
-- Description:	Send value to msbroker
-- =============================================

local sql1=
[[


CREATE PROCEDURE ]]..name..[[
	@value as varchar(max)
AS
BEGIN
DECLARE @dialog_id uniqueidentifier
DECLARE @Message varchar(max)
--DECLARE @bMessage VARBINARY(max)

set @dialog_id = ']]..dialogID..[['


SET @Message =  @value;
SEND ON CONVERSATION @dialog_id
   MESSAGE TYPE ]]..option.typeinfo.typename..[[ (@Message)END

]]

  self:exeSql_(sql0)
  self:exeSql_(sql1)

  return "exec "..name.." 'hello word'"
  end
-- ----------------------------------------------------------------------
-- ----------------------------------------------------------------------

  function s:CreateProcedureRecv(option,name)
   check_(option)
  local dialogID = GetDialogID(option).dialogID
ss.l("recive.dialogID:"..dialogID)

local sql0=[[
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[]]..name..[[]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN 
 DROP PROCEDURE [dbo].[]]..name..[[]
end
]]

-- =============================================
-- Author:		analiz.msbroker.serv
-- Create date: ]]..ss.b("$(now)").. [[ 
-- Description:	Recive value to msbroker
-- =============================================

local sql1=
[[


CREATE PROCEDURE ]]..name.. [[ 
	@limit as int,
	@timeout as int
AS
BEGIN
waitfor (
RECEIVE top(@limit )  cast(message_body as varchar(max)) AS message



   FROM ContractTestBin1_reciveQueue 
  ),timeout @timeout
end
]]
--   where conversation_handle=']]..dialogID..[['
  self:exeSql_(sql0)
  self:exeSql_(sql1)

  return "exec "..name.." 10,10000"
  end


  return s
end


-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
