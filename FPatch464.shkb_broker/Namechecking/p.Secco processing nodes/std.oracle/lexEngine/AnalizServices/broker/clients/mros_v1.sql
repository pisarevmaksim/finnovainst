use staging_namecheck

DECLARE @task_id varchar(255)
DECLARE @task varchar(1000)
DECLARE @RC int
DECLARE @status varchar(max)


set @task_id='MROS_REPORT_'+cast(newid() as varchar(100))

set @task = '{task_name="mros.set_of_payments",
              task_id="'+@task_id+'",
              f_bank_id=949,
              source_id=1,
              entity_reference="<PLEASE FILL>",
              report_reason="<PLEASE FILL>",
              r_action="<PLEASE FILL>",
              report_indicators={"2003G"},
              set_of_payments={},
              set_of_orders_id={"5786273652836"},
              set_of_orders_nr={}
}'



EXECUTE @RC = BROKER_run_data_extraction_task @task,@task_id

  print  'wait result'
  set @status='start'
while not (@status='COMPLETE' or  @status ='ERROR') begin
  EXECUTE @status= BROKER_check_data_extraction_task_statusF @task_id
  print  @status
  WAITFOR DELAY '00:00:03'
end
print  'finish:'+@status


select top 20 *, cast(data as varchar(max)) from file_storage order by id desc