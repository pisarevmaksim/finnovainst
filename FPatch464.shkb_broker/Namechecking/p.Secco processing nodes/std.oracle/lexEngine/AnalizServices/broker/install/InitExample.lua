local ss = analiz.libs.serv



local broker_option=global_oracle_setting.broker.broker_option

local serv = ss.factory.msbroker.CreateServ(broker_option)


ss.l("-------------------------------------------------------------------------")
ss.l("-------------------------------------------------------------------------")
ss.l("-------------------------------------------------------------------------")
ss.l("-------------------------------------------------------------------------")
--serv:initBroker()
--serv:CreateType(broker_option.typeinfo)
--serv:FullCreateService(broker_option,broker_option.typeinfo)

local example_send   = serv:CreateProcedureSend(broker_option,"Send_"..broker_option.typeinfo.typename.."Example")
local example_recv = serv:CreateProcedureRecv(broker_option,"Recv_"..broker_option.typeinfo.typename.."Example")


ss.l("send example")
ss.l(example_send)
ss.l("recive example")
ss.l(example_recv)


