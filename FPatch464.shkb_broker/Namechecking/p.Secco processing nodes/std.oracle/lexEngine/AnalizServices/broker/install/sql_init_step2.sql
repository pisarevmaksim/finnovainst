CREATE TABLE [dbo].[BROKER_data_extractionStatus](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TaskID] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[dt] [datetime] NULL,
	[Comment] [varchar](100) NULL,
	[Valid] [int] NULL
) ON [PRIMARY]

GO

CREATE INDEX [BROKER_data_extractionStatus_index] ON [dbo].[BROKER_data_extractionStatus]
(
	[TaskID] ASC
)


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
USE [staging_namecheck]
GO
/****** Object:  StoredProcedure [dbo].[BROKER_run_data_extraction_task]    Script Date: 26.11.2019 13:21:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BROKER_run_data_extraction_task]
	
	@task varchar(4000),
	@taskid varchar(100)
AS
BEGIN

if @taskid is not NULL 
 INSERT INTO [dbo].[BROKER_data_extractionStatus]
           ([TaskID]
           ,[Status]
           ,[dt],
		   Valid)
     VALUES
           (@taskid
           ,'OPEN'
           ,GetDate(),1   )

exec [dbo].[Send_TypeBROKER_DATA_v2Example] @task
END



-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE Procedure BROKER_check_data_extraction_task_status
	
	@TaskId varchar(100)
AS
BEGIN
declare @status varchar(100)
select top 1 @status=STATUS from BROKER_data_extractionStatus where TaskID=@TaskId and Valid=1 order by dt desc

IF @status is NULL BEGIN
 set @status ='NOT FOUND TaskID'
end 
select @status as STATUS 
END


GO

CREATE FUNCTION [dbo].[BROKER_check_data_extraction_task_statusF]
(	
	@TaskId varchar(100)
)
RETURNs varchar(100) 
	
AS
BEGIN

declare @status varchar(100)

select top 1 @status=STATUS from BROKER_data_extractionStatus 
where TaskID=@TaskId and Valid=1 order by dt desc

IF @status is NULL BEGIN
 set @status ='NOT FOUND TaskID'
end 


RETURN @status
end

GO