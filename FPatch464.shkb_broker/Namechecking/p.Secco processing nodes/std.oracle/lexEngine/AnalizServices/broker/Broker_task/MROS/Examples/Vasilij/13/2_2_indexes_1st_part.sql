--indexes
create index FA_MROS_KD_ADRI1 on FA_MROS_KD_ADR (kd_lnr,userbk_nr);

create index FA_MROS_KD_VSBI1 on FA_MROS_KD_VSB (kd_lnr,userbk_nr);

create index FA_MROS_KD_EMAILI1 on FA_MROS_KD_EMAIL (kd_lnr,userbk_nr);
/*
create unique index FA_MROS_KD_ADR_XMLC2 on FA_MROS_KD_ADR_XML (kd_lnr,userbk_nr);

create unique index FA_MROS_KD_VSB_XMLC2 on FA_MROS_KD_VSB_XML (kd_lnr,userbk_nr);

create unique index FA_MROS_KD_EMAIL_XMLC2 on FA_MROS_KD_EMAIL_XML (kd_lnr,userbk_nr);
*/
delete from FA_MROS_KD_STAMM kd where exists
	(select * from
		(select edit_kd_nr,userbk_nr,count(*) as cnt from F2LDATP0.kd_stamm group by edit_kd_nr,userbk_nr having count(*)>1) s
	where s.userbk_nr=kd.userbk_nr and s.edit_kd_nr=kd.edit_kd_nr);
create unique index FA_MROS_KD_STAMMC2 on FA_MROS_KD_STAMM (kd_lnr,userbk_nr,kd_htyp_cd,partner_flag);
create unique index FA_MROS_KD_STAMMC3 on FA_MROS_KD_STAMM (edit_kd_nr,userbk_nr,partner_flag);
create index FA_MROS_KD_STAMMI1 on FA_MROS_KD_STAMM (kd_lnr,bank_flag,userbk_nr,partner_flag);   --!!!!!!NEW!!!!!!

delete from FA_MROS_KT_STAMM kt where exists
	(select * from
		(select edit_kt_nr,userbk_nr,count(*) as cnt from F2LDATP0.kt_stamm group by edit_kt_nr,userbk_nr having count(*)>1) s
	where s.userbk_nr=kt.userbk_nr and s.edit_kt_nr=kt.edit_kt_nr);
create unique index FA_MROS_KT_STAMMC2 on FA_MROS_KT_STAMM (kt_lnr,userbk_nr);
create unique index FA_MROS_KT_STAMMI1 on FA_MROS_KT_STAMM (edit_kt_nr,userbk_nr);

create unique index FA_MROS_ZV_ZLG_TC2 on FA_MROS_ZV_ZLG_T (zv_zlg_sys_lnr,userbk_nr);
create index FA_MROS_ZV_ZLG_TI1 on FA_MROS_ZV_ZLG_T (auf_lnr,userbk_nr);
create index FA_MROS_ZV_ZLG_TI2 on FA_MROS_ZV_ZLG_T (auf_id,userbk_nr);

delete from FA_MROS_ZV_AUF zv where exists
	(select * from
		(select trak_ref_lnr,userbk_nr,count(*) as cnt from 
    			(select distinct trak_ref_lnr,auf_lnr,auf_id,userbk_nr from FA_MROS_ZV_AUF) s
		group by trak_ref_lnr,userbk_nr having count(*)>1) ss
	where ss.userbk_nr=zv.userbk_nr and ss.trak_ref_lnr=zv.trak_ref_lnr);
create index FA_MROS_ZV_AUFI1 on FA_MROS_ZV_AUF (auf_lnr,userbk_nr);
create index FA_MROS_ZV_AUFI2 on FA_MROS_ZV_AUF (auf_id,userbk_nr);
create index FA_MROS_ZV_AUFI3 on FA_MROS_ZV_AUF (land_lnr_dom, userbk_nr);
create index FA_MROS_ZV_AUFI4 on FA_MROS_ZV_AUF (land_lnr_dom1, userbk_nr);
create index FA_MROS_ZV_AUFI5 on FA_MROS_ZV_AUF (trak_ref_lnr, userbk_nr);

create unique index FA_MROS_KT_SALDOI1 on FA_MROS_KT_SALDO (kt_lnr,userbk_nr);

create unique index FA_MROS_KD_KDC2 on FA_MROS_KD_KD (kd_kd_lnr,userbk_nr);
create index FA_MROS_KD_KDI1 on FA_MROS_KD_KD (kd_lnr,userbk_nr);
create index FA_MROS_KD_KDI2 on FA_MROS_KD_KD (kd_lnr_zuw,userbk_nr);

create unique index FA_MROS_VT_STAMMC2 on FA_MROS_vt_stamm (vt_lnr,userbk_nr);
create index FA_MROS_VT_STAMMI1 on FA_MROS_vt_stamm (kd_lnr,userbk_nr);
create index FA_MROS_VT_STAMMI2 on FA_MROS_vt_stamm (kt_lnr,userbk_nr);

create index FA_MROS_VT_ZUORDI1 on FA_MROS_VT_ZUORD (vt_lnr,userbk_nr);
create index FA_MROS_VT_ZUORDI2 on FA_MROS_VT_ZUORD (kd_lnr,userbk_nr);
create index FA_MROS_VT_ZUORDI3 on FA_MROS_VT_ZUORD (kt_lnr,userbk_nr);

create index FA_MROS_SR_FELD_WERTI1 on FA_MROS_SR_FELD_WERT (kd_lnr,userbk_nr);

