use staging_namecheck

DECLARE @task varchar(max)
DECLARE @status varchar(max)
DECLARE @task_id bigint 



set @task = '{task_name = "mros.report_activity", f_bank_id = "942", 
entity_reference = "<PLEASE FILL>", report_reason = "<PLEASE FILL>", 
r_action = "<PLEASE FILL>", report_indicators = {"0001M", "1004V", "2012G"}, 
set_of_accounts = {{chid = "550.028.358", rsn = "account with person owner"},
                   {chid  ="500.445.010", rsn = "account with person owner"},
				   {chid="550.407.308",rsn = "account with person owner"}}, 
set_of_customers = {{chid = "512.019", rsn = "person", p_flag = nil},
{chid = "520.100", rsn = "entity", p_flag = nil},
{chid = "550.112", rsn = "family", p_flag = 0},

                 },
 rp_email = "support@prospero.ch"}'



exec  MROS_data_request_v2 @task,@task_id OUTPUT
select @task_id as TASK_ID

select @status=status from MROS_data_status  where TaskID=@task_id
  print  'task.id:'+cast(@task_id as varchar(255))
  print  'wait result'
  set @status='start'
while not (@status='COMPLETE' or  @status ='ERROR') begin
 select @status=status from MROS_data_status  where TaskID=@task_id
  print  @status
  WAITFOR DELAY '00:00:01'
end
print  'finish:'+@status

select * from MROS_data_status  where TaskID=@task_id


/*
-- run under PROSPERO-scheme (PROSPERO)

--report with Activity node
SELECT FAQMRSDM.report_activity (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref.'
	,REPORT_REASON => 'rep.reas.'
	,R_ACTION => 'rep.act.'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_CLIENTS => FAF_MROS_IdRsn_t(
		FAF_MROS_IdRsn('512.019',NULL,'person'),
		FAF_MROS_IdRsn('520.100',NULL,'entity'),
		FAF_MROS_IdRsn('550.112',0,'family'))
	,SET_OF_ACCOUNTS => FAF_MROS_IdRsn_t(
		FAF_MROS_IdRsn('550.028.358',NULL,'account with person owner'),
		FAF_MROS_IdRsn('500.445.010',NULL,'account with entity owner'),
		FAF_MROS_IdRsn('550.407.308',NULL,'account with family owner'))
) FROM DUAL; 
*/