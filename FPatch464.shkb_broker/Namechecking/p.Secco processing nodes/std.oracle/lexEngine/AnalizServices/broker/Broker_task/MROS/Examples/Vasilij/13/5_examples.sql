﻿-- run under PROSPERO-scheme (PROSPERO)

--report with Activity node
SELECT FAQMRSDM.report_activity (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref.'
	,REPORT_REASON => 'rep.reas.'
	,R_ACTION => 'rep.act.'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_CLIENTS => FAF_MROS_IdRsn_t(
		FAF_MROS_IdRsn('512.019',NULL,'person'),
		FAF_MROS_IdRsn('520.100',NULL,'entity'),
		FAF_MROS_IdRsn('550.112',0,'family'))
	,SET_OF_ACCOUNTS => FAF_MROS_IdRsn_t(
		FAF_MROS_IdRsn('550.028.358',NULL,'account with person owner'),
		FAF_MROS_IdRsn('500.445.010',NULL,'account with entity owner'),
		FAF_MROS_IdRsn('550.407.308',NULL,'account with family owner'))
) FROM DUAL; 


--report with Transaction node
SELECT FAQMRSDM.set_of_payments (
	F_BANK_ID => 949
	,RP_NAME => 'John'
	,RP_SURNAME => 'Smith'
	,RP_PHONE => '+123'
	,RP_EMAIL => 'ya@gmail.com'
	,RP_OCCUPATION => 'the important person'
	,RP_ADDRESS => 'Common street, b.1'
	,RP_CITY => 'Zürich'
	,FIU_REF_NUMBER => 'STR 1/01.01.2020'
	,ENTITY_REFERENCE => 'ent.ref'
	,REPORT_REASON => 'rep.reas.'
	,R_ACTION => 'rep.act.'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_PAYMENT_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(59585716405127),
		FAF_MROS_numID(59331206957840),
		FAF_MROS_numID(58126436855493)
	)
	,SET_OF_ORDER_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('ZV20050429/001207')
	)
) FROM DUAL; 

	 
--TRANSACTION HISTORY REPORT   
--report with Transactions from kt_buch
SELECT FAQMRSDM.booking_report (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_TR_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(58363516489234),
		FAF_MROS_numID(58614091351531)
	)
	,AC_NR => '500.197.008'
) FROM DUAL;  

 
SELECT FAQMRSDM.historical_report (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_CL_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('533.533')
	)
	,SET_OF_AC_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('533.533.015')
	)
	,N_TRANS_LIMIT => 5
) FROM DUAL; 

--reports with deposit/withdrawals
SELECT FAQMRSDM.booking_report (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_TR_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(50987805527357)
	)
	,AC_NR => '549.712.313'
) FROM DUAL; 

SELECT FAQMRSDM.booking_report (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_TR_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(59464528223935)
	)
	,AC_NR => '500.284.018'
) FROM DUAL; 

SELECT FAQMRSDM.booking_report (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_TR_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(44561535229647)
	)
	,AC_NR => '500.529.078'
) FROM DUAL; 

--another person, other than the owner, deposits into the account
SELECT FAQMRSDM.booking_report (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_TR_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(52812597653510)
	)
	,AC_NR => '555.666.489'
) FROM DUAL; 
