--[[
 вариант генерации отчета mros --set of transactions
--]]
local ss = analiz.libs.serv

local IS_ENABLED=true

ss.l("create:mros.history_report")

return CreateMROSWorker({
  name = "mros.history_report",
  is_enabled = IS_ENABLED,
  templates={
    mssql={
      generate_report="$(template_mros_dir)\\mssql\\history_report.etlua",
      publicate_report ="$(template_mros_dir)\\publicate_report.etlua"
    },
    oracle={
      generate_report="$(template_mros_dir)\\oracle\\history_report.etlua",
      publicate_report ="$(template_mros_dir)\\publicate_report.etlua"
    }

  },
 -- аргументы-массивы которые должны быть проверены
  task_def_arrs={"report_indicators","set_of_transactions_id","set_of_customers_id",
                       "set_of_accounts"},
  validate={"f_bank_id:int,not_null",
--            "source_id:int,not_null",
            "[]set_of_transactions.id:int",
            "[]set_of_payments.id:int",
            "[]set_of_orders.id:int"}

}
)
