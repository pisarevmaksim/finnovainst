local ss = analiz.libs.serv

local IS_ENABLED=true

local function CreateWorker()
  local worker = {}
  worker.name="RELOAD_ORGCLIENT"

  function  worker:Register(ctx)
    if IS_ENABLED==true then 
      ctx:Register(self,self.name)
    end
  end

  function  worker:DoTask(ctx,task)
     ss.l("ping start")
      ctx:addStatus(task.task_id,"start")
--
      ctx.orgclient_serv = CreateOrgClientEngine()
      ctx:addStatus(task.task_id,"COMPLETE")
     ss.l("ping end")
  end
 return worker
end



return CreateWorker()
