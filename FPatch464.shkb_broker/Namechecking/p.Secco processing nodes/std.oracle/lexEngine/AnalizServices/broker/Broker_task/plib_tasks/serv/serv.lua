local ss = analiz.libs.serv
--"Namechecking\p.Secco processing nodes\std.oracle\lexEngine\AnalizServices\broker\Broker_task\plib_task\serv\serv.lua"
local plib = analiz.libs.plib

local PLIB_PWD = "plib_aml"

function GetArg()
 function get_(i)  ss.e("PARAM_FROM_ARG tmp "..i)  return ss.g("tmp") end
 local o = {}
 o.arg0 = get_(0)
 o.arg1 = get_(1)
 o.arg2 = get_(2)
 o.arg3 = get_(3)
 o.arg4 = get_(4)
 o.arg5 = get_(5)
 o.arg6 = get_(6)
 o.arg7 = get_(7)
 return o
end


local function go_cmd(cmd,step_info)
  ss.e("ANALIZ_SAFE_EXECUTE tmp_r tmp_emessage "..cmd)
  if ss.g("tmp_emessage")~="" then return {failed=true,emessage = ss.g("tmp_emessage"),step=step_info,last_cmd = ss.b(cmd)} end
  return {emessage=""}
end

local function ExtractNameFromVer(verid)
ss.s("plibRef_tmp1",verid)


  local ret =go_cmd("PLIB_LOGIN db_h "..PLIB_PWD,"plib.login")
  if ret.failed then return ret end


  ret =go_cmd("PLIB_OPEN_MODEL_CUR_VER h  db_h $(plibRef_tmp1)","open model")
  if ret.failed then 
    ss.e("PLIB_LOGOUT db_h")
    return  ret
 end


  ret =go_cmd("PLIB_GET_MODEL_FIELDS_FIELD_LIST all_fields h","load all fields")
  if ret.failed then ss.e("PLIB_CLOSE_MODEL h") ss.e("PLIB_LOGOUT db_h") return ret end

  ret =go_cmd("PLIB_EXTRACT_MIN_SET_FIELDS_FOR_CALC2 in_fields  h $(empty)","load trasform field")
  if ret.failed then ss.e("PLIB_CLOSE_MODEL h") ss.e("PLIB_LOGOUT db_h") return ret end

  ss.e("PLIB_CLOSE_MODEL h")
  ss.e("PLIB_LOGOUT db_h")
  ss.e("DIFFERENCE_FIELDS_LIST delta_fields $(all_fields) $(in_fields)")

local ret = {emessage="",
--             input_fields = ss.mysplit(ss.g("in_fields"),";"),
--             transform_fields= ss.mysplit(ss.g("delta_fields"),";")
             fields = ss.mysplit(ss.g("all_fields"),";")
}
 return ret
end
local function exe_sql(sql)
 ss.s("tmp",sql)
 local cmd = "EXEC_SQL "..PLIB_PWD.." $(tmp)"
 ss.e("echo "..cmd)
 ss.e(cmd)
end
-- ----------------------------------------------------------------------------------------
local function Update_ModelField(ver_id,fields)
  local function fix_(s) return ss.ReplaceValue(s,"'","''") end
  exe_sql("delete from ModelField where ModelImplID="..ver_id)
  for i=1,#fields do
    sql = "INSERT INTO [dbo].[ModelField]([ModelImplID],[FieldName],[FieldType],[MinValue],[MaxValue],[ConstValue],[Description])   "
   sql=sql.." VALUES ("..ver_id..",'"..fix_(fields[i]).."','string',0,0,NULL,'auto create')"
   exe_sql(sql)
  end
  return {emessage=""}--todo
end





function update_verid(ver_id)
 ss.l("verid:"..ss.def(ver_id,"nul"))
  local ret=ExtractNameFromVer(ver_id)
  if ret.emessage=="" then 
     ret.r =  Update_ModelField(ver_id,ret.fields)
     if ret.r.emessage~="" then ret.emessage= ret.r.emessage end
  end
 return ret
end
-- ----------------------------------------------------------------------------------------
function update_model(model_name)
  local pp = plib.CreateServ()
  local ret = pp:Login(PLIB_PWD)
  if ret.emessage~="" then return ret end
  local ret = pp:GetModelInfo(model_name)
  pp:Logout()
  if ret.emessage~="" then 
    return ret 
  end
  local ver_id = ret.ver_id
  if ver_id=="" or ver_id==nil then 
   return {emessage =" not found active version model:"..model_name}
  end
  return update_verid(ver_id)
end

function update_cfg(cfg_name)
  local pp = plib.CreateServ()
  local ret = pp:Login(PLIB_PWD)
  if ret.emessage~="" then return ret end

  local ver_ids={}
  local config = base_models[cfg_name]
  if config==nil then return {emessage="not found configs:"..cfg_name} end
  for i=1,#config do
    model_name = config[i]
    local ret = pp:GetModelInfo(model_name)
    if ret.emessage~="" then 
      pp:Logout()
      return ret
    end
    if ver_id=="" or ver_id==nil then 
     return {emessage =" not found active version model:"..model_name}
   end

    ver_ids[#ver_ids+1]=ret.ver_id
  end

  pp:Logout()

  for i=1,#ver_ids do
    local ret=update_verid(ver_ids[i])
    if ret.emessage~="" then       return re    end
  end

  return {emessage="",ver_ids=ver_ids}

end

