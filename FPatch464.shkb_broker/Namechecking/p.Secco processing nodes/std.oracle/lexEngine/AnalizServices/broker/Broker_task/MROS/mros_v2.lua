--[[
 вариант генерации отчета использующий xml от Никиты и процедуру от Василия
--]]
local ss = analiz.libs.serv

local IS_ENABLED=true


local function CreateWorker()
  local worker = {}
  worker.name="MROS_REPORT"

  function  worker:Register(ctx)
    if IS_ENABLED==true then 
      ctx:Register(self,self.name)
    end
  end

  function  worker:DoTask(ctx,task)

   if task.xml ==nil then 
    ss.l("failed task.not found xml:")
    ss.tprint(task)
    ctx:addStatus(task.task_id,"ERROR","not found xml")
   end
   local sql = [[
set ansi_nulls, ansi_padding, ansi_warnings, concat_null_yields_null, quoted_identifier, arithabort on;
set numeric_roundabort off;

declare @xx xml;
declare @str varchar(6000);
set @str = N']]..task.xml..[[' 
set @xx  = cast(@str as xml)
exec MROS_report_generation @xx
]]
   ss.s("tmp",sql)
   ss.e("EXEC_SQL AmlWeb $(tmp)")
    ctx:addStatus(task.task_id,"COMPLETE")      
  end
 return worker
end



return CreateWorker()
