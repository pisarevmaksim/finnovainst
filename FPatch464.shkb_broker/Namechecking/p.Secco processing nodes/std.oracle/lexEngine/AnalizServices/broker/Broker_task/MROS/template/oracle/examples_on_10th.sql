﻿--report with Activity node
SELECT FAF_MROS.report_activity (
	F_BANK_ID => 949
	,SOURCE_ID => 0
	,ENTITY_REFERENCE => 'ent.ref.'
	,REPORT_REASON => 'rep.reas.'
	,R_ACTION => 'rep.act.'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_CLIENTS => FAF_MROS_IdRsn_t(
		FAF_MROS_IdRsn(10140,NULL,NULL,'person 1'),
		FAF_MROS_IdRsn(NULL,'512.019',NULL,'person 2'),
		FAF_MROS_IdRsn(NULL,'520.100',NULL,'entity 1'),
		FAF_MROS_IdRsn(2000149,NULL,NULL,'entity 2'),
		FAF_MROS_IdRsn(NULL,'550.112',0,'family 1'),
		FAF_MROS_IdRsn(2001147,NULL,NULL,'family 2'))
	,SET_OF_ACCOUNTS => FAF_MROS_IdRsn_t(
		FAF_MROS_IdRsn(10065,NULL,NULL,'account with person owner'),
		FAF_MROS_IdRsn(44620222370057,NULL,NULL,'account with entity owner1'),
		FAF_MROS_IdRsn(NULL,'500.445.010',NULL,'account with entity owner2'),
		FAF_MROS_IdRsn(10919,NULL,NULL,'account with family owner'))
) FROM DUAL; 
/

--report with Transaction node
SELECT FAF_MROS.set_of_payments (
	F_BANK_ID => 949
	,SOURCE_ID => 0
	,ENTITY_REFERENCE => 'ent.ref.'
	,REPORT_REASON => 'rep.reas.'
	,R_ACTION => 'rep.act.'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_PAYMENT_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(58130244425175),
		FAF_MROS_numID(58130244425108),
		FAF_MROS_numID(59426249919664),
		FAF_MROS_numID(58357283301609),
		FAF_MROS_numID(58185780398462)
	)
	,SET_OF_ORDER_ID => FAF_MROS_numID_t(
        FAF_MROS_numID(58277119821140)
	)
	,SET_OF_ORDER_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('ZV20160620/066787')
	)
) FROM DUAL; 
/

		 
--TRANSACTION HISTORY REPORT   
--report with Transactions from kt_buch
SELECT FAF_MROS.report_client_trans (
	F_BANK_ID => 949
	,SOURCE_ID=> 1
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_TR_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(58337585423330),
		FAF_MROS_numID(58337585423331)
	)
	,SET_OF_CL_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(39367633705787)
	)
	,SET_OF_CL_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('501.753')
	)
	,SET_OF_AC_ID => FAF_MROS_numID_t(
		FAF_MROS_numID(1001500)
	)
	,SET_OF_AC_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('520.753.457')
	)
	,DATE_FROM => 20160627
	,DATE_TO => 20160627
	,LOW_AMOUNT => 1000
	,DEBIT => 1
	,CREDIT => 1
	,N_TRANS_LIMIT => 10
) FROM DUAL;  
/





