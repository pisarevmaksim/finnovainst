﻿truncate table FA_MROS_KD_ADR;
truncate table FA_MROS_KD_VSB;
truncate table FA_MROS_KD_EMAIL;
truncate table FA_MROS_KD_STAMM;
truncate table FA_MROS_KT_STAMM;
truncate table FA_MROS_ZV_ZLG_T;
truncate table FA_MROS_ZV_AUF;
truncate table FA_MROS_KT_SALDO;
truncate table FA_MROS_KD_KD;
truncate table FA_MROS_VT_STAMM;
truncate table FA_MROS_VT_ZUORD;
truncate table FA_MROS_SR_FELD_WERT;
commit;

--address
insert into FA_MROS_KD_ADR
select 
	adr.userbk_nr,
	adr.kd_lnr,
	adr.strasse,
	adr.ORT_BEZ as ADR_ORT_BEZ,
	adr.region_cd,
	adr.PLZ as ADR_PLZ,
	ort.ORT_BEZ as ORT_ORT_BEZ,
	ort.PLZ as ORT_PLZ,
	lnd.iso_land_cd,
	lnd.bez_lang as LAND_BEZ
from 
	F2LDATP0.KD_ADR adr
left join
	F2LDATP0.AL_ORT ort
on adr.userbk_nr=ort.userbk_nr and adr.ort_lnr=ort.ort_lnr
left join
	F2LDATP0.AL1LAND lnd
on adr.userbk_nr=lnd.userbk_nr and adr.land_lnr=lnd.land_lnr and lnd.sprache_cd=5
where adr.Strasse is not null 
	and adr.kd_adr_lnr=11 
	and (adr.von_dat is null or adr.von_dat < sysdate) 
	and (adr.bis_dat is null or adr.bis_dat >= sysdate);

--person_id
insert into FA_MROS_KD_VSB
select 
	vsb.userbk_nr,
	vsb.kd_lnr,
	vsb.iddoc_cd,
	vsb.kd_ausw_nr,
	vsb.ausstell_dat,
	vsb.abl_dat,
	vsb.kd_vsb_beglaub,
	lnd.iso_land_cd,
	lnd.bez_lang as LAND_BEZ
from 
	F2LDATP0.KD_VSB vsb
left join
	F2LDATP0.AL1LAND lnd
on vsb.userbk_nr=lnd.userbk_nr and vsb.land_lnr=lnd.land_lnr and lnd.sprache_cd=5
where NVL(vsb.kd_vsb_ok_cd, '1') = '1';

--phone
insert into FA_MROS_KD_EMAIL
select distinct 
	adr.userbk_nr,
	adr.kd_lnr,
	eml.mail_art_cd,
	eml.email_adr
from
	F2LDATP0.KD_ADR adr
join
	F2LDATP0.KD_EMAIL eml
on adr.userbk_nr=eml.userbk_nr and adr.kd_email_lnr=eml.kd_email_lnr
where eml.email_adr is not null
	and adr.kd_adr_lnr=11 
	and (adr.von_dat is null or adr.von_dat < sysdate) 
	and (adr.bis_dat is null or adr.bis_dat >= sysdate);

--clients
insert into FA_MROS_KD_STAMM
with
	cntr as
		(select userbk_nr, land_lnr, iso_land_cd, bez_lang from F2LDATP0.al1land where sprache_cd=5)
select 
	kd.userbk_nr,
	kd.kd_lnr,
	kd.edit_kd_nr,
	kd.kd_htyp_cd,
	kd.partner_flag,
	case substr(kdkat.steuer_cd, 9, 1)
		when 'B' then 1
		when 'I' then 1
		when 'O' then 1
		when 'K' then 0
		when 'N' then 0 
		when 'P' then 0 
	end as bank_flag,	
	kd.geschlecht_cd,
	kd.vorname,
	kd.name_1,
	kd.kd_bez,
	kd.kd_rstat_cd,
	rstat.bez_lang as KD_RSTAT_BEZ,
	kd.kd_rstat_ausp_cd,
	rausp.bez_lang as KD_RSTAT_AUSP_BEZ,
	kd.kdkat_cd,
	kdkat.bez_lang as kdkat_bez,
	kd.kd_segm_cd,
	segm.bez_lang as KD_SEGM_BEZ,
	kd.rform_cd,
	rform.bez_lang as RFORM_BEZ,
	kd.uid_nr,
	kd.geburt_dat,
	kd.geburt_jahr,
	case
		when kd.geburt_jahr is not null then
			case
				when extract(year from sysdate) >=kd.geburt_jahr then extract(year from sysdate) - kd.geburt_jahr
			end
		else
			case
				when kd.geburt_dat is not null then
					case
						when extract(year from sysdate) >= extract(year from kd.geburt_dat) then extract(year from sysdate) - extract(year from kd.geburt_dat)
						else null
					end
			end
	end as ALTER_JAHR,
	kd.tod_dat,
	kd.tod_jahr,	
	case 
		when kd.EROEFF_DAT<to_date('01.01.1801','dd.mm.yyyy') 
		then 999 
		else NVL(round(((select sysdate from DUAL)-kd.EROEFF_DAT)/365,2),0)
	end as EROEFF_DAT,
	kd.kd_sitz_gsch_cd,	
	tit.bez_lang as titel,
	ort.ORT_BEZ as GEBURT_ORT,
	kd.noga_cd,
	kd.fin_vollm_cd,
	noga.bez_lang as NOGA_BEZ,	
	kd.beruf_cd,
	beru.bez_lang as BERUF_BEZ,
	round(NVL(kd.KAP,0)/1000) as KAP,
	emai.email_adr,
	kd.kd_vip_cd,
	kd.kd_pep_cd,
	kd.kd_pep_cd_auto,
	kd.kd_pep_dt_auto,
	kd.kd_grp_art_cd,
	kd.land_lnr_nat,
	nat1.bez_lang as LAND_LNR_NAT_BEZ,
	nat1.iso_land_cd as ISO_LAND_CODE_NAT,
	kd.land_lnr_nat_2,
	nat2.bez_lang as LAND_LNR_NAT_BEZ_2,
	nat2.iso_land_cd as ISO_LAND_CODE_NAT_2,
	kd.land_lnr_ausl,
	ausl.bez_lang as LAND_LNR_AUSL_BEZ,
	ausl.iso_land_cd as ISO_CODE_AUSL,
	to_number(to_char(kd.eroeff_dat, 'yyyymmdd')) as EROEFF_DAT_REL,
	kd.kd_stat_cd,
	gw.kd_gwv_cd,
	gw.sys_dt as kd_gw_sys_dt,
	gw_cd.bez_lang as kd_gwv_code_bez,
	vsb.kd_vsb_stat_cd, 
	vsbst.bez_lang as KD_VSB_STAT_BEZ,
	vsb.vsb_stat_dat		
from
	(select
		userbk_nr,
		kd_lnr,
		edit_kd_nr,
		0 as partner_flag,
		geschlecht_cd,
		vorname,
		name_1,
		kd_htyp_cd,
		geburt_dat,
		geburt_jahr,
		tod_dat,
		kd_grp_art_cd,
		beruf_cd,
		tod_jahr,
		titel_cd,
		ort_lnr_heimat,
		land_lnr_nat,
		land_lnr_nat_2,
		land_lnr_ausl,
		eroeff_dat,
		noga_cd,
		kd_sitz_gsch_cd,
		uid_nr,
		kd_rstat_cd,
		kd_bez,
		kap,
		kd_pep_cd,
		kd_vip_cd,
		rform_cd,
		kd_segm_cd,
		kd_pep_cd_auto,
		kd_pep_dt_auto,
		fin_vollm_cd,
		kd_rstat_ausp_cd,
		kd_stat_cd,
		kdkat_cd
	from
		F2LDATP0.kd_stamm
	union all
	select
		userbk_nr,
		kd_lnr,
		edit_kd_nr,
		1 as partner_flag,
		geschlecht_cd_p as geschlecht_cd,
		vorname_p as vorname,
		name_1_p as name_1,
		kd_htyp_cd,
		geburt_dat_p as geburt_dat,
		geburt_jahr_p as geburt_jahr,
		tod_dat_p as tod_dat,
		kd_grp_art_cd,
		beruf_cd,
		tod_jahr_p as tod_jahr,
		titel_cd_p as titel_cd,
		ort_lnr_heimat_p as ort_lnr_heimat,
		land_lnr_nat_p as land_lnr_nat,
		land_lnr_nat_p_2 as land_lnr_nat_2,
		land_lnr_ausl,
		eroeff_dat,
		null as noga_cd,
		null as kd_sitz_gsch_cd,
		null as uid_nr,
		null as kd_rstat_cd,
		null as kd_bez,
		null as kap,
		kd_pep_cd,
		kd_vip_cd,
		rform_cd,
		kd_segm_cd,
		kd_pep_cd_auto,
		kd_pep_dt_auto,
		fin_vollm_cd,
		kd_rstat_ausp_cd,
		kd_stat_cd,
		kdkat_cd
	from
		F2LDATP0.kd_stamm
	where
		kd_htyp_cd=2) kd
left join
	(select distinct userbk_nr, titel_cd, 
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,titel_cd order by VERS desc) as bez_lang
	from F2LDATP0.kd1titel where sprache_cd=5) tit
on kd.userbk_nr=tit.userbk_nr and kd.titel_cd=tit.titel_cd
left join
	F2LDATP0.al_ort ort
on kd.userbk_nr=ort.userbk_nr and kd.ort_lnr_heimat=ort.ort_lnr
left join
	F2LDATP0.al12noga_08 noga
on kd.userbk_nr=noga.userbk_nr and kd.noga_cd=noga.noga_cd and noga.sprache_cd=5
left join
	(select distinct userbk_nr, kd_rstat_cd, 
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,kd_rstat_cd order by VERS desc) as bez_lang 
	from F2LDATP0.kd1rstat where sprache_cd=5) rstat
on kd.userbk_nr=rstat.userbk_nr and kd.kd_rstat_cd=rstat.kd_rstat_cd
left join
	F2LDATP0.kd1segm segm
on kd.userbk_nr=segm.userbk_nr and kd.kd_segm_cd=segm.kd_segm_cd and segm.sprache_cd=5
left join
	(select distinct userbk_nr, rform_cd, 
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,rform_cd order by VERS desc) as bez_lang 
	from F2LDATP0.kd1rform where sprache_cd=5) rform
on kd.userbk_nr=rform.userbk_nr and kd.rform_cd=rform.rform_cd
left join
	(select distinct userbk_nr, beruf_cd, 
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,beruf_cd order by VERS desc) as bez_lang 
	from F2LDATP0.kd12beruf where sprache_cd=5) beru
on kd.userbk_nr=beru.userbk_nr and kd.beruf_cd=beru.beruf_cd
left join
	F2LDATP0.kd1rstat_ausp rausp
on kd.userbk_nr=rausp.userbk_nr and kd.kd_rstat_ausp_cd=rausp.kd_rstat_ausp_cd and rausp.sprache_cd=5
left join
	(select
		userbk_nr,
		kd_lnr,
		listagg(email_adr,'; ') within group (order by email_adr) as email_adr
	from
		(select distinct adr.userbk_nr, adr.kd_lnr,
			lower(ema.email_adr) as email_adr
		from
			F2LDATP0.kd_adr adr
		join 
			F2LDATP0.kd_email ema
		on ema.userbk_nr=adr.userbk_nr and ema.kd_email_lnr = adr.kd_email_lnr
		where ema.mail_art_cd ='INET'
			and adr.kd_adr_lnr = 11
			and trunc(sysdate) between coalesce(von_dat, to_date('01.01.1600', 'dd.mm.yyyy')) and coalesce(bis_dat, to_date('31.12.2299', 'dd.mm.yyyy'))
		)
	group by userbk_nr, kd_lnr
	) emai 
on kd.userbk_nr=emai.userbk_nr and kd.kd_lnr=emai.kd_lnr
left join 
	cntr nat1
on kd.userbk_nr=nat1.userbk_nr and kd.land_lnr_nat=nat1.land_lnr
left join 
	cntr nat2
on kd.userbk_nr=nat2.userbk_nr and kd.land_lnr_nat_2=nat2.land_lnr
left join 
	cntr ausl
on kd.userbk_nr=ausl.userbk_nr and kd.land_lnr_ausl=ausl.land_lnr
left join 
	F2LDATP0.kd_gwv_stuf gw
on gw.userbk_nr=kd.userbk_nr and gw.kd_lnr = kd.kd_lnr
left join 
	F2LDATP0.kd1gwv gw_cd
on gw_cd.userbk_nr=gw.userbk_nr and gw_cd.kd_gwv_cd = gw.kd_gwv_cd and gw_cd.sprache_cd = 5
left join 
	F2LDATP0.kd_vsb vsb
on vsb.userbk_nr=kd.userbk_nr and vsb.kd_lnr = kd.kd_lnr 
left join 
	(select distinct userbk_nr, kd_vsb_stat_cd, 
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,kd_vsb_stat_cd order by VERS desc) as bez_lang 
	from F2LDATP0.kd1vsb_stat where sprache_cd=5) vsbst	 
on vsbst.userbk_nr=vsb.userbk_nr and vsbst.kd_vsb_stat_cd = vsb.kd_vsb_stat_cd
left join
	F2LDATP0.kd1kdkat kdkat
on kd.userbk_nr = kdkat.userbk_nr and kd.kdkat_cd = kdkat.kdkat_cd and kdkat.sprache_cd=1;

--accounts
insert into FA_MROS_KT_STAMM
select 
	kt.userbk_nr,
	kt.kd_lnr,
	kt.kt_lnr,
	kt.edit_kt_nr,
	kt.sitz_nr,
	sitz.bez_lang as SITZ_NR_BEZ,
	kt.iban_id,
	kt.eroeff_dat,
	kt.aufheb_dat,
	kt.sperr_cd,
	kt.aufheb_cd,
	kt.rubrik,
	kt.wrg_lnr,
	wrg.bez_lang as WRG_BEZ,
	wrg.iso_wrg_cd,
	kt.kt_art_nr,
	art.bez_lang as KT_ART_NR_BEZ,
	art.kt_art_ber_cd,
	artbe.bez_lang as KT_ART_BER_BEZ,
	kt.arbgeb_nr, 
	artbge.bez_lang as ARBGEB_NR_BEZ,
	case 
		when kt.SPERR_CD is not null then 0
		when kt.AUFHEB_DAT<(select sysdate from DUAL) then 0 
		else round(((select sysdate from DUAL)-kt.EROEFF_DAT)/365,2)
	end as ARBGEB_NR_BEZ
from 
	F2LDATP0.kt_stamm kt
left join
	F2LDATP0.AL1WRG wrg
on kt.userbk_nr=wrg.userbk_nr and kt.wrg_lnr=wrg.wrg_lnr and wrg.sprache_cd=5
left join
	F2LDATP0.kt1art art
on kt.userbk_nr=art.userbk_nr and kt.kt_art_nr=art.kt_art_nr and art.sprache_cd=5
left join
	(select distinct userbk_nr,kt_art_ber_cd,sprache_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,kt_art_ber_cd,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.KT1ARTBE) artbe
on artbe.userbk_nr=art.userbk_nr and artbe.kt_art_ber_cd=art.kt_art_ber_cd and artbe.sprache_cd=5
left join
	(select distinct userbk_nr,arbgeb_nr,sprache_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,arbgeb_nr,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.AL1ARBGE) artbge
on artbge.userbk_nr=art.userbk_nr and artbge.arbgeb_nr=art.arbgeb_nr and artbge.sprache_cd=5
left join
	F2LDATP0.AL1SITZ sitz
on kt.userbk_nr=sitz.userbk_nr and kt.sitz_nr=sitz.sitz_nr and sitz.sprache_cd=5;

--payments
insert into FA_MROS_ZV_ZLG_T
select
	zlg.userbk_nr,
	zlg.zv_zlg_sys_lnr,
	zlg.auf_lnr,
	om.auf_id,
	tr_typ.bez_lang as TRAK_REF_TYP_BEZ,
	zlg.trak_ref_typ,
	zlg.trak_ref_lnr,
	om.kd_lnr,
	om.kt_lnr,   
	om.om_text_lnr as auf_om_text_lnr,  
	auf.zv_adr_eff_lnr as auf_zv_adr_eff_lnr,
	auf.zv_eing_ausg_cd,
	om.land_lnr_dom as auf_land_lnr_dom,
	auf.trak_ref_typ as auf_trak_ref_typ,
	auf.trak_ref_lnr as auf_trak_ref_lnr,
	zlg.kt_lnr_b,
	zlg.om_text_lnr,
	zlg.zv_adr_eff_lnr,
	zlg.land_lnr_dom,
	om.SITZ_NR_AUSF,
	sitz.bez_lang as SITZ_NR_BEZ,
	om.ausf_dt_ist,
	om.ausf_dt_soll,
	om.auf_dt_ert,
	NVL(zlg.valuta_man, zlg.valuta) as VALUTA,
	NVL(rt_btpm.finnova_value,rt_btp.finnova_value) as buchtyp_cd, 
	case when rt_btpm.finnova_value is not null then typ_m.bez_lang else typ.bez_lang end as BUCHTYP_BEZ,		
	zlg.BETRAG_BWRG,
	zlg.BETRAG_ZWRG,
	zlg.sh_cd,
	wrg.iso_wrg_cd,
	wrg.bez_lang as WRG_BEZ,
	gfa.gf_art,
	gfa.bez_lang as GF_ART_BEZ,
	gf.gf_nr,
	gf.bez_lang as GF_NR_BEZ
from
	F2LDATP0.ZV_ZLG_T zlg
join
	F2LDATP0.OM_AUF om
on zlg.userbk_nr=om.userbk_nr and zlg.auf_lnr=om.auf_lnr
join
	F2LDATP0.ZV_AUF_T auf
on auf.userbk_nr=om.userbk_nr and auf.auf_lnr=om.auf_lnr
join
	FA_MROS_REFERENCE_TABLE rt_trtp
on zlg.userbk_nr=rt_trtp.userbk_nr and zlg.trak_ref_typ=rt_trtp.finnova_value and rt_trtp.ref_type=11 and rt_trtp.activ_MROS=1
left join
	FA_MROS_REFERENCE_TABLE rt_btp
on zlg.userbk_nr=rt_btp.userbk_nr and zlg.buchtyp_cd=rt_btp.finnova_value and rt_btp.ref_type=10 and rt_btp.activ_MROS=1
left join
	FA_MROS_REFERENCE_TABLE rt_btpm
on zlg.userbk_nr=rt_btpm.userbk_nr and zlg.buchtyp_cd_man=rt_btpm.finnova_value and rt_btpm.ref_type=10 and rt_btpm.activ_MROS=1
left join
	F2LDATP0.AL1GFART gfa
on zlg.userbk_nr=gfa.userbk_nr and zlg.gf_art=gfa.gf_art and gfa.sprache_cd=5
left join
	F2LDATP0.AL1GF gf
on zlg.userbk_nr=gf.userbk_nr and zlg.gf_nr=gf.gf_nr and gf.sprache_cd=5
left join
	F2LDATP0.AL1WRG wrg
on zlg.userbk_nr=wrg.userbk_nr and zlg.zwrg_lnr=wrg.wrg_lnr and wrg.sprache_cd=5
--transaction type from buchtyp_cd
left join
	(select distinct userbk_nr,buchtyp_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,buchtyp_cd order by VERS desc) as bez_lang
	from F2LDATP0.KT1BUCHT where sprache_cd=5) typ
on zlg.userbk_nr=typ.userbk_nr and zlg.buchtyp_cd=typ.buchtyp_cd
--transaction type from buchtyp_cd_man
left join
	(select distinct userbk_nr,buchtyp_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,buchtyp_cd order by VERS desc) as bez_lang
	from F2LDATP0.KT1BUCHT where sprache_cd=5) typ_m
on zlg.userbk_nr=typ_m.userbk_nr and zlg.buchtyp_cd_man=typ_m.buchtyp_cd
--transaction purpose
left join
	(select distinct userbk_nr,trak_ref_typ,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,trak_ref_typ order by VERS desc) as bez_lang
	from F2LDATP0.AL1TRAK_REF_ART where sprache_cd=5) tr_typ
on zlg.userbk_nr=tr_typ.userbk_nr and zlg.trak_ref_typ=tr_typ.trak_ref_typ
--branch
left join
	F2LDATP0.AL1SITZ sitz
on om.userbk_nr=sitz.userbk_nr and om.sitz_nr_ausf=sitz.sitz_nr and sitz.sprache_cd=5
where om.obj_stat_cd in (120, 130, 140, 800, 900)
	and om.obj_id_typ =2500
	and (rt_btpm.finnova_value is not null or (zlg.buchtyp_cd_man is null and rt_btp.finnova_value is not null))
--	and om.auf_dt_ert between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
;

--ZV_AUF_ALL
insert into FA_MROS_ZV_AUF
select zv_auf.userbk_nr, zv_auf.auf_lnr,zv_auf.auf_id, zv_auf.trak_ref_typ, zv_auf.trak_ref_lnr, zv_auf.ORDER_CD, zv_auf.ZV_ZLG_SYS_LNR,
	zv_auf.kt_lnr, zv_auf.kd_lnr, zv_auf.sh_cd, zv_auf.BETRAG_BWRG, zv_auf.BETRAG_KWRG,zv_auf.BETRAG_ZWRG,  
	zv_auf.zwrg_lnr,zv_auf.zv_adr_eff_lnr, zv_auf.om_text_lnr, zv_auf.gf_nr,zv_auf.gf_art,
	zv_auf.land_lnr_dom,zv_auf.land_lnr_dom1,zv_auf.buchtyp_cd, 
	zv_auf.VALUTA, zv_auf.ausf_dt_ist, zv_auf.ausf_dt_soll, zv_auf.auf_dt_ert, zv_auf.SITZ_NR_AUSF,
	tr_typ.bez_lang as TRAK_REF_TYP_BEZ,
	t_typ.bez_lang as BUCHTYP_BEZ,
	wrg.iso_wrg_cd,
	wrg.bez_lang as WRG_BEZ,
	gfa.bez_lang as GF_ART_BEZ,
	gf.bez_lang as GF_NR_BEZ,
	stz.bez_lang as SITZ_NR_BEZ
from
	(select z.userbk_nr, z.auf_lnr,o.auf_id, z.trak_ref_typ, z.trak_ref_lnr, 'Z' as ORDER_CD, z.zv_zlg_sys_lnr,
		z.kt_lnr_b as kt_lnr, NULL as kd_lnr, z.sh_cd, 
		z.BETRAG_BWRG, 
		z.BETRAG_KWRG,
		z.BETRAG_ZWRG,  
		z.zwrg_lnr,z.zv_adr_eff_lnr, z.om_text_lnr, z.gf_nr,z.gf_art,
		z.land_lnr_dom,o.land_lnr_dom as land_lnr_dom1,
		NVL(z.buchtyp_cd_man,z.buchtyp_cd) as buchtyp_cd,
		NVL(z.valuta_man, z.valuta) as VALUTA,
		o.ausf_dt_ist,
		o.ausf_dt_soll,
		o.auf_dt_ert,
		o.SITZ_NR_AUSF	
	from	
		F2LDATP0.zv_zlg_t z
	join
		F2LDATP0.om_auf o
	on o.userbk_nr = z.userbk_nr and o.auf_lnr = z.auf_lnr
	join
		FA_MROS_REFERENCE_TABLE rt_trtp
	on  z.userbk_nr=rt_trtp.userbk_nr and z.trak_ref_typ=rt_trtp.Finnova_value and rt_trtp.ref_type=11 and rt_trtp.activ_MROS=1
	left join
		FA_MROS_REFERENCE_TABLE rt_btp
	on z.userbk_nr=rt_btp.userbk_nr and z.buchtyp_cd=rt_btp.finnova_value and rt_btp.ref_type=10 and rt_btp.activ_MROS=1
	left join
		FA_MROS_REFERENCE_TABLE rt_btpm
	on z.userbk_nr=rt_btpm.userbk_nr and z.buchtyp_cd_man=rt_btpm.finnova_value and rt_btpm.ref_type=10 and rt_btpm.activ_MROS=1
	where
		o.obj_stat_cd in (120, 130, 140, 800, 900)
		and o.obj_id_typ = 2500
		and z.trak_ref_lnr is not null
		and (rt_btpm.finnova_value is not null or (z.buchtyp_cd_man is null and rt_btp.finnova_value is not null))
--		and z.create_dt between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
--		and o.create_dt between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
	union all
	select a.userbk_nr, a.auf_lnr, o.auf_id, a.trak_ref_typ, a.trak_ref_lnr, 'A' as ORDER_CD, cast(null as number(22)) as ZV_ZLG_SYS_LNR,
		o.kt_lnr, o.kd_lnr, a.sh_cd, 
		a.auf_tot_bwrg as BETRAG_BWRG, 
		a.auf_tot_kwrg as BETRAG_KWRG, 
		nvl(a.auf_tot_zwrg_man,a.auf_tot_zwrg) as BETRAG_ZWRG,
		a.zwrg_lnr,a.zv_adr_eff_lnr, o.om_text_lnr, a.gf_nr,a.gf_art,
		1 as land_lnr_dom,o.land_lnr_dom as land_lnr_dom1,
		NVL(a.buchtyp_cd_man,a.buchtyp_cd) as buchtyp_cd,
		NVL(a.valuta_man, a.valuta) as VALUTA,
		o.ausf_dt_ist,
		o.ausf_dt_soll,
		o.auf_dt_ert,
		o.SITZ_NR_AUSF
	from
		F2LDATP0.zv_auf_t a
	join
		F2LDATP0.om_auf o
	on o.userbk_nr = a.userbk_nr and o.auf_lnr = a.auf_lnr
	join
		FA_MROS_REFERENCE_TABLE rt_trtp
	on  a.userbk_nr=rt_trtp.userbk_nr and a.trak_ref_typ=rt_trtp.Finnova_value and rt_trtp.ref_type=11 and rt_trtp.activ_MROS=1
	left join
		FA_MROS_REFERENCE_TABLE rt_btp
	on a.userbk_nr=rt_btp.userbk_nr and a.buchtyp_cd=rt_btp.finnova_value and rt_btp.ref_type=10 and rt_btp.activ_MROS=1
	left join
		FA_MROS_REFERENCE_TABLE rt_btpm
	on a.userbk_nr=rt_btpm.userbk_nr and a.buchtyp_cd_man=rt_btpm.finnova_value and rt_btpm.ref_type=10 and rt_btpm.activ_MROS=1
	where
		o.obj_stat_cd in (120, 130, 140, 800, 900)
		and o.obj_id_typ = 2500
		and a.trak_ref_lnr is not null
		and (rt_btpm.finnova_value is not null or (a.buchtyp_cd_man is null and rt_btp.finnova_value is not null))
--		and a.create_dt between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
--		and o.create_dt between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
	union all
	select a.userbk_nr, a.auf_lnr, o.auf_id, a.trak_ref_typ, z.zv_zlg_sys_lnr as trak_ref_lnr, 'A' as ORDER_CD, cast(null as number(22)) as ZV_ZLG_SYS_LNR, 
		o.kt_lnr, o.kd_lnr, case when z.sh_cd='1' then '2' else '1' end as sh_cd, 
		z.BETRAG_BWRG, 
		z.BETRAG_KWRG,
		z.BETRAG_ZWRG, 
		z.zwrg_lnr,a.zv_adr_eff_lnr, o.om_text_lnr, a.gf_nr,a.gf_art,
		1 as land_lnr_dom, o.land_lnr_dom as land_lnr_dom1,
		NVL(a.buchtyp_cd_man,a.buchtyp_cd) as buchtyp_cd,
		NVL(a.valuta_man, a.valuta) as VALUTA,
		o.ausf_dt_ist,
		o.ausf_dt_soll,
		o.auf_dt_ert,
		o.SITZ_NR_AUSF
	from
		F2LDATP0.zv_auf_t a
	join
		F2LDATP0.zv_zlg_t z
	on z.userbk_nr = a.userbk_nr and z.auf_lnr = a.auf_lnr and a.trak_ref_lnr is null and z.trak_ref_lnr is not null
	join
		F2LDATP0.om_auf o
	on o.userbk_nr = a.userbk_nr and o.auf_lnr = a.auf_lnr
	join
		FA_MROS_REFERENCE_TABLE rt_trtp
	on  a.userbk_nr=rt_trtp.userbk_nr and a.trak_ref_typ=rt_trtp.Finnova_value and rt_trtp.ref_type=11 and rt_trtp.activ_MROS=1
	left join
		FA_MROS_REFERENCE_TABLE rt_btp
	on a.userbk_nr=rt_btp.userbk_nr and a.buchtyp_cd=rt_btp.finnova_value and rt_btp.ref_type=10 and rt_btp.activ_MROS=1
	left join
		FA_MROS_REFERENCE_TABLE rt_btpm
	on a.userbk_nr=rt_btpm.userbk_nr and a.buchtyp_cd_man=rt_btpm.finnova_value and rt_btpm.ref_type=10 and rt_btpm.activ_MROS=1
	where
		o.obj_stat_cd in (120, 130, 140, 800, 900)
		and o.obj_id_typ = 2500
		and (rt_btpm.finnova_value is not null or (a.buchtyp_cd_man is null and rt_btp.finnova_value is not null))
--		and a.create_dt between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
--		and z.create_dt between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
--		and o.create_dt between to_date('01.01.2018','dd.mm.yyyy') and to_date('31.12.2018','dd.mm.yyyy')
	) zv_auf
left join
	F2LDATP0.AL1GFART gfa
on zv_auf.userbk_nr=gfa.userbk_nr and zv_auf.gf_art=gfa.gf_art and gfa.sprache_cd=5
left join
	F2LDATP0.AL1GF gf
on zv_auf.userbk_nr=gf.userbk_nr and zv_auf.gf_nr=gf.gf_nr and gf.sprache_cd=5
left join
	F2LDATP0.AL1WRG wrg
on zv_auf.userbk_nr=wrg.userbk_nr and zv_auf.zwrg_lnr=wrg.wrg_lnr and wrg.sprache_cd=5
--transaction type from buchtyp_cd
left join
	(select distinct userbk_nr,buchtyp_cd,sprache_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,buchtyp_cd,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.KT1BUCHT) t_typ
on zv_auf.userbk_nr=t_typ.userbk_nr and zv_auf.buchtyp_cd=t_typ.buchtyp_cd and t_typ.sprache_cd=5
--transaction purpose
left join
	(select distinct userbk_nr,trak_ref_typ,sprache_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,trak_ref_typ,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.AL1TRAK_REF_ART) tr_typ
on zv_auf.userbk_nr=tr_typ.userbk_nr and zv_auf.trak_ref_typ=tr_typ.trak_ref_typ and tr_typ.sprache_cd=5
--branch
left join
    F2LDATP0.AL1SITZ stz
on zv_auf.userbk_nr=stz.userbk_nr and zv_auf.SITZ_NR_AUSF=stz.sitz_nr and stz.sprache_cd=5
where auf_id is not null;

--saldo
insert into FA_MROS_KT_SALDO
select
	x.userbk_nr,
	x.kt_lnr,
	SUM(NVL(S.saldo_bwrg, 0) + NVL(X.saldo_bwrg, 0)) as saldo_bwrg,
	SUM(NVL(S.saldo_kwrg, 0) + NVL(X.saldo_kwrg, 0)) as saldo_kwrg
from 
	F2LDATP0.KT_SALDO X
left join
	F2LDATP0.KT_SALDO_DISP S
on S.kt_lnr = X.kt_lnr and S.kt_best_lnr = X.kt_best_lnr and S.userbk_nr = X.userbk_nr
group by x.userbk_nr,x.kt_lnr;

--KD_KD
insert into FA_MROS_KD_KD
select 
	userbk_nr,
	kd_kd_lnr,
	kd_lnr,
	kd_lnr_zuw,
	valid_bis_dat,
	kd_kd_verb_cd,
	btlg_satz
from F2LDATP0.KD_KD;

--vt_stamm
insert into FA_MROS_VT_STAMM
select 
	vt.userbk_nr,
	vt.vt_lnr,
	vt.vt_typ_cd,
	v_typ.bez_lang as VT_type,
	vt.vt_stat_cd,
	v_st.bez_lang as VT_status,
	vt.vt_stat_dt,
	vt.kd_lnr,
	vt.kt_lnr,
	vt.kt_gnr,
	vt.beginn_dat,
	vt.ende_dat 
from 
	F2LDATP0.vt_stamm vt
left join
	(select distinct userbk_nr,vt_stat_cd,sprache_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,vt_stat_cd,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.VT1STAT) v_st
on vt.userbk_nr=v_st.userbk_nr and vt.vt_stat_cd=v_st.vt_stat_cd and v_st.sprache_cd=5
left join
	(select distinct userbk_nr,vt_typ_cd,sprache_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,vt_typ_cd,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.VT1TYP) v_typ
on vt.userbk_nr=v_typ.userbk_nr and vt.vt_typ_cd=v_typ.vt_typ_cd and v_typ.sprache_cd=5
where vt.vt_stat_cd = 90
	and vt.vt_typ_cd in (2, 233, 310, 702, 6001, 6039, 6100, 6101, 6102, 6103, 6104, 6105, 6106, 6107, 6108, 6109);

--vt_zuord
insert into FA_MROS_VT_ZUORD
select 
	vtz.userbk_nr,
	vtz.vt_lnr,
	vtz.kd_lnr,
	vtz.kt_lnr,
	vtz.kt_gnr,
	vtz.sb_sign_recht_cd,
	rcht.bez_lang as rights
from 
	F2LDATP0.vt_zuord vtz
join 
	F2LDATP0.vt_stamm vts
on vts.userbk_nr=vtz.userbk_nr and vts.vt_lnr = vtz.vt_lnr
left join
	(select distinct userbk_nr,sb_sign_recht_cd,sprache_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,sb_sign_recht_cd,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.SB1SIGN_RECHT) rcht
on vtz.userbk_nr=rcht.userbk_nr and vtz.sb_sign_recht_cd=rcht.sb_sign_recht_cd and rcht.sprache_cd=5
where vts.vt_stat_cd = 90
	and vts.vt_typ_cd in (2, 233, 310, 702, 6001, 6039, 6100, 6101, 6102, 6103, 6104, 6105, 6106, 6107, 6108, 6109);

--FA_MROS_SR_FELD_WERT
insert into FA_MROS_SR_FELD_WERT
select 
	kd.userbk_nr,
	kd.kd_lnr,
	sfz.sr_feld_zus_lnr,
	sfz.sr_feld_bez,
	sfw.Feld_wert
from 
	F2LDATP0.sr_feld_zus sfz
join 
	F2LDATP0.sr_feld_wert sfw
on sfw.userbk_nr = sfz.userbk_nr and sfw.sr_feld_zus_lnr = sfz.sr_feld_zus_lnr
join 
	F2LDATP0.kd_stamm kd
on kd.userbk_nr = sfw.userbk_nr and kd.create_id = sfw.create_id
where sfz.Sr_feld_bez in ('ARBEITGEBER2',
	'BERUF/FUNKTION',
	'BERUFLICHE STELLUNG DER LETZTE',
	'BERUFLICHER WERDEGANG',
	'BRANCHE DER TÄTIGKEIT',
	'BRANCHE DER TÄTIGKEIT 2',
	'ERWERBSTÄTIGKEIT',
	'ERWERBSTÄTIGKEIT 2',
	'GESAMTVERMÖGEN',
	'GESCHÄFTSTÄTIGKEIT',
	'GESCHÄFTSTÄTIGKEIT IM AUSL. 5',
	'GESCHÄFTSTÄTIGKEIT IM AUSLAND',
	'GESCHÄFTSTÄTIGKEIT IM AUSLAND - LÄNDER',
	'GESCHÄFTSTÄTIGKEIT IM AUSLAND - LÄNDER 2',
	'JÄHRLICHER UMSATZ',
	'JÄHRLICHES EINKOMMEN',
	'ORGANISATION - ANZAHL MA',
	'ORGANISATION - BEHERRSCHUNG',
	'ORGANISATION - BEHERRSCHUNG JU',
	'ORGANISATION - BÖRSENKOTIERUNG',
	'ORGANISATION - BÜRO GESCHÄFTSR',
	'ORGANISATION - KUNDE IST TEIL',
	'SONSTIGE (FREITEXT)',
	'SONSTIGE (FREITEXT) 2',
	'SONSTIGE (FREITEXT) 3',
	'SONSTIGE (FREITEXT) 4',
	'SONSTIGE (FREITEXT) 5',
	'SONSTIGE (FREITEXT) 6',
	'SONSTIGE (FREITEXT) 7',
	'SONSTIGE (FREITEXT) 8',
	'SONSTIGES (TEXTFELD)',
	'SONSTIGES (TEXTFELD) 7',
	'WEITERGEHENDE INFORMATIONEN',
	'WESHALB/WOZU BEZIEHUNG Z. BANK',
	'WESHALB/WOZU WIBE EINE BEZIEHU',
	'WIRTSCHAFTLICHER HINTERGRUND',
	'WIRTSCHAFTLICHER HINTERGRUND /',
	'WIRTSCHAFTLICHER HINTERGRUND 2',
	'ZWECK DER GESCHÄFTSBEZIEHUNG',
	'ZWECK DER GESCHÄFTSBEZIEHUNG2');

commit;
