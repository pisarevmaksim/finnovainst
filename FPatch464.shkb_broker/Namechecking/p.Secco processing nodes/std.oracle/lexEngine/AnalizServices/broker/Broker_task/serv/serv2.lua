local ss = analiz.libs.serv
local etlua = require "etlua"
local fileserv = CreateFileServ()
local finn_serv =analiz.mix.finnova.serv


function execute_report(pwd,sql,field,timeout,def_value)
 ss.s("tmp_s",sql)
 ss.s("tmp_pwd",pwd)
 ss.s("tmp_f",field)
 ss.s("tmp_t",timeout)
 ss.s("tmp_d",def_value)
 ss.e("echo ANALIZ_SAFE_EXECUTE rcode emessage_tmp SET_PARAM_FROM_SQL_DEFAULT_TIMEOUT tmp $(tmp_pwd) $(tmp_f) $(tmp_d) $(tmp_t) $(tmp_s)")
 ss.e("ANALIZ_SAFE_EXECUTE rcode emessage_tmp SET_PARAM_FROM_SQL_DEFAULT_TIMEOUT tmp $(tmp_pwd) $(tmp_f) $(tmp_d) $(tmp_t) $(tmp_s)")
 local v = ss.g("tmp")
 local emessage = ss.g("emessage_tmp")
 if emessage=="" then emessage=nil end
 return v,emessage
end


function execute_sql_oe(sql)
ss.l("execute_sql(oe)")
 ss.s("tmp_s",sql)
 ss.e("echo ANALIZ_SAFE_EXECUTE rcode emessage_tmp EXEC_SQL AmlWeb  $(tmp_t) $(tmp_s)")
 ss.e("ANALIZ_SAFE_EXECUTE rcode emessage_tmp EXEC_SQL AmlWeb  $(tmp_s)")

 local emessage = ss.g("emessage_tmp")
 if emessage=="" then emessage=nil end
 return emessage
end

function execute_sql(sql,timeout)
ss.l("execute_sql")
 ss.s("tmp_s",sql)
 ss.s("tmp_t",timeout)
 ss.e("echo ANALIZ_SAFE_EXECUTE rcode emessage_tmp EXEC_SQL_TIMEOUT AmlWeb  $(tmp_t) $(tmp_s)")
 ss.e("ANALIZ_SAFE_EXECUTE rcode emessage_tmp EXEC_SQL_TIMEOUT AmlWeb  $(tmp_t) $(tmp_s)")

 local emessage = ss.g("emessage_tmp")
 if emessage=="" then emessage=nil end
 return emessage
end

function execute_sql_ret(sql,timeout,fieldname,def_value)
ss.l("execute_sql")
 ss.s("tmp_s",sql)
 ss.s("tmp_t",timeout)
 ss.s("tmp_field",fieldname)
 ss.s("tmp_def",def_value)

 ss.e("echo ANALIZ_SAFE_EXECUTE rcode emessage_tmp SET_PARAM_FROM_SQL_DEFAULT_TIMEOUT tmp AmlWeb $(tmp_field)  $(tmp_def) $(tmp_t) $(tmp_s)")
 ss.e("ANALIZ_SAFE_EXECUTE rcode emessage_tmp SET_PARAM_FROM_SQL_DEFAULT_TIMEOUT tmp AmlWeb $(tmp_field)  $(tmp_def) $(tmp_t) $(tmp_s)")

 local emessage = ss.g("emessage_tmp")
 local val = ss.g("tmp")
 if emessage=="" then emessage=nil end
 return val,emessage
end



function comma_add(i)
 if i==1 then return "" end
 return ","
end
function fix_sql(str)
  if type(str)~="string" then return str end
  if str:find("'")==nil then return str end
  return ss.ReplaceValue(str,"'","''")
end

function fix_sql_ora(str)
  return fix_sql(str)
end


local function to_summary(str)
 ss.s("tmp",str)
 ss.l("to_summary:"..str)
 ss.e("echo ADD_TO_FLIE $(summary_log) [$(Now)]\t$(tmp)$(new_line)")
 ss.e("ADD_TO_FILE $(summary_log) [$(Now)]\t$(tmp)$(new_line)")
end

local function load_etlua(fn)
  if not fileserv:FileExists(fn) then
    ss.l("failed not found:"..fn)
    return nil
  end
  ss.e("load_param tmp "..fn)
  local t,err = etlua.compile(analiz:get("tmp"))
  if t==nil then ss.l("failed parse template:"..fn) end
  if err~=nil then ss.tprint(err) end
  return t
end

local function addStatus_(ctx,taskid,status,comment,option)
 local render_status = ctx.common.render.status
 if option==nil then option={} end
 if option.render~=nil then render_status=option.render end

 if comment==nil then comment="NULL" else comment="'"..fix_sql(comment).."'" end


 if taskid==nil then
   ss.l("failed.taskid is NULL")
   ss.l("status:"..status)
   ss.l("comment:"..comment)
 end

ss.l("addStatus\t:"..ss.def(taskid,"task null").."\t"..ss.def(status,"status_null").."\t"..ss.def(comment,"comment:nil"))

local sql =render_status({taskid=taskid,status=status,comment=comment,option=option})
--[=[
local sql = [[
INSERT INTO [dbo].[BROKER_data_extractionStatus]
           ([TaskID]
           ,[Status]
           ,[dt]
           ,[Comment]
           ,[Valid])
     VALUES
           (']]..fix_sql(taskid)..[['
           ,']]..fix_sql(status)..[['
           ,GetDate()
           ,]]..comment..[[
           ,1)
]]
--]=]
ss.s("sql",sql)
ss.e("EXEC_SQL AmlWeb $(sql)")
end

--
local function RegisterAll(ctx)
 ss.l("RegisterAll.start")
 local dir = ss.b("$(bin)\\AnalizServices\\broker\\Broker_task")
ss.l("dir:"..dir)

 local files = fileserv:GetFiles(dir,"*.lua",true)
ss.l("dir:"..dir)
ss.tprint(files)

ss.l("____________________________")
 local use_dir_cache={}
 for i=1,#files do
   local fn = dir.."\\"..files[i]
   local ldir = fileserv:ExtractPath(fn)
   local mode = use_dir_cache[ldir]
   if mode==nil then 
     if fileserv:FileExists(ldir.."\\notask.dir") then mode="notask" else mode="task_dir" end
     use_dir_cache[ldir]=mode
   end
   if mode=="task_dir" then 
     ss.l("reg1:"..fn)
     local t = dofile(fn)
     t:Register(ctx)
   end
 end
 ss.l("RegisterAll.end")
end
--
function LoadRenders(dir)
 ss.l("LoadRenders:"..dir)
 local files = fileserv:GetFiles(dir,"*.etlua",true)
ss.l("dir:"..dir)
ss.tprint(files)
local res = {}
ss.l("____________________________")
 for i=1,#files do
   local fn = dir.."\\"..files[i]
   local name = fileserv:ExtractName(fn)
   name = fileserv:ChangeFileExt(name,"")
   ss.l("reg1:"..name..":"..fn)
   local t = load_etlua(fn)
   res[name]=t
 end
 ss.l("LoadRenders:"..dir..".end")

 return res
end

function CreateBrokerCtx(op)
 local ctx={}
 ctx.option_=op
 local broker_option = op.broker_op
 ss.tprint(broker_option)

 local reciver = ss.factory.msbroker.CreateReciver(broker_option)

 ctx.reciver_= reciver
 ctx.workers_={}
 ctx.render={}


 function ctx:addStatus(taskid,status,comment,options)
   ctx:summary("add_status.taskid:"..ss.def(taskid,"<nil>").." status:"..ss.def(status,"nil").." comment:"..ss.def(comment,""))
   return addStatus_(self,taskid,status,comment,options)
 end

 function ctx:load_template(fn)
  return load_etlua(fn)
 end
 function ctx:Register(worker,name)
  self.workers_[name]=worker
 end

 local function fixStructTask_(task)
-- эта логика нужна для задач типа mros
-- когда клиент не хочет заморачиватся генерацией task_id
-- чтобы переложить генерацию на mssql процедуру там генерируется следующая затычка:
--set @task_out = '{task_id="'+cast(@task_id as varchar(255))+'",real_user_task_='+@task+'}'
-- а этот код возвращает все на свои места
  local rTask = task.real_user_task_
  if rTask==nil then return task end
  rTask.task_id = task.task_id
  return rTask
 end

 function ctx:DoTask(task)
  task = fixStructTask_(task)
  if task.task_id   ==nil then return self:failedTask(task,"task id is null") end
  if task.task_name ==nil then return self:failedTask(task,"task_name is null") end

  local name = task.task_name
  local worker=  self.workers_[name]
  if worker~=nil then 
     ctx:summary("start task:"..task.task_id..":"..task.task_name)
       return worker:DoTask(ctx,task) 
  end
  return self:failedTask(task,"not found worker for:"..name ) 
 end
 function ctx:failedTask(task,msg)
  local status_option={}

--  status_option.render = ctx.mros.render.status
  ctx:summary("failed task:"..msg)
  ctx:summary(ss.toJson(task))
  self:addStatus(ss.def(task.task_id,"<task_id_not_found>"),"ERROR",msg,status_option)
  return nil,msg
 end

 function ctx:summary(str)
   to_summary(str)
 end


  ctx:summary("start")
  ctx:summary(ss.b("pid=$(txm_pid)"))
  ctx:summary(ss.b("host:$(COMPUTERNAME)"))
  ctx:summary(ss.b("_ANALIZ_TYPE:$(_ANALIZ_TYPE)"))
  ctx:summary(ss.b("_ANALIZ_VER:$(_ANALIZ_VER)"))
-- add all tasks 
 RegisterAll(ctx)


 return ctx
end
