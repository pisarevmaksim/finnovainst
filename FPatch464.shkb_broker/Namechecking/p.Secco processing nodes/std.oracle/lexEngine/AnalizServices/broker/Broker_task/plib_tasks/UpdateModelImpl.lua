local ss = analiz.libs.serv

local IS_ENABLED=true

local function CreateWorker()
  local worker = {}
  worker.name="UPDATE_MODEL_IMPL"

  function  worker:Register(ctx)
    if IS_ENABLED==true then 
      ctx:Register(self,self.name)
    end
  end

  function  worker:DoTask(ctx,task)
     ss.l("Update model impl")
      ctx:addStatus(task.task_id,"start")
      local verid = ss.def(task.verid,"")
      local r =  update_verid(verid)
      if r.emessage=="" then 
        ctx:addStatus(task.task_id,"COMPLETE")
      else
        ctx:addStatus(task.task_id,"ERROR",r.emessage)
      end

     ss.l("ping end")
  end
 return worker
end



return CreateWorker()
