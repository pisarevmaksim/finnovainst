--all scripts should be run under PROSPERO-scheme

--alter session set db_securefile=ignore;

--drop tables if exist
declare 
	tst number;
begin	
	--technical tables
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_LAENDERKAT';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_LAENDERKAT';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_INT_PERS_CHARGE';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_INT_PERS_CHARGE';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_REFERENCE_TABLE';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_REFERENCE_TABLE';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_INT_WRG';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_INT_WRG';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_INT_LAND';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_INT_LAND';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_INT_INDIZ';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_INT_INDIZ';   
	end if;

	--data tables
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KT_STAMM';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KT_STAMM';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_ADR';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_ADR';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_ADR_XML';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_ADR_XML';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_STAMM';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_STAMM';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_OM_TEXT';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_OM_TEXT';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_KD';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_KD';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_ZV_ADR_EFF';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_ZV_ADR_EFF';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_ZV_ZLG_T';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_ZV_ZLG_T';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_VSB';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_VSB';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_VSB_XML';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_VSB_XML';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_EMAIL';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_EMAIL';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KD_EMAIL_XML';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KD_EMAIL_XML';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KT_BUCH';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KT_BUCH';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KT_BUCH_EA';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KT_BUCH_EA';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_VT_STAMM';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_VT_STAMM';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_VT_ZUORD';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_VT_ZUORD';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_ZV_AUF';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_ZV_AUF';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_KT_SALDO';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_KT_SALDO';   
	end if;
	SELECT count(*) into tst FROM USER_TABLES where TabLE_NAME = 'FA_MROS_SR_FELD_WERT';
	if tst>0 then
		EXECUTE IMMEDIATE 'drop table FA_MROS_SR_FELD_WERT';   
	end if;
end;



--create technical tables
create table FA_MROS_LAENDERKAT (
	USERBK_NR number(4, 0) not null,
	LAND_LNR number(4, 0) not null,
	ISO_LAND_CD varchar2(2 byte) null,
	COUNTRY_RISK number null
);

create table FA_MROS_INT_PERS_CHARGE 
	(userbk_nr_home number(4,0) NOT NULL,
	institution_name varchar2(255) NOT NULL,
	swift varchar2(11) NOT NULL,
	rentity_id number(8,0) NOT NULL,
	first_name varchar2(100) NOT NULL,
	last_name varchar2(100) NOT NULL,
	tph_number varchar2(50) NOT NULL,
	tph_communication_type number(2,0) NULL,
	address varchar2(100) NOT NULL,
	city varchar2(255) NOT NULL);

create table FA_MROS_REFERENCE_TABLE
	(userbk_nr number(4,0) NOT NULL,
	ref_type number(3,0) NOT NULL,
	Finnova_name varchar2(100) NULL,
	MROS_name varchar2(100) NULL,
	MROS_lookup_table varchar2(100) NULL,
	Finnova_value varchar2(5) NOT NULL,
	MROS_value varchar2(5) NULL,
	activ_MROS number(1,0) NOT NULL,
	activ_data_load number(1,0) NOT NULL,
	special_mark varchar2(50) NULL,
	comments varchar2(4000) NULL);

create table FA_MROS_INT_WRG
	(iso_wrg_cd varchar2(4) NOT NULL);

create table FA_MROS_INT_LAND
	(iso_land_cd varchar2(3) NOT NULL);

create table FA_MROS_INT_INDIZ
	(indicator varchar2(25) NOT NULL);



--create data tables
CREATE TABLE FA_MROS_KD_ADR(
	USERBK_NR number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	STRASSE varchar2(35) NULL,
	ADR_ORT_BEZ varchar2(35) NULL,
	REGION_CD number(4, 0) NULL,
	ADR_PLZ varchar2(20) NULL,
	ORT_ORT_BEZ varchar2(35) NULL,
	ORT_PLZ varchar2(20) NULL,
	ISO_LAND_CD varchar2(3) NULL,
	LAND_BEZ varchar2(80) NULL
);

CREATE TABLE FA_MROS_KD_VSB(
	USERBK_NR number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	IDDOC_CD number(4, 0) NULL,
	KD_AUSW_NR varchar2(24) NULL,
	AUSSTELL_DAT date NULL,
	ABL_DAT date NULL,
	KD_VSB_BEGLAUB varchar2(50) NULL,
	ISO_LAND_CD varchar2(3) NULL,
	LAND_BEZ varchar2(80) NULL
);

CREATE TABLE FA_MROS_KD_EMAIL(
	USERBK_NR number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	MAIL_ART_CD varchar2(4) NULL,
	EMAIL_ADR varchar2(50) NOT NULL
);
/*
CREATE TABLE FA_MROS_KD_ADR_XML(
	userbk_nr number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	KD_ADR XMLType NULL
);

CREATE TABLE FA_MROS_KD_VSB_XML(
	userbk_nr number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	KD_VSB XMLType NULL
);

CREATE TABLE FA_MROS_KD_EMAIL_XML(
	userbk_nr number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	KD_EMAIL XMLType NULL
);
*/
CREATE TABLE FA_MROS_KD_STAMM(
	USERBK_NR number(4, 0) NULL,
	KD_LNR number(22, 0) NULL,
	EDIT_KD_NR varchar2(50) NULL,
	KD_HTYP_CD number(1, 0) NULL,
	PARTNER_FLAG number(1,0) NULL,
	BANK_FLAG number(1,0) NULL,
	GESCHLECHT_CD number(2, 0) NULL,
	VORNAME varchar2(15) NULL,
	NAME_1 varchar2(35) NULL,
	KD_BEZ varchar2(35) NULL,
	KD_RSTAT_CD number(38, 0) NULL,
	KD_RSTAT_BEZ varchar2(80) NULL,
	KD_RSTAT_AUSP_CD number(4, 0) NULL,
	KD_RSTAT_AUSP_BEZ varchar2(80) NULL,
	KDKAT_CD number(22, 0) NULL,
	KDKAT_BEZ varchar2(80) NULL,
	KD_SEGM_CD number(2, 0) NULL,
	KD_SEGM_BEZ varchar2(80) NULL,
	RFORM_CD number(4, 0) NULL,
	RFORM_BEZ varchar2(80) NULL,
	UID_NR varchar2(30) NULL,
	GEBURT_DAT date NULL,
	GEBURT_JAHR number(4, 0) NULL,
	ALTER_JAHR number(38, 0) NULL,
	TOD_DAT date NULL,
	TOD_JAHR number(4, 0) NULL,
	EROEFF_DAT number(5, 2) NULL,
	KD_SITZ_GSCH_CD varchar2(1) NULL,
	TITEL varchar2(80) NULL,
	GEBURT_ORT varchar2(35) NULL,
	NOGA_CD varchar2(6) NULL,
	FIN_VOLLM_CD varchar2(1) NULL,
	NOGA_BEZ varchar2(80) NULL,
	BERUF_CD varchar2(10) NULL,
	BERUF_BEZ varchar2(80) NULL,
	KAP number(38, 0) NULL,
	EMAIL_ADR varchar2(4000) NULL,
	KD_VIP_CD varchar2(1) NULL,
	KD_PEP_CD varchar2(1) NULL,
	KD_PEP_CD_AUTO varchar2(1) NULL,
	KD_PEP_DT_AUTO date NULL,
	KD_GRP_ART_CD number(3, 0) NULL,
	LAND_LNR_NAT number(4, 0) NULL,
	LAND_LNR_NAT_BEZ varchar2(80) NULL,
	ISO_LAND_CODE_NAT varchar2(3) NULL,
	LAND_LNR_NAT_2 number(4, 0) NULL,
	LAND_LNR_NAT_BEZ_2 varchar2(80) NULL,
	ISO_LAND_CODE_NAT_2 varchar2(3) NULL,
	LAND_LNR_AUSL number(4, 0) NULL,
	LAND_LNR_AUSL_BEZ varchar2(80) NULL,
	ISO_CODE_AUSL varchar2(3) NULL,
	EROEFF_DAT_REL number(8, 0) NULL,
	KD_STAT_CD number(4,0) NULL,
	KD_GWV_CD number(4,0) NULL,
	KD_GW_SYS_DT date NULL,
	KD_GWV_CODE_BEZ varchar2(300) NULL,
	KD_VSB_STAT_CD varchar2(50) NULL,
	KD_VSB_STAT_BEZ varchar2(300) NULL,
	VSB_STAT_DAT date NULL
);

CREATE TABLE FA_MROS_KT_STAMM(
	USERBK_NR number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	KT_LNR number(22, 0) NOT NULL,
	EDIT_KT_NR varchar2(50) NOT NULL,
	SITZ_NR number(4, 0) NOT NULL,
	SITZ_NR_BEZ varchar2(300) NULL,
	IBAN_ID varchar2(34) NULL,
	EROEFF_DAT date NULL,
	AUFHEB_DAT date NULL,
	SPERR_CD number(2, 0) NULL,
	AUFHEB_CD varchar2(2) NULL,
	RUBRIK varchar2(80) NULL,
	WRG_LNR number(4, 0) NOT NULL,
	WRG_BEZ varchar2(80) NULL,
	ISO_WRG_CD varchar2(4) NULL,
	KT_ART_NR number(4, 0) NOT NULL,
	KT_ART_NR_BEZ varchar2(80) NULL,
	KT_ART_BER_CD number(2, 0) NULL,
	KT_ART_BER_BEZ varchar2(80) NULL,
	ARBGEB_NR number(4, 0) NULL,
	ARBGEB_NR_BEZ varchar2(80) NULL,
	KT_LFZ_JAHR number(5, 2) NULL
);

CREATE TABLE FA_MROS_ZV_ZLG_T(
	USERBK_NR number(4, 0) NOT NULL,
	ZV_ZLG_SYS_LNR number(22, 0) NULL,
	AUF_LNR number(22, 0) NOT NULL,
	AUF_ID varchar2(50) NULL,
	TRAK_REF_TYP_BEZ varchar2(1000) NULL,
	TRAK_REF_TYP number(4, 0) NULL,
	TRAK_REF_LNR number(22, 0) NULL,
	KD_LNR number(22, 0) NULL,
	KT_LNR number(22, 0) NULL,
	AUF_OM_TEXT_LNR number(22, 0) NULL,
	AUF_ZV_ADR_EFF_LNR number(22, 0) NULL,
	ZV_EING_AUSG_CD varchar2(1) NULL,
	AUF_LAND_LNR_DOM number(4, 0) NULL,
	AUF_TRAK_REF_TYP number(4, 0) NULL,
	AUF_TRAK_REF_LNR number(22, 0) NULL,
	KT_LNR_B number(22, 0) NULL,
	OM_TEXT_LNR number(22, 0) NULL,
	ZV_ADR_EFF_LNR number(22, 0) NULL,
	LAND_LNR_DOM number(4, 0) NULL,
	SITZ_NR_AUSF number(4, 0) NULL,
	SITZ_NR_BEZ varchar2(300) NULL,
	AUSF_DT_IST date NULL,
	AUSF_DT_SOLL date NULL,
	AUF_DT_ERT date NULL,
	VALUTA date NULL,
	BUCHTYP_CD number(38, 0) NULL,
	BUCHTYP_BEZ varchar2(80) NULL,
	BETRAG_BWRG number(20, 6) NOT NULL,
	BETRAG_ZWRG number(20, 6) NULL,
	SH_CD varchar2(1) NOT NULL,
	ISO_WRG_CD varchar2(4) NULL,
	WRG_BEZ varchar2(80) NULL,
	GF_ART varchar2(4) NULL,
	GF_ART_BEZ varchar2(80) NULL,
	GF_NR varchar2(4) NULL,
	GF_NR_BEZ varchar2(80) NULL
);

CREATE TABLE FA_MROS_OM_TEXT(
	USERBK_NR number(4, 0) NOT NULL,
	OM_TEXT_LNR number(22, 0) NOT NULL,
	TEXT_REC varchar2(2000) NULL
);

CREATE TABLE FA_MROS_ZV_ADR_EFF(
	USERBK_NR number(4, 0) NOT NULL,
	ZV_ADR_EFF_LNR number(22, 0) NOT NULL,
	KT_ID_CD number(6, 0) NULL,
	KT_ID_TEXT varchar2(50) NULL,
	KT_ID varchar2(35) NULL,
	KT_EXT_ART_CD varchar2(10) NULL,
	EXT_KT_NR varchar2(35) NULL,
	BIC varchar2(35) NULL,
	ADR_ZEILEN varchar2(500) NULL,
	EMPF_ART_CD number(2,0) NULL,
	ZV_ADR_EFF_LNR_PRI number(22, 0) NULL,
	AUF_KT_ID_CD number(6, 0) NULL,
	AUF_KT_ID varchar2(35) NULL,
	AUF_KT_EXT_ART_CD varchar2(10) NULL,
	AUF_EXT_KT_NR varchar2(35) NULL,
	AUF_BIC varchar2(35) NULL,
	AUF_ADR_ZEILEN varchar2(500) NULL
);

CREATE TABLE FA_MROS_ZV_AUF(
	USERBK_NR number(4, 0) NOT NULL,
	AUF_LNR number(22, 0) NOT NULL,
	AUF_ID varchar2(50) NULL,
	TRAK_REF_TYP number(4, 0) NULL,
	TRAK_REF_LNR number(22, 0) NULL,
	ORDER_CD varchar2(1) NOT NULL,
	ZV_ZLG_SYS_LNR number(22, 0) NULL,
	KT_LNR number(22, 0) NULL,
	KD_LNR number(22, 0) NULL,
	SH_CD varchar2(1) NOT NULL,
	BETRAG_BWRG number(20, 6) NOT NULL,
	BETRAG_KWRG number(20, 6) NULL,
	BETRAG_ZWRG number(20, 6) NULL,
	ZWRG_LNR number(4,0) NULL,
	ZV_ADR_EFF_LNR number(22, 0) NULL,
	OM_TEXT_LNR number(22, 0) NULL,
	GF_NR varchar2(4) NULL,
	GF_ART varchar2(4) NULL,
	LAND_LNR_DOM number(4, 0) NULL,
	LAND_LNR_DOM1 number(4, 0) NULL,
	BUCHTYP_CD number(38, 0) NULL,
	VALUTA date NULL,
	AUSF_DT_IST date NULL,
	AUSF_DT_SOLL date NULL,
	AUF_DT_ERT date NULL,
	SITZ_NR_AUSF number(4, 0) NULL,
	TRAK_REF_TYP_BEZ varchar2(1000) NULL,
	BUCHTYP_BEZ varchar2(80) NULL,
	ISO_WRG_CD varchar2(4) NULL,
	WRG_BEZ varchar2(80) NULL,
	GF_ART_BEZ varchar2(80) NULL,
	GF_NR_BEZ varchar2(80) NULL,
	SITZ_NR_BEZ varchar2(300) NULL
);

CREATE TABLE FA_MROS_KT_BUCH(
	USERBK_NR number(4, 0) NOT NULL,
	CREATE_ID number(22, 0) NOT NULL,
	AUF_LNR number(22, 0) NOT NULL,
	AUF_ID varchar2(50) NOT NULL,
	KT_BUCH_LNR number(22, 0) NOT NULL,
	KT_BUCH_DET_LNR number(22, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	EDIT_KD_NR varchar2(50) NOT NULL,
	KT_GNR number(22, 0) NULL,
	KT_LNR number(22, 0) NOT NULL,
	EDIT_KT_NR varchar2(50) NOT NULL,
	KD_HTYP_CD number(1,0) NOT NULL,
	SITZ_NR number(4,0) NULL,
	SITZ_NR_BEZ varchar2(300) NULL,
	BUCH_DAT_A date NULL,
	BUCH_DAT_B date NULL,
	BUCH_DAT_C date NULL,
	BUCH_DAT_A_1 number(10, 0) NOT NULL,
	VALUTA date NULL,
	SH_CD varchar2(1) NULL,
	BUCHTYP_CD number(4, 0) NULL,
	BUCHTYP_BEZ varchar2(1000) NULL,
	BETRAG_BWRG number(24, 4) NOT NULL,
	BETRAG_KWRG number(24, 4) NOT NULL,
	PERF_CD number(1,0) NULL,
	TEXT_EXT varchar2(1000) NULL,
	TEXT_ZUS varchar2(1000) NULL,
	TRAK_REF_LNR number(22, 0) NOT NULL,
	TRAK_REF_TYP number(4, 0) NOT NULL,
	TRAK_REF_TYP_BEZ varchar2(1000) NULL,
	WRG_LNR number(4,0) NULL,
	WRG_BEZ varchar2(80) NULL,
	ISO_WRG_CD varchar2(4) NULL,
	LAND_LNR_DOM_RISK number(2,0) NULL,
	LAND_LNR_DOM_INTERNAT number(1,0) NULL,
	LAND_LNR_DOM_MAX_RISK varchar2(3) NULL
);

CREATE TABLE FA_MROS_KT_BUCH_EA(
	USERBK_NR number(4, 0) NOT NULL,
	CREATE_ID number(22, 0) NOT NULL,
	KT_BUCH_LNR number(22, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	EDIT_KD_NR varchar2(50) NOT NULL,
	KT_GNR number(22, 0) NULL,
	KT_LNR number(22, 0) NOT NULL,
	EDIT_KT_NR varchar2(50) NOT NULL,
	KD_HTYP_CD number(1,0) NOT NULL,
	SITZ_NR number(4,0) NULL,
	SITZ_NR_BEZ varchar2(300) NULL,
	BUCH_DAT_A date NULL,
	BUCH_DAT_B date NULL,
	BUCH_DAT_C date NULL,
	BUCH_DAT_A_1 number(10,0) NOT NULL,
	VALUTA date NULL,
	SH_CD varchar2(1) NULL,
	BUCHTYP_CD number(4, 0) NULL,
	BUCHTYP_BEZ varchar2(1000) NULL,
	GF_NR number(4, 0) NULL,
	GF_NR_BEZ varchar2(1000) NULL,
	BETRAG_BWRG number(24, 4) NULL,
	BETRAG_KWRG number(24, 4) NULL,
	PERF_CD number(1,0) NULL,
	TEXT_EXT varchar2(1000) NULL,
	TEXT_ZUS varchar2(1000) NULL,
	TRAK_REF_LNR number(22, 0) NULL,
	TRAK_REF_TYP number(4, 0) NULL,
	TRAK_REF_TYP_BEZ varchar2(1000) NULL,
	WRG_LNR number(4,0) NULL,
	WRG_BEZ varchar2(80) NULL,
	ISO_WRG_CD varchar2(4) NULL,
	ADR_ZEILE_1_EZ varchar2(100) NULL,
	ADR_ZEILE_2_EZ varchar2(100) NULL,
	ADR_ZEILE_3_EZ varchar2(100) NULL,
	ADR_ZEILE_4_EZ varchar2(100) NULL,
	ADR_ZEILE_5_EZ varchar2(100) NULL,
	WITHDRAWAL_FLAG number(1,0) NULL
);

CREATE TABLE FA_MROS_KT_SALDO(
	USERBK_NR number(4, 0) NOT NULL,
	KT_LNR number(22, 0) NOT NULL,
	SALDO_BWRG number(20, 6) NULL,
	SALDO_KWRG number(20, 6) NULL
);

CREATE TABLE FA_MROS_KD_KD(
	USERBK_NR number(4, 0) NOT NULL,
	KD_KD_LNR number(22, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	KD_LNR_ZUW number(22, 0) NULL,
	VALID_BIS_DAT date NULL,
	KD_KD_VERB_CD number(4, 0) NULL,
	BTLG_SATZ number(6, 2) NULL
);

CREATE TABLE FA_MROS_VT_STAMM(
	USERBK_NR number(4, 0) NOT NULL,
	VT_LNR number(22, 0) NOT NULL,
	VT_TYP_CD number(4, 0) NOT NULL,
	VT_TYP_BEZ varchar2(80) NULL,
	VT_STAT_CD number(2, 0) NOT NULL,
	VT_STAT_BEZ varchar2(80) NULL,
	VT_STAT_DT date NULL,
	KD_LNR number(22, 0) NOT NULL,
	KT_LNR number(22, 0) NULL,
	KT_GNR number(22, 0) NULL,
	BEGINN_DAT date NULL,
	ENDE_DAT date NULL
);

CREATE TABLE FA_MROS_VT_ZUORD(
	USERBK_NR number(4, 0) NOT NULL,
	VT_LNR number(22, 0) NOT NULL,
	KD_LNR number(22, 0) NULL,
	KT_LNR number(22, 0) NULL,
	KT_GNR number(22, 0) NULL,
	SB_SIGN_RECHT_CD number(2, 0) NULL,
	SB_SIGN_RECHT_BEZ varchar2(80) NULL
);

CREATE TABLE FA_MROS_SR_FELD_WERT(
	USERBK_NR number(4, 0) NOT NULL,
	KD_LNR number(22, 0) NOT NULL,
	SR_FELD_ZUS_LNR number(22, 0) NULL,
	SR_FELD_BEZ varchar2(40) NULL,
	FELD_WERT varchar2(2000) NULL
);
