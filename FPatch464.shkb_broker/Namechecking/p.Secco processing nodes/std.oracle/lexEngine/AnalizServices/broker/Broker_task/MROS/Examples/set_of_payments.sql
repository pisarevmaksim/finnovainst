use staging_namecheck

DECLARE @task varchar(max)
DECLARE @status varchar(max)
DECLARE @task_id bigint 



set @task = '{task_name = "mros.set_of_payments", 
             f_bank_id = "942",
			 rp_name="John",
			 rp_surname="Smith",
			 rp_phone="+123",
			 rp_email="ya@gmail.com",
			 rp_occupation="the important person",
			 rp_address="Common street, b.1",
			 rp_city="Z�rich",
			 fiu_ref_number="STR 1/01.01.2020",
             entity_reference = "ent.ref", 
			 report_reason = "rep.reas.",
             r_action = "rep.act.", 
			 
			 report_indicators = {"0001M", "1004V", "2012G"}, 
             
             set_of_payments = {{id = "59585716405127"},{id="59331206957840"},{id="58126436855493"}}, 
             set_of_orders = {{chid = "ZV20050429/001207"}}

	,df_payment_id = 111
	,df_order_nr = "ZV1111/00000"
	,df_description = "swift transaction"
	,df_date ="20200331 15:43:24"
	,df_currency = "USD"
	,df_amount = -100.56
	,df_debit_credit = "D"
	,df_account_id = 1001143
	,df_customer_id = 10191
	,df_customer_nr = "555"
	,df_bank_name = "super Bank"
	,df_swift = "SW1278G8678"
	,df_account = "CH676897897888"	
	,df_comments = "John Smith, Z�rich"

			 }'



exec  MROS_data_request_v2 @task,@task_id OUTPUT
select @task_id as TASK_id

select @status=status from MROS_data_status  where TaskID=@task_id
  print  'task.id:'+cast(@task_id as varchar(255))
  print  'wait result'
  set @status='start'
while not (@status='COMPLETE' or  @status ='ERROR') begin
 select @status=status from MROS_data_status  where TaskID=@task_id
  print  @status
  WAITFOR DELAY '00:00:01'
end
print  'finish:'+@status

select * from MROS_data_status  where TaskID=@task_id

