local ss = analiz.libs.serv
local finn_serv =analiz.mix.finnova.serv






local function fix_q(str)
  if str:find("'")==nil then return str end
  return ss.ReplaceValue(str,"'","''")
end


-- --------------------------------------------------
function    sleep(sec)
  ss.e("SLEEP sec "..sec)
end



-- --------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
local function failed_getX_(ctx,taskstr,template_arg)
local t1=template_arg
local f1=taskstr:find(t1,1)
if f1==nil then 
   ss.l("Not found template:"..t1)
   ss.l("task skip:"..taskstr)
   return nil
end
 f1= f1+t1:len()
 local f2=taskstr:find('"',f1)
 local val=taskstr:sub(f1,f2-1)
 return val
end

local  function failed_getTaskID(ctx,taskstr)
 return failed_getX_(ctx,taskstr,'task_id="')
end


local function failed_getTaskName(ctx,taskstr)
 return failed_getX_(ctx,taskstr,'task_name="')
end

local function failed_getTaskType(ctx,taskstr)
 return failed_getX_(ctx,taskstr,'task_type="')
end



function failedTask(ctx,taskstr,emessage)
 ss.l("failedTask.start")
 --find TaskID
 local taskid=failed_getTaskID(ctx,taskstr)
 if taskid==nil then
   ss.l("failedTask.not found taskid")
  return
 end
 ctx:addStatus(taskid,"ERROR","failed parse task:"..emessage)

 local task_name = failed_getTaskName(ctx,taskstr)
 if task_name==nil then  task_name="unknown" end
 ss.l("task_name:"..task_name)
 local worker=  ctx.workers_[task_name]
 if worker~=nil and worker.failedTask~=nil then 
     worker:failedTask(ctx,{task_id=taskid,task_name=task_name,taskstr=taskstr,emessage=emessage})
    return
 end

 local tasktype = failed_getTaskType(ctx,taskstr)
 local send_err=false

  local status_option={}
  if tasktype=="mros" then 
    ss.l("send error to mros.system")
    status_option.render = ctx.mros.render.status
    send_err=true
  end

  if send_err then 
    ctx:addStatus(taskid,"ERROR","failed parse task:"..emessage,status_option)
    return 
  end
 ss.l("failedTask.not found worker or failedTask")

end
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------


-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
local function transform_(body)
  local r = ss.ReplaceValue(body,"'","''")
  return r
end
local function fix_(b)
 b = ss.ReplaceValue(b,"'","_")
 b = ss.ReplaceValue(b," ","_")
 b = ss.ReplaceValue(b,".","_")
 b = ss.ReplaceValue(b,":","_")
 b = ss.ReplaceValue(b,"*","_")
 b = ss.ReplaceValue(b,"?","_")
 return b
end



function PublicReport(task,result_)
 ss.l("public report for:"..task.process_id)
 local emessage =nil

 local fn=fix_(task.taskid)..".xml"
 local report_sql = transform_(result_)
--<?xml version="1.0" encoding="iso-8859-1"?>
 local nikita_sql = [[
set ansi_nulls, ansi_padding, ansi_warnings, concat_null_yields_null, quoted_identifier, arithabort on;
set numeric_roundabort off;

DECLARE @process_id varchar(50) = ']]..task.process_id..[[' --processid
DECLARE @username varchar(50) --= 'sup' --имя пользователя, оставившего коммент
DECLARE @date datetime = GetDate(); --дата
DECLARE @text varchar(max) = 'Comment text' --текст коммента
DECLARE @name varchar(max) = ']]..fn..[[' --имя файла в аттаче
declare @data varbinary(max) = CAST(']]..analiz_test_AnsiToUtf8(report_sql)..[[' as varbinary(max)) --содержимое файла в аттаче


select top 1 @username = username from avapi_processes where id=']]..task.process_id..[['	

EXECUTE [dbo].[InsertProcessCommentWithFile] 
   @process_id
  ,@username
  ,@date
  ,@text
  ,@name
  ,@data

]]

 ss.s("tmp",nikita_sql)
 ss.e("echo Exec_SQL AmlWeb $(tmp)")
 ss.e("Exec_SQL AmlWeb $(tmp)")
 return "stub_1",emessage
end
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
  local function get_sort_bids(infobids)
   local bids={}
   for k,v in pairs(infobids) do bids[#bids+1]=k end
   table.sort(bids)
   return bids
  end
-- -------
  local function extract_transactions(s,infobids)
   local t={}
   for i=1,#s do
     local b = s[i]     local vv = infobids[b] 
     t[#t+1]=vv.F_CUSTOMER_ID
   end
   return t
  end

  local function extract_transactions_str(s,infobids)
   local t={}
   for i=1,#s do
     local b = s[i]     local vv = infobids[b] 
     local r={}
     r.id =vv.F_CUSTOMER_ID
     local inout =finn_serv.GetDirectType(vv)
     if inout=="incoming" then    r.isIn=1 else r.isIn=0 end
     t[#t+1]=r
   end
   return t
  end
-- ----------------------------------
local function GetProcessComments(pwd_arg,process_id)
 local res =""
 local s = "select TEXT  from avapi_processes_comments where process_id='"..process_id.."' order by id"

    local e,emessage = createSqlEngine({pwd=pwd_arg,sql=s,isUpperHeader="1"})
    if emessage~="" then
       return nil,emessage
    end

    local res = ""
    local last=""
    local isNext,emessage = e:Next()
    while isNext do
      if emessage~="" then
       e:Destroy()
       return nil,emessage
      end
      local r = e:Record()
      if last~=r.TEXT then
        if res~="" then res=res..";|;" end
        last =r.TEXT
        res = res..last
      end
      isNext,emessage = e:Next()
    end

    e:Destroy()
    return res
end
-- ----------------------------------
local function GetAlertsMsg_(s,infobids,nameresults)
 local res = ""
 for i=1,#s do
  local bid = s[i]
  local n = infobids[bid] if n==nil then n={} end
  local r = nameresults[bid] if r==nil then r={} end
  local msg = ss.def(n.FIRSTNAME,"").." "..ss.def(n.LASTNAME,"")..";"..ss.def(n.COUNTRY,"")..";"..
              ss.def(n.DOB,"")..";"..ss.def(r.LIST,"")..";"..ss.def(r.LIST_ID,"")
   if res~="" then res = res..";|;" end
   res = res..msg
 end
 return res
end
local function addp(s)
  if s=="" then return s end
  return s.."."
end
-- ----------------------------------
function CreateReportArgs_sar(args)
  local process_id  = args.process_id
  local infobids    = args.bids
  local nameresults = args.nameresults

  local s=get_sort_bids(infobids)
-- -------
  local function find_first(name)
   for i=1,#s do
     local b = s[i]     local vv = infobids[b]     local val = vv[name]
     if val~=nil then return val end
   end
   return nil 
  end
-- -------

  local emessage=""
  local bank_id = find_first("F_BANK_ID")
  if bank_id=="" then emessage="BANKA_ID cannot be empty" end
  local r = {F_BANK_ID=bank_id,
                SOURCE_ID=1,
                schema=addp(global_oracle_setting.mros.schema),
                ENTITY_REFERENCE='ent.ref',
                REPORT_REASON=ss.ReplaceValue(GetAlertsMsg_(s,infobids,nameresults),"&","_"),
                R_ACTION = GetProcessComments(args.pwd,process_id),
                indicators={'2001G','1036V','3006B'},
                transactions=extract_transactions(s,infobids)}

  return {args=r,rtype="sar"},emessage
end

-- ----------------------------------
function CreateReportArgs_str(args)
  local process_id  = args.process_id
  local infobids    = args.bids
  local nameresults = args.nameresults

  local s=get_sort_bids(infobids)
-- -------
  local function find_first(name)
   for i=1,#s do
     local b = s[i]     local vv = infobids[b]     local val = vv[name]
     if val~=nil then return val end
   end
   return nil 
  end
-- -------
  local emessage=""
  local bank_id = find_first("F_BANK_ID")
  if bank_id=="" then emessage="BANKA_ID cannot be empty" end

  local r = {F_BANK_ID=bank_id,
                SOURCE_ID=1,
                schema=addp(global_oracle_setting.mros.schema),
                ENTITY_REFERENCE='ent.ref',
                REPORT_REASON=ss.ReplaceValue(GetAlertsMsg_(s,infobids,nameresults),"&","_"),
                R_ACTION = GetProcessComments(args.pwd,process_id),
                indicators={'2001G','1036V','3006B'},
                transactions=extract_transactions_str(s,infobids)}

  return {args=r,rtype="str"},emessage
end

-- ----------------------------------
function CreateReportArgs(args)
  if args.rtype=="sar" then return CreateReportArgs_sar(args) end
  if args.rtype=="str" then return CreateReportArgs_str(args) end

  return nil,"CreateReportArgs.unknown rtype:"..ss.def(d.rtype,"<nil>")

end
-- ----------------------------------
function ora_quota_fix(val)
  if val:find("'")==nil then return val end
  return ss.ReplaceValue(val,"'","_")
end
-- ----------------------------------

function Separated_transaction(infobids)
  local str={}
  local sar={}
  local str_count=0
  local sar_count=0
  for k,v in pairs(infobids) do
    local cur=str
    if finn_serv.IsKundenstamm(v) then
        sar[k]=v sar_count=sar_count+1
    else
        str[k]=v str_count=str_count+1
    end
  end
  local res={}
  if str_count>0 then
     local r ={bids=str,rtype="str"}
     res[#res+1]=r
  end
  if sar_count>0 then
     local r ={bids=sar,rtype="sar"}
     res[#res+1]=r
  end
  return res
end
