local ss = analiz.libs.serv
local fileserv = CreateFileServ()

ss.e("LUA_EXECUTE_FILE lua $(dir)\\..\\broker\\Broker_task\\MROS\\serv\\mros_template_task.lua")


function mros_init(ctx)
 ctx.mros={}
 ctx.mros.render={}
 ctx.mros.render.status = ctx:load_template(ss.b("$(dir)\\..\\broker\\Broker_task\\MROS\\serv\\status.etlua"))
 ctx.mros.tmp={}
 ctx.mros.tmp.task_dir = ss.b("$(temp)\\lex\\broker\\history\\$(UNIQUE_IDENTIFIER)")
 fileserv:ForceDir(ctx.mros.tmp.task_dir)

 ctx.render.mros=LoadRenders(ss.b("$(bin)\\AnalizServices\\broker\\Broker_task\\MROS\\template\\common"))
 ctx.orgclient_serv = CreateOrgClientEngine()
end