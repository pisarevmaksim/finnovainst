use staging_namecheck

DECLARE @task varchar(max)
DECLARE @status varchar(max)
DECLARE @task_id bigint 



set @task = '{task_name = "mros.booking_report", 
f_bank_id = "942", 
 entity_reference = "ent.ref",
 report_reason = "rep.res", 
r_action = "r.act",
 rp_email = "support@prospero.ch",
 report_indicators = {"0001M", "1004V", "2012G"},
 fiu_ref_number = 12345, debit = 1, credit = 1, 
 set_of_transactions = {{id = "59464528223935"}},
 set_of_accounts={{chid="500.284.018"}} 

}'



exec  MROS_data_request_v2 @task,@task_id OUTPUT
select @task_id as TASK_ID

select @status=status from MROS_data_status  where TaskID=@task_id
  print  'task.id:'+cast(@task_id as varchar(255))
  print  'wait result'
  set @status='start'
while not (@status='COMPLETE' or  @status ='ERROR') begin
 select @status=status from MROS_data_status  where TaskID=@task_id
  print  @status
  WAITFOR DELAY '00:00:01'
end
print  'finish:'+@status

select * from MROS_data_status  where TaskID=@task_id

