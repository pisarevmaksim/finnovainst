﻿--truncate table FA_MROS_REFERENCE_TABLE;

declare
	F_BANK_ID number default 949;   --Bank's number
	sprache number default 5;       --sprache = 1 means German, sprache = 5 means English
	fill_flag number default 1;     --fill_flag = 1 means that the script puts certain values in MROS_value from pronoe for userbk_nr = 949. 
	--ATTENTION: In any case, if fill_flag = 1 or not, all values in the matching must be checked!
begin
	--region (branch)
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		1 as ref_type,
		'KT_STAMM.sitz_nr' as Finnova_name,
		't_account_my_client.branch' as MROS_name,
		'8.20 Allowed values for Type State_type' as MROS_lookup_table,
		sitz_nr as Finnova_value,
		NULL as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Branch is ' || bez_lang as comments
	from F2LDATP0.AL1SITZ
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by sitz_nr;

	--region (state)
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		2 as ref_type,
		'KD_ADR.region_cd' as Finnova_name,
		't_address.state' as MROS_name,
		'8.20 Allowed values for Type State_type' as MROS_lookup_table,
		region_cd as Finnova_value,
		NULL as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'State is ' || bez_lang as comments
	from 
		(select distinct userbk_nr,region_cd,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,region_cd,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.AL1REGI) s
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by region_cd;

	--domicile company flag
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		3 as ref_type,
		'KD_STAMM.kd_sitz_gsch_cd' as Finnova_name,
		't_entity_my_client.tax_reg_number' as MROS_name,
		'8.21 Allowed values for Type yes_no_type' as MROS_lookup_table,
		kd_sitz_gsch_cd as Finnova_value,
		NULL as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		NULL as comments
	from 
		(select distinct userbk_nr,kd_sitz_gsch_cd from F2LDATP0.KD_STAMM where kd_sitz_gsch_cd is not null)
	where userbk_nr=F_BANK_ID
	order by kd_sitz_gsch_cd;

	--gender
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		4 as ref_type,
		'KD_STAMM.geschlecht_cd' as Finnova_name,
		't_person_my_client.gender' as MROS_name,
		'8.19 Gender type' as MROS_lookup_table,
		geschlecht_cd as Finnova_value,
		NULL as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Gender is ' || bez_lang as comments
	from 
		(select distinct userbk_nr,geschlecht_cd,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,geschlecht_cd,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.KD1GESCHLECHT) s 
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by geschlecht_cd;

	--ID type
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		5 as ref_type,
		'KD_VSB.iddoc_cd' as Finnova_name,
		't_person_identification.type' as MROS_name,
		'8.5 Identifier type' as MROS_lookup_table,
		iddoc_cd as Finnova_value,
		4 as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Identifier type is ' || bez_lang as comments
	from 
		F2LDATP0.KD1IDDOC 
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by iddoc_cd;

	--contact type
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		6 as ref_type,
		'KD_EMAIL.mail_art_cd' as Finnova_name,
		't_phone.tph_contact_type' as MROS_name,
		'8.9 Phone Address Type' as MROS_lookup_table,
		mail_art_cd as Finnova_value,
		2 as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Contact type is ' || bez_lang as comments
	from 
		(select distinct userbk_nr,mail_art_cd,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,mail_art_cd,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.AL1MAILA) s  
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by mail_art_cd;

	--phone type
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		7 as ref_type,
		'KD_EMAIL.mail_art_cd' as Finnova_name,
		't_phone.tph_communication_type' as MROS_name,
		'8.10 Communication Type' as MROS_lookup_table,
		mail_art_cd as Finnova_value,
		4 as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Contact type is ' || bez_lang as comments
	from 
		(select distinct userbk_nr,mail_art_cd,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,mail_art_cd,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.AL1MAILA) s   
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by mail_art_cd;

	--company type
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		8 as ref_type,
		'KD_STAMM.kd_rstat_cd' as Finnova_name,
		't_entity_my_client.incorporation_legal_form' as MROS_name,
		'8.11 Entity Legal Form Type' as MROS_lookup_table,
		kd_rstat_cd as Finnova_value,
		4 as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Incorporation legal form is ' || bez_lang as comments
	from 
		(select distinct userbk_nr,kd_rstat_cd,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,kd_rstat_cd,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.KD1RSTAT) s 
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by kd_rstat_cd;

	--account type
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		9 as ref_type,
		'KT1ART.kt_art_ber_cd' as Finnova_name,
		't_account_my_client.personal_account_type' as MROS_name,
		'8.3 Account type' as MROS_lookup_table,
		kt_art_ber_cd as Finnova_value,
		14 as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Account type is ' || bez_lang as comments
	from 
		(select distinct userbk_nr,kt_art_ber_cd,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,kt_art_ber_cd,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.KT1ARTBE) s 
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by kt_art_ber_cd;

	--transaction type
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		10 as ref_type,
		'KT_BUCH/ZV_ZLG_T.buchtyp_cd' as Finnova_name,
		'transaction.transmode_code' as MROS_name,
		'8.6 Conduction Type - Transaction Mode' as MROS_lookup_table,
		buchtyp_cd as Finnova_value,
		13 as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		'Transaction mode is ' || bez_lang as comments
	from 
		(select distinct userbk_nr,buchtyp_cd,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,buchtyp_cd,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.KT1BUCHT) s 
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by buchtyp_cd;
	
	--trak_ref_typ
	insert into FA_MROS_REFERENCE_TABLE 
	select 
		F_BANK_ID as userbk_nr,
		11 as ref_type,
		'KT_BUCH/ZV_ZLG_T.trak_ref_typ' as Finnova_name,
		'no matching required' as MROS_name,
		NULL as MROS_lookup_table,
		trak_ref_typ as Finnova_value,
		NULL as MROS_value,
		1 as activ_MROS,
		1 as activ_data_load,
		'' as special_mark,
		bez_lang as comments
	from 
		(select distinct userbk_nr,trak_ref_typ,sprache_cd,
			first_value(bez_lang ignore nulls) over (partition by userbk_nr,trak_ref_typ,sprache_cd order by VERS desc) as bez_lang
		from F2LDATP0.AL1TRAK_REF_ART) s 
	where userbk_nr=F_BANK_ID and sprache_cd=sprache
	order by trak_ref_typ;

	--certain attributes are bank-independent:
	--domicile company flag
	update FA_MROS_REFERENCE_TABLE set MROS_value = 'yes'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 3 and Finnova_value = '1';
	update FA_MROS_REFERENCE_TABLE set MROS_value = 'no'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 3 and Finnova_value = '0';
	--gender
	update FA_MROS_REFERENCE_TABLE set MROS_value = 'E'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 4 and Finnova_value = '0';
	update FA_MROS_REFERENCE_TABLE set MROS_value = 'W'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 4 and Finnova_value = '1';
	update FA_MROS_REFERENCE_TABLE set MROS_value = 'M'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 4 and Finnova_value = '2';
	update FA_MROS_REFERENCE_TABLE set MROS_value = 'U'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 4 and Finnova_value = '9';

	if fill_flag=1 then	
		--region (branch)
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'ZH'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '100';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '101';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '102';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '103';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'VD'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '104';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'BE'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '200';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'LU'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '202';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '204';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'BL'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '300';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'BS'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '301';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '302';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '303';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'SG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 1 and Finnova_value = '400';
		--region (state)
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AI'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '2';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AR'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '3';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'BE'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '4';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'ZH'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '5';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'BL'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '6';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'BS'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '7';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'FR'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '8';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'GE'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '9';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'AG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '10';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'JU'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '11';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'LU'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '12';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'NE'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '13';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'NW'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '14';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'OW'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '15';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'SG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '16';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'SH'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '17';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'SO'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '18';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'SZ'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '19';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'TI'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '21';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'UR'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '22';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'VD'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '23';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'VS'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '24';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'ZG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '25';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'GR'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '26';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'GL'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '27';
		update FA_MROS_REFERENCE_TABLE set MROS_value = 'TG'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 2 and Finnova_value = '28';
		--ID type
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 5 and Finnova_value = '20';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '2'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 5 and Finnova_value = '30';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 5 and Finnova_value = '40';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '6'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 5 and Finnova_value = '60';
		--contact type
		update FA_MROS_REFERENCE_TABLE set MROS_value = '4'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 6 and Finnova_value = 'EMG';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 6 and Finnova_value = 'EMP';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '4'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 6 and Finnova_value = 'FG1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 6 and Finnova_value = 'FP1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '4'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 6 and Finnova_value = 'TG1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '4'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 6 and Finnova_value = 'TG2';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 6 and Finnova_value = 'TP1';
		--phone type
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'APO';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'BPO';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'EMAI';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'EMG';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '3'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'EMP';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'FAXH';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'FAXN';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'FG1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'FP1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'TA1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'TBI1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'TG1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'TG2';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '2'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'TN1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'TP1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'TT1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '2'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 7 and Finnova_value = 'T1';
		--company type
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '1';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '9'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '2';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '15'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '3';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '14'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '5';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '10'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '6';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '16'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '7';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '8';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '2'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '12';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '16'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '17';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '18';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '10'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '28';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '14'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '29';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '15'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '30';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '14'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '38';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '2'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '42';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '15'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '43';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '45';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '46';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '15'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '55';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'    where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '56';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '16'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '57';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '16'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 8 and Finnova_value = '58';
		--account type
		update FA_MROS_REFERENCE_TABLE set MROS_value = '2'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '51';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '15'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '52';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '53';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '58';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '4'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '59';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '4'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '61';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '62';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '8'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '63';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '17'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 9 and Finnova_value = '67';
		--transaction type
		update FA_MROS_REFERENCE_TABLE set activ_MROS = 0  where userbk_nr=F_BANK_ID and ref_type = 10
			and Finnova_value in ('7','15','17','24','25','34','44','45','46','49','50','51','59','61','62','63','75','76','77','82','83','84','86','88','90','91','92','93','94','95','96','97','98','99','100','101','102','105','107','108','109','111','114','115','117','119','120','122','123','124','125','128','131','133','134','135','136','137','138','139','140','141','142','143','144','145','146','148','149','150','151','152','153','154','155','156','159','161','162','163','164','165','167','168','169','170','171','172','173','174','175','179','180','187','190','191','193','194','195','196','197','200','201','202','203','204','205','206','207','208','209','210','211','213','214','215','216','217','218','219','220','221','222','225','226','227','228','229','230','231','232','233','234','235','236','237','238','239','240','243','244','245','246','251','257','265','269','271','275','277','281','282','286','300','302','303','304','305','306','309','310','311','312','315','316','317','318','319','357','360','380','400','401','402','403','404','405','406','407','408','409','410','411','412','413','420','422','430','432','457','471','502','503','505','515','516','524','552','553','561','580','581','582','583','584','585','586','587','589','604','700','701','721','724','725','726','727','728','729','730','731','732','733','734','736','761','764','765','766','767','768','769','770','771','772','773','774','776','880','999');
		update FA_MROS_REFERENCE_TABLE set activ_data_load = 0  where userbk_nr=F_BANK_ID and ref_type = 10
			and Finnova_value in ('7','15','17','24','25','34','44','45','46','49','50','51','59','61','62','63','75','76','77','82','83','84','86','88','90','91','92','93','94','95','96','97','98','99','100','101','102','105','107','108','109','111','114','115','117','119','120','122','123','124','125','128','131','133','134','135','136','137','138','139','140','141','142','143','144','145','146','148','149','150','151','152','153','154','155','156','159','161','162','163','164','165','167','168','169','170','171','172','173','174','175','179','180','187','190','191','193','194','195','196','197','200','201','202','203','204','205','206','207','208','209','210','211','213','214','215','216','217','218','219','220','221','222','225','226','227','228','229','230','231','232','233','234','235','236','237','238','239','240','243','244','245','246','251','257','265','269','271','275','277','281','282','286','300','302','303','304','305','306','309','310','311','312','315','316','317','318','319','357','360','380','400','401','402','403','404','405','406','407','408','409','410','411','412','413','420','422','430','432','457','471','502','503','505','515','516','524','552','553','561','580','581','582','583','584','585','586','587','589','604','700','701','721','724','725','726','727','728','729','730','731','732','733','734','736','761','764','765','766','767','768','769','770','771','772','773','774','776','880','999');
		update FA_MROS_REFERENCE_TABLE set special_mark='withdrawal' where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value in (10,87,104);
		update FA_MROS_REFERENCE_TABLE set special_mark='deposit' where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = 11;
		update FA_MROS_REFERENCE_TABLE set MROS_value = '22'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '2';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '22'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '3';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '6';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '1'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '8';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '2'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '12';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '22';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '18'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '39';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '40';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '42';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '43';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '18'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '64';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '18'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '65';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '21'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '248';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '21'  where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '249';
		update FA_MROS_REFERENCE_TABLE set MROS_value = '5'   where activ_MROS=1 and userbk_nr=F_BANK_ID and ref_type = 10 and Finnova_value = '605';
		--trak_ref_typ
		update FA_MROS_REFERENCE_TABLE set activ_MROS=0 where userbk_nr=F_BANK_ID and ref_type = 11
			and Finnova_value in (0,4,9,29,30,39,40,41,51,52,53,54,55,56,57,58,60,62,63,64,65,66,67,68,69,70,71,72,73,75,77,78,80,81,83,84,85,86,87,88,89,90,91,92,95,96,98,100,110,111,115,116,120,121,122,123,124,125,130,904,8000,9001,9100);		
		update FA_MROS_REFERENCE_TABLE set activ_data_load=0 where userbk_nr=F_BANK_ID and ref_type = 11
			and Finnova_value in (0,4,9,29,30,39,40,41,51,52,53,54,55,56,57,58,60,62,63,64,65,66,67,68,69,70,71,72,73,75,77,78,80,81,83,84,85,86,87,88,89,90,91,92,95,96,98,100,110,111,115,116,120,121,122,123,124,125,130,904,8000,9001,9100);		
	end if;
	commit;
end;