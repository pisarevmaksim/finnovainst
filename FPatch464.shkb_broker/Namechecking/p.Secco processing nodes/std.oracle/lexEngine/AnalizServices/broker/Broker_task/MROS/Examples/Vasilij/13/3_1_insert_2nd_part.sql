﻿truncate table FA_MROS_OM_TEXT;
truncate table FA_MROS_ZV_ADR_EFF;
truncate table FA_MROS_KT_BUCH;
truncate table FA_MROS_KT_BUCH_EA;
commit;



--input messages
insert into FA_MROS_OM_TEXT
select om_t.userbk_nr,om_t.om_text_lnr,
	listagg(t_typ.om_text_typ||': '||trim(om_t.text_rec),'; '||chr(10)||chr(13)) within group (order by om_t.om_text_typ_cd asc) as text_rec
from
	F2LDATP0.om_text om_t
left join
	(select distinct userbk_nr,om_text_typ_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,om_text_typ_cd order by VERS desc) as om_text_typ
	from F2LDATP0.om1text_typ where sprache_cd=5) t_typ
on om_t.userbk_nr=t_typ.userbk_nr and om_t.om_text_typ_cd=t_typ.om_text_typ_cd
where exists
	(select * from FA_MROS_ZV_ZLG_T pay1 where pay1.userbk_nr=om_t.userbk_nr and pay1.om_text_lnr=om_t.om_text_lnr)
or exists 
	(select * from FA_MROS_ZV_ZLG_T pay2 where pay2.userbk_nr=om_t.userbk_nr and pay2.auf_om_text_lnr=om_t.om_text_lnr)
group by om_t.userbk_nr,om_t.om_text_lnr;

--output messages
insert into FA_MROS_ZV_ADR_EFF
select 
	adr.userbk_nr,
	adr.zv_adr_eff_lnr,
	adr.kt_id_cd,
	kt_id.kt_id_text,
	adr.kt_id,
	adr.kt_ext_art_cd,
	adr.ext_kt_nr,
	adr.bic,
	adr.adr_zeilen,
	adr.empf_art_cd,
	adr.zv_adr_eff_lnr_pri,
	adr_pr.kt_id_cd as auf_kt_id_cd,
	adr_pr.kt_id as auf_kt_id,
	adr_pr.kt_ext_art_cd as auf_kt_ext_art_cd,
	adr_pr.ext_kt_nr as auf_ext_kt_nr,
	adr_pr.bic as auf_bic,
	adr_pr.adr_zeilen as auf_adr_zeilen
from
	F2LDATP0.ZV_ADR_EFF adr
left join
	F2LDATP0.ZV_ADR_EFF adr_pr
on adr.userbk_nr=adr_pr.userbk_nr and adr.zv_adr_eff_lnr_pri=adr_pr.zv_adr_eff_lnr
left join
	(select distinct userbk_nr,kt_id_cd,
		first_value(bez_lang ignore nulls) over (partition by userbk_nr,kt_id_cd order by VERS desc) as kt_id_text
	from F2LDATP0.ZV1KT_ID where sprache_cd=5) kt_id
on adr.userbk_nr=kt_id.userbk_nr and adr.kt_id_cd=kt_id.kt_id_cd
where exists
	(select * from FA_MROS_ZV_ZLG_T pay1 where pay1.userbk_nr=adr.userbk_nr and pay1.zv_adr_eff_lnr=adr.zv_adr_eff_lnr)
	or exists
	(select * from FA_MROS_ZV_ZLG_T pay2 where pay2.userbk_nr=adr.userbk_nr and pay2.auf_zv_adr_eff_lnr=adr.zv_adr_eff_lnr);

--transactions
insert into FA_MROS_KT_BUCH
with 
    buchungen as 
        (select
            kt.CREATE_ID,
            kt.USERBK_NR,
            KT_LNR,
            KT_BEST_LNR,
            KT_BUCH_LNR,
            KT_BUCH_LNR as KT_BUCH_DET_LNR,
            BUCH_GNR,
            TRAK_REF_LNR,
            TRAK_REF_TYP,
            BUCH_DAT_A,
            BUCH_DAT_B,
            BUCH_DAT_C,
            VALUTA,
            BELEG_NR,
            TRAK_NR,
            BUCH_STAT_CD,
            SITZ_NR,
            SH_CD,
            BUCHTYP_CD,
            TEXT_ZUS,
            TEXT_EXT,
            TEXT_INT,
            BETRAG_BWRG,
            BETRAG_KWRG,
            KAP_ZINS_CD,
            PERF_CD,
            KOST_STL_LNR,
            BUCH_PERI_BEG,
            BUCH_PERI_END,
            UMS_PFL_CD,
            ABR_PRINT_CD,
            DISPL_DET_CD,
            CHRGL_KAWRG,
            CHANGE_KAWRG,
            SPH_NACHTR_CD,
            BUCH_AUSGL_ID,
            KT_BUCH_LNR_B as MG_KT_BUCH_NR,
            KD_LNR_ERF
        from
            F2LDATP0.KT_BUCH kt
        join
            FA_MROS_REFERENCE_TABLE rt_trtp
        on  kt.userbk_nr=rt_trtp.userbk_nr and kt.trak_ref_typ=rt_trtp.Finnova_value and rt_trtp.ref_type=11 and rt_trtp.activ_data_load=1
        join
            FA_MROS_REFERENCE_TABLE rt_btp
        on kt.userbk_nr=rt_btp.userbk_nr and kt.buchtyp_cd=rt_btp.finnova_value and rt_btp.ref_type=10 and rt_btp.activ_data_load=1
    --	where kt.buch_dat_a between to_date('01.01.2018', 'dd.mm.yyyy') and to_date('31.12.2018', 'dd.mm.yyyy')			 
        union all
        select
            kt.CREATE_ID,
            kt.USERBK_NR,
            KT_LNR,
            KT_BEST_LNR,
            KT_BUCH_LNR,
            KT_BUCH_DET_LNR,
            BUCH_GNR,
            TRAK_REF_LNR,
            TRAK_REF_TYP,
            BUCH_DAT_A,
            BUCH_DAT_B,
            BUCH_DAT_C,
            VALUTA,
            BELEG_NR,
            TRAK_NR,
            BUCH_STAT_CD,
            SITZ_NR,
            SH_CD,
            BUCHTYP_CD,
            TEXT_ZUS,
            TEXT_EXT,
            TEXT_INT,
            BETRAG_BWRG,
            BETRAG_KWRG,
            KAP_ZINS_CD,
            PERF_CD,
            KOST_STL_LNR,
            BUCH_PERI_BEG,
            BUCH_PERI_END,
            UMS_PFL_CD,
            ABR_PRINT_CD,
            DISPL_DET_CD,
            CHRGL_KAWRG,
            CHANGE_KAWRG,
            SPH_NACHTR_CD,
            BUCH_AUSGL_ID,
            MG_KT_BUCH_NR,
            KD_LNR_ERF
        from
            F2LDATP0.KT_BUCH_DET kt
        join
            FA_MROS_REFERENCE_TABLE rt_trtp
        on  kt.userbk_nr=rt_trtp.userbk_nr and kt.trak_ref_typ=rt_trtp.Finnova_value and rt_trtp.ref_type=11 and rt_trtp.activ_data_load=1
        join
            FA_MROS_REFERENCE_TABLE rt_btp
        on kt.userbk_nr=rt_btp.userbk_nr and kt.buchtyp_cd=rt_btp.finnova_value and rt_btp.ref_type=10 and rt_btp.activ_data_load=1
    --	where kt.buch_dat_a between to_date('01.01.2018', 'dd.mm.yyyy') and to_date('31.12.2018', 'dd.mm.yyyy')				 
        ),
	trans_LAND_BEZ as
        (select
            auf_zv.userbk_nr,
            auf_zv.trak_ref_lnr,
            auf_zv.auf_lnr,
            auf_zv.auf_id,
            max(nvl(cntr_r1.COUNTRY_RISK, 1)) as COUNTRY_RISK,
            max(
                case
                    when nvl(cntr_r1.iso_land_cd, 'CH') = 'CH' then 0
                    else 1
                end
            ) as international,
            max(nvl(cntr_r1.iso_land_cd, 'CH')) keep(dense_rank last order by cntr_r1.COUNTRY_RISK) as iso_land_cd,
            max(nvl(cntr_r2.COUNTRY_RISK, 1)) as COUNTRY_RISK1,
            max(
                case
                    when nvl(cntr_r2.iso_land_cd, 'CH') = 'CH' then 0
                    else 1
                end
            ) as international1,
            max(nvl(cntr_r2.iso_land_cd, 'CH')) keep(dense_rank last order by cntr_r1.COUNTRY_RISK) as iso_land_cd1
        from
            FA_MROS_ZV_AUF auf_zv 
        left join 
            FA_MROS_LAENDERKAT cntr_r1			 -- Migros Bank risk categories
        on auf_zv.userbk_nr = cntr_r1.userbk_nr and auf_zv.land_lnr_dom = cntr_r1.land_lnr
        left join 
            FA_MROS_LAENDERKAT cntr_r2			 -- Migros Bank risk categories
        on auf_zv.userbk_nr = cntr_r2.userbk_nr and auf_zv.land_lnr_dom1 = cntr_r2.land_lnr
        group by
            auf_zv.userbk_nr,
            auf_zv.trak_ref_lnr,
            auf_zv.auf_lnr,
            auf_zv.auf_id
        ) 
select 
	buch.userbk_nr,
	buch.create_id,
	trans_LAND_BEZ.auf_lnr,
	trans_LAND_BEZ.auf_id,
	buch.kt_buch_lnr,
	buch.KT_BUCH_DET_LNR,
	kd.kd_lnr,
	kd.edit_kd_nr,
	kt.kt_gnr,
	kt.kt_lnr,
	kt.edit_kt_nr,
	kd.kd_htyp_cd,
	buch.sitz_nr,
	sitz.bez_lang as SITZ_NR_BEZ,
	buch.buch_dat_a,
	buch.buch_dat_b,
	buch.buch_dat_c,
	extract(year from buch.buch_dat_a) * 10000 + extract(month from buch.buch_dat_a) * 100 + extract(day from buch.buch_dat_a) as BUCH_DAT_A_1,
	buch.valuta,
	case to_char(buch.sh_cd)
		when '1' then 'D'
		when '2' then 'C'
	end as SH_CD,
	buch.buchtyp_cd,
	typ.bez_lang as BUCHTYP_BEZ,
	buch.betrag_bwrg,
	buch.betrag_kwrg,
	buch.perf_cd,
	buch.text_ext,
	buch.text_zus,
	buch.trak_ref_lnr,
	buch.trak_ref_typ,
	tr_typ.bez_lang as TRAK_REF_TYP_BEZ,
	kt.wrg_lnr,
	wrg.bez_lang as WRG_BEZ,
	wrg.iso_wrg_cd,
	case
		when nvl(trans_LAND_BEZ.COUNTRY_RISK, 1) >= nvl(trans_LAND_BEZ.COUNTRY_RISK1, 1) then nvl(trans_LAND_BEZ.COUNTRY_RISK, 1)
		else nvl(trans_LAND_BEZ.COUNTRY_RISK1, 1)
	end as LAND_LNR_DOM_RISK,
	case
		when nvl(trans_LAND_BEZ.international, 0) + nvl(trans_LAND_BEZ.international1, 0) > 0 then 1
		else 0
	end as LAND_LNR_DOM_INTERNAT,
	case
		when nvl(trans_LAND_BEZ.COUNTRY_RISK1, 1) > nvl(trans_LAND_BEZ.COUNTRY_RISK, 1)
			 or nvl(trans_LAND_BEZ.iso_land_cd, 'CH') = 'CH' 
		then nvl(trans_LAND_BEZ.iso_land_cd1, 'CH')
		else nvl(trans_LAND_BEZ.iso_land_cd, 'CH')
	end as LAND_LNR_DOM_MAX_RISK
from
	buchungen buch	-- Use Buchungen instead of KTWBUCH_DET
join 
	F2LDATP0.kt_stamm kt
on buch.userbk_nr = kt.userbk_nr and buch.kt_lnr = kt.kt_lnr
join 
	FA_MROS_KD_STAMM kd
on kt.userbk_nr = kd.userbk_nr and kt.kd_lnr = kd.kd_lnr and kd.bank_flag=0 and kd.partner_flag=0
--currency name
left join 
	F2LDATP0.al1wrg wrg
on kt.userbk_nr = wrg.userbk_nr and kt.wrg_lnr = wrg.wrg_lnr and wrg.sprache_cd=1
--transaction purpose
left join 
	(select distinct userbk_nr, trak_ref_typ,sprache_cd,
		first_value(bez_lang ignore nulls) over(partition by userbk_nr,trak_ref_typ,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.al1trak_ref_art) tr_typ
on buch.userbk_nr = tr_typ.userbk_nr and buch.trak_ref_typ = tr_typ.trak_ref_typ and tr_typ.sprache_cd = 5
--transaction type
left join 
	(select distinct userbk_nr,buchtyp_cd,sprache_cd,
		first_value(bez_lang ignore nulls) over(partition by userbk_nr,buchtyp_cd,sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.kt1bucht) typ
on buch.userbk_nr = typ.userbk_nr and buch.buchtyp_cd = typ.buchtyp_cd and typ.sprache_cd = 5
--branch
left join 
	F2LDATP0.al1sitz sitz
on buch.userbk_nr = sitz.userbk_nr and buch.sitz_nr = sitz.sitz_nr and sitz.sprache_cd = 5
--join transactions (KTWBUCH_DET) with payments (ZV_ZLG_T) in order to find international transactions
join 
	trans_LAND_BEZ
on buch.userbk_nr = trans_LAND_BEZ.userbk_nr and buch.trak_ref_lnr = trans_LAND_BEZ.trak_ref_lnr;

--deposit/withdrawal
insert into FA_MROS_KT_BUCH_EA
select 
	buch.userbk_nr,
	buch.create_id,
	buch.kt_buch_lnr,
	kd.kd_lnr,
	kd.edit_kd_nr,
	kt.kt_gnr,
	kt.kt_lnr,
	kt.edit_kt_nr,
	kd.kd_htyp_cd,
	buch.sitz_nr,
	sitz.bez_lang as SITZ_NR_BEZ,
	buch.buch_dat_a,
	buch.buch_dat_b,
	buch.buch_dat_c,
	to_char(buch.buch_dat_a,'yyyymmdd') as BUCH_DAT_A_1,
	buch.valuta,
	case to_char(buch.sh_cd)
		when '1' then 'D'
		when '2' then 'C'
	end as SH_CD,
	buch.buchtyp_cd,
	typ.bez_lang as BUCHTYP_BEZ,
	grp.gf_nr,
	gf.bez_lang as GF_NR_BEZ,
	buch.betrag_bwrg,
	buch.betrag_kwrg,
	buch.perf_cd,
	buch.text_ext,
	buch.text_zus,
	buch.trak_ref_lnr,
	buch.trak_ref_typ,
	tr_typ.bez_lang as TRAK_REF_TYP_BEZ,
	kt.wrg_lnr,
	wrg.bez_lang as WRG_BEZ,
	wrg.iso_wrg_cd,
	trim(replace(replace(replace(av.adr_zeile_1_ez,chr(9),''),chr(13),''),chr(10),'')) as adr_zeile_1_ez,
	trim(replace(replace(replace(av.adr_zeile_2_ez,chr(9),''),chr(13),''),chr(10),'')) as adr_zeile_2_ez,
	trim(replace(replace(replace(av.adr_zeile_3_ez,chr(9),''),chr(13),''),chr(10),'')) as adr_zeile_3_ez,
	trim(replace(replace(replace(av.adr_zeile_4_ez,chr(9),''),chr(13),''),chr(10),'')) as adr_zeile_4_ez,
	trim(replace(replace(replace(av.adr_zeile_5_ez,chr(9),''),chr(13),''),chr(10),'')) as adr_zeile_5_ez,
	case when rt.special_mark='withdrawal' then 1 else 0 end as withdrawal_flag
from
	F2LDATP0.KT_BUCH buch
join
	FA_MROS_REFERENCE_TABLE rt
on buch.userbk_nr=rt.userbk_nr and buch.buchtyp_cd=rt.Finnova_value and rt.ref_type=10 and rt.activ_data_load=1 and rt.special_mark in ('withdrawal','deposit')
join 
	F2LDATP0.kt_stamm kt
on buch.userbk_nr = kt.userbk_nr and buch.kt_lnr = kt.kt_lnr
join 
	FA_MROS_KD_STAMM kd
on kt.userbk_nr = kd.userbk_nr and kt.kd_lnr = kd.kd_lnr and kd.bank_flag=0 and kd.partner_flag=0
--currency name
left join 
	F2LDATP0.al1wrg wrg
on kt.userbk_nr = wrg.userbk_nr and kt.wrg_lnr = wrg.wrg_lnr and wrg.sprache_cd=1
--transaction purpose
left join 
	(select distinct userbk_nr, trak_ref_typ, sprache_cd,
		first_value(bez_lang ignore nulls) over(partition by userbk_nr, trak_ref_typ, sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.al1trak_ref_art) tr_typ
on buch.userbk_nr = tr_typ.userbk_nr and buch.trak_ref_typ = tr_typ.trak_ref_typ and tr_typ.sprache_cd = 5
--transaction type
left join 
	(select distinct userbk_nr, buchtyp_cd, sprache_cd,
		first_value(bez_lang ignore nulls) over(partition by userbk_nr, buchtyp_cd, sprache_cd order by VERS desc) as bez_lang
	from F2LDATP0.kt1bucht) typ
on buch.userbk_nr = typ.userbk_nr and buch.buchtyp_cd = typ.buchtyp_cd and typ.sprache_cd = 5
--branch
left join 
	F2LDATP0.al1sitz sitz
on buch.userbk_nr = sitz.userbk_nr and buch.sitz_nr = sitz.sitz_nr and sitz.sprache_cd = 5
--special cases
left join
	F2LDATP0.K2_AVIS_DATA av
on buch.userbk_nr=av.userbk_nr and buch.trak_ref_lnr=av.k2_buch_gnr and buch.kt_lnr=av.kt_lnr
left join
	F2LDATP0.k2_buch_grp grp
on buch.userbk_nr=grp.userbk_nr and buch.trak_ref_lnr=grp.k2_buch_gnr and grp.k2_buch_pos_lnr=buch.trak_nr
left join 
	F2LDATP0.al1gf gf
on gf.userbk_nr=grp.userbk_nr and gf.gf_nr=grp.gf_nr and gf.sprache_cd=5
--where buch.buch_dat_a between to_date('01.01.2018', 'dd.mm.yyyy') and to_date('31.12.2018', 'dd.mm.yyyy')	
;

commit;
