--indexes
create unique index FA_MROS_OM_TEXTC2 on FA_MROS_OM_TEXT (om_text_lnr,userbk_nr);

create unique index FA_MROS_ZV_ADR_EFFC2 on FA_MROS_ZV_ADR_EFF (zv_adr_eff_lnr,userbk_nr);

create unique index FA_MROS_KT_BUCHC2 on FA_MROS_KT_BUCH (buch_dat_a_1,userbk_nr,kt_buch_lnr,kt_buch_det_lnr);
create index FA_MROS_KT_BUCHI1 on FA_MROS_KT_BUCH (kt_buch_lnr,userbk_nr);
create index FA_MROS_KT_BUCHI2 on FA_MROS_KT_BUCH (kt_lnr,userbk_nr);
create index FA_MROS_KT_BUCHI3 on FA_MROS_KT_BUCH (edit_kt_nr,userbk_nr);
create index FA_MROS_KT_BUCHI4 on FA_MROS_KT_BUCH (kd_lnr,userbk_nr);
create index FA_MROS_KT_BUCHI5 on FA_MROS_KT_BUCH (edit_kd_nr,userbk_nr);

create unique index FA_MROS_KT_BUCH_EAC2 on FA_MROS_KT_BUCH_EA (buch_dat_a_1,userbk_nr,kt_buch_lnr);
create unique index FA_MROS_KT_BUCH_EAC3 on FA_MROS_KT_BUCH_EA (kt_buch_lnr,userbk_nr);
create index FA_MROS_KT_BUCH_EAI1 on FA_MROS_KT_BUCH_EA (kt_lnr,userbk_nr);
create index FA_MROS_KT_BUCH_EAI2 on FA_MROS_KT_BUCH_EA (edit_kt_nr,userbk_nr);
create index FA_MROS_KT_BUCH_EAI3 on FA_MROS_KT_BUCH_EA (kd_lnr,userbk_nr);
create index FA_MROS_KT_BUCH_EAI4 on FA_MROS_KT_BUCH_EA (edit_kd_nr,userbk_nr);


