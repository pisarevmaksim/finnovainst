--we create global types in the scheme not in package
create or replace TYPE FAF_MROS_IdRsn force as object (chID varchar2(50),partner_flag number(1),rsn varchar2(4000));
create or replace TYPE FAF_MROS_IdRsn_t is table of FAF_MROS_IdRsn;
create or replace TYPE FAF_MROS_charID force as object (chID varchar2(50));
create or replace TYPE FAF_MROS_charID_t is table of FAF_MROS_charID;
create or replace TYPE FAF_MROS_numID force as object (nID number(24));
create or replace TYPE FAF_MROS_numID_t is table of FAF_MROS_numID;
create or replace TYPE FAF_MROS_NumNum force as object (ord number(24),acc number(24));
create or replace TYPE FAF_MROS_NumNum_t is table of FAF_MROS_NumNum;

CREATE OR REPLACE PACKAGE FAF_MROS_DM AS
	--Char processing
	FUNCTION char_process (
		ch varchar2,
		mv varchar2,
		n int)
	RETURN varchar2;

	--Transaction node from payments
	FUNCTION trans_node_from_payments (
		F_BANK_ID number,
		SET_OF_PAYMENT_ID FAF_MROS_numID_t,
		SET_OF_ORDER_NR FAF_MROS_charID_t)
	RETURN xmltype;

	--caption part of report (technical function)
	function head_part_of_report (
		F_BANK_ID number,
		RP_NAME varchar2,
		RP_SURNAME varchar2,
		RP_PHONE varchar2,
		RP_EMAIL varchar2,
		RP_OCCUPATION varchar2,
		RP_ADDRESS varchar2,
		RP_CITY varchar2,
		FIU_REF_NUMBER varchar2,
		ENTITY_REFERENCE varchar2,	
		REPORT_REASON varchar2,
		R_ACTION varchar2,
		REPORT_INDICATORS FAF_MROS_charID_t,
		REPORT xmltype,
		STR_FLAG int
	) 
	return xmltype;

	--report with Transaction node
	FUNCTION set_of_payments (
		F_BANK_ID number default 949,
		RP_NAME varchar2 default NULL,
		RP_SURNAME varchar2 default NULL,
		RP_PHONE varchar2 default NULL,
		RP_EMAIL varchar2 default NULL,
		RP_OCCUPATION varchar2 default NULL,
		RP_ADDRESS varchar2 default NULL,
		RP_CITY varchar2 default NULL,
		FIU_REF_NUMBER varchar2 default NULL,
		ENTITY_REFERENCE varchar2 default 'n/a',	
		REPORT_REASON varchar2 default 'n/a',
		R_ACTION varchar2 default 'n/a',
		REPORT_INDICATORS FAF_MROS_charID_t default NULL,
		SET_OF_PAYMENT_ID FAF_MROS_numID_t default NULL,
		SET_OF_ORDER_NR FAF_MROS_charID_t default NULL)
	RETURN xmltype;

	--transactions report from ktwbuch_det (transaction number collection)
	FUNCTION booking_report (
		F_BANK_ID number default 949,
		RP_NAME varchar2 default NULL,
		RP_SURNAME varchar2 default NULL,
		RP_PHONE varchar2 default NULL,
		RP_EMAIL varchar2 default NULL,
		RP_OCCUPATION varchar2 default NULL,
		RP_ADDRESS varchar2 default NULL,
		RP_CITY varchar2 default NULL,
		FIU_REF_NUMBER varchar2 default NULL,
		ENTITY_REFERENCE varchar2 default 'n/a',	
		REPORT_REASON varchar2 default 'n/a',
		R_ACTION varchar2 default 'n/a',
		REPORT_INDICATORS FAF_MROS_charID_t default NULL,
		SET_OF_TR_ID FAF_MROS_numID_t default NULL,
		AC_NR varchar2 default NULL) 
	RETURN xmltype;

	--transactions report from ktwbuch_det (clients/accounts + filter)
	FUNCTION historical_report (
		F_BANK_ID number default 949,
		RP_NAME varchar2 default NULL,
		RP_SURNAME varchar2 default NULL,
		RP_PHONE varchar2 default NULL,
		RP_EMAIL varchar2 default NULL,
		RP_OCCUPATION varchar2 default NULL,
		RP_ADDRESS varchar2 default NULL,
		RP_CITY varchar2 default NULL,
		FIU_REF_NUMBER varchar2 default NULL,
		ENTITY_REFERENCE varchar2 default 'n/a',	
		REPORT_REASON varchar2 default 'n/a',
		R_ACTION varchar2 default 'n/a',
		REPORT_INDICATORS FAF_MROS_charID_t default NULL,
		SET_OF_CL_NR FAF_MROS_charID_t default NULL,
		SET_OF_AC_NR FAF_MROS_charID_t default NULL,
		DATE_FROM number default null,
		DATE_TO number default null,
		LOW_AMOUNT number default 0.01,
		DEBIT number default 1,
		CREDIT number default 1,
		N_TRANS_LIMIT number default 1000) 
	RETURN xmltype;

	--report with Activity node
	FUNCTION report_activity (
		F_BANK_ID number default 949,
		RP_NAME varchar2 default NULL,
		RP_SURNAME varchar2 default NULL,
		RP_PHONE varchar2 default NULL,
		RP_EMAIL varchar2 default NULL,
		RP_OCCUPATION varchar2 default NULL,
		RP_ADDRESS varchar2 default NULL,
		RP_CITY varchar2 default NULL,
		FIU_REF_NUMBER varchar2 default NULL,
		ENTITY_REFERENCE varchar2 default 'n/a',	
		REPORT_REASON varchar2 default 'n/a',
		R_ACTION varchar2 default 'n/a',
		REPORT_INDICATORS FAF_MROS_charID_t default NULL,
		SET_OF_ACCOUNTS FAF_MROS_IdRsn_t default NULL,
		SET_OF_CLIENTS FAF_MROS_IdRsn_t default NULL)
	RETURN xmltype; 
END FAF_MROS_DM;

CREATE OR REPLACE PACKAGE BODY FAF_MROS_DM AS
	--Char processing
	function char_process (
		ch in varchar2,
		mv in varchar2,
		n in int)
	return varchar2
	is
		str varchar2(8000);
	begin
		str:= trim(replace(replace(replace(ch,chr(10),' '),chr(9),' '),chr(13),' '));
		if str is null or str='' then
			str := mv;
		end if;
		if str is not null and n>0 then
			str := substr(str,1,n);
		end if;

		return(str);
	end;


	------------------------------------------------------------------------------------------------------------------


	--Transaction node from payments
	function trans_node_from_payments (
		F_BANK_ID in number,
		SET_OF_PAYMENT_ID in FAF_MROS_numID_t,
		SET_OF_ORDER_NR in FAF_MROS_charID_t
	) 
		return xmltype
	is
		TYPE NumTab IS TABLE OF number;
		TYPE ChTab IS TABLE OF varchar2(50);
	
		pm_id NumTab;
		a1_kt NumTab;
		a1_tp NumTab;
		a1_kd NumTab;
		a1_kdnr ChTab;
		a2_kt NumTab;
		a2_tp NumTab;
		a2_kd NumTab;
		a2_kdnr ChTab;
		om_nr NumTab;
		adr_nr NumTab;
		au_nr NumTab;
		tp NumTab;
		xml_t xmltype default NULL;
		xml_r xmltype default NULL;
		xml_i xmltype default NULL;
		xml_a xmltype default NULL;
		xml_p xmltype default NULL;
		xml_in xmltype default NULL;
		xml_out xmltype default NULL;
		xml_om xmltype default NULL;
		xml_zv xmltype default NULL;
		inst_name varchar2(255);
		swif varchar2(11);	
		n number default 0;	
		sld_bwrg number;
		sld_kwrg number;
		max_items number;
	begin
		max_items:=1000;

		--Bank's name and swift-code from the technical table 
		select char_process(institution_name,'n/a',255) into inst_name from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;
		select char_process(swift,'n/a',11) into swif from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;
		
		select count(*) into n from table(SET_OF_PAYMENT_ID);
		if n>0 then
			begin
				select distinct
					s3.ZV_ZLG_SYS_LNR
					,a1.KT_LNR
					,p1.KD_HTYP_CD
					,p1.KD_LNR
					,p1.EDIT_KD_NR
					,a2.KT_LNR
					,p2.KD_HTYP_CD
					,p2.KD_LNR
					,p2.EDIT_KD_NR
					,s3.zv_adr_eff_lnr
					,s3.auf_zv_adr_eff_lnr
					,s3.om_text_lnr
					,case 
						when s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NOT NULL then 1
						when s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NOT NULL then 4
						when (s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NULL and s3.sh_cd=1) or (s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NULL and s3.sh_cd=1) then 2
						when (s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NULL and s3.sh_cd=2) or (s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NULL and s3.sh_cd=2) then 3
					end
				bulk collect INTO pm_id,a1_kt,a1_tp,a1_kd,a1_kdnr,a2_kt,a2_tp,a2_kd,a2_kdnr,adr_nr,au_nr,om_nr,tp
				from
					FA_MROS_ZV_ZLG_T s3 
				--t_account 
				left join
					FA_MROS_KT_STAMM a1
				on s3.kt_lnr=a1.KT_LNR and s3.userbk_nr=a1.userbk_nr		  
				--t_person/t_entity (account owner)
				left join
					FA_MROS_KD_STAMM p1
				on a1.KD_LNR=p1.KD_LNR and a1.userbk_nr=p1.userbk_nr and p1.partner_flag=0
				--t_account (another side in internal transactions only)
				left join
					FA_MROS_KT_STAMM a2
				on s3.kt_lnr_b=a2.KT_LNR and s3.userbk_nr=a2.userbk_nr		   
				--t_person/t_entity (account owner)
				left join
					FA_MROS_KD_STAMM p2
				on a2.KD_LNR=p2.KD_LNR and a2.userbk_nr=p2.userbk_nr and p2.partner_flag=0
				--acceptable transaction types only
				join
					FA_MROS_REFERENCE_TABLE tr_tp
				on s3.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
				--acceptable trak_ref types only
				join
					FA_MROS_REFERENCE_TABLE ref_tp
				on s3.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
				where s3.ZV_ZLG_SYS_LNR in (select s.nID from table(SET_OF_PAYMENT_ID) s)
					and s3.userbk_nr=F_BANK_ID 
					and rownum<=max_items;
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
					n := 0;
			end;
			if pm_id.count = 0 then
				n := 0;
			end if;
		end if;

		if n=0 then 
			select count(*) into n from table(SET_OF_ORDER_NR);
			if n>0 then
				begin
					select distinct
						s3.ZV_ZLG_SYS_LNR
						,a1.KT_LNR
						,p1.KD_HTYP_CD
						,p1.KD_LNR
						,p1.EDIT_KD_NR
						,a2.KT_LNR
						,p2.KD_HTYP_CD
						,p2.KD_LNR
						,p2.EDIT_KD_NR
						,s3.zv_adr_eff_lnr
						,s3.auf_zv_adr_eff_lnr
						,s3.om_text_lnr
						,case 
							when s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NOT NULL then 1
							when s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NOT NULL then 4
							when (s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NULL and s3.sh_cd=1) or (s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NULL and s3.sh_cd=1) then 2
							when (s3.ZV_EING_AUSG_CD='E' and s3.AUF_ZV_ADR_EFF_LNR is NULL and s3.sh_cd=2) or (s3.ZV_EING_AUSG_CD='A' and s3.ZV_ADR_EFF_LNR is NULL and s3.sh_cd=2) then 3
						end
					bulk collect INTO pm_id,a1_kt,a1_tp,a1_kd,a1_kdnr,a2_kt,a2_tp,a2_kd,a2_kdnr,adr_nr,au_nr,om_nr,tp
					from
						FA_MROS_ZV_ZLG_T s3 
					--t_account 
					left join
						FA_MROS_KT_STAMM a1
					on s3.kt_lnr=a1.KT_LNR and s3.userbk_nr=a1.userbk_nr		  
					--t_person/t_entity (account owner)
					left join
						FA_MROS_KD_STAMM p1
					on a1.KD_LNR=p1.KD_LNR and a1.userbk_nr=p1.userbk_nr and p1.partner_flag=0
					--t_account (another side in internal transactions only)
					left join
						FA_MROS_KT_STAMM a2
					on s3.kt_lnr_b=a2.KT_LNR and s3.userbk_nr=a2.userbk_nr		   
					--t_person/t_entity (account owner)
					left join
						FA_MROS_KD_STAMM p2
					on a2.KD_LNR=p2.KD_LNR and a2.userbk_nr=p2.userbk_nr and p2.partner_flag=0
					--acceptable transaction types only
					join
						FA_MROS_REFERENCE_TABLE tr_tp
					on s3.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
					--acceptable trak_ref types only
					join
						FA_MROS_REFERENCE_TABLE ref_tp
					on s3.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
					where s3.AUF_ID in (select s.chID from table(SET_OF_ORDER_NR) s)
						and s3.userbk_nr=F_BANK_ID 
						and rownum<=max_items;
					EXCEPTION
						WHEN NO_DATA_FOUND THEN
						n := 0;
				end;
				if pm_id.count = 0 then
					n := 0;
				end if;
			end if;
		end if;
	
		if n>0 then
			--Cycle to work with the input set of transaction numbers
			FOR ii IN 1 .. pm_id.count
			LOOP			
				if pm_id(ii) is not null and tp(ii) is not null then
					if tp(ii) = 1 then
						--Input transaction
						begin
							select 
								xmlforest(
									char_process(
										case 
											when instr(s25.adr_zeilen,chr(10))>0 
											then substr(s25.adr_zeilen,1,instr(s25.adr_zeilen,chr(10))-1) 
											else s25.adr_zeilen 
										end
									,'n/a',255) as "institution_name",
									char_process(NVL(s25.bic,s25.kt_id),'n/a',11) as "swift"
								)
							into xml_t
							from
								FA_MROS_ZV_ADR_EFF s25 
							where s25.zv_adr_eff_lnr=au_nr(ii) and s25.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								select 
									xmlforest(
										'n/a' as "institution_name",
										'n/a' as "swift"
									)
								into xml_t
								from dual;
						end;

						begin
							select 
								xmlconcat(
									xml_t,
									xmlelement("account",
										char_process(replace(replace(
											case 
												when instr(s20.text_rec,chr(10))>0 
												then substr(s20.text_rec,instr(s20.text_rec,': ')+2,instr(s20.text_rec,chr(10))-instr(s20.text_rec,': ')-2) 
												else s20.text_rec 
											end
										,'/C/',''),'/',''),'n/a',50)),
									xmlelement("comments",char_process(s20.text_rec,'n/a',4000))
								)
							into xml_in
							from
								FA_MROS_OM_TEXT s20
							where om_text_lnr=om_nr(ii) and userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								select 
									xmlconcat(
										xml_t,
										xmlelement("account",'n/a'),
										xmlelement("comments",'n/a')
									)
								into xml_in
								from dual;
						end;
					end if;

					if tp(ii) = 4 then
						--Output transaction
						begin
							select 
								xmlforest(
									char_process(
										case 
											when ss1.EMPF_ART_CD=1 
											then 
												case 
													when instr(ss1.auf_adr_zeilen,chr(10))>0 
													then substr(ss1.auf_adr_zeilen,1,instr(ss1.auf_adr_zeilen,chr(10))-1) 
													else ss1.auf_adr_zeilen 
												end
											else 
												case 
													when instr(ss1.adr_zeilen,chr(10))>0 
													then substr(ss1.adr_zeilen,1,instr(ss1.adr_zeilen,chr(10))-1) 
													else ss1.adr_zeilen 
												end
										end
									,'n/a',255) as "institution_name",
									char_process(
										case 
											when ss1.EMPF_ART_CD=1 
											then NVL(NVL(ss1.auf_bic,ss1.auf_kt_id),ss1.bic)
											else NVL(ss1.bic,ss1.kt_id)
										end
									,'n/a',11) as "swift",
									char_process(ss1.ext_kt_nr,'n/a',50) as "account",
									case 
										when ss1.EMPF_ART_CD=1 
										then xmlelement("name",
											char_process(ss1.adr_zeilen,NULL,
												case 
													when instr(ss1.adr_zeilen,chr(10))<=3 or instr(ss1.adr_zeilen,chr(10))>255 
													then 255 
													else instr(ss1.adr_zeilen,chr(10))-1 
												end
											))
										else NULL 
									end as "t_entity",
									char_process(ss1.adr_zeilen,'n/a',4000) as "comments"
								)
							into xml_out
							from	 
								FA_MROS_ZV_ADR_EFF ss1 
							where ss1.zv_adr_eff_lnr=adr_nr(ii) and ss1.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								select 
									xmlforest(
										'n/a' as "institution_name",
										'n/a' as "swift",
										'n/a' as "account",
										'n/a' as "comments"
									)
								into xml_out
								from dual;
						end;
					end if;

					--address of OM_AUF.kt_lnr owner
					begin
						select 
							XMLAGG(
								XMLELEment("address",
									XMLforest(
										1 as "address_type",
										char_process(adr.strasse,'n/a',100) as "address",
										char_process(adr.ORT_ORT_BEZ,char_process(adr.ADR_ORT_BEZ,'n/a',255),255) as "city",
										char_process(adr.ADR_PLZ,NULL,10) as "zip",
										NVL(cntr.iso_land_cd,'UNK') as "country_code",
										st.MROS_value as "state"
									)
								)
							)
						into xml_a
						from 
							FA_MROS_KD_ADR adr
						left join
							FA_MROS_REFERENCE_TABLE st
						on st.Finnova_value=adr.region_cd and st.ref_type=2 and st.userbk_nr=F_BANK_ID and st.activ_MROS=1
						left join
							FA_MROS_INT_LAND cntr
						on cntr.iso_land_cd=adr.iso_land_cd
						where adr.KD_LNR=a1_kd(ii) 
							and adr.userbk_nr=F_BANK_ID;
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
							xml_a:=NULL;
					end;

					if a1_tp(ii)<3 then
						--phone of OM_AUF.kt_lnr owner
						begin
							select 
								XMLAGG(
									XMLELEment("phone",
										XMLforest(
											NVL(ct.MROS_value,'1') as "tph_contact_type",
											NVL(ct1.MROS_value,'6') as "tph_communication_type",
											char_process(ph.EMAIL_ADR,'n/a',50) as "tph_number",
											case 
												when ct.MROS_value='2' or ct1.MROS_value='4' 
												then 
													char_process(
														case
															when ct.MROS_value='2'
															then char_process(ct.comments,NULL,4000) || '; '
															else null
														end || 
														case
															when ct1.MROS_value='4'
															then char_process(ct1.comments,NULL,4000)
															else null
														end
													,'n/a',4000)
												else NULL
											end as "comments"
										)
									)
								)
							into xml_p
							from 
								FA_MROS_KD_EMAIL ph
							left join
								FA_MROS_REFERENCE_TABLE ct
							on ct.Finnova_value=ph.mail_art_cd and ct.ref_type=6 and ct.userbk_nr=F_BANK_ID and ct.activ_MROS=1
							left join
								FA_MROS_REFERENCE_TABLE ct1
							on ct1.Finnova_value=ph.mail_art_cd and ct1.ref_type=7 and ct1.userbk_nr=F_BANK_ID and ct1.activ_MROS=1
							where ph.KD_LNR=a1_kd(ii) and ph.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_p:=NULL;
						end;

						--ID of OM_AUF.kt_lnr owner
						begin
							select 
								XMLAGG(
									XMLELEment("identification",
										XMLforest(
											NVL(tp.MROS_value,'4') as "type",
											char_process(id.KD_AUSW_NR,'n/a',255) as "number",
											case 
												when id.AUSSTELL_DAT is null or id.AUSSTELL_DAT < date '1753-01-01' or id.AUSSTELL_DAT > sysdate
												then NULL 
												else to_char(id.AUSSTELL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "issue_date",
											case 
												when id.ABL_DAT is null or id.ABL_DAT<id.AUSSTELL_DAT or id.ABL_DAT < date '1753-01-01'
												then NULL 
												else to_char(id.ABL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "expiry_date",
											char_process(id.KD_VSB_BEGLAUB,NULL,255) as "issued_by", 
											NVL(cntr.iso_land_cd,'UNK') as "issue_country",
											case 
												when NVL(tp.MROS_value,'4')='4' 
												then char_process(tp.comments,'n/a',4000) 
												else NULL
											end as "comments"
										)
									)
								)
							into xml_i
							from 
								FA_MROS_KD_VSB id
							left join
								FA_MROS_REFERENCE_TABLE tp
							on tp.Finnova_value=id.iddoc_cd and tp.ref_type=5 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
							left join
								FA_MROS_INT_LAND cntr
							on cntr.iso_land_cd=id.iso_land_cd
							where id.KD_LNR=a1_kd(ii) and id.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_i:=NULL;
						end;

						--person is owner of OM_AUF.kt_lnr
						begin
							select
								XMLAGG(
									xmlelement("signatory",
										xmlforest(
											case when sd1.partner_flag=0 then 1 else Null end as "is_primary",
											xmlconcat(
												XMLFOREST(													
													NVL(gd.MROS_value,'U') as "gender",
													char_process(sd1.titel,NULL,30) as "title",
													char_process(sd1.VORNAME,'n/a',100) as "first_name",
													char_process(sd1.name_1,'n/a',100) as "last_name",
													case 
														when sd1.GEBURT_DAT is null or sd1.GEBURT_DAT < date '1753-01-01' or sd1.GEBURT_DAT > sysdate
														then 
															case 
																when sd1.GEBURT_JAHR is null or sd1.GEBURT_JAHR < 1753 or sd1.GEBURT_JAHR > extract(year from sysdate)
																then '1900-01-01T00:00:00'
																else to_char(sd1.GEBURT_JAHR) || '-01-01T00:00:00' 
															end 
														else to_char(sd1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end as "birthdate",
													char_process(sd1.GEBURT_ORT,NULL,255) as "birth_place",
													NVL(cntr1.iso_land_cd,'UNK') as "nationality1",
													cntr2.iso_land_cd as "nationality2",
													xml_p as "phones",
													NVL(xml_a,
														XMLELEment("address",
															XMLforest(
																1 as "address_type",
																'n/a' as "address",
																'n/a' as "city",
																'UNK' as "country_code"
															)
														)
													) as "addresses"
												),   
												NVL(xml_i,
													xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
												),		 
												case 
													when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
													then xmlelement("deceased",1)
													else null 
												end,
												case 
													when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
													then xmlelement("date_deceased",
														case 
															when sd1.TOD_DAT is null or sd1.TOD_DAT < date '1753-01-01' or sd1.TOD_DAT > sysdate
															then to_char(sd1.TOD_JAHR) || '-01-01T00:00:00' 
															else to_char(sd1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
														end)
													else null 
												end				
											) as "t_person",
											1 as "role"
										)
									)
								)
							into xml_t
							from
								FA_MROS_KD_STAMM sd1	
							left join
								FA_MROS_REFERENCE_TABLE gd
							on gd.Finnova_value=sd1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
							left join
								FA_MROS_INT_LAND cntr1
							on sd1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
							left join
								FA_MROS_INT_LAND cntr2
							on sd1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd
							where sd1.KD_LNR=a1_kd(ii) and sd1.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								select
									XMLAGG(
										xmlelement("signatory",
											xmlforest(
												xmlconcat(
													XMLFOREST(
														'U' as "gender",
														'n/a' as "first_name",
														'n/a' as "last_name",
														'1900-01-01T00:00:00' as "birthdate",
														'UNK' as "nationality1",
														XMLELEment("address",
															XMLforest(
																1 as "address_type",
																'n/a' as "address",
																'n/a' as "city",
																'UNK' as "country_code"
															)
														) as "addresses"
													),   
													xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
												) as "t_person",
												1 as "role"
											)
										)
									)
								into xml_t
								from dual;
						end;
					else
						--entity is owner of OM_AUF.kt_lnr
						begin
							select 
								xmlelement("t_entity",
									XMLFOREST(
										char_process(sb1.kd_bez,'n/a',255) as "name",
										NVL(lf.MROS_value,'1') as "incorporation_legal_form",
										char_process(sb1.UID_NR,NULL,50) as "incorporation_number",
										char_process(sb1.NOGA_BEZ,NULL,255) as "business",
										NVL(xml_a,
											XMLELEment("address",
												XMLforest(
													1 as "address_type",
													'n/a' as "address",
													'n/a' as "city",
													'UNK' as "country_code"
												)
											) 
										) as "addresses",
										char_process(sb1.EMAIL_ADR,NULL,255) as "url",
										char_process(sb1.GEBURT_ORT,NULL,255) as "incorporation_state",
										NVL(cntr1.iso_land_cd,'UNK') as "incorporation_country_code",
										case 
											when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
											then 
												case 
													when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR<1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
													then NULL 
													else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
												end 
											else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
										end as "incorporation_date",
										case 
											when (sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR >= 1753 and sb1.TOD_JAHR <= extract(year from sysdate))
											then 1 else NULL
										end as "business_closed",
										case 
											when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
											then 
												case 
													when sb1.TOD_JAHR is null or sb1.TOD_JAHR < 1753 or sb1.TOD_JAHR > extract(year from sysdate)
													then null 
													else to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
												end 
											else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
										end as "date_business_closed",
										NVL(dm.MROS_value,'yes') as "tax_reg_number",
										char_process(
											case 
												when lf.MROS_value='4' 
												then 
													case
														when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
														then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); ' || lf.comments
														else NVL(lf.comments,'n/a')
													end 
												else 
													case
														when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
														then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); '
														else NULL
													end 
											end
										,NULL,4000) as "comments"
									)
								)
							into xml_t
							from
								FA_MROS_KD_STAMM sb1
							left join
								FA_MROS_REFERENCE_TABLE lf
							on lf.Finnova_value=sb1.kd_rstat_cd and lf.ref_type=8 and lf.userbk_nr=F_BANK_ID and lf.activ_MROS=1
							left join
								FA_MROS_REFERENCE_TABLE dm
							on dm.Finnova_value=sb1.kd_sitz_gsch_cd and dm.ref_type=3 and dm.userbk_nr=F_BANK_ID and dm.activ_MROS=1
							left join
								FA_MROS_INT_LAND cntr1
							on sb1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
							where sb1.KD_LNR=a1_kd(ii) and sb1.userbk_nr=F_BANK_ID and sb1.partner_flag=0;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								select 
									xmlelement("t_entity",
										XMLFOREST(
											'n/a' as "name",
											1 as "incorporation_legal_form",
											XMLELEment("address",
												XMLforest(
													1 as "address_type",
													'n/a' as "address",
													'n/a' as "city",
													'UNK' as "country_code"
												)									
											) as "addresses",
											'UNK' as "incorporation_country_code",
											'yes' as "tax_reg_number"
										)
									)
								into xml_t
								from dual;
						end;
					end if;

					begin
						select saldo_bwrg,saldo_kwrg into sld_bwrg,sld_kwrg from FA_MROS_KT_SALDO
						where kt_lnr=a1_kt(ii) and userbk_nr=F_BANK_ID and rownum<=1;
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
							begin
								sld_bwrg:=NULL;
								sld_kwrg:=NULL;
							end;
					end;

					--OM_AUF.kt_lnr is always used
					begin
						select
							xmlconcat(
								XMLforest(
									inst_name as "institution_name",
									swif as "swift",
									NVL(br.MROS_value,'ZH') as "branch",
									char_process(sy1.EDIT_KT_NR,'n/a',50) as "account",
									NVL(curr.iso_wrg_cd,'CHF') as "currency_code",
									char_process(sy1.KT_ART_NR_BEZ,NULL,255) as "account_name",
									char_process(sy1.iban_id,'n/a',34) as "iban",
									char_process(a1_kdnr(ii),'n/a',30) as "client_number",
									NVL(tp.MROS_value,'16') as "personal_account_type"
								),
								xml_t,
								XMLforest(
									case 
										when sy1.EROEFF_DAT is null or sy1.EROEFF_DAT <date '1753-01-01' or sy1.EROEFF_DAT > sysdate
										then '1900-01-01T00:00:00'
										else to_char(sy1.EROEFF_DAT,'YYYY-MM-DD') || 'T00:00:00' 
									end as "opened",
									case 
										when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
										then null 
										else to_char(sy1.AUFHEB_DAT,'YYYY-MM-DD') || 'T00:00:00' 
									end as "closed",
									case 
										when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
										then replace(rtrim(to_char(NVL(sld_bwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
										else null 
									end as "balance",
									case 
										when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
										then replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')
										else null 
									end as "date_balance",				
									case 
										when sy1.SPERR_CD=97 or sy1.aufheb_cd='ZZ' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
										then 2 else 1 
									end as "status_code",
									case 
										when sy1.iso_wrg_cd='CHF' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
										then NULL 
										else replace(rtrim(to_char(NVL(sld_kwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
									end as "beneficiary",
									char_process(sy1.SITZ_NR_BEZ,'n/a',255) as "beneficiary_comment",
									char_process(
										case 
											when tp.MROS_value='14' 
											then  
												case
													when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
													then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); ' || tp.comments
													else NVL(tp.comments,'n/a')
												end
											else
												case
													when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
													then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); '
													else NULL
												end 
										end 
									,NULL,4000) as "comments"
								)				   
							)
						into xml_om
						from
							FA_MROS_KT_STAMM sy1
						left join
							FA_MROS_REFERENCE_TABLE br
						on sy1.sitz_nr=br.Finnova_value and br.ref_type=1 and br.userbk_nr=F_BANK_ID and br.activ_MROS=1
						left join
							FA_MROS_REFERENCE_TABLE tp
						on sy1.kt_art_ber_cd=tp.Finnova_value and tp.ref_type=9 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
						left join
							FA_MROS_INT_WRG curr
						on curr.iso_wrg_cd=sy1.iso_wrg_cd 
						where sy1.KT_LNR=a1_kt(ii) and sy1.userbk_nr=F_BANK_ID;
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
							select
								xmlconcat(
									XMLforest(
										'n/a' as "institution_name",
										'n/a' as "swift",
										'ZH' as "branch",
										'n/a' as "account",
										'CHF' as "currency_code",
										'n/a' as "iban",
										'n/a' as "client_number",
										16 as "personal_account_type"
									),
									xml_t,
									XMLforest(
										'1900-01-01T00:00:00' as "opened",
										1 as "status_code"
									)				   
								)
							into xml_om
							from dual;
					end;

					if tp(ii)=2 or tp(ii)=3 then
						--address of ZV_ZLG_T.kt_lnr_b owner
						begin
							select 
								XMLAGG(
									XMLELEment("address",
										XMLforest(
											1 as "address_type",
											char_process(adr.strasse,'n/a',100) as "address",
											char_process(adr.ORT_ORT_BEZ,char_process(adr.ADR_ORT_BEZ,'n/a',255),255) as "city",
											char_process(adr.ADR_PLZ,NULL,10) as "zip",
											NVL(cntr.iso_land_cd,'UNK') as "country_code",
											st.MROS_value as "state"
										)
									)
								)
							into xml_a
							from 
								FA_MROS_KD_ADR adr
							left join
								FA_MROS_REFERENCE_TABLE st
							on st.Finnova_value=adr.region_cd and st.ref_type=2 and st.userbk_nr=F_BANK_ID and st.activ_MROS=1
							left join
								FA_MROS_INT_LAND cntr
							on cntr.iso_land_cd=adr.iso_land_cd
							where adr.KD_LNR=a2_kd(ii) 
								and adr.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_a:=NULL;
						end;
						
						if a2_tp(ii)<3 then
							--phone of ZV_ZLG_T.kt_lnr_b owner
							begin
								select 
									XMLAGG(
										XMLELEment("phone",
											XMLforest(
												NVL(ct.MROS_value,'1') as "tph_contact_type",
												NVL(ct1.MROS_value,'6') as "tph_communication_type",
												char_process(ph.EMAIL_ADR,'n/a',50) as "tph_number",
												case 
													when ct.MROS_value='2' or ct1.MROS_value='4' 
													then 
														char_process(
															case
																when ct.MROS_value='2'
																then char_process(ct.comments,NULL,4000) || '; '
																else null
															end || 
															case
																when ct1.MROS_value='4'
																then char_process(ct1.comments,NULL,4000)
																else null
															end
														,'n/a',4000)
													else NULL
												end as "comments"
											)
										)
									)
								into xml_p
								from 
									FA_MROS_KD_EMAIL ph
								left join
									FA_MROS_REFERENCE_TABLE ct
								on ct.Finnova_value=ph.mail_art_cd and ct.ref_type=6 and ct.userbk_nr=F_BANK_ID and ct.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE ct1
								on ct1.Finnova_value=ph.mail_art_cd and ct1.ref_type=7 and ct1.userbk_nr=F_BANK_ID and ct1.activ_MROS=1
								where ph.KD_LNR=a2_kd(ii) and ph.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_p:=NULL;
							end;

							--ID of ZV_ZLG_T.kt_lnr_b owner
							begin
								select 
									XMLAGG(
										XMLELEment("identification",
											XMLforest(
												NVL(tp.MROS_value,'4') as "type",
												char_process(id.KD_AUSW_NR,'n/a',255) as "number",
												case 
													when id.AUSSTELL_DAT is null or id.AUSSTELL_DAT < date '1753-01-01' or id.AUSSTELL_DAT > sysdate
													then NULL 
													else to_char(id.AUSSTELL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "issue_date",
												case 
													when id.ABL_DAT is null or id.ABL_DAT<id.AUSSTELL_DAT or id.ABL_DAT < date '1753-01-01'
													then NULL 
													else to_char(id.ABL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "expiry_date",
												char_process(id.KD_VSB_BEGLAUB,NULL,255) as "issued_by", 
												NVL(cntr.iso_land_cd,'UNK') as "issue_country",
												case 
													when NVL(tp.MROS_value,'4')='4' 
													then char_process(tp.comments,'n/a',4000) 
													else NULL
												end as "comments"
											)
										)
									)
								into xml_i
								from 
									FA_MROS_KD_VSB id
								left join
									FA_MROS_REFERENCE_TABLE tp
								on tp.Finnova_value=id.iddoc_cd and tp.ref_type=5 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr
								on cntr.iso_land_cd=id.iso_land_cd
								where id.KD_LNR=a2_kd(ii) and id.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_i:=NULL;
							end;

							--person is owner of ZV_ZLG_T.kt_lnr_b
							begin
								select
									XMLAGG(
										xmlelement("signatory",
											xmlforest(
												case when sd1.partner_flag=0 then 1 else Null end as "is_primary",
												xmlconcat(
													XMLFOREST(														
														NVL(gd.MROS_value,'U') as "gender",
														char_process(sd1.titel,NULL,30) as "title",
														char_process(sd1.VORNAME,'n/a',100) as "first_name",
														char_process(sd1.name_1,'n/a',100) as "last_name",
														case 
															when sd1.GEBURT_DAT is null or sd1.GEBURT_DAT < date '1753-01-01' or sd1.GEBURT_DAT > sysdate
															then 
																case 
																	when sd1.GEBURT_JAHR is null or sd1.GEBURT_JAHR < 1753 or sd1.GEBURT_JAHR > extract(year from sysdate)
																	then '1900-01-01T00:00:00'
																	else to_char(sd1.GEBURT_JAHR) || '-01-01T00:00:00' 
																end 
															else to_char(sd1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
														end as "birthdate",
														char_process(sd1.GEBURT_ORT,NULL,255) as "birth_place",
														NVL(cntr1.iso_land_cd,'UNK') as "nationality1",
														cntr2.iso_land_cd as "nationality2",
														xml_p as "phones",
														NVL(xml_a,
															XMLELEment("address",
																XMLforest(
																	1 as "address_type",
																	'n/a' as "address",
																	'n/a' as "city",
																	'UNK' as "country_code"
																)
															)
														) as "addresses"
													),   
													NVL(xml_i,
														xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
													),		 
													case 
														when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
														then xmlelement("deceased",1)
														else null 
													end,
													case 
														when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
														then xmlelement("date_deceased",
															case 
																when sd1.TOD_DAT is null or sd1.TOD_DAT < date '1753-01-01' or sd1.TOD_DAT > sysdate
																then to_char(sd1.TOD_JAHR) || '-01-01T00:00:00' 
																else to_char(sd1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
															end)
														else null 
													end				
												) as "t_person",
												1 as "role"
											)
										)
									)
								into xml_t
								from
									FA_MROS_KD_STAMM sd1		
								left join
									FA_MROS_REFERENCE_TABLE gd
								on gd.Finnova_value=sd1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr1
								on sd1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd	
								left join
									FA_MROS_INT_LAND cntr2
								on sd1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd	
								where sd1.KD_LNR=a2_kd(ii) and sd1.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									select
										XMLAGG(
											xmlelement("signatory",
												xmlforest(
													xmlconcat(
														XMLFOREST(
															'U' as "gender",
															'n/a' as "first_name",
															'n/a' as "last_name",
															'1900-01-01T00:00:00' as "birthdate",
															'UNK' as "nationality1",
															XMLELEment("address",
																XMLforest(
																	1 as "address_type",
																	'n/a' as "address",
																	'n/a' as "city",
																	'UNK' as "country_code"
																)
															) as "addresses"
														),   
														xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
													) as "t_person",
													1 as "role"
												)
											)
										)
									into xml_t
									from dual;
							end;
						else
							--entity is owner of ZV_ZLG_T.kt_lnr_b
							begin
								select 
									xmlelement("t_entity",
										XMLFOREST(
											char_process(sb1.kd_bez,'n/a',255) as "name",
											NVL(lf.MROS_value,'1') as "incorporation_legal_form",
											char_process(sb1.UID_NR,NULL,50) as "incorporation_number",
											char_process(sb1.NOGA_BEZ,NULL,255) as "business",
											NVL(xml_a,
												XMLELEment("address",
													XMLforest(
														1 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													)
												) 
											) as "addresses",
											char_process(sb1.EMAIL_ADR,NULL,255) as "url",
											char_process(sb1.GEBURT_ORT,NULL,255) as "incorporation_state",
											NVL(cntr1.iso_land_cd,'UNK') as "incorporation_country_code",
											case 
												when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
												then 
													case 
														when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR<1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
														then NULL 
														else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
													end 
												else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "incorporation_date",
											case 
												when (sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR >= 1753 and sb1.TOD_JAHR <= extract(year from sysdate))
												then 1 
												else NULL
											end as "business_closed",
											case 
												when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
												then 
													case 
														when sb1.TOD_JAHR is null or sb1.TOD_JAHR < 1753 or sb1.TOD_JAHR > extract(year from sysdate)
														then null 
														else to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
													end 
												else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "date_business_closed",
											NVL(dm.MROS_value,'yes') as "tax_reg_number",
											char_process(
												case 
													when lf.MROS_value='4' 
													then 
														case
															when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
															then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); ' || lf.comments
															else NVL(lf.comments,'n/a')
														end 
													else 
														case
															when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
															then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); '
															else NULL
														end 
												end 
											,NULL,4000) as "comments"
										)
									)
								into xml_t
								from
									FA_MROS_KD_STAMM sb1	
								left join
									FA_MROS_REFERENCE_TABLE lf
								on lf.Finnova_value=sb1.kd_rstat_cd and lf.ref_type=8 and lf.userbk_nr=F_BANK_ID and lf.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE dm
								on dm.Finnova_value=sb1.kd_sitz_gsch_cd and dm.ref_type=3 and dm.userbk_nr=F_BANK_ID and dm.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr1
								on sb1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd					
								where sb1.KD_LNR=a2_kd(ii) and sb1.userbk_nr=F_BANK_ID and sb1.partner_flag=0;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									select 
										xmlelement("t_entity",
											XMLFOREST(
												'n/a' as "name",
												1 as "incorporation_legal_form",
												XMLELEment("address",
													XMLforest(
														1 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													)									
												) as "addresses",
												'UNK' as "incorporation_country_code",
												'yes' as "tax_reg_number"
											)
										)
									into xml_t
									from dual;
							end;
						end if;

						begin
							select saldo_bwrg,saldo_kwrg into sld_bwrg,sld_kwrg from FA_MROS_KT_SALDO
							where kt_lnr=a1_kt(ii) and userbk_nr=F_BANK_ID and rownum<=1;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								begin
									sld_bwrg:=NULL;
									sld_kwrg:=NULL;
								end;
						end;

						--ZV_ZLG_T.kt_lnr_b is used in internal transactions only
						begin
							select
								xmlconcat(
									XMLforest(
										inst_name as "institution_name",
										swif as "swift",
										NVL(br.MROS_value,'ZH') as "branch",
										char_process(sy1.EDIT_KT_NR,'n/a',50) as "account",
										NVL(curr.iso_wrg_cd,'CHF') as "currency_code",
										char_process(sy1.KT_ART_NR_BEZ,NULL,255) as "account_name",
										char_process(sy1.iban_id,'n/a',34) as "iban",
										char_process(a2_kdnr(ii),'n/a',30) as "client_number",
										NVL(tp.MROS_value,'16') as "personal_account_type"
									),
									xml_t,
									XMLforest(
										case 
											when sy1.EROEFF_DAT is null or sy1.EROEFF_DAT <date '1753-01-01' or sy1.EROEFF_DAT > sysdate
											then '1900-01-01T00:00:00'
											else to_char(sy1.EROEFF_DAT,'YYYY-MM-DD') || 'T00:00:00' 
										end as "opened",
										case 
											when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
											then null 
											else to_char(sy1.AUFHEB_DAT,'YYYY-MM-DD') || 'T00:00:00' 
										end as "closed",
										case 
											when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
											then replace(rtrim(to_char(NVL(sld_bwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
											else null 
										end as "balance",
										case 
											when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
											then replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')
											else null 
										end as "date_balance",	
										case 
											when sy1.SPERR_CD=97 or sy1.aufheb_cd='ZZ' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
											then 2 else 1 
										end as "status_code",
										case 
											when sy1.iso_wrg_cd='CHF' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
											then NULL 
											else replace(rtrim(to_char(NVL(sld_kwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
										end as "beneficiary",
										char_process(sy1.SITZ_NR_BEZ,'n/a',255) as "beneficiary_comment",
										char_process(
											case 
												when tp.MROS_value='14' 
												then  
													case
														when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
														then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); ' || tp.comments
														else NVL(tp.comments,'n/a')
													end
												else
													case
														when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
														then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); '
														else NULL
													end 
											end 
										,NULL,4000) as "comments"
									)				   
								)
							into xml_zv
							from
								FA_MROS_KT_STAMM sy1
							left join
								FA_MROS_REFERENCE_TABLE br
							on sy1.sitz_nr=br.Finnova_value and br.ref_type=1 and br.userbk_nr=F_BANK_ID and br.activ_MROS=1
							left join
								FA_MROS_REFERENCE_TABLE tp
							on sy1.kt_art_ber_cd=tp.Finnova_value and tp.ref_type=9 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
							left join
								FA_MROS_INT_WRG curr
							on curr.iso_wrg_cd=sy1.iso_wrg_cd 
							where sy1.KT_LNR=a2_kt(ii) and sy1.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								select
									xmlconcat(
										XMLforest(
											'n/a' as "institution_name",
											'n/a' as "swift",
											'ZH' as "branch",
											'n/a' as "account",
											'CHF' as "currency_code",
											'n/a' as "iban",
											'n/a' as "client_number",
											16 as "personal_account_type"
										),
										xml_t,
										XMLforest(
											'1900-01-01T00:00:00' as "opened",
											1 as "status_code"
										)				   
									)
								into xml_om
								from dual;
						end;
					end if;

					--1 Input transaction (the recipient is OM_AUF.kt_lnr)
					if tp(ii) = 1 then
						begin
							select 
								xmlelement("transaction",
									xmlconcat(
										xmlelement("transactionnumber",s3.ZV_ZLG_SYS_LNR),
										xmlelement("internal_ref_number",char_process(s3.AUF_ID,NULL,50)),
										case when s3.SITZ_NR_AUSF is null then 
											case tr_tp.MROS_value
												when '1' then xmlelement("transaction_location",'n/a')
												when '2' then xmlelement("transaction_location",'n/a')
												else null 
											end
										else
											xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR_AUSF) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
										end,
										xmlelement("transaction_description",
											char_process(
												case 
													when char_process(s3.GF_ART_BEZ,NULL,0) is not null
													then 'Transaction type: ' || char_process(s3.GF_ART_BEZ,NULL,4000) || '; '
													else NULL
												end ||
												case
													when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null
													then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000)
													else NULL
												end
											,'n/a',4000)),
										xmlelement("date_transaction",
											case 
												when 
													s3.ausf_dt_ist is not null and (s3.ausf_dt_soll is null or s3.ausf_dt_ist<=s3.ausf_dt_soll) and (s3.AUF_DT_ERT is null or s3.ausf_dt_ist<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_ist,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.ausf_dt_soll is not null and (s3.ausf_dt_ist is null or s3.ausf_dt_soll<=s3.ausf_dt_ist) and (s3.AUF_DT_ERT is null or s3.ausf_dt_soll<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_soll,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.AUF_DT_ERT is not null and (s3.ausf_dt_soll is null or s3.AUF_DT_ERT<=s3.ausf_dt_soll) and (s3.ausf_dt_ist is null or s3.AUF_DT_ERT<=s3.ausf_dt_ist) 
												then replace(to_char(s3.AUF_DT_ERT,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												else replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')	
											end),
										xmlelement("value_date",replace(to_char(s3.VALUTA,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
										xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')),
										xmlelement("transmode_comment",
											case 
												when tr_tp.MROS_value=13 
												then char_process(tr_tp.comments,'n/a',50)
												else NULL
											end), 
										xmlelement("amount_local",
											replace(rtrim(to_char(
												case
													when tr_tp.MROS_value=16 then 0
													else 
														case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
												end
											, 'fm9999999999999990d999'), ',.'),',','.')
										),
										xmlelement("t_from",
											XMLconcat(
												xmlelement("from_funds_code",1),
												xmlelement("from_account",xml_in)						
											)
										),
										xmlelement("t_to_my_client",
											XMLconcat(
												xmlelement("to_funds_code",1),
												xmlelement("to_foreign_currency",
													XMLforest(
														NVL(curr.iso_wrg_cd,'CHF') as "foreign_currency_code",
														replace(rtrim(to_char(abs(NVL(s3.BETRAG_ZWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
														1 as "foreign_exchange_rate"
													)
												),
												xmlelement("to_account",xml_om)
											)
										),
										case 
											when s3.betrag_bwrg<0 then xmlelement("comments",'It is a return operation. ') 
											else NULL
										end
									)
								)
							into xml_t
							from
								FA_MROS_ZV_ZLG_T s3
							join
								FA_MROS_REFERENCE_TABLE tr_tp
							on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
							left join
								FA_MROS_INT_WRG curr
							on curr.iso_wrg_cd=s3.iso_wrg_cd 
							where s3.ZV_ZLG_SYS_LNR=pm_id(ii) and s3.userbk_nr=F_BANK_ID;	
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_t:=NULL;
						end;
					end if;

					--2 Internal transaction (the recipient is OM_AUF.kt_lnr)
					if tp(ii) = 2 then
						begin
							select 
								xmlelement("transaction",
									xmlconcat(
										xmlelement("transactionnumber",s3.ZV_ZLG_SYS_LNR),
										xmlelement("internal_ref_number",char_process(s3.AUF_ID,NULL,50)),
										case when s3.SITZ_NR_AUSF is null then 
											case tr_tp.MROS_value
												when '1' then xmlelement("transaction_location",'n/a')
												when '2' then xmlelement("transaction_location",'n/a')
												else null 
											end
										else
											xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR_AUSF) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
										end,
										xmlelement("transaction_description",
											char_process(
												case 
													when char_process(s3.GF_ART_BEZ,NULL,0) is not null
													then 'Transaction type: ' || char_process(s3.GF_ART_BEZ,NULL,4000) || '; '
													else NULL
												end ||
												case
													when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null
													then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000)
													else NULL
												end
											,'n/a',4000)),
										xmlelement("date_transaction",
											case 
												when 
													s3.ausf_dt_ist is not null and (s3.ausf_dt_soll is null or s3.ausf_dt_ist<=s3.ausf_dt_soll) and (s3.AUF_DT_ERT is null or s3.ausf_dt_ist<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_ist,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.ausf_dt_soll is not null and (s3.ausf_dt_ist is null or s3.ausf_dt_soll<=s3.ausf_dt_ist) and (s3.AUF_DT_ERT is null or s3.ausf_dt_soll<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_soll,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.AUF_DT_ERT is not null and (s3.ausf_dt_soll is null or s3.AUF_DT_ERT<=s3.ausf_dt_soll) and (s3.ausf_dt_ist is null or s3.AUF_DT_ERT<=s3.ausf_dt_ist) 
												then replace(to_char(s3.AUF_DT_ERT,'YYYY-MM-DD HH24:MI:SS'),' ','T')
											else replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')	
											end),
										xmlelement("value_date",replace(to_char(s3.VALUTA,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
										xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
										xmlelement("transmode_comment",
											case 
												when tr_tp.MROS_value=13 
												then char_process(tr_tp.comments,'n/a',50)
												else NULL
											end), 
										xmlelement("amount_local",
											replace(rtrim(to_char(
												case
													when tr_tp.MROS_value=16 then 0
													else
														case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
												end
											, 'fm9999999999999990d999'), ',.'),',','.')
										),
										xmlelement("t_from_my_client",
											XMLconcat(
												xmlelement("from_funds_code",1),
												xmlelement("from_foreign_currency",
													XMLforest(
														NVL(curr.iso_wrg_cd,'CHF') as "foreign_currency_code",
														replace(rtrim(to_char(abs(NVL(s3.BETRAG_ZWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
														1 as "foreign_exchange_rate"
													)
												),
												xmlelement("from_account",xml_zv)
											)
										),
										xmlelement("t_to_my_client",
											XMLconcat(
												xmlelement("to_funds_code",1),
												xmlelement("to_foreign_currency",
													XMLforest(
														NVL(curr.iso_wrg_cd,'CHF') as "foreign_currency_code",
														replace(rtrim(to_char(abs(NVL(s3.BETRAG_ZWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
														1 as "foreign_exchange_rate"
													)
												),
												xmlelement("to_account",xml_om)
											)
										),
										case 
											when s3.betrag_bwrg<0 then xmlelement("comments",'It is a return operation. ') 
											else NULL
										end
									)
								)
							into xml_t
							from
								FA_MROS_ZV_ZLG_T s3 
							join
								FA_MROS_REFERENCE_TABLE tr_tp
							on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
							left join
								FA_MROS_INT_WRG curr
							on curr.iso_wrg_cd=s3.iso_wrg_cd 
							where s3.ZV_ZLG_SYS_LNR=pm_id(ii) and s3.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_t:=NULL;
						end;	
					end if;

					--3 Internal transaction (the sender is OM_AUF.kt_lnr)
					if tp(ii) = 3 then
						begin
							select 
								xmlelement("transaction",
									xmlconcat(
										xmlelement("transactionnumber",s3.ZV_ZLG_SYS_LNR),
										xmlelement("internal_ref_number",char_process(s3.AUF_ID,NULL,50)),
										case when s3.SITZ_NR_AUSF is null then 
											case tr_tp.MROS_value
												when '1' then xmlelement("transaction_location",'n/a')
												when '2' then xmlelement("transaction_location",'n/a')
												else null 
											end
										else
											xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR_AUSF) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
										end,
										xmlelement("transaction_description",
											char_process(
												case 
													when char_process(s3.GF_ART_BEZ,NULL,0) is not null
													then 'Transaction type: ' || char_process(s3.GF_ART_BEZ,NULL,4000) || '; '
													else NULL
												end ||
												case
													when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null
													then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000)
													else NULL
												end
											,'n/a',4000)),
										xmlelement("date_transaction",
											case 
												when 
													s3.ausf_dt_ist is not null and (s3.ausf_dt_soll is null or s3.ausf_dt_ist<=s3.ausf_dt_soll) and (s3.AUF_DT_ERT is null or s3.ausf_dt_ist<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_ist,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.ausf_dt_soll is not null and (s3.ausf_dt_ist is null or s3.ausf_dt_soll<=s3.ausf_dt_ist) and (s3.AUF_DT_ERT is null or s3.ausf_dt_soll<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_soll,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.AUF_DT_ERT is not null and (s3.ausf_dt_soll is null or s3.AUF_DT_ERT<=s3.ausf_dt_soll) and (s3.ausf_dt_ist is null or s3.AUF_DT_ERT<=s3.ausf_dt_ist) 
												then replace(to_char(s3.AUF_DT_ERT,'YYYY-MM-DD HH24:MI:SS'),' ','T')
											else replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')	
											end),
										xmlelement("value_date",replace(to_char(s3.VALUTA,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
										xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
										xmlelement("transmode_comment",
											case 
												when tr_tp.MROS_value=13 
												then char_process(tr_tp.comments,'n/a',50)
												else NULL
											end), 
										xmlelement("amount_local",
											replace(rtrim(to_char(
												case
													when tr_tp.MROS_value=16 then 0
													else
														case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
												end
											, 'fm9999999999999990d999'), ',.'),',','.')
										),
										xmlelement("t_from_my_client",
											XMLconcat(
												xmlelement("from_funds_code",1),
												xmlelement("from_foreign_currency",
													XMLforest(
														NVL(curr.iso_wrg_cd,'CHF') as "foreign_currency_code",
														replace(rtrim(to_char(abs(NVL(s3.BETRAG_ZWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
														1 as "foreign_exchange_rate"
													)
												),
												xmlelement("from_account",xml_om)
											)
										),
										xmlelement("t_to_my_client",
											XMLconcat(
												xmlelement("to_funds_code",1),
												xmlelement("to_foreign_currency",
													XMLforest(
														NVL(curr.iso_wrg_cd,'CHF') as "foreign_currency_code",
														replace(rtrim(to_char(abs(NVL(s3.BETRAG_ZWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
														1 as "foreign_exchange_rate"
													)
												),
												xmlelement("to_account",xml_zv)
											)
										),
										case 
											when s3.betrag_bwrg<0 then xmlelement("comments",'It is a return operation. ') 
											else NULL
										end
									)
								)
							into xml_t
							from
								FA_MROS_ZV_ZLG_T s3 
							join
								FA_MROS_REFERENCE_TABLE tr_tp
							on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
							left join
								FA_MROS_INT_WRG curr
							on curr.iso_wrg_cd=s3.iso_wrg_cd 
							where s3.ZV_ZLG_SYS_LNR=pm_id(ii) and s3.userbk_nr=F_BANK_ID;	
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_t:=NULL;
						end;
					end if;

					--4 Outgoing transaction (the sender is OM_AUF.kt_lnr)
					if tp(ii) = 4 then
						begin
							select 
								xmlelement("transaction",
									xmlconcat(
										xmlelement("transactionnumber",s3.ZV_ZLG_SYS_LNR),
										xmlelement("internal_ref_number",char_process(s3.AUF_ID,NULL,50)),
										case when s3.SITZ_NR_AUSF is null then 
											case tr_tp.MROS_value
												when '1' then xmlelement("transaction_location",'n/a')
												when '2' then xmlelement("transaction_location",'n/a')
												else null 
											end
										else
											xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR_AUSF) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
										end,
										xmlelement("transaction_description",
											char_process(
												case 
													when char_process(s3.GF_ART_BEZ,NULL,0) is not null
													then 'Transaction type: ' || char_process(s3.GF_ART_BEZ,NULL,4000) || '; '
													else NULL
												end ||
												case
													when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null
													then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000)
													else NULL
												end
											,'n/a',4000)),
										xmlelement("date_transaction",
											case 
												when 
													s3.ausf_dt_ist is not null and (s3.ausf_dt_soll is null or s3.ausf_dt_ist<=s3.ausf_dt_soll) and (s3.AUF_DT_ERT is null or s3.ausf_dt_ist<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_ist,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.ausf_dt_soll is not null and (s3.ausf_dt_ist is null or s3.ausf_dt_soll<=s3.ausf_dt_ist) and (s3.AUF_DT_ERT is null or s3.ausf_dt_soll<=s3.AUF_DT_ERT) 
												then replace(to_char(s3.ausf_dt_soll,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												when 
													s3.AUF_DT_ERT is not null and (s3.ausf_dt_soll is null or s3.AUF_DT_ERT<=s3.ausf_dt_soll) and (s3.ausf_dt_ist is null or s3.AUF_DT_ERT<=s3.ausf_dt_ist) 
												then replace(to_char(s3.AUF_DT_ERT,'YYYY-MM-DD HH24:MI:SS'),' ','T')
											else replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')	
											end),
										xmlelement("value_date",replace(to_char(s3.VALUTA,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
										xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
										xmlelement("transmode_comment",
											case 
												when tr_tp.MROS_value=13 
												then char_process(tr_tp.comments,'n/a',50)
												else NULL
											end), 
										xmlelement("amount_local",
											replace(rtrim(to_char(
												case
													when tr_tp.MROS_value=16 then 0
													else
														case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
												end
											, 'fm9999999999999990d999'), ',.'),',','.')
										),
										xmlelement("t_from_my_client",
											XMLconcat(
												xmlelement("from_funds_code",1),
												xmlelement("from_foreign_currency",
													XMLforest(
														NVL(curr.iso_wrg_cd,'CHF') as "foreign_currency_code",
														replace(rtrim(to_char(abs(NVL(s3.BETRAG_ZWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
														1 as "foreign_exchange_rate"
													)
												),
												xmlelement("from_account",xml_om)
											)
										),
										xmlelement("t_to",
											XMLconcat(
												xmlelement("to_funds_code",1),
												xmlelement("to_account",xml_out)						
											)
										),
										case 
											when s3.betrag_bwrg<0 then xmlelement("comments",'It is a return operation. ') 
											else NULL
										end
									)
								)
							into xml_t
							from
								FA_MROS_ZV_ZLG_T s3 
							join
								FA_MROS_REFERENCE_TABLE tr_tp
							on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
							left join
								FA_MROS_INT_WRG curr
							on curr.iso_wrg_cd=s3.iso_wrg_cd 
							where s3.ZV_ZLG_SYS_LNR=pm_id(ii) and s3.userbk_nr=F_BANK_ID;	
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_t:=NULL;
						end;
					end if;

					select xmlconcat(xml_r,xml_t) into xml_r from dual;
				end if;
			end loop;
		else
			xml_r := NULL;
		end if;

		return(xml_r);
	end;



	------------------------------------------------------------------------------------------------------------------


	--caption part of report (technical function)
	function head_part_of_report (
		F_BANK_ID in number,
		RP_NAME in varchar2,
		RP_SURNAME in varchar2,
		RP_PHONE in varchar2,
		RP_EMAIL in varchar2,
		RP_OCCUPATION in varchar2,
		RP_ADDRESS in varchar2,
		RP_CITY in varchar2,
		FIU_REF_NUMBER in varchar2,
		ENTITY_REFERENCE in varchar2,	
		REPORT_REASON in varchar2,
		R_ACTION in varchar2,
		REPORT_INDICATORS in FAF_MROS_charID_t,
		REPORT xmltype,
		STR_FLAG int
	) 
		return xmltype
	is
		xml_r xmltype;
	begin
		begin
			select 
				xmlelement("report",
					xmlconcat(
						xmlelement("rentity_id",s1.rentity_id),
						xmlelement("submission_code",'E'),
						xmlelement("report_code",
							case
								when STR_FLAG=1 then 'STR'
								else 'SAR'
							end),
						xmlelement("entity_reference",char_process(ENTITY_REFERENCE,'n/a',255)),
						case 
							when char_process(FIU_REF_NUMBER,NULL,0) is not null 
							then xmlelement("fiu_ref_number",char_process(FIU_REF_NUMBER,NULL,255))
							else NULL
						end,
						xmlelement("submission_date",replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
						xmlelement("currency_code_local",'CHF'),
						xmlelement("reporting_person",
							xmlforest(
								case 
									when char_process(RP_NAME,NULL,0) is not null and char_process(RP_SURNAME,NULL,0) is not null 
									then char_process(RP_NAME,'n/a',100)
									else char_process(s1.first_name,'n/a',100)
								end as "first_name",
								case 
									when char_process(RP_NAME,NULL,0) is not null and char_process(RP_SURNAME,NULL,0) is not null 
									then char_process(RP_SURNAME,'n/a',100)
									else char_process(s1.last_name,'n/a',100)
								end as "last_name",
								xmlforest(
									case 
										when char_process(RP_PHONE,null,0) is not null 
										then 
											xmlforest(
												4 as "tph_contact_type",
												1 as "tph_communication_type",
												char_process(RP_PHONE,'n/a',50) as "tph_number"
											)
										else NULL 
									end as "phone",
									case 
										when char_process(RP_EMAIL,null,0) is not null 
										then 
											xmlforest(
												4 as "tph_contact_type",
												3 as "tph_communication_type",
												char_process(RP_EMAIL,'n/a',50) as "tph_number"
											)
										else NULL 
									end as "phone",
									case 
										when char_process(RP_EMAIL,null,0) is null and char_process(RP_PHONE,null,0) is null 
										then 
											xmlforest(
												4 as "tph_contact_type",
												NVL(s1.tph_communication_type,6) as "tph_communication_type",
												char_process(s1.tph_number,'n/a',50) as "tph_number"
											)
										else NULL 
									end as "phone"
								) as "phones",
								case 
									when char_process(RP_ADDRESS,NULL,0) is not NULL and char_process(RP_CITY,NULL,0) is not NULL 
									then 
										xmlelement("address",
											xmlforest(
												4 as "address_type",
												char_process(RP_ADDRESS,'n/a',100) as "address",
												char_process(RP_CITY,'n/a',255) as "city",
												'CH' as "country_code"
											)
										)
									else NULL
								end as "addresses",
								char_process(RP_OCCUPATION,NULL,255) as "occupation"
							)
						),
						xmlelement("location",
							xmlforest(
								4 as "address_type",
								char_process(s1.address,'n/a',100) as "address",
								char_process(s1.city,'n/a',255) as "city",
								'CH' as "country_code"
							)
						),
						xmlelement("reason",char_process(REPORT_REASON,'n/a',8000)),
						xmlelement("action",char_process(R_ACTION,'n/a',8000)),
						REPORT,
						xmlelement("report_indicators",NVL(s2.ind,xmlconcat(xmlelement("indicator",'0001M'),xmlelement("indicator",'1004V'),xmlelement("indicator",'2012G'))))
					)
				)  
			into xml_r
			from 
				(select * from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID) s1
			left join
				(select xmlagg(xmlelement("indicator",s.chID)) as ind from table(REPORT_INDICATORS) s) s2
			on 1=1;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
				select xmlelement("mros_report_error",'The table FA_MROS_INT_PERS_CHARGE does not have the record with userbk_nr_home = ' || F_BANK_ID || '; ') into xml_r from dual;
		end;

		return(xml_r);
	end;


	----------------------------------------------------------------------------------------------------------------------------------


	--report with payments (STR)
	function set_of_payments (
		F_BANK_ID in number default 949,
		RP_NAME in varchar2 default NULL,
		RP_SURNAME in varchar2 default NULL,
		RP_PHONE in varchar2 default NULL,
		RP_EMAIL in varchar2 default NULL,
		RP_OCCUPATION in varchar2 default NULL,
		RP_ADDRESS in varchar2 default NULL,
		RP_CITY in varchar2 default NULL,
		FIU_REF_NUMBER in varchar2 default NULL,
		ENTITY_REFERENCE in varchar2 default 'n/a',	
		REPORT_REASON in varchar2 default 'n/a',
		R_ACTION in varchar2 default 'n/a',
		REPORT_INDICATORS in FAF_MROS_charID_t default NULL,
		SET_OF_PAYMENT_ID in FAF_MROS_numID_t default NULL,
		SET_OF_ORDER_NR in FAF_MROS_charID_t default NULL
	) 
		return xmltype
	is
		xml_t xmltype default NULL;
		xml_r xmltype default NULL;
		n number;
		ind FAF_MROS_charID_t;
		str varchar(4000);
	begin
		--indicators checking
		select FAF_MROS_charID(indicator) 
			bulk collect into ind 
		from FA_MROS_INT_INDIZ where indicator in (select trim(chID) from table(REPORT_INDICATORS));
		select count(*) into n from table(ind) where substr(chID,-1)='M';
		if n<>1 then
			str:= 'the report must include only one indicator with type Report_Type (M)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='V';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Predicate_Offenses (V)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='G';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Reason (G)';	
		end if;	
		if length(str)>0 then 
			str:= 'Indicators error: ' || str || '; ';
		end if;
		
		--table FA_MROS_INT_PERS_CHARGE checking
		select count(*) into n from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID;
		if n=0 then 
			str:= str || 'The table FA_MROS_INT_PERS_CHARGE does not have the record with userbk_nr_home = ' || F_BANK_ID || '; ';
		end if;

		--error processing
		if length(str)>0 then 
			select xmlelement("mros_report_error",str) into xml_r from dual;
		end if;

		if xml_r is null then
			select trans_node_from_payments(F_BANK_ID,SET_OF_PAYMENT_ID,SET_OF_ORDER_NR) into xml_t from dual;

			if xml_t is not null then 
				--remain part of the report
				select head_part_of_report(F_BANK_ID,RP_NAME,RP_SURNAME,RP_PHONE,RP_EMAIL,RP_OCCUPATION,RP_ADDRESS,RP_CITY,FIU_REF_NUMBER,ENTITY_REFERENCE,REPORT_REASON,R_ACTION,ind,xml_t,1) into xml_r from dual;
			else
				select xmlelement("mros_report_error",'There are no transactions that meet the conditions; ') into xml_r from dual;
			end if;
		end if;

		return(xml_r);
	end;


	---------------------------------------------------------------------------------------------------------------------------------------------



	--transactions report from ktwbuch_det (transaction number collection)
	function booking_report (
		F_BANK_ID in number default 949,
		RP_NAME in varchar2 default NULL,
		RP_SURNAME in varchar2 default NULL,
		RP_PHONE in varchar2 default NULL,
		RP_EMAIL in varchar2 default NULL,
		RP_OCCUPATION in varchar2 default NULL,
		RP_ADDRESS in varchar2 default NULL,
		RP_CITY in varchar2 default NULL,
		FIU_REF_NUMBER in varchar2 default NULL,
		ENTITY_REFERENCE in varchar2 default 'n/a',	
		REPORT_REASON in varchar2 default 'n/a',
		R_ACTION in varchar2 default 'n/a',
		REPORT_INDICATORS in FAF_MROS_charID_t default NULL,
		SET_OF_TR_ID in FAF_MROS_numID_t default NULL,
		AC_NR in varchar2 default NULL
	) 
		return xmltype
	is
		TYPE NumTab IS TABLE OF number;
		TYPE ChTab IS TABLE OF varchar2(50);
	
		a1_kt NumTab;
		a1_tp NumTab;
		a1_kd NumTab;
		a1_kdnr ChTab;
		tp NumTab;
		trans_id NumTab;
		pmnt_id FAF_MROS_numID_t;
		xml_tr xmltype;
		xml_pm xmltype;
		xml_r xmltype;
		xml_a xmltype;
		xml_p xmltype;
		xml_i xmltype;
		xml_ac xmltype;
		xml_cl xmltype;
		xml_cl1 xmltype;
		xml_t xmltype;
		n number;
		curr varchar2(4);
		sld_bwrg number;
		sld_kwrg number;
		inst_name varchar2(255);
		swif varchar2(11);
		ind FAF_MROS_charID_t;
		str varchar(4000);
		ac_id number;
		max_items number;
	begin
		max_items:=1000;

		--indicators checking
		select FAF_MROS_charID(indicator) 
			bulk collect into ind 
		from FA_MROS_INT_INDIZ where indicator in (select trim(chID) from table(REPORT_INDICATORS));
		select count(*) into n from table(ind) where substr(chID,-1)='M';
		if n<>1 then
			str:= 'the report must include only one indicator with type Report_Type (M)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='V';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Predicate_Offenses (V)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='G';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Reason (G)';	
		end if;	
		if length(str)>0 then 
			str:= 'Indicators error: ' || str || '; ';
		end if;
		
		--table FA_MROS_INT_PERS_CHARGE checking
		select count(*) into n from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID;
		if n=0 then 
			str:= str || 'The table FA_MROS_INT_PERS_CHARGE does not have the record with userbk_nr_home = ' || F_BANK_ID || '; ';
		end if;

		--account ID checking
		begin
			select KT_LNR into ac_id from FA_MROS_KT_STAMM where userbk_nr=F_BANK_ID and EDIT_KT_NR=AC_NR and rownum<=1;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
				ac_id := NULL;
		end;
		if ac_id is null then 
			str:= str || 'Account number not found; ';
		end if;

		--transactions set checking
		select count(*) into n from table(SET_OF_TR_ID) s;
		if n=0 then
			str:= str || 'At least one transaction number should be provided; ';
		end if;

		--error processing
		if length(str)>0 then 
			select xmlelement("mros_report_error",str) into xml_r from dual;
		end if;

		if xml_r is null then			
			--list of appropriate payments
			begin
				select 
					FAF_MROS_numID(ZV_ZLG_SYS_LNR)
				bulk collect INTO pmnt_id
				from 
					(select 
						distinct pm.ZV_ZLG_SYS_LNR
					from
						FA_MROS_KT_BUCH tr
					join
						FA_MROS_ZV_ZLG_T pm
					on tr.userbk_nr=pm.userbk_nr and tr.AUF_LNR=pm.AUF_LNR 
						and (tr.KT_LNR=pm.KT_LNR or tr.KT_LNR=pm.KT_LNR_B)
					--acceptable transaction types only
					join
						FA_MROS_REFERENCE_TABLE tr_tp
					on pm.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
					--acceptable trak_ref types only
					join
						FA_MROS_REFERENCE_TABLE ref_tp
					on pm.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
					where tr.KT_LNR=ac_id
						and tr.KT_BUCH_LNR in (select nID from table(SET_OF_TR_ID) s)
						and tr.userbk_nr=F_BANK_ID 
						and rownum<=max_items) s;
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
					pmnt_id := NULL;
			end;

			select count(*) into n from table(pmnt_id);
			if n>0 then 
				select trans_node_from_payments(F_BANK_ID,pmnt_id,NULL) into xml_pm from dual;
			end if;

			if n<max_items then
				--list of deposits/withdrawals
				begin			 
					select KT_BUCH_LNR,KT_LNR,KD_HTYP_CD,KD_LNR,EDIT_KD_NR,
						case 
							when special_mark='withdrawal' then 1
							when special_mark='deposit' then 
								case
									when adr_zeile_1_ez is not null
									then 3
									else 2
								end
						end						
					bulk collect into trans_id,a1_kt,a1_tp,a1_kd,a1_kdnr,tp
					from 
						(select 
							distinct tr.KT_BUCH_LNR,tr_tp.special_mark,tr.buch_dat_a,a1.KT_LNR,p1.KD_HTYP_CD,p1.KD_LNR,p1.EDIT_KD_NR,tr.adr_zeile_1_ez
						from
							FA_MROS_KT_BUCH_EA tr
						--t_account 
						left join
							FA_MROS_KT_STAMM a1
						on tr.KT_LNR=a1.KT_LNR and tr.userbk_nr=a1.userbk_nr		  
						--t_person/t_entity (account owner)
						left join
							FA_MROS_KD_STAMM p1
						on a1.KD_LNR=p1.KD_LNR and a1.userbk_nr=p1.userbk_nr and p1.partner_flag=0
						--acceptable transaction types only
						join
							FA_MROS_REFERENCE_TABLE tr_tp
						on tr.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
						where tr.KT_LNR=ac_id
							and tr.KT_BUCH_LNR in (select nID from table(SET_OF_TR_ID) s)
							and tr.userbk_nr=F_BANK_ID 
							and tr_tp.special_mark in ('withdrawal','deposit')
							and rownum<=max_items-n) s;
					EXCEPTION
						WHEN NO_DATA_FOUND THEN
						trans_id := NULL;
				end;

				if trans_id is not null then
					--Bank's name and swift-code from the technical table 
					select char_process(institution_name,'n/a',255) into inst_name from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;
					select char_process(swift,'n/a',11) into swif from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;
				
					--Cycle to work with the input set of transaction numbers
					FOR ii IN 1 .. trans_id.count
					LOOP			
						if trans_id(ii) is not null and tp(ii) is not null then
							--address
							begin
								select 
									XMLAGG(
										XMLELEment("address",
											XMLforest(
												1 as "address_type",
												char_process(adr.strasse,'n/a',100) as "address",
												char_process(adr.ORT_ORT_BEZ,char_process(adr.ADR_ORT_BEZ,'n/a',255),255) as "city",
												char_process(adr.ADR_PLZ,NULL,10) as "zip",
												NVL(cntr.iso_land_cd,'UNK') as "country_code",
												st.MROS_value as "state"
											)
										)
									)
								into xml_a
								from 
									FA_MROS_KD_ADR adr
								left join
									FA_MROS_REFERENCE_TABLE st
								on st.Finnova_value=adr.region_cd and st.ref_type=2 and st.userbk_nr=F_BANK_ID and st.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr
								on cntr.iso_land_cd=adr.iso_land_cd
								where adr.KD_LNR=a1_kd(ii) 
									and adr.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_a:=NULL;
							end;

							if a1_tp(ii)<3 then
								--phone
								begin
									select 
										XMLAGG(
											XMLELEment("phone",
												XMLforest(
													NVL(ct.MROS_value,'1') as "tph_contact_type",
													NVL(ct1.MROS_value,'6') as "tph_communication_type",
													char_process(ph.EMAIL_ADR,'n/a',50) as "tph_number",
													case 
														when ct.MROS_value='2' or ct1.MROS_value='4' 
														then 
															char_process(
																case
																	when ct.MROS_value='2'
																	then char_process(ct.comments,NULL,4000) || '; '
																	else null
																end || 
																case
																	when ct1.MROS_value='4'
																	then char_process(ct1.comments,NULL,4000)
																	else null
																end
															,'n/a',4000)
														else NULL
													end as "comments"
												)
											)
										)
									into xml_p
									from 
										FA_MROS_KD_EMAIL ph
									left join
										FA_MROS_REFERENCE_TABLE ct
									on ct.Finnova_value=ph.mail_art_cd and ct.ref_type=6 and ct.userbk_nr=F_BANK_ID and ct.activ_MROS=1
									left join
										FA_MROS_REFERENCE_TABLE ct1
									on ct1.Finnova_value=ph.mail_art_cd and ct1.ref_type=7 and ct1.userbk_nr=F_BANK_ID and ct1.activ_MROS=1
									where ph.KD_LNR=a1_kd(ii) and ph.userbk_nr=F_BANK_ID;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_p:=NULL;
								end;

								--ID
								begin
									select 
										XMLAGG(
											XMLELEment("identification",
												XMLforest(
													NVL(tp.MROS_value,'4') as "type",
													char_process(id.KD_AUSW_NR,'n/a',255) as "number",
													case 
														when id.AUSSTELL_DAT is null or id.AUSSTELL_DAT < date '1753-01-01' or id.AUSSTELL_DAT > sysdate
														then NULL 
														else to_char(id.AUSSTELL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end as "issue_date",
													case 
														when id.ABL_DAT is null or id.ABL_DAT<id.AUSSTELL_DAT or id.ABL_DAT < date '1753-01-01'
														then NULL 
														else to_char(id.ABL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end as "expiry_date",
													char_process(id.KD_VSB_BEGLAUB,NULL,255) as "issued_by", 
													NVL(cntr.iso_land_cd,'UNK') as "issue_country",
													case 
														when NVL(tp.MROS_value,'4')='4' 
														then char_process(tp.comments,'n/a',4000) 
														else NULL
													end as "comments"
												)
											)
										)
									into xml_i
									from 
										FA_MROS_KD_VSB id
									left join
										FA_MROS_REFERENCE_TABLE tp
									on tp.Finnova_value=id.iddoc_cd and tp.ref_type=5 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
									left join
										FA_MROS_INT_LAND cntr
									on cntr.iso_land_cd=id.iso_land_cd
									where id.KD_LNR=a1_kd(ii) and id.userbk_nr=F_BANK_ID;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_i:=NULL;
								end;

								--person
								begin
									select
										xmlconcat(
											XMLFOREST(															
												NVL(gd.MROS_value,'U') as "gender",
												char_process(sd1.titel,NULL,30) as "title",
												char_process(sd1.VORNAME,'n/a',100) as "first_name",
												char_process(sd1.name_1,'n/a',100) as "last_name",
												case 
													when sd1.GEBURT_DAT is null or sd1.GEBURT_DAT < date '1753-01-01' or sd1.GEBURT_DAT > sysdate
													then 
														case 
															when sd1.GEBURT_JAHR is null or sd1.GEBURT_JAHR < 1753 or sd1.GEBURT_JAHR > extract(year from sysdate)
															then '1900-01-01T00:00:00'
															else to_char(sd1.GEBURT_JAHR) || '-01-01T00:00:00' 
														end 
													else to_char(sd1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "birthdate",
												char_process(sd1.GEBURT_ORT,NULL,255) as "birth_place",
												NVL(cntr1.iso_land_cd,'UNK') as "nationality1",
												cntr2.iso_land_cd as "nationality2",
												xml_p as "phones",
												NVL(xml_a,
													XMLELEment("address",
														XMLforest(
															1 as "address_type",
															'n/a' as "address",
															'n/a' as "city",
															'UNK' as "country_code"
														)
													)
												) as "addresses"
											),   
											NVL(xml_i,
												xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
											),		 
											case 
												when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
												then xmlelement("deceased",1)
												else null 
											end,
											case 
												when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
												then xmlelement("date_deceased",
													case 
														when sd1.TOD_DAT is null or sd1.TOD_DAT < date '1753-01-01' or sd1.TOD_DAT > sysdate
														then to_char(sd1.TOD_JAHR) || '-01-01T00:00:00' 
														else to_char(sd1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end)
												else null 
											end
										)			
									into xml_cl
									from
										FA_MROS_KD_STAMM sd1	
									left join
										FA_MROS_REFERENCE_TABLE gd
									on gd.Finnova_value=sd1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
									left join
										FA_MROS_INT_LAND cntr1
									on sd1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
									left join
										FA_MROS_INT_LAND cntr2
									on sd1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd
									where sd1.KD_LNR=a1_kd(ii) and sd1.userbk_nr=F_BANK_ID and sd1.partner_flag=0;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										select
											xmlconcat(
												XMLFOREST(
													'U' as "gender",
													'n/a' as "first_name",
													'n/a' as "last_name",
													'1900-01-01T00:00:00' as "birthdate",
													'UNK' as "nationality1",
													XMLELEment("address",
														XMLforest(
															1 as "address_type",
															'n/a' as "address",
															'n/a' as "city",
															'UNK' as "country_code"
														)
													) as "addresses"
												),   
												xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
											)
										into xml_cl
										from dual;
								end;
								begin
									select
										XMLAGG(
											xmlelement("signatory",
												xmlforest(
													case when sd1.partner_flag=0 then 1 else Null end as "is_primary",
													xmlconcat(
														XMLFOREST(															
															NVL(gd.MROS_value,'U') as "gender",
															char_process(sd1.titel,NULL,30) as "title",
															char_process(sd1.VORNAME,'n/a',100) as "first_name",
															char_process(sd1.name_1,'n/a',100) as "last_name",
															case 
																when sd1.GEBURT_DAT is null or sd1.GEBURT_DAT < date '1753-01-01' or sd1.GEBURT_DAT > sysdate
																then 
																	case 
																		when sd1.GEBURT_JAHR is null or sd1.GEBURT_JAHR < 1753 or sd1.GEBURT_JAHR > extract(year from sysdate)
																		then '1900-01-01T00:00:00'
																		else to_char(sd1.GEBURT_JAHR) || '-01-01T00:00:00' 
																	end 
																else to_char(sd1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
															end as "birthdate",
															char_process(sd1.GEBURT_ORT,NULL,255) as "birth_place",
															NVL(cntr1.iso_land_cd,'UNK') as "nationality1",
															cntr2.iso_land_cd as "nationality2",
															xml_p as "phones",
															NVL(xml_a,
																XMLELEment("address",
																	XMLforest(
																		1 as "address_type",
																		'n/a' as "address",
																		'n/a' as "city",
																		'UNK' as "country_code"
																	)
																)
															) as "addresses"
														),   
														NVL(xml_i,
															xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
														),		 
														case 
															when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
															then xmlelement("deceased",1)
															else null 
														end,
														case 
															when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
															then xmlelement("date_deceased",
																case 
																	when sd1.TOD_DAT is null or sd1.TOD_DAT < date '1753-01-01' or sd1.TOD_DAT > sysdate
																	then to_char(sd1.TOD_JAHR) || '-01-01T00:00:00' 
																	else to_char(sd1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end)
															else null 
														end				
													) as "t_person",
													1 as "role"
												)
											)
										)
									into xml_cl1
									from
										FA_MROS_KD_STAMM sd1	
									left join
										FA_MROS_REFERENCE_TABLE gd
									on gd.Finnova_value=sd1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
									left join
										FA_MROS_INT_LAND cntr1
									on sd1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
									left join
										FA_MROS_INT_LAND cntr2
									on sd1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd
									where sd1.KD_LNR=a1_kd(ii) and sd1.userbk_nr=F_BANK_ID;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										select
											XMLAGG(
												xmlelement("signatory",
													xmlforest(
														xmlconcat(
															XMLFOREST(
																'U' as "gender",
																'n/a' as "first_name",
																'n/a' as "last_name",
																'1900-01-01T00:00:00' as "birthdate",
																'UNK' as "nationality1",
																XMLELEment("address",
																	XMLforest(
																		1 as "address_type",
																		'n/a' as "address",
																		'n/a' as "city",
																		'UNK' as "country_code"
																	)
																) as "addresses"
															),   
															xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
														) as "t_person",
														1 as "role"
													)
												)
											)
										into xml_cl1
										from dual;
								end;
							else
								--entity
								begin
									select 
										XMLFOREST(
											char_process(sb1.kd_bez,'n/a',255) as "name",
											NVL(lf.MROS_value,'1') as "incorporation_legal_form",
											char_process(sb1.UID_NR,NULL,50) as "incorporation_number",
											char_process(sb1.NOGA_BEZ,NULL,255) as "business",
											NVL(xml_a,
												XMLELEment("address",
													XMLforest(
														1 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													)
												) 
											) as "addresses",
											char_process(sb1.EMAIL_ADR,NULL,255) as "url",
											char_process(sb1.GEBURT_ORT,NULL,255) as "incorporation_state",
											NVL(cntr1.iso_land_cd,'UNK') as "incorporation_country_code",
											case 
												when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
												then 
													case 
														when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR<1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
														then NULL 
														else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
													end 
												else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "incorporation_date",
											case 
												when (sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR >= 1753 and sb1.TOD_JAHR <= extract(year from sysdate))
												then 1 else NULL
											end as "business_closed",
											case 
												when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
												then 
													case 
														when sb1.TOD_JAHR is null or sb1.TOD_JAHR < 1753 or sb1.TOD_JAHR > extract(year from sysdate)
														then null 
														else to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
													end 
												else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "date_business_closed",
											NVL(dm.MROS_value,'yes') as "tax_reg_number",
											char_process(
												case 
													when lf.MROS_value='4' 
													then 
														case
															when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
															then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); ' || lf.comments
															else NVL(lf.comments,'n/a')
														end 
													else 
														case
															when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
															then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); '
															else NULL
														end 
												end
											,NULL,4000) as "comments"
										)
									into xml_cl
									from
										FA_MROS_KD_STAMM sb1
									left join
										FA_MROS_REFERENCE_TABLE lf
									on lf.Finnova_value=sb1.kd_rstat_cd and lf.ref_type=8 and lf.userbk_nr=F_BANK_ID and lf.activ_MROS=1
									left join
										FA_MROS_REFERENCE_TABLE dm
									on dm.Finnova_value=sb1.kd_sitz_gsch_cd and dm.ref_type=3 and dm.userbk_nr=F_BANK_ID and dm.activ_MROS=1
									left join
										FA_MROS_INT_LAND cntr1
									on sb1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
									where sb1.KD_LNR=a1_kd(ii) and sb1.userbk_nr=F_BANK_ID and sb1.partner_flag=0;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										select 
											XMLFOREST(
												'n/a' as "name",
												1 as "incorporation_legal_form",
												XMLELEment("address",
													XMLforest(
														1 as "address_type",
														'n/a' as "address",
														'n/a' as "city",
														'UNK' as "country_code"
													)									
												) as "addresses",
												'UNK' as "incorporation_country_code",
												'yes' as "tax_reg_number"
											)
										into xml_cl
										from dual;
									select xmlelement("t_entity",xml_cl) into xml_cl1 from dual;
								end;
							end if;

							begin
								select saldo_bwrg,saldo_kwrg into sld_bwrg,sld_kwrg from FA_MROS_KT_SALDO
								where kt_lnr=a1_kt(ii) and userbk_nr=F_BANK_ID and rownum<=1;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									begin
										sld_bwrg:=NULL;
										sld_kwrg:=NULL;
									end;
							end;

							--account
							begin
								select
									xmlconcat(
										XMLforest(
											inst_name as "institution_name",
											swif as "swift",
											NVL(br.MROS_value,'ZH') as "branch",
											char_process(sy1.EDIT_KT_NR,'n/a',50) as "account",
											NVL(curr.iso_wrg_cd,'CHF') as "currency_code",
											char_process(sy1.KT_ART_NR_BEZ,NULL,255) as "account_name",
											char_process(sy1.iban_id,'n/a',34) as "iban",
											char_process(a1_kdnr(ii),'n/a',30) as "client_number",
											NVL(tp.MROS_value,'16') as "personal_account_type"
										),
										xml_cl1,
										XMLforest(
											case 
												when sy1.EROEFF_DAT is null or sy1.EROEFF_DAT <date '1753-01-01' or sy1.EROEFF_DAT > sysdate
												then '1900-01-01T00:00:00'
												else to_char(sy1.EROEFF_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "opened",
											case 
												when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
												then null 
												else to_char(sy1.AUFHEB_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "closed",
											case 
												when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
												then replace(rtrim(to_char(NVL(sld_bwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
												else null 
											end as "balance",
											case 
												when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
												then replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												else null 
											end as "date_balance",				
											case 
												when sy1.SPERR_CD=97 or sy1.aufheb_cd='ZZ' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
												then 2 else 1 
											end as "status_code",
											case 
												when sy1.iso_wrg_cd='CHF' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
												then NULL 
												else replace(rtrim(to_char(NVL(sld_kwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
											end as "beneficiary",
											char_process(sy1.SITZ_NR_BEZ,'n/a',255) as "beneficiary_comment",
											char_process(
												case 
													when tp.MROS_value='14' 
													then  
														case
															when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
															then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); ' || tp.comments
															else NVL(tp.comments,'n/a')
														end
													else
														case
															when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
															then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); '
															else NULL
														end 
												end 
											,NULL,4000) as "comments"
										)				   
									)
								into xml_ac
								from
									FA_MROS_KT_STAMM sy1
								left join
									FA_MROS_REFERENCE_TABLE br
								on sy1.sitz_nr=br.Finnova_value and br.ref_type=1 and br.userbk_nr=F_BANK_ID and br.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE tp
								on sy1.kt_art_ber_cd=tp.Finnova_value and tp.ref_type=9 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
								left join
									FA_MROS_INT_WRG curr
								on curr.iso_wrg_cd=sy1.iso_wrg_cd 
								where sy1.KT_LNR=a1_kt(ii) and sy1.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									select
										xmlconcat(
											XMLforest(
												'n/a' as "institution_name",
												'n/a' as "swift",
												'ZH' as "branch",
												'n/a' as "account",
												'CHF' as "currency_code",
												'n/a' as "iban",
												'n/a' as "client_number",
												16 as "personal_account_type"
											),
											xml_cl,
											XMLforest(
												'1900-01-01T00:00:00' as "opened",
												0 as "balance",
												replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T') as "date_balance",
												1 as "status_code",
												'n/a' as "beneficiary_comment"
											)				   
										)
									into xml_ac
									from dual;
							end;

							begin
								select
									cr.iso_wrg_cd
								into curr
								from
									FA_MROS_KT_STAMM ac
								join
									FA_MROS_INT_WRG cr
								on cr.iso_wrg_cd=ac.iso_wrg_cd
								where ac.KT_LNR=a1_kt(ii) and ac.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									curr:= NULL;
							end;

							--1 withdrawal (from account to client)
							if tp(ii) = 1 then
								begin
									select 
										xmlelement("transaction",
											xmlconcat(
												xmlelement("transactionnumber",s3.KT_BUCH_LNR),											
												case when s3.SITZ_NR is null then 
													case tr_tp.MROS_value
														when '1' then xmlelement("transaction_location",'n/a')
														when '2' then xmlelement("transaction_location",'n/a')
														else null 
													end
												else
													xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
												end,
												xmlelement("transaction_description",
													char_process(
														case 
															when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null 
															then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000) || '; '
															else NULL
														end ||
														case
															when char_process(s3.text_ext,NULL,0) is not null 
															then 'Text: ' || char_process(s3.text_ext,NULL,4000) || '; ' || char_process(s3.text_zus,NULL,4000)
															else
																case
																	when char_process(s3.text_zus,NULL,0) is not null 
																	then 'Text: ' || char_process(s3.text_zus,NULL,4000) || '; ' 
																	else NULL
																end
														end
													,'n/a',4000)),
												xmlelement("date_transaction",replace(to_char(s3.buch_dat_a,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("value_date",replace(to_char(s3.valuta,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
												xmlelement("transmode_comment",
												case 
													when tr_tp.MROS_value=13 
													then char_process(tr_tp.comments,'n/a',50)
													else NULL
												end), 
												xmlelement("amount_local",
													replace(rtrim(to_char(
														case
															when tr_tp.MROS_value=16 then 0
															else
																case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
														end
													, 'fm9999999999999990d999'), ',.'),',','.')
												),
												xmlelement("t_from_my_client",
													XMLconcat(
														xmlelement("from_funds_code",1),
														xmlelement("from_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("from_account",xml_ac)
													)
												),
												xmlelement("t_to_my_client",
													XMLconcat(
														xmlelement("to_funds_code",1),
														xmlelement("to_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														case 
															when a1_tp(ii)<3 
															then xmlelement("to_person",xml_cl)
															else xmlelement("to_entity",xml_cl)
														end
													)
												),
												case 
													when s3.BETRAG_BWRG<0 then xmlelement("comments",'It is a return operation. ') 
													else NULL
												end
											)
										)
									into xml_t
									from
										FA_MROS_KT_BUCH_EA s3
									join
										FA_MROS_REFERENCE_TABLE tr_tp
									on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
									where s3.KT_BUCH_LNR=trans_id(ii) and s3.userbk_nr=F_BANK_ID;	
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_t:=NULL;
								end;
							end if;

							--2 deposit (from client to his/her account)
							if tp(ii) = 2 then
								begin
									select 
										xmlelement("transaction",
											xmlconcat(
												xmlelement("transactionnumber",s3.KT_BUCH_LNR),
												case when s3.SITZ_NR is null then 
													case tr_tp.MROS_value
														when '1' then xmlelement("transaction_location",'n/a')
														when '2' then xmlelement("transaction_location",'n/a')
														else null 
													end
												else
													xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
												end,
												xmlelement("transaction_description",
													char_process(
														case 
															when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null 
															then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000) || '; '
															else NULL
														end ||
														case
															when char_process(s3.text_ext,NULL,0) is not null 
															then 'Text: ' || char_process(s3.text_ext,NULL,4000) || '; ' || char_process(s3.text_zus,NULL,4000)
															else
																case
																when char_process(s3.text_zus,NULL,0) is not null 
																then 'Text: ' || char_process(s3.text_zus,NULL,4000) || '; ' 
																else NULL
															end
														end
													,'n/a',4000)),
												xmlelement("date_transaction",replace(to_char(s3.buch_dat_a,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("value_date",replace(to_char(s3.valuta,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
												xmlelement("transmode_comment",
												case 
													when tr_tp.MROS_value=13 
													then char_process(tr_tp.comments,'n/a',50)
													else NULL
												end), 
												xmlelement("amount_local",
													replace(rtrim(to_char(
														case
															when tr_tp.MROS_value=16 then 0
															else
																case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
														end
													, 'fm9999999999999990d999'), ',.'),',','.')
												),
												xmlelement("t_from_my_client",
													XMLconcat(
														xmlelement("from_funds_code",1),
														xmlelement("from_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														case 
															when a1_tp(ii)<3 
															then xmlelement("from_person",xml_cl)
															else xmlelement("from_entity",xml_cl)
														end
													)
												),
												xmlelement("t_to_my_client",
													XMLconcat(
														xmlelement("to_funds_code",1),
														xmlelement("to_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("to_account",xml_ac)
													)
												),
												case 
													when s3.BETRAG_BWRG<0 then xmlelement("comments",'It is a return operation. ') 
													else NULL
												end
											)
										)
									into xml_t
									from
										FA_MROS_KT_BUCH_EA s3
									join
										FA_MROS_REFERENCE_TABLE tr_tp
									on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
									where s3.KT_BUCH_LNR=trans_id(ii) and s3.userbk_nr=F_BANK_ID;	
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_t:=NULL;
								end;
							end if;

							--3 deposit (from client to someone other's account)
							if tp(ii) = 3 then
								begin
									select 
										xmlelement("transaction",
											xmlconcat(
												xmlelement("transactionnumber",s3.KT_BUCH_LNR),
												case when s3.SITZ_NR is null then 
													case tr_tp.MROS_value
														when '1' then xmlelement("transaction_location",'n/a')
														when '2' then xmlelement("transaction_location",'n/a')
														else null 
													end
												else
													xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
												end,
												xmlelement("transaction_description",
													char_process(
														case 
															when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null 
															then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000) || '; '
															else NULL
														end ||
														case
															when char_process(s3.text_ext,NULL,0) is not null 
															then 'Text: ' || char_process(s3.text_ext,NULL,4000) || '; ' || char_process(s3.text_zus,NULL,4000)
															else
																case
																when char_process(s3.text_zus,NULL,0) is not null 
																then 'Text: ' || char_process(s3.text_zus,NULL,4000) || '; ' 
																else NULL
															end
														end
													,'n/a',4000)),
												xmlelement("date_transaction",replace(to_char(s3.buch_dat_a,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("value_date",replace(to_char(s3.valuta,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
												xmlelement("transmode_comment",
												case 
													when tr_tp.MROS_value=13 
													then char_process(tr_tp.comments,'n/a',50)
													else NULL
												end), 
												xmlelement("amount_local",
													replace(rtrim(to_char(
														case
															when tr_tp.MROS_value=16 then 0
															else
																case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
														end
													, 'fm9999999999999990d999'), ',.'),',','.')
												),
												xmlelement("t_from",
													XMLconcat(
														xmlelement("from_funds_code",1),
														xmlelement("from_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("from_person",
															xmlelement("first_name",
																char_process(
																	case
																		when REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,0,'i')>0
																		then substr(s3.adr_zeile_1_ez,REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]]',1,1,0,'i'),REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,0,'i')-REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]]',1,1,0,'i')+1)
																		else 'n/a'
																	end
																,'n/a',100)
															),
															xmlelement("last_name",
																char_process(
																	case
																		when REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,0,'i')>0
																		then substr(s3.adr_zeile_1_ez,REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,1,'i')-1)
																		else s3.adr_zeile_1_ez
																	end 
																,'n/a',100)
															),
															xmlelement("comments",
																char_process(
																	s3.adr_zeile_1_ez ||
																	case 
																		when s3.adr_zeile_2_ez is not null
																		then '; ' || s3.adr_zeile_2_ez
																		else  NULL
																	end ||
																	case 
																		when s3.adr_zeile_3_ez is not null
																		then '; ' || s3.adr_zeile_3_ez
																		else  NULL
																	end ||
																	case 
																		when s3.adr_zeile_4_ez is not null
																		then '; ' || s3.adr_zeile_4_ez
																		else  NULL
																	end ||
																	case 
																		when s3.adr_zeile_5_ez is not null
																		then '; ' || s3.adr_zeile_5_ez
																		else  NULL
																	end 
																,'n/a',4000)
															)
														)
													)
												),
												xmlelement("t_to_my_client",
													XMLconcat(
														xmlelement("to_funds_code",1),
														xmlelement("to_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("to_account",xml_ac)
													)
												),
												case 
													when s3.BETRAG_BWRG<0 then xmlelement("comments",'It is a return operation. ') 
													else NULL
												end
											)
										)
									into xml_t
									from
										FA_MROS_KT_BUCH_EA s3
									join
										FA_MROS_REFERENCE_TABLE tr_tp
									on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
									where s3.KT_BUCH_LNR=trans_id(ii) and s3.userbk_nr=F_BANK_ID;	
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_t:=NULL;
								end;
							end if;

							select xmlconcat(xml_tr,xml_t) into xml_tr from dual;
						end if;
					end loop;
				end if;
			end if;

			if xml_pm is not null or xml_tr is not null then 
				--remain part of the report
				select XMLconcat(xml_pm,xml_tr) into xml_t from dual;
				select head_part_of_report(F_BANK_ID,RP_NAME,RP_SURNAME,RP_PHONE,RP_EMAIL,RP_OCCUPATION,RP_ADDRESS,RP_CITY,FIU_REF_NUMBER,ENTITY_REFERENCE,REPORT_REASON,R_ACTION,ind,xml_t,1) into xml_r from dual;
			else
				select xmlelement("mros_report_error",'There are no transactions that meet the conditions; ') into xml_r from dual;
			end if;
		end if;

		return(xml_r);
	end;



	---------------------------------------------------------------------------------------------------------------------------------------------



	--transactions report from ktwbuch_det (clients/accounts + filter)
	function historical_report (
		F_BANK_ID in number default 949,
		RP_NAME in varchar2 default NULL,
		RP_SURNAME in varchar2 default NULL,
		RP_PHONE in varchar2 default NULL,
		RP_EMAIL in varchar2 default NULL,
		RP_OCCUPATION in varchar2 default NULL,
		RP_ADDRESS in varchar2 default NULL,
		RP_CITY in varchar2 default NULL,
		FIU_REF_NUMBER in varchar2 default NULL,
		ENTITY_REFERENCE in varchar2 default 'n/a',	
		REPORT_REASON in varchar2 default 'n/a',
		R_ACTION in varchar2 default 'n/a',
		REPORT_INDICATORS in FAF_MROS_charID_t default NULL,
		SET_OF_CL_NR in FAF_MROS_charID_t default NULL,
		SET_OF_AC_NR in FAF_MROS_charID_t default NULL,
		DATE_FROM in number default null,
		DATE_TO in number default null,
		LOW_AMOUNT in number default 0.01,
		DEBIT in number default 1,
		CREDIT in number default 1,
		N_TRANS_LIMIT in number default 1000
	) 
		return xmltype
	is
		TYPE NumTab IS TABLE OF number;
		TYPE ChTab IS TABLE OF varchar2(50);
	
		a1_kt NumTab;
		a1_tp NumTab;
		a1_kd NumTab;
		a1_kdnr ChTab;
		tp NumTab;
		orders FAF_MROS_NumNum_t;
		trans_id NumTab;
		pmnt_id FAF_MROS_numID_t;
		xml_tr xmltype;
		xml_pm xmltype;
		xml_r xmltype;
		xml_a xmltype;
		xml_p xmltype;
		xml_i xmltype;
		xml_ac xmltype;
		xml_cl xmltype;
		xml_t xmltype;
		n number;
		d1 date;
		d2 date;
		tr_lim number;
		curr varchar2(4);
		sld_bwrg number;
		sld_kwrg number;
		inst_name varchar2(255);
		swif varchar2(11);
		LA number;
		ind FAF_MROS_charID_t;
		str varchar(4000);
	begin
		--indicators checking
		select FAF_MROS_charID(indicator) 
			bulk collect into ind 
		from FA_MROS_INT_INDIZ where indicator in (select trim(chID) from table(REPORT_INDICATORS));
		select count(*) into n from table(ind) where substr(chID,-1)='M';
		if n<>1 then
			str:= 'the report must include only one indicator with type Report_Type (M)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='V';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Predicate_Offenses (V)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='G';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Reason (G)';	
		end if;	
		if length(str)>0 then 
			str:= 'Indicators error: ' || str || '; ';
		end if;
		
		--table FA_MROS_INT_PERS_CHARGE checking
		select count(*) into n from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID;
		if n=0 then 
			str:= str || 'The table FA_MROS_INT_PERS_CHARGE does not have the record with userbk_nr_home = ' || F_BANK_ID || '; ';
		end if;

		--Debit/Credit checking
		if DEBIT=0 and CREDIT=0 then
			str:= str || 'At least one from Debit/Credit should be nonzero; ';
		end if;

		--error processing
		if length(str)>0 then 
			select xmlelement("mros_report_error",str) into xml_r from dual;
		end if;

		if xml_r is null then
			--input dates checking
			if DATE_FROM<19000101 or DATE_FROM>to_number(to_char(sysdate,'yyyymmdd')) then 
				d1:= NULL;
			else
				begin
					select to_date(to_char(DATE_FROM),'yyyymmdd') into d1 from dual;
					Exception 
						WHEN Others THEN
						d1:= NULL;
				end;
			end if;

			if DATE_TO<19000101 or DATE_TO>to_number(to_char(sysdate,'yyyymmdd')) then 
				d2:= NULL;
			else
				begin
					select to_date(to_char(DATE_TO),'yyyymmdd') into d2 from dual;
					Exception 
						WHEN Others THEN
						d2:= NULL;
				end;
			end if;
			
			if d1>d2 then 
				d2:=NULL;
			end if;

			--N_TRANS_LIMIT checking
			tr_lim:=N_TRANS_LIMIT;
			if tr_lim is null or tr_lim<=0 then
				tr_lim:= 1000;
			end if;

			--Low BETRAG_KWRG checking
			LA:=LOW_AMOUNT;
			if LA is null or LA<0.01 then
				LA:=0.01;
			end if;

			--list of relevant transactions
			select count(*) into n from table(SET_OF_AC_NR);
			if n>0 then
				begin			 
					select FAF_MROS_NumNum(AUF_LNR,KT_LNR)
					bulk collect into orders
					from 
						(select * from FA_MROS_KT_BUCH 
						where EDIT_KT_NR in (select chID from table(SET_OF_AC_NR))
							and userbk_nr=F_BANK_ID 
							and buch_dat_a>=NVL(d1,to_date('01.01.1753','dd.mm.yyyy'))
							and buch_dat_a<=NVL(d2,to_date('01.01.2200','dd.mm.yyyy'))
							and abs(BETRAG_BWRG)>=LA
							and (SH_CD = case when DEBIT=0 then '' else 'D' end
								or SH_CD = case when CREDIT=0 then '' else 'C' end)				
						order by buch_dat_a desc) s
					where rownum<=tr_lim;
					EXCEPTION
						WHEN NO_DATA_FOUND THEN
						n := 0;
				end;
			end if;

			if n=0 then
				select count(*) into n from table(SET_OF_CL_NR);
				if n>0 then 
					begin
						select FAF_MROS_NumNum(AUF_LNR,KT_LNR)
						bulk collect into orders
						from 
							(select * from FA_MROS_KT_BUCH 
							where EDIT_KD_NR in (select chID from table(SET_OF_CL_NR)) 
								and userbk_nr=F_BANK_ID
								and buch_dat_a>=NVL(d1,to_date('01.01.1900','dd.mm.yyyy'))
								and buch_dat_a<=NVL(d2,to_date('01.01.2200','dd.mm.yyyy'))
								and abs(BETRAG_BWRG)>=LA
								and (SH_CD = case when DEBIT=0 then '' else 'D' end
									or SH_CD = case when CREDIT=0 then '' else 'C' end)				
							order by buch_dat_a desc) s
						where rownum<=tr_lim;
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
							n := 0;		
					end;
				end if;
			end if;			

			if n>0 then
				--list of appropriate payments
				begin
					select 
						FAF_MROS_numID(ZV_ZLG_SYS_LNR)
					bulk collect INTO pmnt_id
					from 
						(select
							distinct pm.ZV_ZLG_SYS_LNR,pm.AUF_DT_ERT
						from
							table(orders) o
						join
							FA_MROS_ZV_ZLG_T pm
						on pm.userbk_nr=F_BANK_ID and o.ord=pm.AUF_LNR 
							and (o.acc=pm.KT_LNR or o.acc=pm.KT_LNR_B)
						--acceptable transaction types only
						join
							FA_MROS_REFERENCE_TABLE tr_tp
						on pm.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
						--acceptable trak_ref types only
						join
							FA_MROS_REFERENCE_TABLE ref_tp
						on pm.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
						where abs(pm.betrag_bwrg)>=LA
						order by pm.AUF_DT_ERT desc)
					where rownum<=tr_lim;
					EXCEPTION
						WHEN NO_DATA_FOUND THEN
						pmnt_id := NULL;
				end;
			end if;

			select count(*) into n from table(pmnt_id);
			if n>0 then 
				select trans_node_from_payments(F_BANK_ID,pmnt_id,NULL) into xml_pm from dual;
			end if;

			tr_lim:=tr_lim-n;

			if tr_lim>0 then
				--list of deposits/withdrawals
				select count(*) into n from table(SET_OF_AC_NR);
				if n>0 then
					begin			 
						select KT_BUCH_LNR,KT_LNR,KD_HTYP_CD,KD_LNR,EDIT_KD_NR,
							case 
								when special_mark='withdrawal' then 1
								when special_mark='deposit' then 
									case
										when adr_zeile_1_ez is not null
										then 3
										else 2
									end
							end						
						bulk collect into trans_id,a1_kt,a1_tp,a1_kd,a1_kdnr,tp
						from 
							(select 
								distinct tr.KT_BUCH_LNR,tr_tp.special_mark,tr.buch_dat_a,a1.KT_LNR,p1.KD_HTYP_CD,p1.KD_LNR,p1.EDIT_KD_NR,tr.adr_zeile_1_ez
							from
								FA_MROS_KT_BUCH_EA tr
							--t_account 
							left join
								FA_MROS_KT_STAMM a1
							on tr.KT_LNR=a1.KT_LNR and tr.userbk_nr=a1.userbk_nr		  
							--t_person/t_entity (account owner)
							left join
								FA_MROS_KD_STAMM p1
							on a1.KD_LNR=p1.KD_LNR and a1.userbk_nr=p1.userbk_nr and p1.partner_flag=0
							--acceptable transaction types only
							join
								FA_MROS_REFERENCE_TABLE tr_tp
							on tr.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
							where tr.EDIT_KT_NR in (select chID from table(SET_OF_AC_NR))
								and tr.userbk_nr=F_BANK_ID 
								and tr.buch_dat_a>=NVL(d1,to_date('01.01.1753','dd.mm.yyyy'))
								and tr.buch_dat_a<=NVL(d2,to_date('01.01.2200','dd.mm.yyyy'))
								and abs(tr.BETRAG_BWRG)>=LA
								and (tr.SH_CD = case when DEBIT=0 then '' else 'D' end
									or tr.SH_CD = case when CREDIT=0 then '' else 'C' end)
								and tr_tp.special_mark in ('withdrawal','deposit')
							order by tr.buch_dat_a desc)
						where rownum<=tr_lim;
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
							n := 0;
					end;
				end if;

				if n=0 then
					select count(*) into n from table(SET_OF_CL_NR);
					if n>0 then
						begin			 
							select KT_BUCH_LNR,KT_LNR,KD_HTYP_CD,KD_LNR,EDIT_KD_NR,
								case 
									when special_mark='withdrawal' then 1
									when special_mark='deposit' then 
										case
											when adr_zeile_1_ez is not null
											then 3
											else 2
										end
								end						
							bulk collect into trans_id,a1_kt,a1_tp,a1_kd,a1_kdnr,tp
							from 
								(select 
									distinct tr.KT_BUCH_LNR,tr_tp.special_mark,tr.buch_dat_a,a1.KT_LNR,p1.KD_HTYP_CD,p1.KD_LNR,p1.EDIT_KD_NR,tr.adr_zeile_1_ez
								from
									FA_MROS_KT_BUCH_EA tr
								--t_account 
								left join
									FA_MROS_KT_STAMM a1
								on tr.KT_LNR=a1.KT_LNR and tr.userbk_nr=a1.userbk_nr		  
								--t_person/t_entity (account owner)
								left join
									FA_MROS_KD_STAMM p1
								on a1.KD_LNR=p1.KD_LNR and a1.userbk_nr=p1.userbk_nr and p1.partner_flag=0
								--acceptable transaction types only
								join
									FA_MROS_REFERENCE_TABLE tr_tp
								on tr.buchtyp_cd=tr_tp.Finnova_value and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
								--acceptable trak_ref types only
								join
									FA_MROS_REFERENCE_TABLE ref_tp
								on tr.trak_ref_typ=ref_tp.Finnova_value and ref_tp.userbk_nr=F_BANK_ID and ref_tp.ref_type=11 and ref_tp.activ_MROS=1
								where tr.EDIT_KD_NR in (select chID from table(SET_OF_CL_NR))
									and tr.userbk_nr=F_BANK_ID 
									and tr.buch_dat_a>=NVL(d1,to_date('01.01.1753','dd.mm.yyyy'))
									and tr.buch_dat_a<=NVL(d2,to_date('01.01.2200','dd.mm.yyyy'))
									and abs(tr.BETRAG_BWRG)>=LA
									and (tr.SH_CD = case when DEBIT=0 then '' else 'D' end
										or tr.SH_CD = case when CREDIT=0 then '' else 'C' end)
									and tr_tp.special_mark in ('withdrawal','deposit')
								order by tr.buch_dat_a desc)
							where rownum<=tr_lim;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								n := 0;
						end;
					end if;
				end if;

				if n>0 then
					--Bank's name and swift-code from the technical table 
					select char_process(institution_name,'n/a',255) into inst_name from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;
					select char_process(swift,'n/a',11) into swif from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;
				
					--Cycle to work with the input set of transaction numbers
					FOR ii IN 1 .. trans_id.count
					LOOP			
						if trans_id(ii) is not null and tp(ii) is not null then
							--address
							begin
								select 
									XMLAGG(
										XMLELEment("address",
											XMLforest(
												1 as "address_type",
												char_process(adr.strasse,'n/a',100) as "address",
												char_process(adr.ORT_ORT_BEZ,char_process(adr.ADR_ORT_BEZ,'n/a',255),255) as "city",
												char_process(adr.ADR_PLZ,NULL,10) as "zip",
												NVL(cntr.iso_land_cd,'UNK') as "country_code",
												st.MROS_value as "state"
											)
										)
									)
								into xml_a
								from 
									FA_MROS_KD_ADR adr
								left join
									FA_MROS_REFERENCE_TABLE st
								on st.Finnova_value=adr.region_cd and st.ref_type=2 and st.userbk_nr=F_BANK_ID and st.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr
								on cntr.iso_land_cd=adr.iso_land_cd
								where adr.KD_LNR=a1_kd(ii) 
									and adr.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_a:=NULL;
							end;

							if a1_tp(ii)<3 then
								--phone
								begin
									select 
										XMLAGG(
											XMLELEment("phone",
												XMLforest(
													NVL(ct.MROS_value,'1') as "tph_contact_type",
													NVL(ct1.MROS_value,'6') as "tph_communication_type",
													char_process(ph.EMAIL_ADR,'n/a',50) as "tph_number",
													case 
														when ct.MROS_value='2' or ct1.MROS_value='4' 
														then 
															char_process(
																case
																	when ct.MROS_value='2'
																	then char_process(ct.comments,NULL,4000) || '; '
																	else null
																end || 
																case
																	when ct1.MROS_value='4'
																	then char_process(ct1.comments,NULL,4000)
																	else null
																end
															,'n/a',4000)
														else NULL
													end as "comments"
												)
											)
										)
									into xml_p
									from 
										FA_MROS_KD_EMAIL ph
									left join
										FA_MROS_REFERENCE_TABLE ct
									on ct.Finnova_value=ph.mail_art_cd and ct.ref_type=6 and ct.userbk_nr=F_BANK_ID and ct.activ_MROS=1
									left join
										FA_MROS_REFERENCE_TABLE ct1
									on ct1.Finnova_value=ph.mail_art_cd and ct1.ref_type=7 and ct1.userbk_nr=F_BANK_ID and ct1.activ_MROS=1
									where ph.KD_LNR=a1_kd(ii) and ph.userbk_nr=F_BANK_ID;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_p:=NULL;
								end;

								--ID
								begin
									select 
										XMLAGG(
											XMLELEment("identification",
												XMLforest(
													NVL(tp.MROS_value,'4') as "type",
													char_process(id.KD_AUSW_NR,'n/a',255) as "number",
													case 
														when id.AUSSTELL_DAT is null or id.AUSSTELL_DAT < date '1753-01-01' or id.AUSSTELL_DAT > sysdate
														then NULL 
														else to_char(id.AUSSTELL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end as "issue_date",
													case 
														when id.ABL_DAT is null or id.ABL_DAT<id.AUSSTELL_DAT or id.ABL_DAT < date '1753-01-01'
														then NULL 
														else to_char(id.ABL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end as "expiry_date",
													char_process(id.KD_VSB_BEGLAUB,NULL,255) as "issued_by", 
													NVL(cntr.iso_land_cd,'UNK') as "issue_country",
													case 
														when NVL(tp.MROS_value,'4')='4' 
														then char_process(tp.comments,'n/a',4000) 
														else NULL
													end as "comments"
												)
											)
										)
									into xml_i
									from 
										FA_MROS_KD_VSB id
									left join
										FA_MROS_REFERENCE_TABLE tp
									on tp.Finnova_value=id.iddoc_cd and tp.ref_type=5 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
									left join
										FA_MROS_INT_LAND cntr
									on cntr.iso_land_cd=id.iso_land_cd
									where id.KD_LNR=a1_kd(ii) and id.userbk_nr=F_BANK_ID;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_i:=NULL;
								end;

								--person
								begin
									select
										XMLAGG(
											xmlelement("signatory",
												xmlforest(
													case when sd1.partner_flag=0 then 1 else Null end as "is_primary",
													xmlconcat(
														XMLFOREST(														
															NVL(gd.MROS_value,'U') as "gender",
															char_process(sd1.titel,NULL,30) as "title",
															char_process(sd1.VORNAME,'n/a',100) as "first_name",
															char_process(sd1.name_1,'n/a',100) as "last_name",
															case 
																when sd1.GEBURT_DAT is null or sd1.GEBURT_DAT < date '1753-01-01' or sd1.GEBURT_DAT > sysdate
																then 
																	case 
																		when sd1.GEBURT_JAHR is null or sd1.GEBURT_JAHR < 1753 or sd1.GEBURT_JAHR > extract(year from sysdate)
																		then '1900-01-01T00:00:00'
																		else to_char(sd1.GEBURT_JAHR) || '-01-01T00:00:00' 
																	end 
																else to_char(sd1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
															end as "birthdate",
															char_process(sd1.GEBURT_ORT,NULL,255) as "birth_place",
															NVL(cntr1.iso_land_cd,'UNK') as "nationality1",
															cntr2.iso_land_cd as "nationality2",
															xml_p as "phones",
															NVL(xml_a,
																XMLELEment("address",
																	XMLforest(
																		1 as "address_type",
																		'n/a' as "address",
																		'n/a' as "city",
																		'UNK' as "country_code"
																	)
																)
															) as "addresses"
														),   
														NVL(xml_i,
															xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
														),		 
														case 
															when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
															then xmlelement("deceased",1)
															else null 
														end,
														case 
															when (sd1.TOD_DAT is not null and sd1.TOD_DAT >= date '1753-01-01' and sd1.TOD_DAT <= sysdate) or (sd1.TOD_JAHR is not null and sd1.TOD_JAHR>=1753 and sd1.TOD_JAHR <= extract(year from sysdate)) 
															then xmlelement("date_deceased",
																case 
																	when sd1.TOD_DAT is null or sd1.TOD_DAT < date '1753-01-01' or sd1.TOD_DAT > sysdate
																	then to_char(sd1.TOD_JAHR) || '-01-01T00:00:00' 
																	else to_char(sd1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end)
															else null 
														end				
													) as "t_person",
													1 as "role"
												)
											)
										)
									into xml_cl
									from
										FA_MROS_KD_STAMM sd1	
									left join
										FA_MROS_REFERENCE_TABLE gd
									on gd.Finnova_value=sd1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
									left join
										FA_MROS_INT_LAND cntr1
									on sd1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
									left join
										FA_MROS_INT_LAND cntr2
									on sd1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd
									where sd1.KD_LNR=a1_kd(ii) and sd1.userbk_nr=F_BANK_ID;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										select
											XMLAGG(
												xmlelement("signatory",
													xmlforest(
														xmlconcat(
															XMLFOREST(
																'U' as "gender",
																'n/a' as "first_name",
																'n/a' as "last_name",
																'1900-01-01T00:00:00' as "birthdate",
																'UNK' as "nationality1",
																XMLELEment("address",
																	XMLforest(
																		1 as "address_type",
																		'n/a' as "address",
																		'n/a' as "city",
																		'UNK' as "country_code"
																	)
																) as "addresses"
															),   
															xmlelement("identification",XMLforest(4 as "type",'n/a' as "number",'UNK' as "issue_country",'n/a' as "comments"))
														) as "t_person",
														1 as "role"
													)
												)
											)
										into xml_cl
										from dual;
								end;
							else
								--entity
								begin
									select 
										xmlelement("t_entity",
											XMLFOREST(
												char_process(sb1.kd_bez,'n/a',255) as "name",
												NVL(lf.MROS_value,'1') as "incorporation_legal_form",
												char_process(sb1.UID_NR,NULL,50) as "incorporation_number",
												char_process(sb1.NOGA_BEZ,NULL,255) as "business",
												NVL(xml_a,
													XMLELEment("address",
														XMLforest(
															1 as "address_type",
															'n/a' as "address",
															'n/a' as "city",
															'UNK' as "country_code"
														)
													) 
												) as "addresses",
												char_process(sb1.EMAIL_ADR,NULL,255) as "url",
												char_process(sb1.GEBURT_ORT,NULL,255) as "incorporation_state",
												NVL(cntr1.iso_land_cd,'UNK') as "incorporation_country_code",
												case 
													when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
													then 
														case 
															when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR<1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
															then NULL 
															else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
														end 
													else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "incorporation_date",
												case 
													when (sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR >= 1753 and sb1.TOD_JAHR <= extract(year from sysdate))
													then 1 else NULL
												end as "business_closed",
												case 
													when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
													then 
														case 
															when sb1.TOD_JAHR is null or sb1.TOD_JAHR < 1753 or sb1.TOD_JAHR > extract(year from sysdate)
															then null 
															else to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
														end 
													else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "date_business_closed",
												NVL(dm.MROS_value,'yes') as "tax_reg_number",
												char_process(
													case 
														when lf.MROS_value='4' 
														then 
															case
																when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
																then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); ' || lf.comments
																else NVL(lf.comments,'n/a')
															end 
														else 
															case
																when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
																then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); '
																else NULL
															end 
													end
												,NULL,4000) as "comments"
											)
										)
									into xml_cl
									from
										FA_MROS_KD_STAMM sb1
									left join
										FA_MROS_REFERENCE_TABLE lf
									on lf.Finnova_value=sb1.kd_rstat_cd and lf.ref_type=8 and lf.userbk_nr=F_BANK_ID and lf.activ_MROS=1
									left join
										FA_MROS_REFERENCE_TABLE dm
									on dm.Finnova_value=sb1.kd_sitz_gsch_cd and dm.ref_type=3 and dm.userbk_nr=F_BANK_ID and dm.activ_MROS=1
									left join
										FA_MROS_INT_LAND cntr1
									on sb1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
									where sb1.KD_LNR=a1_kd(ii) and sb1.userbk_nr=F_BANK_ID and sb1.partner_flag=0;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										select 
											xmlelement("t_entity",
												XMLFOREST(
													'n/a' as "name",
													1 as "incorporation_legal_form",
													XMLELEment("address",
														XMLforest(
															1 as "address_type",
															'n/a' as "address",
															'n/a' as "city",
															'UNK' as "country_code"
														)									
													) as "addresses",
													'UNK' as "incorporation_country_code",
													'yes' as "tax_reg_number"
												)
											)
										into xml_cl
										from dual;
								end;
							end if;

							begin
								select saldo_bwrg,saldo_kwrg into sld_bwrg,sld_kwrg from FA_MROS_KT_SALDO
								where kt_lnr=a1_kt(ii) and userbk_nr=F_BANK_ID and rownum<=1;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									begin
										sld_bwrg:=NULL;
										sld_kwrg:=NULL;
									end;
							end;

							--account
							begin
								select
									xmlconcat(
										XMLforest(
											inst_name as "institution_name",
											swif as "swift",
											NVL(br.MROS_value,'ZH') as "branch",
											char_process(sy1.EDIT_KT_NR,'n/a',50) as "account",
											NVL(curr.iso_wrg_cd,'CHF') as "currency_code",
											char_process(sy1.KT_ART_NR_BEZ,NULL,255) as "account_name",
											char_process(sy1.iban_id,'n/a',34) as "iban",
											char_process(a1_kdnr(ii),'n/a',30) as "client_number",
											NVL(tp.MROS_value,'16') as "personal_account_type"
										),
										xml_cl,
										XMLforest(
											case 
												when sy1.EROEFF_DAT is null or sy1.EROEFF_DAT <date '1753-01-01' or sy1.EROEFF_DAT > sysdate
												then '1900-01-01T00:00:00'
												else to_char(sy1.EROEFF_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "opened",
											case 
												when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
												then null 
												else to_char(sy1.AUFHEB_DAT,'YYYY-MM-DD') || 'T00:00:00' 
											end as "closed",
											case 
												when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
												then replace(rtrim(to_char(NVL(sld_bwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
												else null 
											end as "balance",
											case 
												when sy1.AUFHEB_DAT is null or sy1.AUFHEB_DAT<sy1.EROEFF_DAT or sy1.AUFHEB_DAT<date '1753-01-01' or sy1.AUFHEB_DAT > sysdate
												then replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')
												else null 
											end as "date_balance",				
											case 
												when sy1.SPERR_CD=97 or sy1.aufheb_cd='ZZ' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
												then 2 else 1 
											end as "status_code",
											case 
												when sy1.iso_wrg_cd='CHF' or (sy1.AUFHEB_DAT is not null and sy1.AUFHEB_DAT>=NVL(sy1.EROEFF_DAT,date '1753-01-01') and sy1.AUFHEB_DAT>=date '1753-01-01' and sy1.AUFHEB_DAT<= sysdate)
												then NULL 
												else replace(rtrim(to_char(NVL(sld_kwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
											end as "beneficiary",
											char_process(sy1.SITZ_NR_BEZ,'n/a',255) as "beneficiary_comment",
											char_process(
												case 
													when tp.MROS_value='14' 
													then  
														case
															when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
															then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); ' || tp.comments
															else NVL(tp.comments,'n/a')
														end
													else
														case
															when curr.iso_wrg_cd is null and sy1.WRG_BEZ is not null and sy1.iso_wrg_cd is not null
															then 'Actual currency is ' || sy1.WRG_BEZ || ' (' || sy1.iso_wrg_cd || '); '
															else NULL
														end 
												end 
											,NULL,4000) as "comments"
										)				   
									)
								into xml_ac
								from
									FA_MROS_KT_STAMM sy1
								left join
									FA_MROS_REFERENCE_TABLE br
								on sy1.sitz_nr=br.Finnova_value and br.ref_type=1 and br.userbk_nr=F_BANK_ID and br.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE tp
								on sy1.kt_art_ber_cd=tp.Finnova_value and tp.ref_type=9 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
								left join
									FA_MROS_INT_WRG curr
								on curr.iso_wrg_cd=sy1.iso_wrg_cd 
								where sy1.KT_LNR=a1_kt(ii) and sy1.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									select
										xmlconcat(
											XMLforest(
												'n/a' as "institution_name",
												'n/a' as "swift",
												'ZH' as "branch",
												'n/a' as "account",
												'CHF' as "currency_code",
												'n/a' as "iban",
												'n/a' as "client_number",
												16 as "personal_account_type"
											),
											xml_cl,
											XMLforest(
												'1900-01-01T00:00:00' as "opened",
												0 as "balance",
												replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T') as "date_balance",
												1 as "status_code",
												'n/a' as "beneficiary_comment"
											)				   
										)
									into xml_ac
									from dual;
							end;

							begin
								select
									cr.iso_wrg_cd
								into curr
								from
									FA_MROS_KT_STAMM ac
								join
									FA_MROS_INT_WRG cr
								on cr.iso_wrg_cd=ac.iso_wrg_cd
								where ac.KT_LNR=a1_kt(ii) and ac.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									curr:= NULL;
							end;

							--1 withdrawal (from account to client)
							if tp(ii) = 1 then
								begin
									select 
										xmlelement("transaction",
											xmlconcat(
												xmlelement("transactionnumber",s3.KT_BUCH_LNR),
												case when s3.SITZ_NR is null then 
													case tr_tp.MROS_value
														when '1' then xmlelement("transaction_location",'n/a')
														when '2' then xmlelement("transaction_location",'n/a')
														else null 
													end
												else
													xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
												end,
												xmlelement("transaction_description",
													char_process(
														case 
															when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null 
															then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000) || '; '
															else NULL
														end ||
														case
															when char_process(s3.text_ext,NULL,0) is not null 
															then 'Text: ' || char_process(s3.text_ext,NULL,4000) || '; ' || char_process(s3.text_zus,NULL,4000)
															else
																case
																when char_process(s3.text_zus,NULL,0) is not null 
																then 'Text: ' || char_process(s3.text_zus,NULL,4000) || '; ' 
																else NULL
															end
														end
													,'n/a',4000)),
												xmlelement("date_transaction",replace(to_char(s3.buch_dat_a,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("value_date",replace(to_char(s3.valuta,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
												xmlelement("transmode_comment",
												case 
													when tr_tp.MROS_value=13 
													then char_process(tr_tp.comments,'n/a',50)
													else NULL
												end), 
												xmlelement("amount_local",
													replace(rtrim(to_char(
														case
															when tr_tp.MROS_value=16 then 0
															else
																case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
														end
													, 'fm9999999999999990d999'), ',.'),',','.')
												),
												xmlelement("t_from_my_client",
													XMLconcat(
														xmlelement("from_funds_code",1),
														xmlelement("from_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("from_account",xml_ac)
													)
												),
												xmlelement("t_to_my_client",
													XMLconcat(
														xmlelement("to_funds_code",1),
														xmlelement("to_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														case 
															when a1_tp(ii)<3 
															then xmlelement("to_person",xml_cl)
															else xmlelement("to_entity",xml_cl)
														end
													)
												),
												case 
													when s3.BETRAG_BWRG<0 then xmlelement("comments",'It is a return operation. ') 
													else NULL
												end
											)
										)
									into xml_t
									from
										FA_MROS_KT_BUCH_EA s3
									join
										FA_MROS_REFERENCE_TABLE tr_tp
									on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
									where s3.KT_BUCH_LNR=trans_id(ii) and s3.userbk_nr=F_BANK_ID;	
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_t:=NULL;
								end;
							end if;

							--2 deposit (from client to his/her account)
							if tp(ii) = 2 then
								begin
									select 
										xmlelement("transaction",
											xmlconcat(
												xmlelement("transactionnumber",s3.KT_BUCH_LNR),
												case when s3.SITZ_NR is null then 
													case tr_tp.MROS_value
														when '1' then xmlelement("transaction_location",'n/a')
														when '2' then xmlelement("transaction_location",'n/a')
														else null 
													end
												else
													xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
												end,
												xmlelement("transaction_description",
													char_process(
														case 
															when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null 
															then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000) || '; '
															else NULL
														end ||
														case
															when char_process(s3.text_ext,NULL,0) is not null 
															then 'Text: ' || char_process(s3.text_ext,NULL,4000) || '; ' || char_process(s3.text_zus,NULL,4000)
															else
																case
																when char_process(s3.text_zus,NULL,0) is not null 
																then 'Text: ' || char_process(s3.text_zus,NULL,4000) || '; ' 
																else NULL
															end
														end
													,'n/a',4000)),
												xmlelement("date_transaction",replace(to_char(s3.buch_dat_a,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("value_date",replace(to_char(s3.valuta,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
												xmlelement("transmode_comment",
												case 
													when tr_tp.MROS_value=13 
													then char_process(tr_tp.comments,'n/a',50)
													else NULL
												end), 
												xmlelement("amount_local",
													replace(rtrim(to_char(
														case
															when tr_tp.MROS_value=16 then 0
															else
																case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
														end
													, 'fm9999999999999990d999'), ',.'),',','.')
												),
												xmlelement("t_from_my_client",
													XMLconcat(
														xmlelement("from_funds_code",1),
														xmlelement("from_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														case 
															when a1_tp(ii)<3 
															then xmlelement("from_person",xml_cl)
															else xmlelement("from_entity",xml_cl)
														end
													)
												),
												xmlelement("t_to_my_client",
													XMLconcat(
														xmlelement("to_funds_code",1),
														xmlelement("to_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("to_account",xml_ac)
													)
												),
												case 
													when s3.BETRAG_BWRG<0 then xmlelement("comments",'It is a return operation. ') 
													else NULL
												end
											)
										)
									into xml_t
									from
										FA_MROS_KT_BUCH_EA s3
									join
										FA_MROS_REFERENCE_TABLE tr_tp
									on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
									where s3.KT_BUCH_LNR=trans_id(ii) and s3.userbk_nr=F_BANK_ID;	
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_t:=NULL;
								end;
							end if;

							--3 deposit (from client to someone other's account)
							if tp(ii) = 3 then
								begin
									select 
										xmlelement("transaction",
											xmlconcat(
												xmlelement("transactionnumber",s3.KT_BUCH_LNR),
												case when s3.SITZ_NR is null then 
													case tr_tp.MROS_value
														when '1' then xmlelement("transaction_location",'n/a')
														when '2' then xmlelement("transaction_location",'n/a')
														else null 
													end
												else
													xmlelement("transaction_location",'Branch ' || to_char(s3.SITZ_NR) || ' (' || char_process(s3.SITZ_NR_BEZ,'',235) || ')')
												end,
												xmlelement("transaction_description",
													char_process(
														case 
															when char_process(s3.TRAK_REF_TYP_BEZ,NULL,0) is not null 
															then 'Business type: ' || char_process(s3.TRAK_REF_TYP_BEZ,NULL,4000) || '; '
															else NULL
														end ||
														case
															when char_process(s3.text_ext,NULL,0) is not null 
															then 'Text: ' || char_process(s3.text_ext,NULL,4000) || '; ' || char_process(s3.text_zus,NULL,4000)
															else
																case
																when char_process(s3.text_zus,NULL,0) is not null 
																then 'Text: ' || char_process(s3.text_zus,NULL,4000) || '; ' 
																else NULL
															end
														end
													,'n/a',4000)),
												xmlelement("date_transaction",replace(to_char(s3.buch_dat_a,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("value_date",replace(to_char(s3.valuta,'YYYY-MM-DD HH24:MI:SS'),' ','T')),
												xmlelement("transmode_code",NVL(tr_tp.MROS_value,'15')), 
												xmlelement("transmode_comment",
												case 
													when tr_tp.MROS_value=13 
													then char_process(tr_tp.comments,'n/a',50)
													else NULL
												end), 
												xmlelement("amount_local",
													replace(rtrim(to_char(
														case
															when tr_tp.MROS_value=16 then 0
															else
																case when abs(s3.BETRAG_BWRG)<0.01 then 0.01 else NVL(abs(s3.BETRAG_BWRG),0.01) end
														end
													, 'fm9999999999999990d999'), ',.'),',','.')
												),
												xmlelement("t_from",
													XMLconcat(
														xmlelement("from_funds_code",1),
														xmlelement("from_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("from_person",
															xmlelement("first_name",
																char_process(
																	case
																		when REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,0,'i')>0
																		then substr(s3.adr_zeile_1_ez,REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]]',1,1,0,'i'),REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,0,'i')-REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]]',1,1,0,'i')+1)
																		else 'n/a'
																	end
																,'n/a',100)
															),
															xmlelement("last_name",
																char_process(
																	case
																		when REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,0,'i')>0
																		then substr(s3.adr_zeile_1_ez,REGEXP_INSTR(s3.adr_zeile_1_ez, '[[:alpha:]][^[:alpha:]]+[[:alpha:]]',1,1,1,'i')-1)
																		else s3.adr_zeile_1_ez
																	end 
																,'n/a',100)
															),
															xmlelement("comments",
																char_process(
																	s3.adr_zeile_1_ez ||
																	case 
																		when s3.adr_zeile_2_ez is not null
																		then '; ' || s3.adr_zeile_2_ez
																		else  NULL
																	end ||
																	case 
																		when s3.adr_zeile_3_ez is not null
																		then '; ' || s3.adr_zeile_3_ez
																		else  NULL
																	end ||
																	case 
																		when s3.adr_zeile_4_ez is not null
																		then '; ' || s3.adr_zeile_4_ez
																		else  NULL
																	end ||
																	case 
																		when s3.adr_zeile_5_ez is not null
																		then '; ' || s3.adr_zeile_5_ez
																		else  NULL
																	end 
																,'n/a',4000)
															)
														)
													)
												),
												xmlelement("t_to_my_client",
													XMLconcat(
														xmlelement("to_funds_code",1),
														xmlelement("to_foreign_currency",
															XMLforest(
																NVL(curr,'CHF') as "foreign_currency_code",
																replace(rtrim(to_char(abs(NVL(s3.BETRAG_KWRG,NVL(s3.BETRAG_BWRG,0.01))), 'fm9999999999999990d999'), ',.'),',','.') as "foreign_amount",
																1 as "foreign_exchange_rate"
															)
														),
														xmlelement("to_account",xml_ac)
													)
												),
												case 
													when s3.BETRAG_BWRG<0 then xmlelement("comments",'It is a return operation. ') 
													else NULL
												end
											)
										)
									into xml_t
									from
										FA_MROS_KT_BUCH_EA s3
									join
										FA_MROS_REFERENCE_TABLE tr_tp
									on tr_tp.Finnova_value=s3.buchtyp_cd and tr_tp.userbk_nr=F_BANK_ID and tr_tp.ref_type=10 and tr_tp.activ_MROS=1
									where s3.KT_BUCH_LNR=trans_id(ii) and s3.userbk_nr=F_BANK_ID;	
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_t:=NULL;
								end;
							end if;

							select xmlconcat(xml_tr,xml_t) into xml_tr from dual;
						end if;
					end loop;
				end if;
			end if;

			if xml_pm is not null or xml_tr is not null then 
				--remain part of the report
				select xmlconcat(xml_pm,xml_tr) into xml_t from dual;
				select head_part_of_report(F_BANK_ID,RP_NAME,RP_SURNAME,RP_PHONE,RP_EMAIL,RP_OCCUPATION,RP_ADDRESS,RP_CITY,FIU_REF_NUMBER,ENTITY_REFERENCE,REPORT_REASON,R_ACTION,ind,xml_t,1) into xml_r from dual;
			else
				select xmlelement("mros_report_error",'There are no transactions that meet the conditions; ') into xml_r from dual;
			end if;
		end if;

		return(xml_r);
	end;



	----------------------------------------------------------------------------------------------------------------------------------


	--SAR report (collection of accounts and clients)
	function report_activity (
		F_BANK_ID in number default 949,
		RP_NAME in varchar2 default NULL,
		RP_SURNAME in varchar2 default NULL,
		RP_PHONE in varchar2 default NULL,
		RP_EMAIL in varchar2 default NULL,
		RP_OCCUPATION in varchar2 default NULL,
		RP_ADDRESS in varchar2 default NULL,
		RP_CITY in varchar2 default NULL,
		FIU_REF_NUMBER in varchar2 default NULL,
		ENTITY_REFERENCE in varchar2 default 'n/a',	
		REPORT_REASON in varchar2 default 'n/a',
		R_ACTION in varchar2 default 'n/a',
		REPORT_INDICATORS in FAF_MROS_charID_t default NULL,
		SET_OF_ACCOUNTS in FAF_MROS_IdRsn_t default NULL,
		SET_OF_CLIENTS in FAF_MROS_IdRsn_t default NULL
	) 
		return xmltype
	is
		TYPE NumTab IS TABLE OF number;
		TYPE ChTab IS TABLE OF varchar2(4000);
	
		AC_ID NumTab;
		CL_ID NumTab;
		CL_NR ChTab;
		RSN ChTab;

		xml_p xmltype;
		xml_i xmltype;
		xml_r xmltype;
		xml_a xmltype;
		xml_cl xmltype;
		xml_t xmltype;
		inst_name varchar2(255);
		swif varchar2(11);
		n_id number;
		ch_id varchar2(50);
		p_flag number;
		s_reason varchar2(4000);
		n number;
		m number;
		sld_bwrg number;
		sld_kwrg number;
		kd_id number;
		kd_nr varchar2(50);
		k number default 0;       --technical variable: it's a flag of fact that at least one record was founded
		ind FAF_MROS_charID_t;
		str varchar(4000);
		max_items number;
	begin
		max_items:=1000;

		--indicators checking
		select FAF_MROS_charID(indicator) 
			bulk collect into ind 
		from FA_MROS_INT_INDIZ where indicator in (select trim(chID) from table(REPORT_INDICATORS));
		select count(*) into n from table(ind) where substr(chID,-1)='M';
		if n<>1 then
			str:= 'the report must include only one indicator with type Report_Type (M)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='V';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Predicate_Offenses (V)';
		end if;
		select count(*) into n from table(ind) where substr(chID,-1)='G';
		if n<1 then
			str:= case when length(str)>0 then str || ', ' else '' end || 'the report must include at least one indicator with type Reason (G)';	
		end if;	
		if length(str)>0 then 
			str:= 'Indicators error: ' || str || '; ';
		end if;
		
		--table FA_MROS_INT_PERS_CHARGE checking
		select count(*) into n from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID;
		if n=0 then 
			str:= str || 'The table FA_MROS_INT_PERS_CHARGE does not have the record with userbk_nr_home = ' || F_BANK_ID || '; ';
		end if;

		--error processing
		if length(str)>0 then 
			select xmlelement("mros_report_error",str) into xml_r from dual;
		end if;

		if xml_r is null then
			select char_process(institution_name,'n/a',255) into inst_name from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;
			select char_process(swift,'n/a',11) into swif from FA_MROS_INT_PERS_CHARGE where userbk_nr_home=F_BANK_ID and rownum<=1;

			--accounts
			select count(*) into n from table(SET_OF_ACCOUNTS);
			if n>0 then
				if n>max_items then
					n:=max_items;
				end if;
				--Cycle to work with the input set of account numbers
				for ii in 1 .. n 
				loop
					--Variable to impute next value in condition 'where account number = n_id'
					ch_id := SET_OF_ACCOUNTS(ii).chID;
					s_reason := SET_OF_ACCOUNTS(ii).rsn;
					xml_cl:=NULL;
					kd_id:=NULL;
					kd_nr:=NULL;

					if ch_id is not null then
						begin
							select KT_LNR,KD_LNR 
							into n_id,kd_id 
							from FA_MROS_KT_STAMM 
							where EDIT_KT_NR=ch_id and userbk_nr=F_BANK_ID;
							EXCEPTION
							WHEN NO_DATA_FOUND THEN
								kd_id:=NULL;
						end;
					end if;

					if kd_id is not null then
						begin
							select EDIT_KD_NR 
							into kd_nr 
							from FA_MROS_KD_STAMM 
							where KD_LNR=kd_id and userbk_nr=F_BANK_ID and partner_flag=0;
							EXCEPTION
							WHEN NO_DATA_FOUND THEN
								kd_nr:=NULL;
						end;
					end if;
					
					if n_id is not null then	
						if kd_id is not null then
							begin
								select 
									XMLAGG(
										XMLELEment("address",
											XMLforest(
												1 as "address_type",
												char_process(adr.strasse,'n/a',100) as "address",
												char_process(adr.ORT_ORT_BEZ,char_process(adr.ADR_ORT_BEZ,'n/a',255),255) as "city",
												char_process(adr.ADR_PLZ,NULL,10) as "zip",
												NVL(cntr.iso_land_cd,'UNK') as "country_code",
												st.MROS_value as "state"
											)
										)
									)
								into xml_a
								from 
									FA_MROS_KD_ADR adr
								left join
									FA_MROS_REFERENCE_TABLE st
								on st.Finnova_value=adr.region_cd and st.ref_type=2 and st.userbk_nr=F_BANK_ID and st.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr
								on cntr.iso_land_cd=adr.iso_land_cd
								where adr.KD_LNR=kd_id 
									and adr.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_a:=NULL;
							end;

							begin
								select 
									XMLAGG(
										XMLELEment("identification",
											XMLforest(
												NVL(tp.MROS_value,'4') as "type",
												char_process(id.KD_AUSW_NR,'n/a',255) as "number",
												case 
													when id.AUSSTELL_DAT is null or id.AUSSTELL_DAT < date '1753-01-01' or id.AUSSTELL_DAT > sysdate
													then NULL 
													else to_char(id.AUSSTELL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "issue_date",
												case 
													when id.ABL_DAT is null or id.ABL_DAT<id.AUSSTELL_DAT or id.ABL_DAT < date '1753-01-01'
													then NULL 
													else to_char(id.ABL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "expiry_date",
												char_process(id.KD_VSB_BEGLAUB,NULL,255) as "issued_by", 
												NVL(cntr.iso_land_cd,'UNK') as "issue_country",
												case 
													when NVL(tp.MROS_value,'4')='4' 
													then char_process(tp.comments,'n/a',4000) 
													else NULL
												end as "comments"
											)
										)
									)
								into xml_i
								from 
									FA_MROS_KD_VSB id
								left join
									FA_MROS_REFERENCE_TABLE tp
								on tp.Finnova_value=id.iddoc_cd and tp.ref_type=5 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr
								on cntr.iso_land_cd=id.iso_land_cd
								where id.KD_LNR=kd_id and id.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_i:=NULL;
							end;

							begin
								select 
									XMLAGG(
										XMLELEment("phone",
											XMLforest(
												NVL(ct.MROS_value,'1') as "tph_contact_type",
												NVL(ct1.MROS_value,'6') as "tph_communication_type",
												char_process(ph.EMAIL_ADR,'n/a',50) as "tph_number",
												case 
													when ct.MROS_value='2' or ct1.MROS_value='4' 
													then 
														char_process(
															case
																when ct.MROS_value='2'
																then char_process(ct.comments,NULL,4000) || '; '
																else null
															end || 
															case
																when ct1.MROS_value='4'
																then char_process(ct1.comments,NULL,4000)
																else null
															end
														,'n/a',4000)
													else NULL
												end as "comments"
											)
										)
									)
								into xml_p
								from 
									FA_MROS_KD_EMAIL ph
								left join
									FA_MROS_REFERENCE_TABLE ct
								on ct.Finnova_value=ph.mail_art_cd and ct.ref_type=6 and ct.userbk_nr=F_BANK_ID and ct.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE ct1
								on ct1.Finnova_value=ph.mail_art_cd and ct1.ref_type=7 and ct1.userbk_nr=F_BANK_ID and ct1.activ_MROS=1
								where ph.KD_LNR=kd_id and ph.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_p:=NULL;
							end;

							begin
								select
									XMLAGG(
										case when sb1.KD_HTYP_CD=3 then 								
											xmlelement("t_entity",
												xmlconcat(
													XMLFOREST(
														char_process(sb1.kd_bez,'n/a',255) as "name",
														NVL(lf.MROS_value,'1') as "incorporation_legal_form",
														char_process(sb1.UID_NR,NULL,50) as "incorporation_number",
														char_process(sb1.NOGA_BEZ,NULL,255) as "business",
														xml_a as "addresses",
														char_process(sb1.EMAIL_ADR,NULL,255) as "url",
														char_process(sb1.GEBURT_ORT,NULL,255) as "incorporation_state",
														cntr1.iso_land_cd as "incorporation_country_code",
														case 
															when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
															then 
																case 
																	when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR<1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
																	then NULL 
																	else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
																end 
															else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
														end as "incorporation_date",
														case 
															when (sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR >= 1753 and sb1.TOD_JAHR <= extract(year from sysdate))
															then 1 else NULL 
														end as "business_closed",
														case 
															when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
															then 
																case 
																	when sb1.TOD_JAHR is null or sb1.TOD_JAHR < 1753 or sb1.TOD_JAHR > extract(year from sysdate)
																	then null 
																	else to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
																end 
															else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
														end as "date_business_closed",
														dm.MROS_value as "tax_reg_number",
														char_process(
															case 
																when lf.MROS_value='4' 
																then 
																	case
																		when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
																		then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); ' || lf.comments
																		else NVL(lf.comments,'n/a')
																	end 
																else 
																	case
																		when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
																		then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); '
																		else NULL
																	end 
															end
														,NULL,4000) as "comments"
													)
												)
											)
										else 
											xmlelement("signatory",
												xmlforest(
													case when sb1.partner_flag=0 then 1 else Null end as "is_primary",
													xmlconcat(
														XMLFOREST(
															NVL(gd.MROS_value,'U') as "gender",
															char_process(sb1.titel,NULL,30) as "title",
															char_process(sb1.VORNAME,'n/a',100) as "first_name",
															char_process(sb1.name_1,'n/a',100) as "last_name",
															case 
																when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
																then 
																	case 
																		when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR < 1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
																		then null
																		else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
																	end 
																else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
															end as "birthdate",
															char_process(sb1.GEBURT_ORT,NULL,255) as "birth_place",
															cntr1.iso_land_cd as "nationality1",
															cntr2.iso_land_cd as "nationality2",
															xml_p as "phones",
															xml_a as "addresses"
														),   
														xml_i,
														case 
															when (sb1.TOD_DAT is not null and sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR is not null and sb1.TOD_JAHR>=1753 and sb1.TOD_JAHR <= extract(year from sysdate))
															then xmlelement("deceased",1)
															else null 
														end,		 
														case 
															when (sb1.TOD_DAT is not null and sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR is not null and sb1.TOD_JAHR>=1753 and sb1.TOD_JAHR <= extract(year from sysdate))
															then xmlelement("date_deceased",
																case 
																	when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
																	then to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
																	else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end)
															else null 
														end				
													) as "t_person",
													1 as "role"
												)
											)
										end
									) 
								into xml_cl
								from
									FA_MROS_KD_STAMM sb1
								left join
									FA_MROS_REFERENCE_TABLE gd
								on gd.Finnova_value=sb1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE lf
								on lf.Finnova_value=sb1.kd_rstat_cd and lf.ref_type=8 and lf.userbk_nr=F_BANK_ID and lf.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE dm
								on dm.Finnova_value=sb1.kd_sitz_gsch_cd and dm.ref_type=3 and dm.userbk_nr=F_BANK_ID and dm.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr1
								on sb1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd	
								left join
									FA_MROS_INT_LAND cntr2
								on sb1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd					
								where sb1.KD_LNR=kd_id and sb1.userbk_nr=F_BANK_ID
								group by sb1.userbk_nr,sb1.KD_LNR;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_cl:=NULL;
							end;
						end if;

						begin
							select saldo_bwrg,saldo_kwrg into sld_bwrg,sld_kwrg from FA_MROS_KT_SALDO
							where kt_lnr=n_id and userbk_nr=F_BANK_ID and rownum<=1;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								begin
									sld_bwrg:=NULL;
									sld_kwrg:=NULL;
								end;
						end;

						begin
							select		 
								xmlagg(
									xmlelement("report_party",
										xmlforest(
											xmlconcat(
												XMLforest(
													inst_name as "institution_name",
													swif as "swift",
													NVL(br.MROS_value,'ZH') as "branch",
													char_process(s1.EDIT_KT_NR,'n/a',50) as "account",
													curr.iso_wrg_cd as "currency_code",
													char_process(s1.KT_ART_NR_BEZ,NULL,255) as "account_name",
													char_process(s1.iban_id,NULL,34) as "iban",
													char_process(kd_nr,'n/a',30) as "client_number",
													NVL(tp.MROS_value,'16') as "personal_account_type"
												),
												xml_cl,
												XMLforest(
													case 
														when s1.EROEFF_DAT is null or s1.EROEFF_DAT <date '1753-01-01' or s1.EROEFF_DAT > sysdate
														then null
														else to_char(s1.EROEFF_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end as "opened",
													case 
														when s1.AUFHEB_DAT is null or s1.AUFHEB_DAT<s1.EROEFF_DAT or s1.AUFHEB_DAT<date '1753-01-01' or s1.AUFHEB_DAT > sysdate
														then null 
														else to_char(s1.AUFHEB_DAT,'YYYY-MM-DD') || 'T00:00:00' 
													end as "closed",
													case 
														when s1.AUFHEB_DAT is null or s1.AUFHEB_DAT<s1.EROEFF_DAT or s1.AUFHEB_DAT<date '1753-01-01' or s1.AUFHEB_DAT > sysdate
														then replace(rtrim(to_char(NVL(sld_bwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
														else null 
													end as "balance",
													case 
														when s1.AUFHEB_DAT is null or s1.AUFHEB_DAT<s1.EROEFF_DAT or s1.AUFHEB_DAT<date '1753-01-01' or s1.AUFHEB_DAT > sysdate
														then replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')
														else null 
													end as "date_balance",	
													case 
														when s1.SPERR_CD=97 or s1.aufheb_cd='ZZ' or (s1.AUFHEB_DAT is not null and s1.AUFHEB_DAT>=NVL(s1.EROEFF_DAT,date '1753-01-01') and s1.AUFHEB_DAT>=date '1753-01-01' and s1.AUFHEB_DAT<= sysdate) 
														then 2 else 1 
													end as "status_code",
													case 
														when s1.iso_wrg_cd='CHF' or (s1.AUFHEB_DAT is not null and s1.AUFHEB_DAT>=NVL(s1.EROEFF_DAT,date '1753-01-01') and s1.AUFHEB_DAT>=date '1753-01-01' and s1.AUFHEB_DAT<= sysdate)
														then NULL 
														else replace(rtrim(to_char(NVL(sld_kwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
													end as "beneficiary",
													char_process(s1.SITZ_NR_BEZ,'n/a',255) as "beneficiary_comment",
													char_process(
														case 
															when tp.MROS_value='14' 
															then  
																case
																	when curr.iso_wrg_cd is null and s1.WRG_BEZ is not null and s1.iso_wrg_cd is not null
																	then 'Actual currency is ' || s1.WRG_BEZ || ' (' || s1.iso_wrg_cd || '); ' || tp.comments
																	else NVL(tp.comments,'n/a')
																end
															else
																case
																	when curr.iso_wrg_cd is null and s1.WRG_BEZ is not null and s1.iso_wrg_cd is not null
																	then 'Actual currency is ' || s1.WRG_BEZ || ' (' || s1.iso_wrg_cd || '); '
																	else NULL
																end 
														end 
													,'n/a',4000) as "comments"
												)				   
											) as "account",
											10 as "significance",
											s_reason as "reason"
										)				
									)	
								)  
							into xml_r
							from 
								FA_MROS_KT_STAMM s1
							left join
								FA_MROS_REFERENCE_TABLE br
							on s1.sitz_nr=br.Finnova_value and br.ref_type=1 and br.userbk_nr=F_BANK_ID and br.activ_MROS=1
							left join
								FA_MROS_REFERENCE_TABLE tp
							on s1.kt_art_ber_cd=tp.Finnova_value and tp.ref_type=9 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
							left join
								FA_MROS_INT_WRG curr
							on curr.iso_wrg_cd=s1.iso_wrg_cd 
							where s1.KT_LNR=n_id and s1.userbk_nr=F_BANK_ID;
							EXCEPTION
								WHEN NO_DATA_FOUND THEN
								xml_r:=NULL;
						end;
						
						select xmlconcat(xml_t,xml_r) into xml_t from dual;

						if k = 0 then
							select count(*) into k from FA_MROS_KT_STAMM where KT_LNR=n_id and userbk_nr=F_BANK_ID;
						end if;
					end if;
				end loop;	
			else
				select count(*) into n from table(SET_OF_CLIENTS);
				if n>0 then
					begin
						select distinct ac.KT_LNR,ac.KD_LNR,cl.chID,cl.rsn
						bulk collect into AC_ID,CL_ID,CL_NR,RSN 
						from
							table(SET_OF_CLIENTS) cl
						join
							FA_MROS_KD_STAMM cls
						on cl.chid=cls.EDIT_KD_NR and cls.userbk_nr=F_BANK_ID and cls.partner_flag=0
						join
							FA_MROS_KT_STAMM ac
						on ac.KD_LNR=cls.KD_LNR and ac.userbk_nr=cls.userbk_nr
						where rownum<=max_items;
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
							AC_ID:=NULL;
					end;
					
					if AC_ID is not null then
						n := AC_ID.count;
						if n>max_items then
							n:=max_items;
						end if;
						--Cycle to work with the input set of account numbers
						for ii in 1 .. n
						loop
							--Variable to impute next value in condition 'where account number = n_id'
							n_id := AC_ID(ii);
							kd_id := CL_ID(ii);
							kd_nr := CL_NR(ii);
							s_reason := RSN(ii);
							xml_cl:=NULL;
			
							if n_id is not null then	
								if kd_id is not null then
									begin
										select 
											XMLAGG(
												XMLELEment("address",
													XMLforest(
														1 as "address_type",
														char_process(adr.strasse,'n/a',100) as "address",
														char_process(adr.ORT_ORT_BEZ,char_process(adr.ADR_ORT_BEZ,'n/a',255),255) as "city",
														char_process(adr.ADR_PLZ,NULL,10) as "zip",
														NVL(cntr.iso_land_cd,'UNK') as "country_code",
														st.MROS_value as "state"
													)
												)
											)
										into xml_a
										from 
											FA_MROS_KD_ADR adr
										left join
											FA_MROS_REFERENCE_TABLE st
										on st.Finnova_value=adr.region_cd and st.ref_type=2 and st.userbk_nr=F_BANK_ID and st.activ_MROS=1
										left join
											FA_MROS_INT_LAND cntr
										on cntr.iso_land_cd=adr.iso_land_cd
										where adr.KD_LNR=kd_id 
											and adr.userbk_nr=F_BANK_ID;
										EXCEPTION
											WHEN NO_DATA_FOUND THEN
											xml_a:=NULL;
									end;

									begin
										select 
											XMLAGG(
												XMLELEment("identification",
													XMLforest(
														NVL(tp.MROS_value,'4') as "type",
														char_process(id.KD_AUSW_NR,'n/a',255) as "number",
														case 
															when id.AUSSTELL_DAT is null or id.AUSSTELL_DAT < date '1753-01-01' or id.AUSSTELL_DAT > sysdate
															then NULL 
															else to_char(id.AUSSTELL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
														end as "issue_date",
														case 
															when id.ABL_DAT is null or id.ABL_DAT<id.AUSSTELL_DAT or id.ABL_DAT < date '1753-01-01'
															then NULL 
															else to_char(id.ABL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
														end as "expiry_date",
														char_process(id.KD_VSB_BEGLAUB,NULL,255) as "issued_by", 
														NVL(cntr.iso_land_cd,'UNK') as "issue_country",
														case 
															when NVL(tp.MROS_value,'4')='4' 
															then char_process(tp.comments,'n/a',4000) 
															else NULL
														end as "comments"
													)
												)
											)
										into xml_i
										from 
											FA_MROS_KD_VSB id
										left join
											FA_MROS_REFERENCE_TABLE tp
										on tp.Finnova_value=id.iddoc_cd and tp.ref_type=5 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
										left join
											FA_MROS_INT_LAND cntr
										on cntr.iso_land_cd=id.iso_land_cd
										where id.KD_LNR=kd_id and id.userbk_nr=F_BANK_ID;
										EXCEPTION
											WHEN NO_DATA_FOUND THEN
											xml_i:=NULL;
									end;

									begin
										select 
											XMLAGG(
												XMLELEment("phone",
													XMLforest(
														NVL(ct.MROS_value,'1') as "tph_contact_type",
														NVL(ct1.MROS_value,'6') as "tph_communication_type",
														char_process(ph.EMAIL_ADR,'n/a',50) as "tph_number",
														case 
															when ct.MROS_value='2' or ct1.MROS_value='4' 
															then 
																char_process(
																	case
																		when ct.MROS_value='2'
																		then char_process(ct.comments,NULL,4000) || '; '
																		else null
																	end || 
																	case
																		when ct1.MROS_value='4'
																		then char_process(ct1.comments,NULL,4000)
																		else null
																	end
																,'n/a',4000)
															else NULL
														end as "comments"
													)
												)
											)
										into xml_p
										from 
											FA_MROS_KD_EMAIL ph
										left join
											FA_MROS_REFERENCE_TABLE ct
										on ct.Finnova_value=ph.mail_art_cd and ct.ref_type=6 and ct.userbk_nr=F_BANK_ID and ct.activ_MROS=1
										left join
											FA_MROS_REFERENCE_TABLE ct1
										on ct1.Finnova_value=ph.mail_art_cd and ct1.ref_type=7 and ct1.userbk_nr=F_BANK_ID and ct1.activ_MROS=1
										where ph.KD_LNR=kd_id and ph.userbk_nr=F_BANK_ID;
										EXCEPTION
											WHEN NO_DATA_FOUND THEN
											xml_p:=NULL;
									end;

									begin
										select
											XMLAGG(
												case when sb1.KD_HTYP_CD=3 then 								
													xmlelement("t_entity",
														xmlconcat(
															XMLFOREST(
																char_process(sb1.kd_bez,'n/a',255) as "name",
																NVL(lf.MROS_value,'1') as "incorporation_legal_form",
																char_process(sb1.UID_NR,NULL,50) as "incorporation_number",
																char_process(sb1.NOGA_BEZ,NULL,255) as "business",
																xml_a as "addresses",
																char_process(sb1.EMAIL_ADR,NULL,255) as "url",
																char_process(sb1.GEBURT_ORT,NULL,255) as "incorporation_state",
																cntr1.iso_land_cd as "incorporation_country_code",
																case 
																	when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
																	then 
																		case 
																			when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR<1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
																			then NULL 
																			else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
																		end 
																	else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end as "incorporation_date",
																case 
																	when (sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR >= 1753 and sb1.TOD_JAHR <= extract(year from sysdate))
																	then 1 else NULL 
																end as "business_closed",
																case 
																	when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
																	then 
																		case 
																			when sb1.TOD_JAHR is null or sb1.TOD_JAHR < 1753 or sb1.TOD_JAHR > extract(year from sysdate)
																			then null 
																			else to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
																		end 
																	else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end as "date_business_closed",
																dm.MROS_value as "tax_reg_number",
																char_process(
																	case 
																		when lf.MROS_value='4' 
																		then 
																			case
																				when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
																				then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); ' || lf.comments
																				else NVL(lf.comments,'n/a')
																			end 
																		else 
																			case
																				when cntr1.iso_land_cd is null and sb1.LAND_LNR_NAT_BEZ is not null and sb1.ISO_LAND_CODE_NAT is not null
																				then 'Actual country is ' || sb1.LAND_LNR_NAT_BEZ || ' (' || sb1.ISO_LAND_CODE_NAT || '); '
																				else NULL
																			end 
																	end
																,NULL,4000) as "comments"
															)
														)
													)
												else 
													xmlelement("signatory",
														xmlforest(
															case when sb1.partner_flag=0 then 1 else Null end as "is_primary",
															xmlconcat(
																XMLFOREST(
																	NVL(gd.MROS_value,'U') as "gender",
																	char_process(sb1.titel,NULL,30) as "title",
																	char_process(sb1.VORNAME,'n/a',100) as "first_name",
																	char_process(sb1.name_1,'n/a',100) as "last_name",
																	case 
																		when sb1.GEBURT_DAT is null or sb1.GEBURT_DAT < date '1753-01-01' or sb1.GEBURT_DAT > sysdate
																		then 
																			case 
																				when sb1.GEBURT_JAHR is null or sb1.GEBURT_JAHR < 1753 or sb1.GEBURT_JAHR > extract(year from sysdate)
																				then null
																				else to_char(sb1.GEBURT_JAHR) || '-01-01T00:00:00' 
																			end 
																		else to_char(sb1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																	end as "birthdate",
																	char_process(sb1.GEBURT_ORT,NULL,255) as "birth_place",
																	cntr1.iso_land_cd as "nationality1",
																	cntr2.iso_land_cd as "nationality2",
																	xml_p as "phones",
																	xml_a as "addresses"
																),   
																xml_i,
																case 
																	when (sb1.TOD_DAT is not null and sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR is not null and sb1.TOD_JAHR>=1753 and sb1.TOD_JAHR <= extract(year from sysdate))
																	then xmlelement("deceased",1)
																	else null 
																end,		 
																case 
																	when (sb1.TOD_DAT is not null and sb1.TOD_DAT >= date '1753-01-01' and sb1.TOD_DAT <= sysdate) or (sb1.TOD_JAHR is not null and sb1.TOD_JAHR>=1753 and sb1.TOD_JAHR <= extract(year from sysdate))
																	then xmlelement("date_deceased",
																		case 
																			when sb1.TOD_DAT is null or sb1.TOD_DAT < date '1753-01-01' or sb1.TOD_DAT > sysdate
																			then to_char(sb1.TOD_JAHR) || '-01-01T00:00:00' 
																			else to_char(sb1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																		end)
																	else null 
																end				
															) as "t_person",
															1 as "role"
														)
													)
												end
											) 
										into xml_cl
										from
											FA_MROS_KD_STAMM sb1
										left join
											FA_MROS_REFERENCE_TABLE gd
										on gd.Finnova_value=sb1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
										left join
											FA_MROS_REFERENCE_TABLE lf
										on lf.Finnova_value=sb1.kd_rstat_cd and lf.ref_type=8 and lf.userbk_nr=F_BANK_ID and lf.activ_MROS=1
										left join
											FA_MROS_REFERENCE_TABLE dm
										on dm.Finnova_value=sb1.kd_sitz_gsch_cd and dm.ref_type=3 and dm.userbk_nr=F_BANK_ID and dm.activ_MROS=1
										left join
											FA_MROS_INT_LAND cntr1
										on sb1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd	
										left join
											FA_MROS_INT_LAND cntr2
										on sb1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd					
										where sb1.KD_LNR=kd_id and sb1.userbk_nr=F_BANK_ID
										group by sb1.userbk_nr,sb1.KD_LNR;
										EXCEPTION
											WHEN NO_DATA_FOUND THEN
											xml_cl:=NULL;
									end;
								end if;

								begin
									select saldo_bwrg,saldo_kwrg into sld_bwrg,sld_kwrg from FA_MROS_KT_SALDO
									where kt_lnr=n_id and userbk_nr=F_BANK_ID and rownum<=1;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										begin
											sld_bwrg:=NULL;
											sld_kwrg:=NULL;
										end;
								end;

								begin
									select		 
										xmlagg(
											xmlelement("report_party",
												xmlforest(
													xmlconcat(
														XMLforest(
															inst_name as "institution_name",
															swif as "swift",
															NVL(br.MROS_value,'ZH') as "branch",
															char_process(s1.EDIT_KT_NR,'n/a',50) as "account",
															curr.iso_wrg_cd as "currency_code",
															char_process(s1.KT_ART_NR_BEZ,NULL,255) as "account_name",
															char_process(kd_nr,'n/a',30) as "client_number",
															NVL(tp.MROS_value,'16') as "personal_account_type"
														),
														xml_cl,
														XMLforest(
															case 
																when s1.EROEFF_DAT is null or s1.EROEFF_DAT <date '1753-01-01' or s1.EROEFF_DAT > sysdate
																then null
																else to_char(s1.EROEFF_DAT,'YYYY-MM-DD') || 'T00:00:00' 
															end as "opened",
															case 
																when s1.AUFHEB_DAT is null or s1.AUFHEB_DAT<s1.EROEFF_DAT or s1.AUFHEB_DAT<date '1753-01-01' or s1.AUFHEB_DAT > sysdate
																then null 
																else to_char(s1.AUFHEB_DAT,'YYYY-MM-DD') || 'T00:00:00' 
															end as "closed",
															case 
																when s1.AUFHEB_DAT is null or s1.AUFHEB_DAT<s1.EROEFF_DAT or s1.AUFHEB_DAT<date '1753-01-01' or s1.AUFHEB_DAT > sysdate
																then replace(rtrim(to_char(NVL(sld_bwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
																else null 
															end as "balance",
															case 
																when s1.AUFHEB_DAT is null or s1.AUFHEB_DAT<s1.EROEFF_DAT or s1.AUFHEB_DAT<date '1753-01-01' or s1.AUFHEB_DAT > sysdate
																then replace(to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),' ','T')
																else null 
															end as "date_balance",	
															case 
																when s1.SPERR_CD=97 or s1.aufheb_cd='ZZ' or (s1.AUFHEB_DAT is not null and s1.AUFHEB_DAT>=NVL(s1.EROEFF_DAT,date '1753-01-01') and s1.AUFHEB_DAT>=date '1753-01-01' and s1.AUFHEB_DAT<= sysdate) 
																then 2 else 1 
															end as "status_code",
															case 
																when s1.iso_wrg_cd='CHF' or (s1.AUFHEB_DAT is not null and s1.AUFHEB_DAT>=NVL(s1.EROEFF_DAT,date '1753-01-01') and s1.AUFHEB_DAT>=date '1753-01-01' and s1.AUFHEB_DAT<= sysdate)
																then NULL 
																else replace(rtrim(to_char(NVL(sld_kwrg,0), 'fm9999999999999990d999'), ',.'),',','.') 
															end as "beneficiary",
															char_process(s1.SITZ_NR_BEZ,'n/a',255) as "beneficiary_comment",
															char_process(
																case 
																	when tp.MROS_value='14' 
																	then  
																		case
																			when curr.iso_wrg_cd is null and s1.WRG_BEZ is not null and s1.iso_wrg_cd is not null
																			then 'Actual currency is ' || s1.WRG_BEZ || ' (' || s1.iso_wrg_cd || '); ' || tp.comments
																			else NVL(tp.comments,'n/a')
																		end
																	else
																		case
																			when curr.iso_wrg_cd is null and s1.WRG_BEZ is not null and s1.iso_wrg_cd is not null
																			then 'Actual currency is ' || s1.WRG_BEZ || ' (' || s1.iso_wrg_cd || '); '
																			else NULL
																		end 
																end 
															,'n/a',4000) as "comments"
														)				   
													) as "account",
													10 as "significance",
													s_reason as "reason"
												)				
											)	
										)  
									into xml_r
									from 
										FA_MROS_KT_STAMM s1
									left join
										FA_MROS_REFERENCE_TABLE br
									on s1.sitz_nr=br.Finnova_value and br.ref_type=1 and br.userbk_nr=F_BANK_ID and br.activ_MROS=1
									left join
										FA_MROS_REFERENCE_TABLE tp
									on s1.kt_art_ber_cd=tp.Finnova_value and tp.ref_type=9 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
									left join
										FA_MROS_INT_WRG curr
									on curr.iso_wrg_cd=s1.iso_wrg_cd 
									where s1.KT_LNR=n_id and s1.userbk_nr=F_BANK_ID;
									EXCEPTION
										WHEN NO_DATA_FOUND THEN
										xml_r:=NULL;
								end;
								
								select xmlconcat(xml_t,xml_r) into xml_t from dual;

								if k = 0 then
									select count(*) into k from FA_MROS_KT_STAMM where KT_LNR=n_id and userbk_nr=F_BANK_ID;
								end if;
							end if;
						end loop;
					else
						n := 0;
					end if;	
				end if;
			end if;

			

			--clients	
			if n<max_items then
				select count(*) into m from table(SET_OF_CLIENTS);
				if m>0 then
					--Cycle to work with the input set of client numbers
					if m>max_items-n then
						m:=max_items-n;
					end if;
					for ii in 1 .. m
					loop
						--Variable to impute next value in condition 'where client number = n_id'
						ch_id := SET_OF_CLIENTS(ii).chID;
						s_reason := SET_OF_CLIENTS(ii).rsn;
						p_flag := SET_OF_CLIENTS(ii).partner_flag;
						if ch_id is not null then
							begin
								select KD_LNR into n_id from FA_MROS_KD_STAMM where EDIT_KD_NR=ch_id and userbk_nr=F_BANK_ID and partner_flag=0 and rownum<=1;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									n_id:=NULL;
							end;
						end if;

						if n_id is not null then
							begin
								select 
									XMLAGG(
										XMLELEment("address",
											XMLforest(
												1 as "address_type",
												char_process(adr.strasse,'n/a',100) as "address",
												char_process(adr.ORT_ORT_BEZ,char_process(adr.ADR_ORT_BEZ,'n/a',255),255) as "city",
												char_process(adr.ADR_PLZ,NULL,10) as "zip",
												NVL(cntr.iso_land_cd,'UNK') as "country_code",
												st.MROS_value as "state"
											)
										)
									)
								into xml_a
								from 
									FA_MROS_KD_ADR adr
								left join
									FA_MROS_REFERENCE_TABLE st
								on st.Finnova_value=adr.region_cd and st.ref_type=2 and st.userbk_nr=F_BANK_ID and st.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr
								on cntr.iso_land_cd=adr.iso_land_cd
								where adr.KD_LNR=n_id 
									and adr.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_a:=NULL;
							end;

							begin
								select 
									XMLAGG(
										XMLELEment("identification",
											XMLforest(
												NVL(tp.MROS_value,'4') as "type",
												char_process(id.KD_AUSW_NR,'n/a',255) as "number",
												case 
													when id.AUSSTELL_DAT is null or id.AUSSTELL_DAT < date '1753-01-01' or id.AUSSTELL_DAT > sysdate
													then NULL 
													else to_char(id.AUSSTELL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "issue_date",
												case 
													when id.ABL_DAT is null or id.ABL_DAT<id.AUSSTELL_DAT or id.ABL_DAT < date '1753-01-01'
													then NULL 
													else to_char(id.ABL_DAT,'YYYY-MM-DD') || 'T00:00:00' 
												end as "expiry_date",
												char_process(id.KD_VSB_BEGLAUB,NULL,255) as "issued_by", 
												NVL(cntr.iso_land_cd,'UNK') as "issue_country",
												case 
													when NVL(tp.MROS_value,'4')='4' 
													then char_process(tp.comments,'n/a',4000) 
													else NULL
												end as "comments"
											)
										)
									)
								into xml_i
								from 
									FA_MROS_KD_VSB id
								left join
									FA_MROS_REFERENCE_TABLE tp
								on tp.Finnova_value=id.iddoc_cd and tp.ref_type=5 and tp.userbk_nr=F_BANK_ID and tp.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr
								on cntr.iso_land_cd=id.iso_land_cd
								where id.KD_LNR=n_id and id.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_i:=NULL;
							end;

							begin
								select 
									XMLAGG(
										XMLELEment("phone",
											XMLforest(
												NVL(ct.MROS_value,'1') as "tph_contact_type",
												NVL(ct1.MROS_value,'6') as "tph_communication_type",
												char_process(ph.EMAIL_ADR,'n/a',50) as "tph_number",
												case 
													when ct.MROS_value='2' or ct1.MROS_value='4' 
													then 
														char_process(
															case
																when ct.MROS_value='2'
																then char_process(ct.comments,NULL,4000) || '; '
																else null
															end || 
															case
																when ct1.MROS_value='4'
																then char_process(ct1.comments,NULL,4000)
																else null
															end
														,'n/a',4000)
													else NULL
												end as "comments"
											)
										)
									)
								into xml_p
								from 
									FA_MROS_KD_EMAIL ph
								left join
									FA_MROS_REFERENCE_TABLE ct
								on ct.Finnova_value=ph.mail_art_cd and ct.ref_type=6 and ct.userbk_nr=F_BANK_ID and ct.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE ct1
								on ct1.Finnova_value=ph.mail_art_cd and ct1.ref_type=7 and ct1.userbk_nr=F_BANK_ID and ct1.activ_MROS=1
								where ph.KD_LNR=n_id and ph.userbk_nr=F_BANK_ID;
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_p:=NULL;
							end;

							begin
								select		 
									xmlagg(
										xmlelement("report_party",
											xmlforest(
												case 
													when s1.KD_HTYP_CD<3 
													then
														xmlconcat(
															XMLFOREST(
																NVL(gd.MROS_value,'U') as "gender",
																char_process(s1.titel,NULL,30) as "title",
																char_process(s1.VORNAME,'n/a',100) as "first_name",
																char_process(s1.name_1,'n/a',100) as "last_name",
																case 
																	when s1.GEBURT_DAT is null or s1.GEBURT_DAT < date '1753-01-01' or s1.GEBURT_DAT>sysdate
																	then 
																		case 
																			when s1.GEBURT_JAHR is null or s1.GEBURT_JAHR < 1753 or s1.GEBURT_JAHR>extract(year from sysdate)
																			then null
																			else to_char(s1.GEBURT_JAHR) || '-01-01T00:00:00' 
																		end 
																	else to_char(s1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end as "birthdate",
																char_process(s1.GEBURT_ORT,NULL,255) as "birth_place",
																cntr1.iso_land_cd as "nationality1",
																cntr2.iso_land_cd as "nationality2",
																xml_p as "phones",
																xml_a as "addresses"
															),   
															xml_i,	
															case 
																when (s1.TOD_DAT is not null and s1.TOD_DAT >= date '1753-01-01' and s1.TOD_DAT<=sysdate) or (s1.TOD_JAHR is not null and s1.TOD_JAHR>=1753 and s1.TOD_JAHR<=extract(year from sysdate))
																then xmlelement("deceased",1)
																else null 
															end,		 
															case 
																when (s1.TOD_DAT is not null and s1.TOD_DAT >= date '1753-01-01' and s1.TOD_DAT<=sysdate) or (s1.TOD_JAHR is not null and s1.TOD_JAHR>=1753 and s1.TOD_JAHR<=extract(year from sysdate)) 
																then xmlelement("date_deceased",
																	case 
																		when s1.TOD_DAT is null or s1.TOD_DAT < date '1753-01-01' or s1.TOD_DAT>sysdate
																		then to_char(s1.TOD_JAHR) || '-01-01T00:00:00' 
																		else to_char(s1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																	end)
																else null 
															end				
														) 
													else NULL 
												end as "person",
												case 
													when s1.KD_HTYP_CD=3 
													then
														xmlconcat(
															XMLFOREST(
																char_process(s1.kd_bez,'n/a',255) as "name",
																NVL(lf.MROS_value,'1') as "incorporation_legal_form",
																char_process(s1.UID_NR,NULL,50) as "incorporation_number",
																char_process(s1.NOGA_BEZ,NULL,255) as "business",
																xml_a as "addresses",
																char_process(s1.EMAIL_ADR,NULL,255) as "url",
																char_process(s1.GEBURT_ORT,NULL,255) as "incorporation_state",
																cntr1.iso_land_cd as "incorporation_country_code",
																case 
																	when s1.GEBURT_DAT is null or s1.GEBURT_DAT < date '1753-01-01' or s1.GEBURT_DAT > sysdate
																	then 
																		case 
																			when s1.GEBURT_JAHR is null or s1.GEBURT_JAHR<1753 or s1.GEBURT_JAHR > extract(year from sysdate)
																			then NULL 
																			else to_char(s1.GEBURT_JAHR) || '-01-01T00:00:00' 
																		end 
																	else to_char(s1.GEBURT_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end as "incorporation_date",
																case 
																	when (s1.TOD_DAT >= date '1753-01-01' and s1.TOD_DAT <= sysdate) or (s1.TOD_JAHR >= 1753 and s1.TOD_JAHR <= extract(year from sysdate))
																	then 1 else NULL 
																end as "business_closed",
																case 
																	when s1.TOD_DAT is null or s1.TOD_DAT < date '1753-01-01' or s1.TOD_DAT > sysdate
																	then 
																		case 
																			when s1.TOD_JAHR is null or s1.TOD_JAHR < 1753 or s1.TOD_JAHR > extract(year from sysdate)
																			then null 
																			else to_char(s1.TOD_JAHR) || '-01-01T00:00:00' 
																		end 
																	else to_char(s1.TOD_DAT,'YYYY-MM-DD') || 'T00:00:00' 
																end as "date_business_closed",
																dm.MROS_value as "tax_reg_number",
																char_process(
																	case 
																		when lf.MROS_value='4' 
																		then 
																			case
																				when cntr1.iso_land_cd is null and s1.LAND_LNR_NAT_BEZ is not null and s1.ISO_LAND_CODE_NAT is not null
																				then 'Actual country is ' || s1.LAND_LNR_NAT_BEZ || ' (' || s1.ISO_LAND_CODE_NAT || '); ' || lf.comments
																				else NVL(lf.comments,'n/a')
																			end 
																		else 
																			case
																				when cntr1.iso_land_cd is null and s1.LAND_LNR_NAT_BEZ is not null and s1.ISO_LAND_CODE_NAT is not null
																				then 'Actual country is ' || s1.LAND_LNR_NAT_BEZ || ' (' || s1.ISO_LAND_CODE_NAT || '); '
																				else NULL
																			end 
																	end
																,NULL,4000) as "comments"
															)	   
														)
													else NULL 
												end as "entity",
												10 as "significance",
												trim(s_reason) as "reason"
											)				
										)	
									)  
								into xml_r
								from 
									FA_MROS_KD_STAMM s1
								left join
									FA_MROS_REFERENCE_TABLE gd
								on gd.Finnova_value=s1.geschlecht_cd and gd.ref_type=4 and gd.userbk_nr=F_BANK_ID and gd.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE lf
								on lf.Finnova_value=s1.kd_rstat_cd and lf.ref_type=8 and lf.userbk_nr=F_BANK_ID and lf.activ_MROS=1
								left join
									FA_MROS_REFERENCE_TABLE dm
								on dm.Finnova_value=s1.kd_sitz_gsch_cd and dm.ref_type=3 and dm.userbk_nr=F_BANK_ID and dm.activ_MROS=1
								left join
									FA_MROS_INT_LAND cntr1
								on s1.ISO_LAND_CODE_NAT=cntr1.iso_land_cd
								left join
									FA_MROS_INT_LAND cntr2
								on s1.ISO_LAND_CODE_NAT_2=cntr2.iso_land_cd
								where s1.KD_LNR=n_id and s1.userbk_nr=F_BANK_ID
									and (p_flag is null or s1.KD_HTYP_CD<>2 or s1.partner_flag = p_flag);
								EXCEPTION
									WHEN NO_DATA_FOUND THEN
									xml_r:=NULL;
							end;
			  								
							select xmlconcat(xml_t,xml_r) into xml_t from dual; 

							if k = 0 then
								select count(*) into k from FA_MROS_KD_STAMM where KD_LNR=n_id and userbk_nr=F_BANK_ID
									and (p_flag is null or KD_HTYP_CD<>2 or partner_flag = p_flag);
							end if;
						end if;				
					end loop; 
				end if;  
			end if;

			if k > 0 then
				--remain part of the report
				select xmlelement("activity",xmlelement("report_parties",xml_t)) into xml_t from dual;
				select head_part_of_report(F_BANK_ID,RP_NAME,RP_SURNAME,RP_PHONE,RP_EMAIL,RP_OCCUPATION,RP_ADDRESS,RP_CITY,FIU_REF_NUMBER,ENTITY_REFERENCE,REPORT_REASON,R_ACTION,ind,xml_t,0) into xml_r from dual;
			else
				select xmlelement("mros_report_error",'There are no records that meet the conditions; ') into xml_r from dual;
			end if;
		end if;

		return(xml_r);
	end;

END FAF_MROS_DM;

