--[[
 вариант генерации отчета mros --set of transactions
--]]
local ss = analiz.libs.serv

local IS_ENABLED=true

ss.l("create:mros.set_of_payments")

return CreateMROSWorker({
  name = "mros.report_activity",
  is_enabled = IS_ENABLED,
  templates={
    mssql={
      generate_report="$(template_mros_dir)\\mssql\\report_activity.etlua",
      publicate_report ="$(template_mros_dir)\\publicate_report.etlua"
   },
   oracle={
      generate_report="$(template_mros_dir)\\oracle\\report_activity.etlua",
      publicate_report ="$(template_mros_dir)\\publicate_report.etlua"
   }

  },
 -- аргументы-массивы которые должны быть проверены
  task_def_arrs={"report_indicators","set_of_accounts_id","set_of_customers_id"},
  validate={"f_bank_id:int,not_null",
--            "source_id:int,not_null",
            "[]set_of_customers.id:int"
           }
  }
)
