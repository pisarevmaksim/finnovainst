--[[

--]]
local ss = analiz.libs.serv


--[[
 option={
  name = "mros.set_of_payments",
  is_enabled = IS_ENABLED
  templates={
    generate_report="$(broker_dir)\\tasks\\MROS\\template\\set_of_transactions.etlua",
    publicate_report ="$(broker_dir)\\tasks\\MROS\\template\\publicate_report.etlua"
  },
 -- аргументы-массивы которые должны быть проверены
  task_def_arrs={"report_indicators","set_of_payments","set_of_orders_nr","set_of_orders_id"}
}
--]]


function FirstElement(val,attr)
  if val==nil then return nil end
  local v = val[1]
  if v==nil then return nil end
  return v[attr]
end
--https://gist.github.com/james2doyle/67846afd05335822c149
function validemail(str)
 if true then return true end -- отключил проверку так как валидация вылетает на пустой строке
  if str == nil then return nil end
  if (type(str) ~= 'string') then
    error("Expected string")
    return nil
  end
  local lastAt = str:find("[^%@]+$")
  local localPart = str:sub(1, (lastAt - 2)) -- Returns the substring before '@' symbol
  local domainPart = str:sub(lastAt, #str) -- Returns the substring after '@' symbol
  -- we werent able to split the email properly
  if localPart == nil then
    return nil, "Local name is invalid"
  end

  if domainPart == nil then
    return nil, "Domain is invalid"
  end
  -- local part is maxed at 64 characters
  if #localPart > 64 then
    return nil, "Local name must be less than 64 characters"
  end
  -- domains are maxed at 253 characters
  if #domainPart > 253 then
    return nil, "Domain must be less than 253 characters"
  end
  -- somthing is wrong
  if lastAt >= 65 then
    return nil, "Invalid @ symbol usage"
  end
  -- quotes are only allowed at the beginning of a the local name
  local quotes = localPart:find("[\"]")
  if type(quotes) == 'number' and quotes > 1 then
    return nil, "Invalid usage of quotes"
  end
  -- no @ symbols allowed outside quotes
  if localPart:find("%@+") and quotes == nil then
    return nil, "Invalid @ symbol usage in local part"
  end
  -- no dot found in domain name
  if not domainPart:find("%.") then
    return nil, "No TLD found in domain"
  end
  -- only 1 period in succession allowed
  if domainPart:find("%.%.") then
    return nil, "Too many periods in domain"
  end
  if localPart:find("%.%.") then
    return nil, "Too many periods in local part"
  end
  -- just a general match
  if not str:match('[%w]*[%p]*%@+[%w]*[%.]?[%w]*') then
    return nil, "Email pattern test failed"
  end
  -- all our tests passed, so we are ok
  return true
end



local function CreateValidator(test)
 local function isdigit(str)
   if(str:match("%D")) then     return false   end
   return true
 end

  local v = {}
  v.test_ =test

  local function get_name_(n)
    if n.name=="object" then return n.patch_full end
     return n.patch_full.."."..n.name
  end
  function v:test(obj,patch_full,name)


   local n = {}
   n.obj=obj
   n.patch_full=patch_full
   n.name=name
   if self.test_=="any"          then return self:test_any(n) end
   if self.test_=="int,not_null" then return self:test_int_nnul(n) end
   if self.test_=="int"          then return self:test_int(n) end
   if self.test_=="email"        then return self:test_email(n) end

   return false,"unkwon validator type:"..self.test_
  end
  function v:test_any(n)
    return true,""
  end

  local function get_val_(n)
    local v 
    if n.name=="object" then v = n.obj else v = n.obj[n.name] end
    return v
  end
  function v:test_email(n)
    if n.obj==nil then return true,"" end
    local v = get_val_(n)
    if v==nil then return true,"" end
    if type(v)=="table" then return false,get_name_(n).." table is not email" end
    if type(v)=="function" then return false,get_name_(n).." function is not not email" end

    local isok,emessage = validemail(v)
    if isok then return true,"" end
    return false,get_name_(n)..":"..emessage..".value:"..v
  end

  function v:test_int(n)
    if n.obj==nil then return true,"" end

    local v = get_val_(n)
    if v==nil then return true,"" end
    if type(v)=="table" then return false,get_name_(n).." is not digit" end
    if type(v)=="function" then return false,get_name_(n).." is not digit" end
    local isdigit =isdigit(""..v)
    if not(isdigit) then return false,get_name_(n).." not digit:"..v end
    return true,""
  end

  function v:test_int_nnul(n)
    if n.obj==nil then return false,"not found:"..n.patch_full end
    local v = get_val_(n)
    if v==nil then return false,get_name_(n).. "is NULL" end
    if type(v)=="table" then return false,"is not digit" end
    if type(v)=="function" then return false,"is not digit" end
    local isdigit =isdigit(""..v)
    if not(isdigit) then return false,get_name_(n).." not digit:"..v end
    return true,""
  end

  return v
end
local function validate_values_(obj,patchs,level,validator,patch_full)


  if #patchs==level then 
   return validator:test(obj,patch_full,patchs[level])
  end

  local k = patchs[level]
  if k:sub(1,1)=="[" then 
   ss.l("validate array:"..k)
    k=k:sub(3,k:len())
   local sobj=obj[k]
--   if sobj==nil then return false,"not found :"..patch_full.."."..k.."[]" end
   if sobj==nil then return true,"" end
   for i=1,#sobj do
    local isok,emessage= validate_values_(sobj[i],patchs,level+1,validator,patch_full.."."..k.."["..i.."]")
    if not(isok) then return isok,emessage end
   end
   return true,""
  else
   ss.l("validate prop:"..k)
   local sobj=obj[k]
   if sobj==nil then return false,"not found:"..patch_full.."."..k end
   return validate_values_(sobj,patchs,level+1,validator,patch_full.."."..k)
  end
  return false,"validate_values_:internal errors"
end

function CreateMROSWorker(option)
  local worker = {}

  worker.option=option
  worker.name=option.name

  worker.templates={}


  function  worker:Register(ctx)
    if worker.option.is_enabled==true then 
      ctx:Register(self,self.name)
      self:init(ctx) 
    end
  end

  function worker:init(ctx)
   ss.l("worker:init")
   local t = self.templates
   local t_args = self.option.templates


   for k,v in pairs(t_args) do
     ss.l("init template for:"..k)
     local tt={}
     tt.generate_report  =  ctx:load_template(ss.b(v.generate_report))
     tt.publicate_report =  ctx:load_template(ss.b(v.publicate_report))
    if tt.generate_report==nil then ss.l("failed:..t.generate_report:"..k) end
    t[k]=tt
   end



--   ss.a(1)
  end
local function  save_(fn,body)
 ss.s("tmp",fn)
 ss.s("tmp1",body)
ss.e("ADD_TO_FILE $(tmp) $(tmp1)")
end
  function worker:save_task_(ctx,cur_task)
    local fn = ctx.mros.tmp.task_dir.."\\"..cur_task.task_id..".task"
    local body = table.show(cur_task,task)
    save_(fn,body)
  end
  function worker:save_report_sql_(ctx,task_id,sql)
    local fn = ctx.mros.tmp.task_dir.."\\"..task_id..".report_sql"
    save_(fn,sql)
  end
  function worker:history_error(ctx,task_id,msg)
    local fn = ctx.mros.tmp.task_dir.."\\"..task_id..".error"
    save_(fn,msg)
  end
  function worker:save_report_(ctx,task_id,sql)
    local fn = ctx.mros.tmp.task_dir.."\\"..task_id..".xml"
    save_(fn,sql)
  end

  function worker:ValidateItem(ctx,task,v)
    local p=ss.mysplit(v,":")
    local patch=p[1]
    local test =p[2]
    local validator=CreateValidator(test)
    local v = ss.mysplit(patch,".")
    return validate_values_(task,v,1,validator,"task")
  end
  function worker:ValidateTask(ctx,task)
    if self.option.validate==nil then return true,"" end
    local isok=true
    local emessage=""
    for i=1,#self.option.validate do
      local v = self.option.validate[i]
      local isok1,emess = self:ValidateItem(ctx,task,v)
      if not(isok1) then isok=false emessage=emessage.." "..emess end
    end
    return isok,emessage
  end
  function  worker:DoTask(ctx,task)
   local status_option={}
   status_option.render = ctx.mros.render.status

   local isok,emessage = self:ValidateTask(ctx,task)
   if not(isok) then 
       local msg = "Validate:"..emessage
       ctx:addStatus(task.task_id,"ERROR",msg,status_option)
       self:save_task_(ctx,task)
       self:history_error(ctx,task.task_id,msg)
       return
    end


   local function check_(name) if task[name]==nil then task[name]={} end end

   for i=1,#self.option.task_def_arrs do 
     check_(self.option.task_def_arrs[i])
   end



   local bank_id = task.f_bank_id
   if bank_id==nil then ss.failed("task.f_bank_id not found") bank_id="949" end -- use default
   bank_id=bank_id.."" -- convert to string


   local mros_mode = ss.def(ctx.orgclient_serv:get_ex(bank_id).advanced_["MROS_REPORT_MODE"],"mssql")
   local pwd_name_def  = "AmlWeb"
   if mros_mode=="oracle" then pwd_name_def  = "ora_mq" end
   local pwd_name=ss.def(ctx.orgclient_serv:get_ex(bank_id).advanced_["MROS_REPORT_PWD"],pwd_name_def)


   local mros_schema = ss.def(ctx.orgclient_serv:get_ex(bank_id).advanced_["MROS_REPORT_SCHEMA"],"")
   if  mros_schema==nil then  mros_schema="" end
   if  mros_schema~="" then  mros_schema= mros_schema.."." end


   -- для отладки оставим возможность выбрать задачу еще и в task-e
   if task.mros_mode~=nil then 
      mros_mode=  task.mros_mode
     ss.l("use task.mros_mode="..mros_mode)
   end

   -- возможность использовать в запросе чужой банк id (нужна для отладки например)
  task.f_bank_id= ss.def(ctx.orgclient_serv:get_ex(bank_id).advanced_["MROS_REPORT_USE_BANK_ID"],bank_id)

--  mros_mode ="oracle" --for debug
  ss.l("mros_mode for bank_id:"..bank_id.."="..mros_mode)
 ss.tprint(ctx.orgclient_serv:get_ex(bank_id))
ss.l("_______________________")


    local cur_task =  task
    local t = self.templates[mros_mode]
    if t==nil then 
       local msg = "not found template for mros mode:"..mros_mode
       ctx:addStatus(task.task_id,"ERROR",msg,status_option)
       self:history_error(ctx,task.task_id,msg)
       return
    end


    self:save_task_(ctx,cur_task)
    ss.l("generate report..")
    local sql = t.generate_report({task=cur_task,mros_schema=mros_schema,render = ctx.render.mros})
    ss.l("report.sql:"..ss.def(sql,"<nil>"))
    self:save_report_sql_(ctx,task.task_id,sql)
   ss.l("execute report..")
    local val,emessage = execute_report(pwd_name,sql,
                                        "REPORT", -- report_field
                                        "2400",--timeout
                                        "" --def.value
                                     )

    if emessage~=nil then
       local msg = "report failed:"..emessage
       ctx:addStatus(task.task_id,"ERROR",msg,status_option)
       self:history_error(ctx,task.task_id,msg)
       return
    end

   if val==nil then 
      local msg = "report result is null"
       ctx:addStatus(task.task_id,"ERROR",msg,status_option)
       self:history_error(ctx,task.task_id,msg)
       return
   end
local function IsErrorXml(val)
  local tag1 = "<mros_report_error>"
  local tag2 = "</mros_report_error>"
  if val:lower():find(tag1)~=1 then return {iserror=false,msg=""}  end
  val = val:sub(tag1:len()+1)
  val =ss.ReplaceValue(val,tag2,"")
  return {iserror=true,msg=val}
end
   do
      local ret =IsErrorXml(val)
      if ret.iserror then 
        local msg = ret.msg
         ctx:addStatus(task.task_id,"ERROR",msg,status_option)
         self:history_error(ctx,task.task_id,msg)
       return
     end
   end

    ss.l("generate pub sql..")
    local report_name="MROS_"..cur_task.task_id..".xml"
    local user_tag = cur_task.task_id.."_"..ss.b("$(guid)")
    local sql_pub = t.publicate_report({task={user_tag=user_tag,task_id=cur_task.task_id,result=val,report_name=report_name}})
    self:save_report_(ctx,task.task_id,val)
    ss.l("pubicate report..")
--    local emessage = execute_sql(sql_pub, "2400")-- don't work on sql: len(sql)>32kb
    local emessage = execute_sql_oe(sql_pub)

    if emessage~=nil then
      local msg = "public failed :"..emessage
       ctx:addStatus(task.task_id,"ERROR",msg,status_option)
       self:history_error(ctx,task.task_id,msg)
       return
    end
    local fn,emessage = execute_sql_ret("select FN from mros_guid2fn where [guid]='"..user_tag.."'",
                                    "2400",--timeout
                                     "FN","failed")
    ss.l("report filename:"..fn)
    if emessage~=nil then
    local msg = "public failed :"..emessage
       ctx:addStatus(task.task_id,"ERROR",msg,status_option)
       self:history_error(ctx,task.task_id,msg)
       return
    end
    execute_sql("delete from mros_guid2fn where [guid]='"..user_tag.."'", "2400")
    report_name=fn
    status_option.report_name=report_name
    ss.l("before COMPLETE")
    ss.l("report name:"..status_option.report_name)
    ctx:addStatus(task.task_id,"COMPLETE","ok",status_option)
  end


 function worker:failedTask(ctx,taskEx)
   local status_option={}
   status_option.render = ctx.mros.render.status
   ctx:addStatus(taskEx.task_id,"ERROR",taskEx.emessage,status_option)
   self:save_task_(ctx,taskEx)
   self:history_error(ctx,taskEx.task_id,taskEx.emessage)

 end

 return worker
end




