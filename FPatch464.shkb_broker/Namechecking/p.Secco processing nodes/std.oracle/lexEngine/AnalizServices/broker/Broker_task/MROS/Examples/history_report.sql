use staging_namecheck

DECLARE @task varchar(max)
DECLARE @status varchar(max)
DECLARE @task_id bigint 



set @task = '{task_name = "mros.history_report", 
f_bank_id = "942", 
entity_reference = "ent.ref", 
report_reason = "rep.res", 
r_action = "r.act",
 report_indicators = {"0001M", "1004V", "2012G"}, 
 fiu_ref_number = 12345, n_trans_limit = 5,
 --low_amount = 100, debit = 1, credit = 1, date_from = "20171024", date_to = "20171025", 
set_of_customers = {{chid = "533.533"}}, 
set_of_accounts = {{chid = "533.533.015"}}, rp_email = "support@prospero.ch"}'


/*
SELECT FAQMRSDM.historical_report (
	F_BANK_ID => 949
	,ENTITY_REFERENCE => 'ent.ref'   
	,REPORT_REASON => 'rep.res'
	,R_ACTION => 'r.act'
	,REPORT_INDICATORS => FAF_MROS_charID_t(FAF_MROS_charID('0001M'),FAF_MROS_charID('1004V'),FAF_MROS_charID('2012G'))
	,SET_OF_CL_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('533.533')
	)
	,SET_OF_AC_NR => FAF_MROS_charID_t(
		FAF_MROS_charID('533.533.015')
	)
	,N_TRANS_LIMIT => 5
) FROM DUAL; 
*/

exec  MROS_data_request_v2 @task,@task_id OUTPUT
select @task_id as TASK_ID

select @status=status from MROS_data_status  where TaskID=@task_id
  print  'task.id:'+cast(@task_id as varchar(255))
  print  'wait result'
  set @status='start'
while not (@status='COMPLETE' or  @status ='ERROR') begin
 select @status=status from MROS_data_status  where TaskID=@task_id
  print  @status
  WAITFOR DELAY '00:00:01'
end
print  'finish:'+@status

select * from MROS_data_status  where TaskID=@task_id

