--[[
 вариант генерации отчета mros --set of transactions
--]]
local ss = analiz.libs.serv

local IS_ENABLED=true

ss.l("create:mros.set_of_payments")

return CreateMROSWorker({
  name = "mros.set_of_payments",
  is_enabled = IS_ENABLED,
  templates={
    mssql={
      generate_report="$(template_mros_dir)\\mssql\\set_of_transactions.etlua",
      publicate_report ="$(template_mros_dir)\\publicate_report.etlua"
   },
   oracle={
      generate_report="$(template_mros_dir)\\oracle\\set_of_transactions.etlua",
      publicate_report ="$(template_mros_dir)\\publicate_report.etlua"
   }

  },
 -- аргументы-массивы которые должны быть проверены
  task_def_arrs={"report_indicators","set_of_payments","set_of_orders_nr","set_of_orders_id"},
  validate={"f_bank_id:int,not_null",
--            "source_id:int,not_null",
             "rp_email:email",
            "[]set_of_payments.id:int",
            "[]set_of_orders.id:int"}
}
)
