global_oracle_setting={}                   

global_oracle_setting.finnova={}
local ss = analiz.libs.serv

--out interface: from->NAMECHECKING->out

local glocal = global_oracle_setting_local

global_oracle_setting.finnova.from = global_oracle_setting_local.finnova.from
global_oracle_setting.finnova.out  = global_oracle_setting_local.finnova.out


global_oracle_setting.nc_schema = global_oracle_setting_local.nc_schema
analiz:log(global_oracle_setting_local.nc_schema)
--analiz:execute("abort 3")

analiz:log("global_oracle_setting.nc_schema:")
analiz:log(global_oracle_setting.nc_schema)
--analiz:execute("abort 1")
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
local ver_cache =nil
local function GetPrefixVersion()
  local ver = glocal.prefix_ex.actual_version
  if ver~="auto" then return ver end
  if ver_cache~=nil then return ver_cache end
  ss.l("detect prefix version:")
  ss.s("tmp","select PARAM_VALUE AS C from "..global_oracle_setting.nc_schema..".FA_SCHEMA_PARAMS where PARAM_NAME='SCHEMA_VER'")
  ss.s("tmp1","")
  ss.e("SET_PARAM_FROM_SQL tmp1 ora_mq C $(tmp)")
  local v = ss.g("tmp1")
  if v=="" then v="ver1" end
  ver_cache = v
  return v
end

global_oracle_setting.internal = {}

local pref_ver = GetPrefixVersion()
ss.l("pref_ver:"..pref_ver)
local pref = glocal.prefix_ex[pref_ver]
if pref==nil then 
   ss.l("failed not found pref_ver:"..pref_ver)
   ss.e("sleep min 1")
   go_kill()
end

global_oracle_setting.internal.pref_ver=pref_ver
global_oracle_setting.internal.release_queue = ss.def(pref.queue_pref,"").."_ReleaseQ"


global_oracle_setting.internal.in_type  = "SYS.AQ$_JMS_BYTES_MESSAGE"
global_oracle_setting.internal.out_type = ss.def(pref.type_pref,"").."_queue_output_t"
ss.l(global_oracle_setting.internal.out_type)
--ss.a(1)

do
 local s= global_oracle_setting.nc_schema
 if not (s==nil or s=="") then global_oracle_setting.internal.out_type = s.."."..global_oracle_setting.internal.out_type end
end


global_oracle_setting.internal.input_queue=ss.def(pref.queue_pref,"").."_queue_i_input5"
global_oracle_setting.internal.input_queue_kundenstamm =ss.def(pref.queue_pref,"").. "_kundenstamm5"

global_oracle_setting.internal.input_old_queue=ss.def(pref.queue_pref,"").."_queue_input5"
global_oracle_setting.internal.finas_queue=ss.def(pref.queue_pref,"").."_finas_input5"

global_oracle_setting.internal.post_queue=ss.def(pref.queue_pref,"").."_post5"
global_oracle_setting.internal.dynprofiling_queue=ss.def(pref.queue_pref,"").."_queue_DynProfiling5"


global_oracle_setting.internal.output_old_queue =ss.def(pref.queue_pref,"").. "_queue_output"

ss.tprint(global_oracle_setting.internal)
--ss.a(1)




global_oracle_setting.mros={
                             schema=global_oracle_setting_local.mros.schema
                           }
--if global_oracle_setting.mros.schema~="" then 
--       global_oracle_setting.mros.schema=global_oracle_setting.mros.schema.."." 
--end
--ss.l(ss.def(global_oracle_setting.mros.schema,'NULL'))
--ss.a(1)
global_oracle_setting.broker={}
global_oracle_setting.broker.broker_option={pwd="AmlWeb",
                queue="ContractMROS_v2_recvQueue",
                contract="ContractMROS_v2",
                typeinfo={typename="TypeMROS_DATA_v2",validate_type="NONE"}
}
--добавляет массив b к массиву a
local function add_arr_(a,b)
 for i=1,#b do a[#a+1]=b[i] end
end

local function add_arr_upper(a,b)
 for i=1,#b do a[#a+1]=b[i]:upper() end
end

 


-------------------------
global_oracle_setting.fields={}
-- поля которые берутся из xml в внутренню очередь
global_oracle_setting.fields.xml_fields={"bid","firstname","lastname","remittance",
"address","dob","start_dt","list_ver_id","address_raw","remittance_raw",
"ADD_NAME1","BIRTHDAY","BIRTHYEAR",
"GENDER","NATIONALITY", "ADDRESS_STREET",
"ADDRESS_ZIP","ADDRESS_CITY","ADDRESS_COUNTRY",
"currency","amount",
"swift_code","bank_name","account",



}
-- сюда стоит добавлять новые поля
-- они автоматом будут писаться в namechecking и добавлятся в mapping Sample_TRX_filtering_rules(в uppercase регистре)
-- поля из properties сообщения
global_oracle_setting.fields.xml_fields_ex = {
"swift_code_dbtr","swift_code_cdtr",  "ordering_customer","sending_institution","beneficiary_customer",
"debtor_bicorbei",
"creditor_bicorbei","debtor_agent_bic","debtor_agent_mmbId","intermediary_agent_bic",
"intermediary_agent_ClrSysMmbId","creditor_agent_bic","creditor_agent_mmbId",
"ultimate_creditor_bicorbei",
--pacs009
"debtor_bic","debtor_mmbId","creditor_bic","creditor_ClrSysMmbId"


}

add_arr_(global_oracle_setting.fields.xml_fields,global_oracle_setting.fields.xml_fields_ex)


global_oracle_setting.fields.prop_fields={"F_BANK_ID","F_ORDER_ID",
"F_ORDER_NR","F_PAYMENT_ID",
"F_PAYMENT_SYSTEM_ID","F_CUSTOMER_ID", 
"F_ACCOUNT_ID","F_PORTFOLIO_ID",
"F_CONTRACT_ID","MSG_UUID",
"F_PAYMENT_SYSTEM","F_PAYMENT_TYPE",
"F_PROCESS_CODE","F_COMM_TYPE","F_CUSTOMER_NR","PROCESS_CODE","F_CS_MSG_LNR",
"F_PARTNER_CODE"

}



-- поля из properties сообщения (Kundenstamm)
global_oracle_setting.fields.prop_fields_Kundenstamm = {
 "F_BANK_ID","MSG_UUID","F_CUSTOMER_ID",
"F_PROCESS_CODE", --??
"F_PARTNER_CODE",
"F_CUSTOMER_NR","F_CUSTOMER_STATUS"
,"F_CUSTOMER_TYPE","F_CUSTOMER_SEGMENT"
,"F_CUSTOMER_GROUP","F_NOGA_CODE"
,"F_PEP_CODE","F_CUSTOMER_NAME"
,"F_CUSTOMER_SURNAME","F_CUSTOMER_OTHER_NAMES"
,"F_BIRTHDAY","F_BIRTHYEAR","F_GENDER"
,"F_NATIONALITY","F_ADDRESS_STREET"
,"F_ADDRESS_ZIP","F_ADDRESS_CITY"
,"F_ADDRESS_COUNTRY","F_TAX_DOMICILE"
,"F_OTHER_COUNTRIES","PROCESS_CODE"
,"F_BATCH_NR","F_COMM_TYPE"
}

-- константные поля 
global_oracle_setting.fields.const_fields={{name = "USERNAME",value="finnova_user"},
                                           {name="NAMECHECK_CODE",value="GAB"}}


-- поля которые пробрасывает неймчекинг
global_oracle_setting.fields.namechecking_transit = {"NAMECHECK_CODE","LIST_VER_ID",
                                                     "PROSPERO_POST_CONFIG_NAME",
                                                      "ADDRESS","REMITTANCE",
                                                     "ADDRESS_RAW","REMITTANCE_RAW",
                                                     "PROSPERO_TYPE",
                                                     "F_PARTNER_CODE",
"F_PAYMENT_SYSTEM_ID","F_PAYMENT_SYSTEM",--30.11.2018

"PROSPERO_POST_CONFIG_NAME","PROSPERO_AFTER_NC_QUEUENAME",
"SOBACO_SWIFT", --"SOBACO_AMOUNT",
"CURRENCY","AMOUNT", --25.12.2018
"SWIFT_CODE","BANK_NAME","ACCOUNT",
"F_PAYMENT_SYSTEM","F_PAYMENT_TYPE","F_PROCESS_CODE","F_CS_MSG_LNR","F_NOGA_CODE",
"F_ADDRESS_CITY","START_DT","F_NATIONALITY",
"F_CUSTOMER_STATUS","F_ADDRESS_COUNTRY","F_OTHER_COUNTRIES","F_PEP_CODE",
"F_TAX_DOMICILE","F_ADDRESS_STREET","F_CUSTOMER_GROUP", "F_ADDRESS_ZIP",
"F_CUSTOMER_TYPE","F_CUSTOMER_SEGMENT"
}

add_arr_upper(global_oracle_setting.fields.namechecking_transit,global_oracle_setting.fields.xml_fields_ex)


do
 local t = global_oracle_setting.fields.namechecking_transit
 local f = global_oracle_setting.fields.prop_fields
 for i=1,#f do
  t[#t+1]=f[i]
 end
end



global_oracle_setting.fields.interface={}
global_oracle_setting.fields.interface.kundenstamm={"F_BANK_ID","F_CUSTOMER_ID","F_PARTNER_CODE","MSG_UUID","CF_CORRELATION_ID",
--                   "CF_PAYMENT_ID" ,
--"CF_HIT_FLAG",
                   "PROCESS_CODE", --  ao added
                   "F_PROCESS_CODE", -- ao added,
                   "F_CS_MSG_LNR" --21.03.2019

,'F_PEP_CODE','F_CUSTOMER_NR','F_CUSTOMER_OTHER_NAMES' --22.05.2019
}

global_oracle_setting.fields.interface.checkpayment={"F_PAYMENT_SYSTEM","F_PAYMENT_TYPE","F_PROCESS_CODE",
                   "PROCESS_CODE", --  ao added
                   "F_CS_MSG_LNR" --21.03.2019
}




-- -----------------------------------------
 local deadline = {}
 deadline.lex_worker    = {name = "lex_worker",START=60,WAIT={time=5,update=2},FIRST_WORK=30 --[[todo--]],WORK=30}
 deadline.lex_managment = {name = "lex_managment",START=60,WAIT={time=5,update=2},FIRST_WORK=30 --[[todo--]],WORK=30}
 deadline.proxy = {name = "proxy",START=60,LOAD_PRENAMES=45,WAIT={time=5,update=2}}
 deadline.finas = {name = "funas",START=60,WAIT={time=5,update=2},NAMECHECK=20}
 deadline.post  = {name = "post",START=60,WAIT={time=5,update=2},PROCESSING=20}
 deadline.release  = {name = "release",START=60,WAIT={time=5,update=2},PROCESSING=10}






global_oracle_setting.deadline = deadline
-- -----------------------------------------