local ss = analiz.libs.serv


local broker_option=global_oracle_setting.broker.broker_option

local log = analiz.libs.logserv
if log~=nil then 
  log.init()
  SetSecLevel()
end



--local reciver = ss.factory.msbroker.CreateReciver(broker_option)

local ctx = CreateBrokerCtx({broker_op=broker_option})

common_init(ctx)
mros_init(ctx)


local function IsError(emessage)
  if emessage==nil then return false end
  return emessage~=""
end


ctx_last=nil
task_last=nil

local function  doTask_(ctx,task,obj) 
ss.l("doTask_.startZZ")
 ctx_last=ctx
 task_last =task

ss.l("doTask_.go")
 ss.s("emessage","")
ss.l("doTask_.go2")
 ss.e("ANALIZ_SAFE_EXECUTE ret emessage LUA_EXECUTE_FILE lua $(dir)\\..\\broker\\Broker_task\\serv\\doTask_.lua")
ss.l("doTask_.go.end")
 local emessage = ss.g("emessage")
 if emessage~="" then
  ss.l("doTask_.go failedTask:"..emessage)
   failedTask(ctx,obj,emessage,"execute task")
   ctx.rstatus:commit({lazy=false})
 end
ss.l("doTask_.end")
end



-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------
local ii=0
local ok=0
while true do
  deadline:DeadLineLazy("wait",deadline.time.WAIT)
  local objs,emessage 
   objs,emessage = ctx.reciver_:recivevals({limit=1,timeout=10000})
  
  if emessage~="" then
   ss.l("emessage:"..emessage)
   ctx.reciver_:logout()
   if ctx.rstatus~=nil then ctx.rstatus:commit({lazy=false})end
   ss.e("SLEEP sec 30")
   ctx.reciver_:login()
  end
  if (ii%6*5)==0 then
   ss.l("wait.count:"..ii)
  end
  if #objs>0 then
   for i=1,#objs do
    ss.l("decode:"..objs[i])
    local isok=true
    local taskf,emessage = load("return "..objs[i])
    local task ={}
    if taskf~=nil then
      task=taskf()
    else
     ss.l("failed.load.task:"..emessage)
     isok=false
    end
    deadline:DeadLine("go_task",deadline.time.PROCESSING)
    if isok then

       ss.l("task:"..ok.."______________________________________________________________________________________________________________________")
       ss.l("TaskID:"..ss.def(task.task_id,"<nil>"))
       ss.l(ok..":"..ss.toJson_sort(task))
       ok=ok+1

      ctx:DoTask(task)
    else
     failedTask(ctx,objs[i],emessage)
    end
   end
if  ctx.rstatus~=nil then   ctx.rstatus:commit({lazy=true}) end
  ss.l("next")
  end
  if analiz:IsStop() then
   ss.l("user stop")
   break
  end

  ii=ii+1
end
if  ctx.rstatus~=nil then ctx.rstatus:commit({lazy=false}) end
  ss.l("finish service...")
ctx.reciver_:close()
